################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

src\ASF\common\components\memory\serial_flash\at25dfx\at25dfx.c

src\ASF\common\components\memory\serial_flash\at25dfx\at25dfx_hal_spi.c

src\ASF\common\services\clock\sam4e\sysclk.c

src\ASF\common\services\delay\sam\cycle_counter.c

src\ASF\common\services\serial\usart_serial.c

src\ASF\common\services\sleepmgr\sam\sleepmgr.c

src\ASF\common\services\spi\sam_spi\spi_master.c

src\ASF\common\services\storage\ctrl_access\ctrl_access.c

src\ASF\common\utils\stdio\read.c

src\ASF\common\utils\stdio\write.c

src\ASF\sam\drivers\afec\afec.c

src\ASF\sam\drivers\efc\efc.c

src\ASF\sam\drivers\pio\pio.c

src\ASF\sam\drivers\pio\pio_handler.c

src\ASF\sam\drivers\pmc\pmc.c

src\ASF\sam\drivers\pmc\sleep.c

src\ASF\sam\drivers\rstc\rstc.c

src\ASF\sam\drivers\rtc\rtc.c

src\ASF\sam\drivers\spi\spi.c

src\ASF\sam\drivers\tc\tc.c

src\ASF\sam\drivers\uart\uart.c

src\ASF\sam\drivers\usart\usart.c

src\ASF\sam\services\flash_efc\flash_efc.c

src\ASF\thirdparty\fatfs\fatfs-port-r0.09\diskio.c

src\ASF\thirdparty\fatfs\fatfs-port-r0.09\sam\fattime_rtc.c

src\ASF\thirdparty\fatfs\fatfs-r0.09\src\ff.c

src\ASF\thirdparty\fatfs\fatfs-r0.09\src\option\ccsbcs.c

src\ASF\thirdparty\freertos\freertos-7.3.0\source\FreeRTOS_CLI.c

src\ASF\thirdparty\freertos\freertos-7.3.0\source\list.c

src\ASF\thirdparty\freertos\freertos-7.3.0\source\portable\gcc\sam_cm4f\port.c

src\ASF\thirdparty\freertos\freertos-7.3.0\source\portable\memmang\heap_4.c

src\ASF\thirdparty\freertos\freertos-7.3.0\source\queue.c

src\ASF\thirdparty\freertos\freertos-7.3.0\source\tasks.c

src\ASF\thirdparty\freertos\freertos-7.3.0\source\timers.c

src\ASF\thirdparty\wireless\addons\sio2host\uart\sio2host.c

src\blCodeApi\bootloaderSmOperations.c

src\blCodeApi\bootloaderStateMachine.c

src\blCodeApi\externalFlash.c

src\blCodeApi\md5.c

src\blCodeApi\md5Validation.c

src\blCodeApi\userTimer.c

src\blCodeApi\xmodem.c

src\ASF\common\boards\user_board\init.c

src\ASF\common\utils\interrupt\interrupt_sam_nvic.c

src\ASF\sam\utils\cmsis\sam4e\source\templates\exceptions.c

src\ASF\sam\utils\cmsis\sam4e\source\templates\gcc\startup_sam4e.c

src\ASF\sam\utils\cmsis\sam4e\source\templates\system_sam4e.c

src\ASF\sam\utils\syscalls\gcc\syscalls.c

src\blCodeApi\internalFlash.c

src\main.c

