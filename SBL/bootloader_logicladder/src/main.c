/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 * 
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */
#include "./blCodeApi/common.h"
#include "conf_bootloader.h"
#include "crc16.h"
#include "./blCodeApi/internalFlash.h"
#include "./blCodeApi/externalFlash.h"
#include "../blCodeApi/xmodem.h"
#include "../blCodeApi/bootloaderSmOperations.h"
#include "../blCodeApi/bootloaderStateMachine.h"
#include "sam4e8c.h"
#include "tc.h"


// functions
static void configure_console(void);
static void system_init(void);
static void peripheral_init(void);

volatile int timerCounter=0;
volatile unsigned char flagTimer=0;

static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,									// Baud rate -115200
		.charlength = UART_SERIAL_CHAR_LENGTH,							// Character Length 8 bit
		.paritytype = CONF_UART_PARITY,									// No parity
		.stopbits   = UART_SERIAL_STOP_BIT								// 1 stop bit
	};

	stdio_serial_init(CONF_UART1, &uart_serial_options);						// for Debug     UART1(PA5-PA6)
}

static void peripheral_init()
{
	configure_console();	// initialize the UART0, UART1, USART1
}
	
static void system_init(void)
{	
	sysclk_init();
	board_init();
}

/* Functions just to test and debug modules individually used during development*/
#if 0
static int internalFlashWriteReadTesting()
{
	unsigned int sizeOfFile = 128;
	unsigned char WriteBuff[MAX_CHUNK_SIZE];
	unsigned char ReadBuff[MAX_CHUNK_SIZE] = {0};
	int ret;
	unsigned char dataByte = 0x00;
	
	int chunkCount = sizeOfFile / MAX_CHUNK_SIZE;
	if(sizeOfFile % MAX_CHUNK_SIZE != 0) {
		chunkCount = chunkCount + 1;
	}
	
	for(int i = 0; i < chunkCount; i++) {
		for(int j = 0; j < sizeof(WriteBuff); j++) {
			WriteBuff[j] = dataByte++;
		}
		if(i == 0) {
			ret = internalFlashAppWrite((void*)WriteBuff, sizeOfFile, 1);
		} else {
			ret = internalFlashAppWrite((void*)WriteBuff, sizeOfFile, 0);
		}
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		}
	}
	
	printf("flash write success\r\n");

	for(int i = 0; i < chunkCount; i++) {
		if(i == 0) {
			ret = internalFlashAppRead((void*)ReadBuff, sizeOfFile, 1);
		} else {
			ret = internalFlashAppRead((void*)ReadBuff, sizeOfFile, 0);	
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		} else {
			printf("Chunk No.: %d\r\n", i);
			for(int j = 0; j < sizeof(ReadBuff); j++) {
				printf("user buff data: %u\r\n", ReadBuff[j]);
			}
		}
	}
	
	printf("flash read success\r\n");
}

static int externalFlashWriteReadTesting()
{
	unsigned int sizeOfFile = 256;
	unsigned char WriteBuff[MAX_CHUNK_SIZE];
	unsigned char ReadBuff[MAX_CHUNK_SIZE] = {0};
	int ret;
	unsigned char dataByte = 0x00;
	
	int chunkCount = sizeOfFile / MAX_CHUNK_SIZE;
	if(sizeOfFile % MAX_CHUNK_SIZE != 0) {
		chunkCount = chunkCount + 1;
	}
	
	for(int i = 0; i < chunkCount; i++) {
		for(int j = 0; j < sizeof(WriteBuff); j++) {
			WriteBuff[j] = dataByte++;
		}
		if(i == 0) {
			ret = externalFlashAppWrite((void*)WriteBuff, sizeOfFile, 1, 0, 0);
		} else {
			ret = externalFlashAppWrite((void*)WriteBuff, sizeOfFile, 0, 0, 0);
		}
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		}
	}
	
	printf("flash write success\r\n");
	
	for(int i = 0; i < chunkCount; i++) {
		if(i == 0) {
			ret = externalFlashAppRead((void*)ReadBuff, sizeOfFile, 1, 0);
		} else {
			ret = externalFlashAppRead((void*)ReadBuff, sizeOfFile, 0, 0);	
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		} else {
			printf("Chunk No.: %d\r\n", i);
			
			for(unsigned int j = 0; j < sizeof(ReadBuff); j++) {
				printf("data: %u\r\n", *(ReadBuff + j));
			}
		}
	}
	
	printf("flash read success\r\n");
}

static int writeDataFromExternalToInternalFlash()
{
	unsigned int sizeOfFile = 20048;
	unsigned char ReadBuff[MAX_CHUNK_SIZE] = {0};
	int ret;
	
	int chunkCount = sizeOfFile / MAX_CHUNK_SIZE;
	if(sizeOfFile % MAX_CHUNK_SIZE != 0) {
		chunkCount = chunkCount + 1;
	}
	
	/* take backup of the existing app in external flash*/
	for(int i = 0; i < chunkCount; i++) {
		if(i == 0) {
			ret = internalFlashAppRead((void*)ReadBuff, sizeOfFile, 1);
		} else {
			ret = internalFlashAppRead((void*)ReadBuff, sizeOfFile, 0);
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		}
		
		if(i == 0) {
			ret = externalFlashAppWrite((void*)ReadBuff, sizeOfFile, 1, 1, 0);
		} else {
			ret = externalFlashAppWrite((void*)ReadBuff, sizeOfFile, 0, 1, 0);
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		}
	}
	
	printf("backup of existing app taken successfully\r\n");
	
	for(int i = 0; i < chunkCount; i++) {
		if(i == 0) {
			/* to test backup area got correct f/w*/
			ret = externalFlashAppRead((void*)ReadBuff, sizeOfFile, 1, 1);
			
			/* actual data to take from new area*/ 
			//ret = externalFlashAppRead((void*)ReadBuff, sizeOfFile, 1, 0);
		} else {
			ret = externalFlashAppRead((void*)ReadBuff, sizeOfFile, 0, 1);
			//ret = externalFlashAppRead((void*)ReadBuff, sizeOfFile, 0, 0);
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		}
		
		if(i == 0) {
			ret = internalFlashAppWrite((void*)ReadBuff, sizeOfFile, 1);
		} else {
			ret = internalFlashAppWrite((void*)ReadBuff, sizeOfFile, 0);
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		}
	}
	printf("File Successfully written to internal flash\r\n");
}

static int xmodemFileTransferTesting()
{
	int ret;
	unsigned int sizeOfFile = 20048;
	//unsigned int sizeOfFile = 15000;
	unsigned char ReadBuff[MAX_CHUNK_SIZE] = {0};
		
	int chunkCount = sizeOfFile / MAX_CHUNK_SIZE;
	if(sizeOfFile % MAX_CHUNK_SIZE != 0) {
		chunkCount = chunkCount + 1;
	}
	
	ret = xmodem_receive_file(sizeOfFile);
	if(ret != OPER_STATE_SUCCESS) {
		return 1;
	}
	
/*
	for(int i = 0; i < chunkCount; i++) {
		if(i == 0) {
			ret = externalFlashAppRead((void*)ReadBuff, sizeOfFile, 1, 0);
		} else {
			ret = externalFlashAppRead((void*)ReadBuff, sizeOfFile, 0, 0);	
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		} else {
			printf("\r\n-----Chunk No.: %d-----\r\n", i);
			for(unsigned int j = 0; j < sizeof(ReadBuff); j++) {
				printf("%x", *(ReadBuff + j));
			}
			printf("\r\n");
		}
	}
*/
	/* write to internal*/
	writeDataFromExternalToInternalFlash();
	printf("File successfully received over serial\r\n");
}
#endif

static void externalWatchdogInit()
{
	pmc_enable_periph_clk(ID_TC);	// Warning TC number is the channel not TC number.
	
	tc_init(TC, CHANNEL,
	TC_CMR_WAVE |					// Waveform Mode is enabled.
	TC_CMR_CPCTRG |					// UP mode with automatic trigger on RC Compare
	TC_CMR_TCCLKS_TIMER_CLOCK2		// Use slow clock to avoid overflow. TC_CMR_TCCLKS_TIMER_CLOCK5
	);

	tc_write_rc(TC, CHANNEL, 0xffff);			// Load the highest possible value into TC. ffff--27379,32768--54789 //600=1.2sec --2995600

	/* Configure TC interrupts for TC TC_CHANNEL_CAPTURE only */
	NVIC_SetPriority(TC_IRQn, CHANNEL);	
	NVIC_EnableIRQ(TC_IRQn);
	tc_enable_interrupt(TC, CHANNEL, TC_IER_CPCS);
	tc_start(TC, CHANNEL);	
}

void TC_Handler(void)							// Used as Watchdog timer handler
{
	if ((tc_get_status(TC, CHANNEL) & TC_SR_CPCS) == TC_SR_CPCS)
	{
		if(++timerCounter >= 145)
		{
			//printf("timer counter: %d\r\n", timerCounter);
			timerCounter=0;
			ioport_set_pin_level(WDT_IP_GPIO, flagTimer);
			flagTimer^=1;
		}
	}
}

int main (void) 
{
	int ret;
	
	/* peripherals initializations*/
	system_init();
	peripheral_init();
	externalWatchdogInit();
	
	printf("\r\n---------------------------------------------------------\r\n");
	printf("\r\n**********************LL_CCP Bootloader v1.0*****************\r\n");
	printf("\r\n---------------------------------------------------------\r\n");
	
	//jump_to_app();	//to test jump to app
	
	#if 1
	/* initialize internal flash*/
	ret = internalFlashInit();
	if(OPER_STATE_SUCCESS != ret) {
		printf("\r\nError: Internal Flash Init!!\r\n");
		jump_to_app();
		return 0;
	}
	
	/* initialize external flash*/
	ret = externalFlashInit();
	if(OPER_STATE_SUCCESS != ret) {
		printf("\r\nError: External Flash Init!!\r\n");
		jump_to_app();
		return 0;
	}
	executeBootloaderStateMachine();
	#endif
	
	return 0;
}









