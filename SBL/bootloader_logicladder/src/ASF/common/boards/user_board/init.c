/**
 * \file
 *
 * \brief User board initialization template
 *
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>
#include "../src/blCodeApi/common.h"
#include "ioport.h"


/** SPI MISO pin definition. */
#define SPI_MISO_GPIO         (PIO_PA12_IDX)
#define SPI_MISO_FLAGS        (IOPORT_MODE_MUX_A)
/** SPI MOSI pin definition. */
#define SPI_MOSI_GPIO         (PIO_PA13_IDX)
#define SPI_MOSI_FLAGS        (IOPORT_MODE_MUX_A)
/** SPI SPCK pin definition. */
#define SPI_SPCK_GPIO         (PIO_PA14_IDX)
#define SPI_SPCK_FLAGS        (IOPORT_MODE_MUX_A)

/** SPI chip select 0 pin definition. (Only one configuration is possible) */
#define SPI_NPCS0_GPIO        (PIO_PA11_IDX)
#define SPI_NPCS0_FLAGS       (IOPORT_MODE_MUX_A)
/** SPI chip select 1 pin definition. (multiple configurations are possible) */
#define SPI_NPCS1_PA9_GPIO    (PIO_PA9_IDX)
#define SPI_NPCS1_PA9_FLAGS   (IOPORT_MODE_MUX_B)
#define SPI_NPCS1_PA31_GPIO   (PIO_PA31_IDX)
#define SPI_NPCS1_PA31_FLAGS  (IOPORT_MODE_MUX_A)
#define SPI_NPCS1_PB14_GPIO   (PIO_PB14_IDX)
#define SPI_NPCS1_PB14_FLAGS  (IOPORT_MODE_MUX_A)
#define SPI_NPCS1_PC4_GPIO    (PIO_PC4_IDX)
#define SPI_NPCS1_PC4_FLAGS   (IOPORT_MODE_MUX_B)
/** SPI chip select 2 pin definition. (multiple configurations are possible) */
#define SPI_NPCS2_PA10_GPIO   (PIO_PA10_IDX)
#define SPI_NPCS2_PA10_FLAGS  (IOPORT_MODE_MUX_B)
#define SPI_NPCS2_PA30_GPIO   (PIO_PA30_IDX)
#define SPI_NPCS2_PA30_FLAGS  (IOPORT_MODE_MUX_B)
#define SPI_NPCS2_PB2_GPIO    (PIO_PB2_IDX)
#define SPI_NPCS2_PB2_FLAGS   (IOPORT_MODE_MUX_B)
/** SPI chip select 3 pin definition. (multiple configurations are possible) */
#define SPI_NPCS3_PA3_GPIO    (PIO_PA3_IDX)
#define SPI_NPCS3_PA3_FLAGS   (IOPORT_MODE_MUX_B)
#define SPI_NPCS3_PA5_GPIO    (PIO_PA5_IDX)
#define SPI_NPCS3_PA5_FLAGS   (IOPORT_MODE_MUX_B)
#define SPI_NPCS3_PA22_GPIO   (PIO_PA22_IDX)
#define SPI_NPCS3_PA22_FLAGS  (IOPORT_MODE_MUX_B)

/* Select the SPI module that AT25DFx is connected to */
//#define AT25DFX_SPI_MODULE          SPI

/* Chip select used by AT25DFx components on the SPI module instance */
//#define AT25DFX_CS      0

/** PCK0 pin definition (PA6) */
#define PIN_PCK0         (PIO_PA6_IDX)
#define PIN_PCK0_MUX     (IOPORT_MODE_MUX_B)
#define PIN_PCK0_FLAGS   (IOPORT_MODE_MUX_B)
#define PIN_PCK0_PORT    IOPORT_PIOA
#define PIN_PCK0_MASK    PIO_PA6B_PCK0
#define PIN_PCK0_PIO     PIOA
#define PIN_PCK0_ID      ID_PIOA
#define PIN_PCK0_TYPE    PIO_PERIPH_B
#define PIN_PCK0_ATTR    PIO_DEFAULT

/**
 * \brief Set peripheral mode for one single IOPORT pin.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param pin IOPORT pin to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 */
#define ioport_set_pin_peripheral_mode(pin, mode) \
	do {\
		ioport_set_pin_mode(pin, mode);\
		ioport_disable_pin(pin);\
	} while (0)




/**
 * \brief Set peripheral mode for IOPORT pins.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param port IOPORT port to configure
 * \param masks IOPORT pin masks to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 */
#define ioport_set_port_peripheral_mode(port, masks, mode) \
	do {\
		ioport_set_port_mode(port, masks, mode);\
		ioport_disable_port(port, masks);\
	} while (0)
	
void board_init(void)
{
	/* This function is meant to contain board-specific initialization code
	 * for, e.g., the I/O pins. The initialization can rely on application-
	 * specific board configuration, found in conf_board.h.
	 */
#ifndef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
		/* Disable the watchdog */
		WDT->WDT_MR = WDT_MR_WDDIS;
#endif
		
	/* Initialize IOPORTs */
	ioport_init();
	
	/*
	* External-Watchdog
	*/
	ioport_set_pin_dir(WDT_IP_GPIO, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(WDT_IP_GPIO, WDT_IP_INACTIVE_LEVEL);
	delay_ms(10);
	
//#ifdef  CONF_BOARD_UART_CONSOLE
		/* Configure UART pins */
		ioport_set_port_peripheral_mode(PINS_UART1_PORT, PINS_UART1,
		PINS_UART1_FLAGS);
//#endif
#ifdef CONF_BOARD_SPI
ioport_set_pin_peripheral_mode(SPI_MISO_GPIO, SPI_MISO_FLAGS);
ioport_set_pin_peripheral_mode(SPI_MOSI_GPIO, SPI_MOSI_FLAGS);
ioport_set_pin_peripheral_mode(SPI_SPCK_GPIO, SPI_SPCK_FLAGS);
#endif

#ifdef CONF_BOARD_SPI_NPCS0
ioport_set_pin_peripheral_mode(SPI_NPCS0_GPIO, SPI_NPCS0_FLAGS);
#endif

#ifdef CONF_BOARD_SPI_NPCS1
ioport_set_pin_peripheral_mode(SPI_NPCS1_PB14_GPIO, SPI_NPCS1_PB14_FLAGS);
#endif
#if 0
		ioport_set_pin_peripheral_mode(TWI0_DATA_GPIO, TWI0_DATA_FLAGS);
		ioport_set_pin_peripheral_mode(TWI0_CLK_GPIO, TWI0_CLK_FLAGS);
#endif
}
