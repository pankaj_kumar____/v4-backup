/*
 * crc16.h
 *
 * Created: 3/18/2020 12:52:42 PM
 *  Author: Ashutosh
 */ 


#ifndef _CRC16_H_
#define _CRC16_H_

unsigned short crc16_ccitt(const void *buf, int len);

#endif /* _CRC16_H_ */