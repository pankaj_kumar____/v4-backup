/*
 * internalFlash.c
 *
 * Created: 3/23/2020 5:57:59 PM
 *  Author: Kartik.Gupta
 */ 

#include "internalFlash.h"
#include "common.h"

#define PAGE_ERASE_COUNT_ONE_SHOT		16		/* no of pages to erase in one go*/

int internalFlashInit(void)
{
	unsigned int state = flash_init(FLASH_ACCESS_MODE_128, CHIP_FLASH_WRITE_WAIT_STATE);
	if (state != FLASH_RC_OK) {
		return OPER_STATE_FLASH_INIT;
	} else {
		return OPER_STATE_SUCCESS;
	}
}

int internalFlashAppWrite(void *dataBuff, unsigned int maxDataSize, char operStart)
{	
	static unsigned int totalBytesWritten = 0;
	static unsigned int remainingBytesToWrite = 0;
	unsigned int state, flashWriteAddress;
	int chunkSize;
	extern char flagTimer;
	
	if(!dataBuff) {
		printf("\r\nError: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	if(maxDataSize > INTERNAL_FLASH_APP_SIZE_MAX || maxDataSize == 0) {
		printf("\r\nError: invalid file size\r\n");
		return OPER_STATE_INVALID_FILE_SIZE;
	}
	
	/* reset the write parameters if operation start set*/
	if(1 == operStart) {
		remainingBytesToWrite = maxDataSize;
		totalBytesWritten = 0;
	}
	
	if(0 == operStart) {
		if(0 == totalBytesWritten) {
			printf("\r\nError: flash operation not started\r\n");
			return OPER_STATE_INVALID_OPERATION;
		}	
	}
	
	remainingBytesToWrite = maxDataSize - totalBytesWritten;
	if(remainingBytesToWrite >= MAX_CHUNK_SIZE) {
		chunkSize = MAX_CHUNK_SIZE;
	} else {
		chunkSize = remainingBytesToWrite; 	
	}
	
	if(0 == remainingBytesToWrite) {
		printf("\r\nWarning: flash already written\r\n");
		return OPER_STATE_INVALID_OPERATION;
	}
	
	if(1 == operStart) {
		flashWriteAddress = INTERNAL_FLASH_APP_START_ADDR;
		printf("Erasing Internal Flash App region\r\n");
		/*erase app area*/
		for(int k = 0; k < INTERNAL_FLASH_APP_PAGE_COUNT / PAGE_ERASE_COUNT_ONE_SHOT; k++) {
			state = flash_erase_page(INTERNAL_FLASH_APP_START_ADDR + \
				PAGE_ERASE_COUNT_ONE_SHOT * INTERNAL_FLASH_PAGE_SIZE * k, IFLASH_ERASE_PAGES_16);
			if(state != FLASH_RC_OK) {
				printf("\r\nError: internal flash erase\r\n");
				return OPER_STATE_FLASH_ERASE;
			}
			delay_ms(50);
		}
				
	} else {
		flashWriteAddress = INTERNAL_FLASH_APP_START_ADDR + totalBytesWritten;
	}
	
	/* The EWP command can only be used in 8 KBytes sector for SAM4E so an erase command is required before write operation.*/
	state = flash_write(flashWriteAddress, dataBuff, chunkSize, 0);
	if (state != FLASH_RC_OK) {
		printf("\r\nError: internal flash write\r\n");
		return OPER_STATE_FLASH_WRITE;
	} else {
		totalBytesWritten = totalBytesWritten + chunkSize;
		remainingBytesToWrite = maxDataSize - totalBytesWritten;
		return OPER_STATE_SUCCESS;
	}
}

int internalFlashAppRead(void *dataBuff, unsigned int maxDataSize, char operStart)
{
	static unsigned int totalBytesRead = 0;
	static unsigned int remainingBytesToRead = 0;
	unsigned int state, flashReadAddress;
	unsigned char *readPtr;
	int chunkSize;
	
	if(!dataBuff) {
		printf("\r\nError: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	if(maxDataSize > INTERNAL_FLASH_APP_SIZE_MAX || maxDataSize == 0) {
		printf("\r\nError: invalid file size\r\n");
		return OPER_STATE_INVALID_FILE_SIZE;
	}
	
	/* reset the read parameters if operation start set*/
	if(1 == operStart) {
		remainingBytesToRead = maxDataSize;
		totalBytesRead = 0;
	}
	
	if(0 == operStart) {
		if(0 == totalBytesRead) {
			printf("\r\nError: flash operation not started\r\n");
			return OPER_STATE_INVALID_OPERATION;
		}
	}
	
	remainingBytesToRead = maxDataSize - totalBytesRead;
	if(remainingBytesToRead >= MAX_CHUNK_SIZE) {
		chunkSize = MAX_CHUNK_SIZE;
	} else {
		chunkSize = remainingBytesToRead;
	}
	
	if(0 == remainingBytesToRead) {
		printf("\r\nWarning: flash already read\r\n");
		return OPER_STATE_INVALID_OPERATION;
	}
	
	if(1 == operStart) {
		flashReadAddress = INTERNAL_FLASH_APP_START_ADDR;
	} else {
		flashReadAddress = INTERNAL_FLASH_APP_START_ADDR + totalBytesRead;
	}
	
	readPtr = (unsigned char*) flashReadAddress;
	
	for(int i = 0; i < chunkSize; i++) {
		*((unsigned char*)(dataBuff) + i) = *readPtr++;
	}
	
	totalBytesRead = totalBytesRead + chunkSize;
	remainingBytesToRead = maxDataSize - totalBytesRead;
	return OPER_STATE_SUCCESS;
}