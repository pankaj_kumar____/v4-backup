/*
 * common.h
 *
 * Created: 3/24/2020 2:16:15 PM
 *  Author: Kartik.Gupta
 */ 


#ifndef COMMON_H_
#define COMMON_H_

#include <asf.h>
#include "stdint.h"
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#define WDT_IP_GPIO            (PIO_PA0_IDX)
#define WDT_IP_ACTIVE_LEVEL    (IOPORT_PIN_LEVEL_LOW)
#define WDT_IP_INACTIVE_LEVEL  (IOPORT_PIN_LEVEL_HIGH)

#define TC			TC0
#define ID_TC		ID_TC1
#define TC_Handler  TC1_Handler
#define TC_IRQn     TC1_IRQn
#define CHANNEL		1

typedef enum {
	OPER_STATE_NA = -1,
	OPER_STATE_SUCCESS,				/* return for success, rest codes to indicate errror*/
	OPER_STATE_INVALID_BUFFER,
	OPER_STATE_INVALID_CONFIG,
	OPER_STATE_INVALID_FILE_SIZE,
	OPER_STATE_INVALID_OPERATION,
	OPER_STATE_FLASH_WRITE,
	OPER_STATE_FLASH_INIT,
	OPER_STATE_READ_DEV_ID,
	OPER_STATE_DEV_UNPROTECT,
	OPER_STATE_FLASH_READ,
	OPER_STATE_FLASH_ERASE,
	OPER_STATE_XMODEM_TRANSFER,
	OPER_STATE_INVALID_CHUNK,
	OPER_STATE_MAX,
} OperationStateCode_e;



#endif /* COMMON_H_ */