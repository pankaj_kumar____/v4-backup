/*
 * bootloaderStateMachine.c
 *
 * Created: 3/30/2020 3:01:28 PM
 *  Author: Kartik.Gupta
 */ 

#include "common.h"
#include "bootloaderStateMachine.h"
#include "bootloaderSmOperations.h"

//*****************************************************************************************//
/* < For debugging purpose*/
/*State Machine Names*/
const char* gStates[] = {
    [STATE_NA] = "NA",
	[STATE_BL_INIT] = "BL_INIT",
	[STATE_GET_BIN_FILE] = "GET_BIN_FILE",
	[STATE_CHECK_FW_ROLLBACK] = "CHECK_FW_ROLLBACK",
	[STATE_CHECK_FW_AVAILABLE] = "CHECK_FW_AVAILABLE",
	[STATE_EX_FLASH_NEW_WRITE] = "EX_FLASH_NEW_WRITE",
	[STATE_EX_FLASH_BACKUP_WRITE] = "EX_FLASH_BACKUP_WRITE",
	[STATE_FILE_VALIDATION] = "FILE_VALIDATION",
	[STATE_IN_FLASH_UART_NEW_WRITE] = "IN_FLASH_UART_NEW_WRITE",
	[STATE_IN_FLASH_HTTP_NEW_WRITE] = "IN_FLASH_HTTP_NEW_WRITE",
	[STATE_IN_FLASH_BACKUP_WRITE] = "IN_FLASH_BACKUP_WRIT",
	[STATE_JUMP_TO_APP] = "JUMP_TO_APP",
    [STATE_MAX] = "MAX"
};

/*Event Names*/
const char* gEvents[] = {
    [EVENT_NA] = "NA",
	[EVENT_BL_INIT] = "BL_INIT",
	[EVENT_BL_OPERATION_INIT] = "BL_OPERATION_INIT",
	[EVENT_USER_INPUT] = "USER_INPUT",
	[EVENT_USER_TIMEOUT] = "USER_TIMEOUT",
	[EVENT_POLL_WAIT] = "POLL_WAIT",
	[EVENT_BL_BIN_FILE_INIT] = "BL_BIN_FILE_INIT",
	[EVENT_FLASH_WRITE_SUCCESS] = "FLASH_WRITE_SUCCESS",
	[EVENT_FLASH_WRITE_FAILED] = "FLASH_WRITE_FAILED",
	[EVENT_FILE_VALIDATION_SUCCESS] = "FILE_VALIDATION_SUCCESS",
	[EVENT_FILE_VALIDATION_FAILED] = "FILE_VALIDATION_FAILED",
	[EVENT_FW_AVAILABLE] = "FW_AVAILABLE",
	[EVENT_FW_NOT_AVAILABLE] = "FW_NOT_AVAILABLE",
	[EVENT_FW_ROLLBACK] = "FW_ROLLBACK",
	[EVENT_FW_NOT_ROLLBACK] = "FW_NOT_ROLLBACK",
    [EVENT_MAX] = "Max"
};
//*************************************************************************************************//

/*Operation to be performed during state transition*/
typedef BlEvents_e (*Operation)(void); 

/*Structure for State Machine Configuration Data*/
typedef struct {
    BlStates_e nextState;   /*Next State*/
    Operation operation;    /*Function to be executed before transitioning to next state*/
} StateMachine_t;

/*To maintain the current state*/
static BlStates_e gCurrentState;

/*State Table for State Machine*/
const StateMachine_t gSm[STATE_MAX][EVENT_MAX]= {
	/*States                    			Events*/                    	/*NextState,                	Operation*/
	[STATE_BL_INIT]					[EVENT_BL_INIT]						= 	{STATE_BL_INIT,					blInit},
	[STATE_BL_INIT]					[EVENT_BL_OPERATION_INIT]			= 	{STATE_BL_INIT,					blHalt},
	[STATE_BL_INIT]					[EVENT_USER_INPUT]					=	{STATE_GET_BIN_FILE,			blBinFileInit},
	[STATE_BL_INIT]					[EVENT_USER_TIMEOUT]				=	{STATE_CHECK_FW_ROLLBACK,		blFwRollback},	
	[STATE_CHECK_FW_ROLLBACK]		[EVENT_FW_ROLLBACK]					=	{STATE_IN_FLASH_BACKUP_WRITE,	blInFlashBackupWrite},
	[STATE_CHECK_FW_ROLLBACK]		[EVENT_FW_NOT_ROLLBACK]				=	{STATE_CHECK_FW_AVAILABLE,		blNewFwAvailable},
	[STATE_BL_INIT]					[EVENT_POLL_WAIT]					=	{STATE_BL_INIT,					blHalt},
	[STATE_GET_BIN_FILE]			[EVENT_BL_BIN_FILE_INIT]			=	{STATE_GET_BIN_FILE,			blBinFileHalt},
	[STATE_GET_BIN_FILE]			[EVENT_USER_TIMEOUT]				=	{STATE_CHECK_FW_AVAILABLE,		blNewFwAvailable},
	[STATE_GET_BIN_FILE]			[EVENT_USER_INPUT]					=	{STATE_EX_FLASH_NEW_WRITE,		blExFlashNewWrite},
	[STATE_CHECK_FW_AVAILABLE]		[EVENT_FW_AVAILABLE]				=	{STATE_EX_FLASH_BACKUP_WRITE,	blExFlashBackupWrite},
	[STATE_CHECK_FW_AVAILABLE]		[EVENT_FW_NOT_AVAILABLE]			=	{STATE_JUMP_TO_APP,				blJumpApplication},	
	[STATE_GET_BIN_FILE]			[EVENT_POLL_WAIT]					=	{STATE_GET_BIN_FILE,			blBinFileHalt},
	[STATE_EX_FLASH_NEW_WRITE]		[EVENT_FLASH_WRITE_SUCCESS]			=	{STATE_FILE_VALIDATION,			blExFlashNewFileValidation},
	[STATE_EX_FLASH_NEW_WRITE]		[EVENT_FLASH_WRITE_FAILED]			=	{STATE_JUMP_TO_APP,				blJumpApplication},
	[STATE_FILE_VALIDATION]			[EVENT_FILE_VALIDATION_SUCCESS]		=	{STATE_IN_FLASH_UART_NEW_WRITE,	blInFlashUartNewWrite},
	[STATE_FILE_VALIDATION]			[EVENT_FILE_VALIDATION_FAILED]		=	{STATE_JUMP_TO_APP,				blJumpApplication},
	[STATE_IN_FLASH_UART_NEW_WRITE]	[EVENT_FLASH_WRITE_SUCCESS]			=	{STATE_JUMP_TO_APP,				blJumpApplication},
	[STATE_IN_FLASH_UART_NEW_WRITE]	[EVENT_FLASH_WRITE_FAILED]			=	{STATE_JUMP_TO_APP,				blJumpApplication},
	[STATE_EX_FLASH_BACKUP_WRITE]	[EVENT_FLASH_WRITE_SUCCESS]			=	{STATE_IN_FLASH_HTTP_NEW_WRITE,	blInFlashHttpNewWrite},
	[STATE_EX_FLASH_BACKUP_WRITE]	[EVENT_FLASH_WRITE_FAILED]			=	{STATE_JUMP_TO_APP,				blJumpApplication},
	[STATE_IN_FLASH_HTTP_NEW_WRITE]	[EVENT_FLASH_WRITE_SUCCESS]			=	{STATE_JUMP_TO_APP,				blJumpApplication},
	[STATE_IN_FLASH_HTTP_NEW_WRITE]	[EVENT_FLASH_WRITE_FAILED]			=	{STATE_IN_FLASH_BACKUP_WRITE,	blInFlashBackupWrite},
	[STATE_IN_FLASH_BACKUP_WRITE]	[EVENT_FLASH_WRITE_SUCCESS]			=	{STATE_JUMP_TO_APP,				blJumpApplication},
	[STATE_IN_FLASH_BACKUP_WRITE]	[EVENT_FLASH_WRITE_FAILED]			=	{STATE_IN_FLASH_BACKUP_WRITE,	blInFlashBackupWriteRetry},							
};


/* execute state machine*/
int executeStateMachine(BlEvents_e evt)
{   
    while (evt != EVENT_NA) {
        if (gSm[gCurrentState][evt].nextState > STATE_NA &&\
                gSm[gCurrentState][evt].nextState < STATE_MAX) {
            int currState = gCurrentState; /*temporarily hold current state*/
            int currEvent = evt; /*temporarily hold current evt*/
            gCurrentState = gSm[gCurrentState][evt].nextState;
            if (gSm[currState][evt].operation != NULL) {
                evt = gSm[currState][currEvent].operation();
            } else {
                evt = EVENT_NA;
            }
           //printf("[%s]-[%s]->[%s]==[%s]==>\r\n", gStates[currState], gEvents[currEvent], gStates[gCurrentState], gEvents[evt]);
         } else {
            break;
         }
    }
	return 0;
}

/*initialize and execute the state machine*/
void executeBootloaderStateMachine(void)
{	
	initBlSmOperations();
	gCurrentState = STATE_BL_INIT;
	executeStateMachine(EVENT_BL_INIT);
}