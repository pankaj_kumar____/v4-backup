/*
 * bootloaderStateMachine.h
 *
 * Created: 3/30/2020 3:01:04 PM
 *  Author: Kartik.Gupta
 */ 


#ifndef BOOTLOADERSTATEMACHINE_H_
#define BOOTLOADERSTATEMACHINE_H_


/*Enum for various state machine states*/
typedef enum {
    STATE_NA = 0,	/*Not Used*/
	STATE_BL_INIT,	/*BL Operation Start State*/
	STATE_GET_BIN_FILE,	/*Get Binary File over xmodem*/	
	STATE_CHECK_FW_ROLLBACK,	/* check to roll back to previous firmware*/
	STATE_CHECK_FW_AVAILABLE,	/*Check whether new firmware available*/
	STATE_EX_FLASH_NEW_WRITE,	/*Write to external flash new area*/ 
	STATE_EX_FLASH_BACKUP_WRITE,	/*Write to external flash backup area*/
	STATE_FILE_VALIDATION,	/*Verifying file integrity*/
	STATE_IN_FLASH_UART_NEW_WRITE,	/*Writing from external flash new area to internal(uart file)*/
	STATE_IN_FLASH_HTTP_NEW_WRITE,	/*Writing from external flash new area to internal(http file)*/
	STATE_IN_FLASH_BACKUP_WRITE,	/*Writing from external flash backup area to internal*/
	STATE_JUMP_TO_APP,	/*Jump to application*/
    STATE_MAX	/*Not Used*/  
} BlStates_e;
	
/*Enum for various state machine events*/
typedef enum {
    EVENT_NA = 0,	/*Not Used*/
	EVENT_BL_INIT,
	EVENT_BL_OPERATION_INIT,
	EVENT_USER_INPUT,
	EVENT_USER_TIMEOUT,
	EVENT_POLL_WAIT,
	EVENT_BL_BIN_FILE_INIT,
	EVENT_FLASH_WRITE_SUCCESS,
	EVENT_FLASH_WRITE_FAILED,
	EVENT_FILE_VALIDATION_SUCCESS,
	EVENT_FILE_VALIDATION_FAILED,
	EVENT_FW_AVAILABLE,
	EVENT_FW_NOT_AVAILABLE,
	EVENT_FW_ROLLBACK,
	EVENT_FW_NOT_ROLLBACK,
    EVENT_MAX	/*Not Used*/
} BlEvents_e;

/* @brief to execute the state machine
 */
void executeBootloaderStateMachine(void);

#endif /* BOOTLOADERSTATEMACHINE_H_ */