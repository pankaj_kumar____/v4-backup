/*
 * xmodem.c
 *
 * Created: 3/19/2020 11:25:44 AM
 *  Author: Ashutosh
 */ 
/*
 * xmodem.c
 *
 * Created: 3/19/2020 10:48:33 AM
 *  Author: Ashutosh
 */ 
#include "xmodem.h"
#include "sysclk.h"
#include "externalFlash.h"
#include "internalFlash.h"
#include "md5Validation.h"
#include "common.h"

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond

/** The definitions are followed by the XMODEM protocol */
#define XMDM_SOH     0x01 /**< Start of heading */
#define XMDM_EOT     0x04 /**< End of text */
#define XMDM_ACK     0x06 /**< Acknowledge  */
#define XMDM_NAK     0x15 /**< negative acknowledge */
#define XMDM_CAN     0x18 /**< Cancel */
#define XMDM_ESC     0x1b /**< Escape */

#define CRC16POLY   0x1021  /**< CRC 16 polynom */
#define PKTLEN_128  128u    /**< Packet length */

static usart_if	usart = CONF_UART1;	/* serial uart instance*/
static char gFileTransferStatus = 0;	/*flag to indicate whether file transfer status*/
static firmwareHeader_s gFirmwareHeaderInfo;	/* firmware file header info received over xmodem*/

/**
 * \brief Get bytes through XMODEM protocol.
 *
 * \param usart  Base address of the USART instance.
 * \param p_data  Pointer to the data buffer.
 * \param ul_length Length of data expected.
 *
 * \return Calculated CRC value
 */
static uint16_t xmodem_get_bytes(usart_if usart, int8_t *p_data,
		uint32_t ul_length)
{
	uint16_t us_crc = 0;
	uint32_t i, j;
	uint8_t c_char;

	for (i = 0; i < ul_length; ++i) {
		usart_serial_getchar(usart, &c_char);
		us_crc = us_crc ^ (int32_t) c_char << 8;
		for (j = 0; j < 8; j++) {
			if (us_crc & 0x8000) {
				us_crc = us_crc << 1 ^ CRC16POLY;
			} else {
				us_crc = us_crc << 1;
			}
		}
		us_crc = us_crc & 0xFFFF;
		*p_data++ = c_char;
	}
	return us_crc;
}

/**
 * \brief Get a packet through XMODEM protocol
 *
 * \param usart  Base address of the USART instance.
 * \param p_data  Pointer to the data buffer.
 * \param uc_sno  Sequence number.
 *
 * \return 0 for success and other value for XMODEM error
 */
static int32_t xmodem_get_packet(usart_if usart, int8_t *p_data,
		uint8_t uc_sno)
{
	uint8_t uc_cp_seq[2], uc_temp[2];
	uint16_t us_crc, us_xcrc;

	xmodem_get_bytes(usart, (int8_t *)uc_cp_seq, 2);

	us_xcrc = xmodem_get_bytes(usart, p_data, PKTLEN_128);

	/* An "endian independent way to combine the CRC bytes. */
	usart_serial_getchar(usart, &uc_temp[0]);
	usart_serial_getchar(usart, &uc_temp[1]);

	us_crc = uc_temp[0] << 8;
	us_crc += uc_temp[1];

	if ((us_crc != us_xcrc) || (uc_cp_seq[0] != uc_sno) ||
			(uc_cp_seq[1] != (uint8_t) ((~(uint32_t)uc_sno) & 0xff))) {
		usart_serial_putchar(usart, XMDM_CAN);
		return (-1);
	}

	return 0;
}

/**
 * \brief Send a packet through XMODEM protocol
 *
 * \param usart  Base address of the USART instance.
 * \param p_data  Pointer to the data buffer.
 * \param uc_sno  Sequnce number.
 *
 * \return 0 for sucess and other value for XMODEM error
 */
static uint8_t xmodem_send_packet(usart_if usart, uint8_t *p_data,
		uint8_t uc_sno)
{
	uint32_t  i, j;
	uint16_t us_check_sum;
	int8_t	 c_data;
	uint8_t	 uc_ack;

	us_check_sum = 0;

	usart_serial_putchar(usart, XMDM_SOH);
	usart_serial_putchar(usart, uc_sno);
	usart_serial_putchar(usart, ((uint8_t)(~(uc_sno))));

	for(i = 0; i < PKTLEN_128; i++) {
		c_data = *p_data++;
		usart_serial_putchar(usart, c_data);

		us_check_sum = us_check_sum ^ (int32_t) c_data << 8;
		for (j = 0; j < 8; j++) {
			if (us_check_sum & 0x8000) {
				us_check_sum = us_check_sum << 1 ^ CRC16POLY;
			} else {
				us_check_sum = us_check_sum << 1;
			}
		}
		us_check_sum = us_check_sum & 0xFFFF;
	}

	/* An "endian independent way to extract the CRC bytes. */
	usart_serial_putchar(usart, (uint8_t)(us_check_sum >> 8));
	usart_serial_putchar(usart, (uint8_t)us_check_sum);

	usart_serial_getchar(usart, &uc_ack);

	return uc_ack;	/* Wait for ack */
}

static unsigned int writeDataToExternalFlash(unsigned char* dataBuff, unsigned int chunkCount, unsigned int fileSize)
{
	int ret;
	
	if(chunkCount == 0) {	/* if first chunk*/
		ret = externalFlashAppWrite((void*)dataBuff, fileSize, 1, 0, 0);
	} else {
		ret = externalFlashAppWrite((void*)dataBuff, fileSize, 0, 0, 0);
	}
	if(ret != OPER_STATE_SUCCESS) {
		return ret;
	}
	
	return OPER_STATE_SUCCESS;	
}

int isXmodemFileTransferredStarted(void)
{
	uint32_t ul_timeout;
	
	if(gFileTransferStatus != 0) {
		return gFileTransferStatus;	
	}

	usart_serial_putchar(usart, 'C');
	ul_timeout = (sysclk_get_peripheral_hz() / 10);
	while (!(usart_serial_is_rx_ready(usart)) && ul_timeout) {
		ul_timeout--;
	}
	if (usart_serial_is_rx_ready(usart)) {
		gFileTransferStatus = 1;
	}

	return gFileTransferStatus;
}

/**
 * \brief Receive the files through XMODEM protocol
 * \return OPER_STATE_SUCCESS for success, else error code
 */
uint32_t xmodem_receive_file(void)
{
	uint8_t c_char;
	int32_t l_done;
	uint8_t uc_sno = 0x01;
	uint32_t ul_size = 0;
	
	unsigned char xmodemDataBuffer[XMODEM_CHUNK_SIZE] = {0};
	unsigned char xmodemBackupBuffer[XMODEM_CHUNK_SIZE] = {0};
	unsigned  char flashDataBuff[MAX_CHUNK_SIZE] = {0};
	unsigned char firmwareHeaderBuff[sizeof(firmwareHeader_s)] = {0};
	unsigned int flashChunkCount = 0;
	unsigned char subFlashDataBuffChunkPtr = 0;
	unsigned char maxSubFlashDataBuffChunks = MAX_CHUNK_SIZE / XMODEM_CHUNK_SIZE;
	int retValue, count = 0;
	int fhSize = sizeof(firmwareHeader_s);	/* 24 bytes*/
	int sizeLargeChunk = sizeof(xmodemDataBuffer) - fhSize;	/* 104 bytes*/	

	if(1 != gFileTransferStatus) {
		return OPER_STATE_INVALID_OPERATION;	
	}
	
	/* Begin to receive the data */
	l_done = 0;
	gFileTransferStatus = 2;
	
	while (l_done == 0) {
		usart_serial_getchar(usart, &c_char);
			
		switch (c_char) {
		/* Start of transfer */
		case XMDM_SOH:
			l_done = xmodem_get_packet(usart, xmodemDataBuffer, uc_sno);
			if (l_done == 0) {
				uc_sno++;
				ul_size += PKTLEN_128;
			
				if(count == 0) {
					/* separate out firmware header on 1st xmodem packet*/
					memcpy(firmwareHeaderBuff, xmodemDataBuffer, fhSize);
					memcpy(&gFirmwareHeaderInfo.firmwareVersion, firmwareHeaderBuff, 4);
					memcpy(&gFirmwareHeaderInfo.firmwareMd5Hash, firmwareHeaderBuff + 4, 16);
					memcpy(&gFirmwareHeaderInfo.firmwareSize, firmwareHeaderBuff + 20, 4);
					
					/* xmodem buffer is not yet full*/
					memcpy(xmodemBackupBuffer, xmodemDataBuffer + fhSize, sizeLargeChunk);			
				} else {
					/* xmodem buffer gets full here*/
					memcpy(xmodemBackupBuffer + sizeLargeChunk,  xmodemDataBuffer, fhSize);
					
					/* fill up flash buffer in chunks*/
					memcpy(flashDataBuff + (sizeof(xmodemBackupBuffer) * (subFlashDataBuffChunkPtr)), \
															xmodemBackupBuffer, sizeof(xmodemBackupBuffer));
					subFlashDataBuffChunkPtr++;
							
					if(0 == subFlashDataBuffChunkPtr % maxSubFlashDataBuffChunks) {
						/* reset chunk pointer to fill up next flash buffer bucket*/
						subFlashDataBuffChunkPtr = 0;
								
						/* gave buffer to get it to written on external flash*/
						retValue = writeDataToExternalFlash(flashDataBuff, flashChunkCount, gFirmwareHeaderInfo.firmwareSize);
						if(retValue != OPER_STATE_SUCCESS) {
							/* cancel sender operation*/
							usart_serial_putchar(usart, XMDM_CAN);
							gFileTransferStatus = 0;
							return retValue;
						}
								
						flashChunkCount++;
						
						/* clear flash buffer*/
						memset(flashDataBuff, '\0', sizeof(flashDataBuff));	
					}
					
					/* xmodem buffer is not yet full*/
					memcpy(xmodemBackupBuffer,  xmodemDataBuffer + fhSize, sizeLargeChunk);
				}
				count++;
			}
			
			usart_serial_putchar(usart, XMDM_ACK);
			break;

		/* End of transfer */
		case XMDM_EOT:
			/* write pending chunk to external flash*/
			/* count = 1 when only one xmodem chunk came, file too small <= 128B*/
			if(subFlashDataBuffChunkPtr >= 0 || count == 1) {
				if(count == 1 || subFlashDataBuffChunkPtr == 0) {
					memcpy(flashDataBuff, xmodemBackupBuffer, sizeLargeChunk);
				} else {
					memcpy(flashDataBuff + (sizeof(xmodemBackupBuffer) * (subFlashDataBuffChunkPtr)), \
														xmodemBackupBuffer, sizeof(xmodemBackupBuffer));
				}
				
				retValue = writeDataToExternalFlash(flashDataBuff, flashChunkCount, gFirmwareHeaderInfo.firmwareSize);
				if(retValue != OPER_STATE_SUCCESS) {
					/* cancel sender operation*/
					usart_serial_putchar(usart, XMDM_CAN);
					gFileTransferStatus = 0;
					return retValue;
				}
				flashChunkCount++;
			}
			
			usart_serial_putchar(usart, XMDM_ACK);
			l_done = ul_size;
			
			break;

		case XMDM_CAN:
		case XMDM_ESC:
		default:
			l_done = -1;
			break;
		}
	}	
	
	printf("\r\n************Firmware Information************\r\n");
	printf("Version: ");
	for(int i = 0; i < 4; i++) {
		printf("%c", gFirmwareHeaderInfo.firmwareVersion[i]);	
	}
	printf("\r\n");
	printf("Size: %d\r\n", gFirmwareHeaderInfo.firmwareSize);
	printf("Md5 Hash: ");
	for(int i = 0; i < 16; i++) {
		printf("%2.2x", gFirmwareHeaderInfo.firmwareMd5Hash[i]);	
	}
	printf("\r\n");
	printf("************************************************\r\n");
	
	if(-1 == l_done) {
		gFileTransferStatus = 0;
		return OPER_STATE_XMODEM_TRANSFER;
	} else {
		gFileTransferStatus = 0;
		return OPER_STATE_SUCCESS;
	}
}

firmwareHeader_s *getFirmwareHeaderInfo(void)
{
	return &gFirmwareHeaderInfo;
}

/**
 * \brief Send the files through XMODEM protocol
 *
 * \param usart  Base address of the USART instance.
 * \param p_buffer  Pointer to send buffer
 * \param ul_length transfer file size
 */
void xmodem_send_file(int8_t *p_buffer, uint32_t ul_length)
{
	uint8_t c_char, uc_sno = 1;
	int32_t l_done;
	uint32_t ul_timeout = (sysclk_get_peripheral_hz() / 10);

	if (ul_length & (PKTLEN_128-1)) {
		ul_length += PKTLEN_128;
		ul_length &= ~(PKTLEN_128-1);
	}

	/* Startup synchronization... */
	/* Wait to receive a NAK or 'C' from receiver. */
	l_done = 0;
	while(!l_done) {
		usart_serial_getchar(usart, &c_char);
		switch (c_char) {
		case XMDM_NAK:
			l_done = 1;
			break;
		case 'C':
			l_done = 1;
			break;
		case 'q':	/* ELS addition, not part of XMODEM spec. */
			return;
		default:
			break;
		}
	}

	l_done = 0;
	uc_sno = 1;
	while (!l_done) {
		c_char = xmodem_send_packet(usart, (uint8_t *)p_buffer, uc_sno);
		switch(c_char) {
		case XMDM_ACK:
			++uc_sno;
			ul_length -= PKTLEN_128;
			p_buffer += PKTLEN_128;
			break;
		case XMDM_NAK:
			break;
		case XMDM_CAN:
		case XMDM_EOT:
		default:
			l_done = -1;
			break;
		}
		if (!ul_length) {
			usart_serial_putchar(usart, XMDM_EOT);
			/* Flush the ACK */
			usart_serial_getchar(usart, &c_char);
			break;
		}
	}
}

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond