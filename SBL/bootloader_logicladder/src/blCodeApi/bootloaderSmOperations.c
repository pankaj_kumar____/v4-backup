/*
 * bootloaderSmOperations.c
 *
 * Created: 3/30/2020 4:44:47 PM
 *  Author: Kartik.Gupta
 */ 


#include "bootloaderSmOperations.h"
#include "common.h"
#include "externalFlash.h"
#include "internalFlash.h"
#include "xmodem.h"
#include "md5Validation.h"
#include "userTimer.h"

#define	USER_BOOT_STOP_TIMEOUT	5	/* timer value in second till when bootlaoder will be accesible*/
#define USER_FILE_STOP_TIMEOUT	120	/* timer value for user to send file over xmodem*/
 
static volatile char gLastTimerExpired;	/* last timer expiry status*/	

/* helper functions declarations*/
static int writeToInternalFlash(int fromBackupRegion);
void timerHandler(void);

void timerHandler(void)
{
	printf("\r\n### Timeout ###!!\r\n");
	gLastTimerExpired = 1;
}

/* jump to main application code*/
void jump_to_app(void)
{
	uint32_t app_start_address=0;
	uint32_t APP_START_ADDRES;   //the linker Address in my case the 0x8000 which is the start of the next application (really the first Byte)

	//this will become the address which is indirect the target address of the main() function in the App

	APP_START_ADDRES = INTERNAL_FLASH_APP_START_ADDR;  //INTERNAL_FLASH_APP_START_ADDR is static in my case so it is #define INTERNAL_FLASH_APP_START_ADDR 0x8000
	app_start_address = *(uint32_t *)(APP_START_ADDRES + 4);  //get back the indirect "jump address" of the main() function in the app

	__set_MSP(*(uint32_t *) APP_START_ADDRES);  //reset the Stack to the given value by the App (not really clear the stack only set back the pointer)

	for(volatile uint32_t i = 0; i < 1000; i++)
	for(volatile uint32_t j = 0; j< 1000; j++);
	SCB->VTOR = ((uint32_t) APP_START_ADDRES & SCB_VTOR_TBLOFF_Msk);  //Set the Vectortable Address into the VTOR where the NVIC then start to search the needed IRQ
	
	tc_stop(TC, CHANNEL);
	asm("bx %0"::"r"(app_start_address));  //jump into the application and land direct in the first Byte of the main() function.
	while(1);
}

BlEvents_e blInit(void)
{
	/* start timer to wait for user input event*/
	printf("\r\n===========Boot Console===========\r\n");
	printf("\r\nPress any key to stop\r\n");
	gLastTimerExpired = 0;
	timerStart(USER_BOOT_STOP_TIMEOUT);
	
	return EVENT_BL_OPERATION_INIT;
}

BlEvents_e blHalt(void)
{
	/* check timeout*/
	if(gLastTimerExpired) {
		return EVENT_USER_TIMEOUT;
	}
	
	delay_s(1);
	if(isTimerRunning()) {
		printf("%d\r\n", getRemainingCountValue());
	}

	/* check for any user input event*/
	uint32_t status = uart_get_status(CONF_UART1);
	if (status & UART_SR_RXRDY) {
		uint8_t received_byte;
		uart_read(CONF_UART1, &received_byte);
		timerStop();
		return EVENT_USER_INPUT;		
	}

	/* if neither event came*/
	return EVENT_POLL_WAIT;	
}

BlEvents_e blBinFileInit(void)
{
	/* reload and start timer for user to give bin file*/
	printf("\r\nWaiting for Firmware Bin File over Xmodem...\r\n");
	delay_s(1);
	
	gLastTimerExpired = 0;
	if(1 == isTimerRunning()) {
		timerStop();	
	} 
	timerStart(USER_FILE_STOP_TIMEOUT);
	
	return EVENT_BL_BIN_FILE_INIT;
}

BlEvents_e blBinFileHalt(void)
{
	/* check timeout*/
	if(gLastTimerExpired) {
		return EVENT_USER_TIMEOUT;
	}
	
	/* check for any file transfer begins*/
	if(1 == isXmodemFileTransferredStarted()) {
		timerStop();
		return EVENT_USER_INPUT;	
	}
	
	/* if neither event came*/
	return EVENT_POLL_WAIT;
}

BlEvents_e blExFlashNewWrite(void)
{
	unsigned state;
	
	/* get the file over xmodem and write on external flash new area*/
	state = xmodem_receive_file();
	if(state != OPER_STATE_SUCCESS) {
		printf("\r\nXmodem File Receive : Failed\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	} else {
		printf("\r\nXmodem File Receive : Success\r\n");
		return EVENT_FLASH_WRITE_SUCCESS;
	}
}

static int md5Validation(char* fetchedMd5Value, int sizeOfFile, char isBackupRegion)
{
	int ret;
	unsigned char dataBuff[MAX_CHUNK_SIZE] = {0};
	unsigned char calculatedHashValue[16] = {0};
	MD5_CTX md5OperationBuffer;
	
	int chunkCount = sizeOfFile / MAX_CHUNK_SIZE;
	if(sizeOfFile % MAX_CHUNK_SIZE != 0) {
		chunkCount = chunkCount + 1;
	}
	
	for(int i = 0; i < chunkCount; i++) {
		if(i == 0) {
			ret = externalFlashAppRead((void*)dataBuff, sizeOfFile, 1, isBackupRegion);
		} else {
			ret = externalFlashAppRead((void*)dataBuff, sizeOfFile, 0, isBackupRegion);
		}
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		}
		
		if(0 == i) {
			ret = calculateAndGetMd5Value(&md5OperationBuffer, dataBuff, sizeOfFile, 1, calculatedHashValue);
		} else {
			ret = calculateAndGetMd5Value(&md5OperationBuffer, dataBuff, sizeOfFile, 0, calculatedHashValue);
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			return 1;
		}
	}
	
	for(int i = 0; i < 16; i++) {
		if(fetchedMd5Value[i] != calculatedHashValue[i]) {
			return 1;
		}
	}
	
	return 0;		
}

BlEvents_e blExFlashNewFileValidation(void)
{
	printf("\r\nValidating Md5: ");
	
	char fetchedMd5Value[16];
	int state;
	
	int sizeOfFile = getFirmwareHeaderInfo()->firmwareSize;
	memcpy(fetchedMd5Value, getFirmwareHeaderInfo()->firmwareMd5Hash, 16);
	
	state = md5Validation(fetchedMd5Value, sizeOfFile, 0);
	if(1 == state) {
		printf("Failed!!\r\n");
		return EVENT_FILE_VALIDATION_FAILED;	
	}

	printf("Success\r\n");
	return EVENT_FILE_VALIDATION_SUCCESS;
}

BlEvents_e blFwRollback(void)
{
	printf("\r\nChecking Rollback Required: ");
	
	int state;
	unsigned char data;
	
	state = getRollbackFlag(&data);
	if(state != OPER_STATE_SUCCESS) {
		printf("Failed\r\n");
		return EVENT_FW_NOT_ROLLBACK;
	} else {
		if(1 == data) {
			printf("Required\r\n");
			return EVENT_FW_ROLLBACK;
		} else {
			printf("Not Required\r\n");
			return EVENT_FW_NOT_ROLLBACK;
		}
	}
}

BlEvents_e blNewFwAvailable(void)
{
	printf("\r\nChecking New firmware Available: ");
	
	/* get the flag status to verify if new fw available in external flash*/
	int state;
	unsigned char data;
	
	state = getFirmwareAvailableFlag(&data);
	if(state != OPER_STATE_SUCCESS) {
		printf("Not Available\r\n");
		return EVENT_FW_NOT_AVAILABLE;
	} else {
		if(1 == data) {
			printf("Available\r\n");
			return EVENT_FW_AVAILABLE;
		} else {
			printf("Not Available\r\n");
			return EVENT_FW_NOT_AVAILABLE;
		}
	}
}

static void printFirmwareInfo(firmwareHeader_s *fhInfo)
{
	printf("\r\n---------------------------------------------------\r\n");
	printf("Version: ");
	for(int i = 0; i < 4; i++) {
		printf("%c", fhInfo->firmwareVersion[i]);
	}
	printf("\r\n");
	printf("Size: %d\r\n", fhInfo->firmwareSize);
	printf("Md5 Hash: ");
	for(int i = 0; i < 16; i++) {
		printf("%2.2x", fhInfo->firmwareMd5Hash[i]);
	}
	printf("\r\n");
	printf("\r\n---------------------------------------------------\r\n");
}

static int writeToInternalFlash(int fromBackupRegion)
{
	firmwareHeader_s fhInfo;
	unsigned int sizeOfFile;
	unsigned char dataBuff[MAX_CHUNK_SIZE];
	char isCurrFw;
	int ret;
	
	if(1 == fromBackupRegion) {
		printf("\r\nBackup Firmware Information\r\n");
		isCurrFw = 1;	
	} else {
		printf("\r\nNew Firmware Information\r\n");
		isCurrFw = 0;
	}
	
	ret = getFirmwareHeader(&fhInfo, isCurrFw);
	if(ret != OPER_STATE_SUCCESS) {
		return ret;
	}
	printFirmwareInfo(&fhInfo);
	
	sizeOfFile = fhInfo.firmwareSize;
	
	int chunkCount = sizeOfFile / MAX_CHUNK_SIZE;
	if(sizeOfFile % MAX_CHUNK_SIZE != 0) {
		chunkCount = chunkCount + 1;
	}
	
	for(int i = 0; i < chunkCount; i++) {
		if(i == 0) {
			ret = externalFlashAppRead((void*)dataBuff, sizeOfFile, 1, fromBackupRegion);
		} else {
			ret = externalFlashAppRead((void*)dataBuff, sizeOfFile, 0, fromBackupRegion);
		}
		if(ret != OPER_STATE_SUCCESS) {
			return ret;
		}
		
		//for testing and debugging
		#if 0
 		printf("\r\n-----------------external read-------------\r\n");
 		for(int test = 0; test < MAX_CHUNK_SIZE; test++) {
 			printf("%x", dataBuff[test]);
 		}
		#endif
		
		if(i == 0) {
			ret = internalFlashAppWrite((void*)dataBuff, sizeOfFile, 1);
		} else {
			ret = internalFlashAppWrite((void*)dataBuff, sizeOfFile, 0);
		}
		if(ret != OPER_STATE_SUCCESS) {
			return ret;
		}
		
		//for testing and debugging
		#if 0
 		if(i == 0) {
 			ret = internalFlashAppRead((void*)dataBuff, sizeOfFile, 1);
 		} else {
 			ret = internalFlashAppRead((void*)dataBuff, sizeOfFile, 0);
 		}
 		if(ret != OPER_STATE_SUCCESS) {
 			return ret;
 		}
 		
 		printf("\r\n-----------------internal read-------------\r\n");
 		for(int test = 0; test < MAX_CHUNK_SIZE; test++) {
 			printf("%x", dataBuff[test]);
 		}
		#endif
		printf("Chunks Written: %d, Chunks Remaining: %d\r\n", i + 1, chunkCount - i - 1); 
	}
	
	return OPER_STATE_SUCCESS;	
}

static BlEvents_e blInFlashNewWrite(void)
{
	int state;
	state = writeToInternalFlash(0);
	if(state != OPER_STATE_SUCCESS) {
		return EVENT_FLASH_WRITE_FAILED;
	} else {
		return EVENT_FLASH_WRITE_SUCCESS;
	}
}

BlEvents_e blInFlashHttpNewWrite(void)
{
	BlEvents_e evt;
	int state;
	
	printf("\r\nWriting New Firmware to Internal Flash: ");
	
	evt = blInFlashNewWrite();
	if(evt != EVENT_FLASH_WRITE_SUCCESS) {
		printf("Failed\r\n");
		return evt;
	}
	
	/* update the rollback flag for first boot*/
	state = updateRollbackFlag(1);	/* new application should clear the flag to prevent further rollback*/
	if(state != OPER_STATE_SUCCESS) {
		printf("Failed\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	printf("Success\r\n");
	return EVENT_FLASH_WRITE_SUCCESS;
}

BlEvents_e blInFlashUartNewWrite(void)
{
	BlEvents_e evt;
	int sysConfigSize = sizeof(exSysBootConfig_s);
	int dnldCfgSize = sizeof(exSysBootConfig_s);
		
	exSysBootConfig_s sysConfig;
	exDnldBootConfig_s dnldConfig;
	
	firmwareHeader_s *fhInfo;
	
	printf("\r\nWriting New Firmware to internal Flash: ");
	
	/* get the firmware file information we get over uart*/
	fhInfo = getFirmwareHeaderInfo();
	
	/* clear boot configurations*/
	memset(&sysConfig, '\0', sysConfigSize);
	memset(&dnldConfig, '\0', dnldCfgSize);
	
	/* update new firmware details*/
	memcpy(&sysConfig.newFirmwareHeader, fhInfo, sizeof(firmwareHeader_s));
	
	/* set the information for current firmware*/
	if(OPER_STATE_SUCCESS != setExternalBootConfig((void*)&sysConfig, sysConfigSize, CONFIG_TYPE_SYSTEM)) {
		printf("Failed\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	evt = blInFlashNewWrite();
	if(evt != EVENT_FLASH_WRITE_SUCCESS) {
		printf("Failed\r\n");
		return evt;
	}
	
	/* update current firmware details*/
	memcpy(&sysConfig.currentFirmwareHeader, fhInfo, sizeof(firmwareHeader_s));
	if(OPER_STATE_SUCCESS != setExternalBootConfig((void*)&sysConfig, sysConfigSize, CONFIG_TYPE_SYSTEM)) {
		printf("Failed\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	/* flush firmware download config*/
	if(OPER_STATE_SUCCESS != setExternalBootConfig((void*)&dnldConfig, dnldCfgSize, CONFIG_TYPE_DOWNLOAD)) {
		printf("Failed\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	printf("Success\r\n");
	
	printf("\r\nNew Firmware Information\r\n");
	printFirmwareInfo(fhInfo);
	
	return EVENT_FLASH_WRITE_SUCCESS;
}

BlEvents_e blExFlashBackupWrite(void)
{
	printf("\r\nTaking backup of current Firmware: ");
	
	firmwareHeader_s currFwInfo;
	unsigned int sizeOfFile;
	unsigned char ReadBuff[MAX_CHUNK_SIZE] = {0};
	int ret;
	unsigned char previousBackupStatus;
	
	ret = getFirmwareHeader(&currFwInfo, 1);
	if(ret != OPER_STATE_SUCCESS) {
		printf("Failed\r\n");	
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	printf("\r\nCurrent Firmware Information\r\n");
	printFirmwareInfo(&currFwInfo);
	
	/* check previous backup status*/
	if(OPER_STATE_SUCCESS != getPreviousBackupStatus(&previousBackupStatus)) {
		printf("Failed\r\n");
		return EVENT_FLASH_WRITE_FAILED;	
	}
	
	if(1 == previousBackupStatus) {	/*backup already taken, not  redoing, to prevent firmware curruption in case of last power failure*/
		printf("Success\r\n");
		return EVENT_FLASH_WRITE_SUCCESS;			
	}
	
	sizeOfFile = currFwInfo.firmwareSize;
	
	int chunkCount = sizeOfFile / MAX_CHUNK_SIZE;
	if(sizeOfFile % MAX_CHUNK_SIZE != 0) {
		chunkCount = chunkCount + 1;
	}
	
	/* take backup of the existing app in external flash*/
	for(int i = 0; i < chunkCount; i++) {
		if(i == 0) {
			ret = internalFlashAppRead((void*)ReadBuff, sizeOfFile, 1);
		} else {
			ret = internalFlashAppRead((void*)ReadBuff, sizeOfFile, 0);
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			printf("Failed\r\n");
			return EVENT_FLASH_WRITE_FAILED;
		}
		
		if(i == 0) {
			ret = externalFlashAppWrite((void*)ReadBuff, sizeOfFile, 1, 1, 0);
		} else {
			ret = externalFlashAppWrite((void*)ReadBuff, sizeOfFile, 0, 1, 0);
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			printf("Failed\r\n");
			return EVENT_FLASH_WRITE_FAILED;
		}
	}
	
	if(1 == md5Validation(currFwInfo.firmwareMd5Hash, currFwInfo.firmwareSize, 1)) {
		printf("Failed\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	/* update last backup status*/
	if(OPER_STATE_SUCCESS != updatePreviousBackupStatus(1)) {
		printf("Failed\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	printf("Success\r\n");
	return EVENT_FLASH_WRITE_SUCCESS;
}

BlEvents_e blInFlashBackupWrite(void)
{
	printf("Writing backup f/w to internal flash: ");
	int state;
	
	state = writeToInternalFlash(1);
	if(state != OPER_STATE_SUCCESS) {
		printf("Failed\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	/* update current firmware details*/
	/* clear the rollback flag*/
	state = updateRollbackFlag(0);
	if(state != OPER_STATE_SUCCESS) {
		printf("Failed!!\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	/* clear the firmware availability flag*/
	state = updateFirmwareAvailableFlag(0);
	if(state != OPER_STATE_SUCCESS) {
		printf("Failed!!\r\n");
		return EVENT_FLASH_WRITE_FAILED;
	}
	
	printf("Success\r\n");
	return EVENT_FLASH_WRITE_SUCCESS;
}

BlEvents_e blJumpApplication(void)
{
	printf("\r\n <<< Jumping to Application Firmware >>>\r\n");
	jump_to_app();
	return EVENT_NA;
}

BlEvents_e blInFlashBackupWriteRetry(void)
{
	printf("\r\nRetrying internal flash backup write\r\n");
	BlEvents_e evt = blInFlashBackupWrite();
	return evt;
}

void initBlSmOperations(void)
{
	timerInit(timerHandler);
}