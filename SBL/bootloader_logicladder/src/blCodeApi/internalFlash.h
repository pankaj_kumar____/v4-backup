/*
 * internalFlash.h
 *
 * Created: 3/23/2020 5:57:38 PM
 *  Author: Kartik.Gupta
 */ 


#ifndef INTERNALFLASH_H_
#define INTERNALFLASH_H_

#include "xmodem.h"

#define INTERNAL_FLASH_START_ADDR		0x400000	/* address where our sbl present*/
#define INTERNAL_FLASH_FINISH_ADDR		0x480000	/* total 512KB*/

/* size of internal flash available for application in bytes*/
#define INTERNAL_FLASH_APP_SIZE_MAX		(INTERNAL_FLASH_FINISH_ADDR - INTERNAL_FLASH_APP_START_ADDR)

/* sbl code space is 32KB, application code remaining space is 480KB*/
#define	INTERNAL_FLASH_APP_START_ADDR		0x408000	/* address from where application starts*/
#define MAX_CHUNK_SIZE						(XMODEM_CHUNK_SIZE * 4)		/* maximum chunk size in bytes*/

#define INTERNAL_FLASH_SECTORS			8		/* no of sectors*/
#define INTERNAL_FLASH_PAGE_SIZE		512		/* page size in bytes*/
#define INTERNAL_FLASH_SECTOR_SIZE		64		/* sector size in kb*/
#define INTERNAL_FLASH_APP_PAGE_COUNT	(INTERNAL_FLASH_FINISH_ADDR - INTERNAL_FLASH_APP_START_ADDR) / INTERNAL_FLASH_PAGE_SIZE
			
/*brief initialize the internal flash memory
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int internalFlashInit(void);

/*brief write data buffer to internal flash memory (at application start address)
 *@param - dataBuff, ptr to the buffer data 
			NOTE:	to avoid errorneous write, max buff size is restricted to  MAX_CHUNK_SIZE
 *@param - maximumDataSize, expected data size to write
 *@param - operStart, flag to indicate to begin writing from start address, if it is set
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int internalFlashAppWrite(void *dataBuff, unsigned int maximumDataSize, char operStart);

/*brief read data buffer from internal flash memory (at application start address)
 *@param - dataBuff, ptr to container buffer data
 *@param - maximumDataSize, expected data size to read
 *@param - operStart, flag to indicate to begin reading from start address, if it is set
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int internalFlashAppRead(void *dataBuff, unsigned int maximumDataSize, char operStart);

#endif /* INTERNALFLASH_H_ */