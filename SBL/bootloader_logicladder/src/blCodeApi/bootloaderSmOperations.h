/*
 * bootloaderSmOperations.h
 *
 * Created: 3/30/2020 4:38:10 PM
 *  Author: Kartik.Gupta
 */ 


#ifndef BOOTLOADERSMOPERATIONS_H_
#define BOOTLOADERSMOPERATIONS_H_

#include "bootloaderStateMachine.h"

/* <Operation to be performed during state transition*/
typedef BlEvents_e (*Operation)(void);

/* jump to application code*/
void jump_to_app(void);

void initBlSmOperations(void);
BlEvents_e blInitOperation(void);
BlEvents_e blInit(void);
BlEvents_e blHalt(void);
BlEvents_e blBinFileInit(void);
BlEvents_e blFwRollback(void);
BlEvents_e blNewFwAvailable(void);
BlEvents_e blBinFileHalt(void);
BlEvents_e blExFlashNewWrite(void);
BlEvents_e blExFlashNewFileValidation(void);
BlEvents_e blJumpApplication(void);
BlEvents_e blInFlashUartNewWrite(void);
BlEvents_e blInFlashHttpNewWrite(void);
BlEvents_e blExFlashBackupWrite(void);
BlEvents_e blInFlashBackupWrite(void);
BlEvents_e blInFlashBackupWriteRetry(void);

#endif /* BOOTLOADERSMOPERATIONS_H_ */