/*
 * md5Validation.c
 *
 * Created: 3/30/2020 3:39:57 AM
 *  Author: Kartik.Gupta
 */ 

#include "common.h"
#include "md5Validation.h"
#include "externalFlash.h"
#include "internalFlash.h"

static unsigned char gLastMd5Value[18];

int calculateAndGetMd5Value(MD5_CTX *md5Operation, unsigned char* dataBuff, unsigned int maxDataSize, \
													char operStart, unsigned char* finalHashValue)
{	
	static unsigned int totalBytesCalculated = 0;
	static unsigned int remainingBytesToCalculate = 0;
	int chunkSize;
	
	if(!dataBuff || !finalHashValue || !md5Operation) {
		memset(gLastMd5Value, '\0', sizeof(gLastMd5Value));
		return OPER_STATE_INVALID_BUFFER;
	}
	
	if(maxDataSize > EXTERNAL_FLASH_APP_SIZE_MAX || maxDataSize == 0) {
		printf("\r\nError: invalid file size\r\n");
		memset(gLastMd5Value, '\0', sizeof(gLastMd5Value));
		return OPER_STATE_INVALID_FILE_SIZE;
	}
	
	if(0 == operStart) {
		if(0 == totalBytesCalculated) {
			printf("\r\nError: md5 operation not yet initialized\r\n");
			memset(gLastMd5Value, '\0', sizeof(gLastMd5Value));
			return OPER_STATE_INVALID_OPERATION;
		}	
	}
	
	if(1 == operStart) {
		totalBytesCalculated = 0;
		remainingBytesToCalculate = 0;
		md5_init(md5Operation);
		memset(gLastMd5Value, '\0', sizeof(gLastMd5Value));
	}
	
	remainingBytesToCalculate = maxDataSize - totalBytesCalculated;
	if(remainingBytesToCalculate >= MAX_CHUNK_SIZE) {
		chunkSize = MAX_CHUNK_SIZE;
	} else {
		chunkSize = remainingBytesToCalculate;
	}
	
	if(remainingBytesToCalculate == 0) {
		printf("\r\nWarning: md5 operation already completed\r\n");;
		return OPER_STATE_INVALID_OPERATION;
	}
	
	md5_update(md5Operation, dataBuff, chunkSize);
	totalBytesCalculated = totalBytesCalculated + chunkSize;
	remainingBytesToCalculate = maxDataSize - totalBytesCalculated;
	if(0 == remainingBytesToCalculate) {
		md5_final(md5Operation, finalHashValue);
		memcpy(gLastMd5Value, finalHashValue, 16);
	}
	
	return OPER_STATE_SUCCESS;
}

unsigned char* getLastMd5Value(void)
{
	if(*gLastMd5Value == '\0') {
		return NULL;
	} else {
		return gLastMd5Value;
	}
}