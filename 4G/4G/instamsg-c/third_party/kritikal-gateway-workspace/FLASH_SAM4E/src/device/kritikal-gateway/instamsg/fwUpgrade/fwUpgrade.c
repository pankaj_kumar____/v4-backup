/*
 * fwUpgrade.c
 *
 * Created: 4/6/2020 5:56:34 PM
 *  Author: Kartik.Gupta
 */ 

#include "fwUpgrade.h"
#include "common.h"
#include "fwUpgradeStateMachine.h"
#include "externalFlash.h"

void beginFwUpgrade(char *serverHostName, char *serverIpAddress, int port)
{
	executeFwUpgradeStateMachine(serverHostName, serverIpAddress, port);
}

void updateSuccessfulFwUpgradeConfigurations(void)
{
	int sizeSys = sizeof(exSysBootConfig_s);
	int sizeDnld = sizeof(exDnldBootConfig_s);
	
	unsigned char sysConfig[sizeSys], dnldConfig[sizeDnld];
	 
	exSysBootConfig_s *cfgSys;
	exDnldBootConfig_s *cfgDnld;
	
	/* initialize external flash*/
	int rc = externalFlashInit();
	if(OPER_STATE_SUCCESS != rc) {
		info_log("error: external flash init\r\n");
		return;
	}
		
	/* read stored configurations*/
	if(OPER_STATE_SUCCESS != getExternalBootConfig((void*)sysConfig, sizeSys, CONFIG_TYPE_SYSTEM)) {
		info_log("error: config read\r\n");
		return;
	}
	
	if(OPER_STATE_SUCCESS != getExternalBootConfig((void*)dnldConfig, sizeDnld, CONFIG_TYPE_DOWNLOAD)) {
		info_log("error: config read\r\n");
		return;
	}
	
	cfgSys = (exSysBootConfig_s*)&sysConfig[0];
	cfgDnld = (exDnldBootConfig_s*)&dnldConfig[0];
	
	/* for first time on new upgrade*/
	/* make the newly upgraded firmware as current firmware, reset new firmware info*/
	if(1 == cfgSys->isRollBackRequested) {	
		memcpy(&cfgSys->currentFirmwareHeader, &cfgSys->newFirmwareHeader, sizeof(firmwareHeader_s));
		memset(&cfgSys->newFirmwareHeader, '\0', sizeof(firmwareHeader_s));
		memset(&cfgDnld->lastFwUpdateInfo, '\0', sizeof(lastFwUpdateInfo_s));
	}
	
	/* clear firmware availability flag*/
	cfgSys->isNewFirmwareAvaliable = 0;
	
	/* clear roll back flag*/
	cfgSys->isRollBackRequested = 0;
	
	/* clear previous backup status*/
	cfgSys->previousBackupStatus = 0;
	
	if(OPER_STATE_SUCCESS != setExternalBootConfig((void*)cfgSys, sizeSys, CONFIG_TYPE_SYSTEM)) {
		info_log("error: config set\r\n");
		return;
	}
	
	if(OPER_STATE_SUCCESS != setExternalBootConfig((void*)cfgDnld, sizeDnld, CONFIG_TYPE_DOWNLOAD)) {
		info_log("error: config set\r\n");
		return;
	}
}