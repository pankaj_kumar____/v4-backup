/*
 * fwUpgradeSmOperations.h
 *
 * Created: 4/10/2020 5:06:48 PM
 *  Author: Kartik.Gupta
 */ 

#include "fwUpgradeStateMachine.h"

#ifndef FWUPGRADESMOPERATIONS_H_
#define FWUPGRADESMOPERATIONS_H_

/* <Operation to be performed during state transition*/
typedef FwUpgradeEvents_e (*Operation)(void);

int initFwUpgradeSmOperations(char *hostname, char* ipaddress, int port);

/* send the initial request to get firmware executable file information*/
FwUpgradeEvents_e fwAvailableRequest(void);

/* check whether upgrade required or not*/
FwUpgradeEvents_e fwUpgradeVerification(void);

/* download the firmware, with feature of resuming if required*/
FwUpgradeEvents_e fwDownload(void);

/* validate integrity of downloaded firmware*/
FwUpgradeEvents_e fwValidate(void);

/* update the firmware config, like file header info, raise update flag*/
FwUpgradeEvents_e fwUpdateConfig(void);

/* reset the system*/
FwUpgradeEvents_e fwResetDevice(void);

/* exit from the state machine and come out of upgrading*/
FwUpgradeEvents_e fwUpgradeExit(void);

#endif /* FWUPGRADESMOPERATIONS_H_ */