/*
 * ll_console.c
 *
 * Created: 12/2/2020 9:29:57 AM
 *  Author: Kartik.Gupta
 */ 
/*
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ 	File Name	: ll_Config.c												                                           +
+	Purpose		:       																							   +
+                 						                                                                               +
+																			                                           +
+ Created by Ashutosh										                                            #21-Aug-2020#  +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/


#include <string.h>
#include <stdio.h>
#include "ll_common.h"
#include "ll_console.h"
#include "ll_config.h"
#include "sam4e8c.h"
#include "uart.h"
#include "device_defines.h"
#include "ll_timer.h"
#include "delay.h"
#include "../../../../../../../../common/instamsg/driver/include/misc.h"
#include "../../../../../../../../common/instamsg/driver/include/log.h"
#include "../../../../../../../common/ioeye/include/serial.h"

#include <string.h>

/*--------------------------------------------------------------------------------------*/
#define PORT_0              0x00
#define PORT_1              0x01
#define PORT_2              0x02

unsigned char         timeOut;
char gIsConsoleTimerSet = 0;
char gSysConsoleState	= SYS_CONSOLE_STATE_BYPASS;

//////////////////////////////////// PRIVATE FUNCTION ////////////////////////////////////
/*
 ****************************************************************************************
~#  Function Name : kbHit                                                               #~ 
~#  Parameter2    : charBuff: Buffer to Store Data, Received from Serial Port           #~
~#                                                                                      #~
~#  Return Type   : No of Bytes Read | 0xAA for Enter Key | 0x55 for ESC Key            #~
~#  Scope         : Local                                                               #~
~#  Functionality : This function will Trap the character received of Serial Port and   #~
~#                  Echo and copy the char into Buffer                                  #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #21/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/
unsigned char kbHit(unsigned char *charBuff) {
    unsigned char configDoller=FALSE;
    unsigned char getChar=0,charCounter=0;
    memset(charBuff,0x00,32);
	/*!> Infinite Loop */
    while(1)
	{
		#if 0
		/*!> If Timeout of 1 Minute Expired */ 
        if(getTimeout()==0)
		{																	
            sg_sprintf(LOG_GLOBAL_BUFFER,"Configuration Timed Out...\r\nDevice Restarting!!!");
            info_log(LOG_GLOBAL_BUFFER);
			/*!>Resetting the Device*/
			resetDevice();                                                  
            while(1);
        }  
		#endif
		
		/*!>Trap the character received of Serial Port*/
		uart_read(CONSOLE_UART,&getChar);
		                                                                                        
        if((getChar!=0x00)&&(getChar!=0xff))
		{
			/*!>In case first '$' Char received*/
            if((configDoller==FALSE)&&(getChar==DR))						
			{                           
				/*!> Clear the RX Buffer and enable the ECHO*/
                configDoller=TRUE;                                             
                getChar=0;
                sg_sprintf(LOG_GLOBAL_BUFFER,CRLF);
				echo_log(LOG_GLOBAL_BUFFER);
            }
            else if(configDoller==TRUE)
			{        
				/*!>If Received the second '$'*/                                
                if(getChar==DR)
				{		
					/*!>Mark it Start of Text*/														
                    charCounter=0;
                    memset(charBuff,0x00,32);                                  
                }
                sg_sprintf(LOG_GLOBAL_BUFFER,"%c",getChar);
				echo_log(LOG_GLOBAL_BUFFER);
				/*!>Echo and copy the char into Buffer*/
                *(charBuff+charCounter)=getChar;    
				/*!>Increment buffer pointer by 1*/                            
                charCounter++;     
				/*!>If '#' Received*/                                             
                if(getChar==HS)													
				{          
					/*!>Then Mark it End of Text*/                                      
                    sg_sprintf(LOG_GLOBAL_BUFFER,"\r\n%s",charBuff);           
					info_log(LOG_GLOBAL_BUFFER);
					/*!>Return the no of character received*/
                    return charCounter;                                         
                }
				getChar =0;
            }
			/*!> If Enter Key Pressed*/
			else if(getChar==CR)												
			{   
				/*!>Return 0xAA to skip to the next*/                                           
                sg_sprintf(LOG_GLOBAL_BUFFER,"%s",CRLF);						
				echo_log(LOG_GLOBAL_BUFFER);
                return 0xAA;
            }
			/*!>If ESC Key pressed then*/
			else if(getChar==EC)												
			{      
				/*!>Return 0x55 to cancel configuration.*/                                        
                return 0x55;													
            }
        }
    }/* End of While */
}/* End of Function kbHit*/




/*
 ****************************************************************************************
~#  Function Name : getConfig                                                           #~ 
~#  Parameters    : None                                                                #~
~#  Return Type   : None                                                                #~
~#  Scope         : Local                                                               #~
~#  Functionality : Load the structure variables with EEPROM Data                       #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #21/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/
void getConfig (void)
{
    
}

//////////////////////////////////// PUBLIC FUNCTION ////////////////////////////////////
/*
 ****************************************************************************************
~#  Function Name : setTimeout                                                          #~ 
~#  Parameters    : Val: Timeout Value in Seconds                                       #~
~#                                                                                      #~
~#  Return Type   : Void                                                                #~
~#  Scope         : Global                                                              #~
~#  Functionality : This function will set the Timeout Value as per the value supplied  #~
~#                  as parameter in Seconds                                             #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #21/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/

void setTimeout(unsigned char Val ){
    timeOut=Val;
}

/*
 ****************************************************************************************
~#  Function Name : getTimeout                                                          #~ 
~#  Parameters    : Void                                                                #~
~#                                                                                      #~
~#  Return Type   : Current Timeout Count in Seconds                                    #~
~#  Scope         : Global                                                              #~
~#  Functionality : This function will return the Current Timeout Count                 #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #21/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/
unsigned char getTimeout( void )
{
    return timeOut;
}

/*
 ****************************************************************************************
~#  Function Name : printConfigValue                                                    #~ 
~#  Parameters    : None                                                                #~
~#  Return Type   : None                                                                #~
~#  Scope         : Global                                                              #~
~#  Functionality : Print the Current value of Configuration Parameters.                #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #21/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/
void printConfigValue( void ) {
    
    unsigned char ret_val=0;
	unsigned char temp_buffer[64]={0x00};
	unsigned char NA[]="---NA---";
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Total Number of Ports |%d|",get_total_no_modbus_port());
	info_log(LOG_GLOBAL_BUFFER);

	sg_sprintf(LOG_GLOBAL_BUFFER, "Enable Message Acknowledgment |%d|",get_message_ack_config());
	info_log(LOG_GLOBAL_BUFFER);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Enable Debug Logging |%d|",get_debug_logging_config());
	info_log(LOG_GLOBAL_BUFFER);

	sg_sprintf(LOG_GLOBAL_BUFFER, "Enable Persist Run Hour |%d|",get_log_run_hour_config());
	info_log(LOG_GLOBAL_BUFFER);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Enable Generator Data Transmission |%d|",get_generator_data_config());
	info_log(LOG_GLOBAL_BUFFER);
	
	ret_val=get_gsm_module_type_config();
	
	if(0x03 == ret_val)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "GSM Module Type |3G|");
	}
	else if(0x04 == ret_val)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "GSM Module Type |4G|");
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "GSM Module Type |UNKNOWN|");
	}
	
	info_log(LOG_GLOBAL_BUFFER);
	
	ret_val=get_gsm_sim_slot_config();
	
	if(0x00 == ret_val)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "GSM SIM inserted in |1st| Slot");
	}
	else if(0x01 == ret_val)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "GSM SIM inserted in |2nd| Slot");
	}
	info_log(LOG_GLOBAL_BUFFER);
	
	ret_val=get_instaMsg_server_address(temp_buffer);
	sg_sprintf(LOG_GLOBAL_BUFFER, "InstaMsg Server Address |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "InstaMsg Server Port Number |%d|",get_server_port_number());
	info_log(LOG_GLOBAL_BUFFER);
	
	ret_val=get_fota_host_address(temp_buffer, sizeof(temp_buffer));
	sg_sprintf(LOG_GLOBAL_BUFFER, "Fota Server Address |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	ret_val=get_fota_host_ip_address(temp_buffer, sizeof(temp_buffer));
	sg_sprintf(LOG_GLOBAL_BUFFER, "Fota Server IP Address |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	ret_val=get_fota_host_name(temp_buffer, sizeof(temp_buffer));
	sg_sprintf(LOG_GLOBAL_BUFFER, "Fota Server Hostname |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Fota Server port number |%d|", get_fota_host_port_number());
	info_log(LOG_GLOBAL_BUFFER);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Enable SSL |%d|",get_ssl_config());
	info_log(LOG_GLOBAL_BUFFER);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Read after Write Count |%d|",get_read_after_write_config());
	info_log(LOG_GLOBAL_BUFFER);
	
	memset(temp_buffer,0x00,64);
	ret_val=get_local_ip_address(temp_buffer);
	sg_sprintf(LOG_GLOBAL_BUFFER, "Device Local IP Address |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	memset(temp_buffer,0x00,64);
	ret_val=get_local_gateway_ip(temp_buffer);
	sg_sprintf(LOG_GLOBAL_BUFFER, "Device Local Gateway IP |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	memset(temp_buffer,0x00,64);
	ret_val=get_local_subnet_mask(temp_buffer);
	sg_sprintf(LOG_GLOBAL_BUFFER, "Device Local Subnet Mask |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	memset(temp_buffer,0x00,64);
	ret_val=get_local_dns_address(temp_buffer);
	sg_sprintf(LOG_GLOBAL_BUFFER, "Device DNS Server Address |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	memset(temp_buffer,0x00,64);
	ret_val=get_imei_no(temp_buffer);
	sg_sprintf(LOG_GLOBAL_BUFFER, "GSM Module IMEI Number |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	memset(temp_buffer,0x00,64);
	ret_val=get_apn(temp_buffer);
	sg_sprintf(LOG_GLOBAL_BUFFER, "Access Point Name |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	memset(temp_buffer,0x00,64);
	ret_val= get_ntp_server_address(temp_buffer);
	sg_sprintf(LOG_GLOBAL_BUFFER, "NTP Server Address |%s|",(ret_val==0)?temp_buffer:NA);
	info_log(LOG_GLOBAL_BUFFER);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Ping Request Interval |%d| Seconds",get_ping_req_interval_in_sec());
	info_log(LOG_GLOBAL_BUFFER);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Data Polling Frequency |%d| Seconds",get_business_logic_interval_in_sec());
	info_log(LOG_GLOBAL_BUFFER);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Location Polling Frequency |%d| Seconds",get_gps_location_interval());
	info_log(LOG_GLOBAL_BUFFER);
	
	for(int i = 0; i < get_total_no_modbus_port(); i++) {
		
		ret_val = get_port_type(i);
		if(PORT_TYPE_RS232 == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Type |%s|",i, "RS-232");
		}
		else if(PORT_TYPE_RS485 == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Type |%s|",i, "RS-485");
		}
		else if(PORT_TYPE_TCP == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Type |%s|",i, "TCP");
		}
		else
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Type |%s|",i, NA);
		}
		info_log(LOG_GLOBAL_BUFFER);
		
		ret_val = get_port_parameter(i, temp_buffer);
		if(0x01 == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Parameter Value |%s|",i, NA);
		}
		else
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Parameter Value |%s|",i, temp_buffer);
		}
		info_log(LOG_GLOBAL_BUFFER);
		
		ret_val = get_port_delimiter(i);
		if(0x00 == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Delimiter Value |%s|",i, NA);
		}
		else
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Delimiter Decimal Value |%d|",i, ret_val);
		}
		info_log(LOG_GLOBAL_BUFFER);
		
		memset(temp_buffer,0x00, sizeof(temp_buffer));

		ret_val = get_modbus_command(i, temp_buffer);
		if(0x01 == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Device Commands |%s|", i, NA);
		}
		else
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Port-%d Device Commands |%s|", i, temp_buffer);
		}
		info_log(LOG_GLOBAL_BUFFER);
	}
}

/*
 ****************************************************************************************
~#  Function Name : isNum                                                               #~ 
~#  Parameter1    : Arry : Source String for numeric validation.                        #~
~#  Return Type   : 0- for Valid Numeric Data| 1- Invalid Numeric Data                  #~
~#  Scope         : Global                                                              #~
~#  Functionality : Validate for all the chars is numerical or not in supplied string   #~
~#                  as parameter                                                        #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~/\~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~                                                                  
~# Date: #13/10/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/

unsigned char isNum(unsigned char *Arry)
{
	unsigned char icnt=0;
	if(*(Arry+0)!= 0)
	{
		while(*(Arry+icnt))
		{
			if((*(Arry+icnt)<'0') || (*(Arry+icnt)>'9'))
			{
				return 1;
			}/*!> End of if Part*/
			
			icnt++;
		}/*!>End of while*/
	}/*!> End of if(*(Arry+0)!= 0)*/
	else
	{
		return 1;	
	}/*!> End of Else Part*/
	return 0;
}/*!> End of Function isNum(PC Arry)*/
/*
****************************************************************************************
~#  Function Name : isValidIP                                                           #~
~#  Parameter1    : ipAddr : Source IP Address String for validation.                   #~
~#  Parameter2    : finalIP: Destination IP Address String after validation.            #~
~#  Return Type   : 0- for Valid IP| 1- Invalid IP.                                     #~
~#  Scope         : Global                                                              #~
~#  Functionality : This function will be used to validate IP Address is correct or not #~
~#                  copy optimized ip in final IP parameter in case of valid IP and     #~
~#                  return 0. Return 1 in case of any ERROR.                            #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~/\~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~
~# Date: #13/10/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/

unsigned char isValidIP(unsigned char *ipAddr, unsigned char *finalIP)
{
	unsigned char *a=ipAddr+1;
	unsigned char dotCount=0,iLoop=0;
	unsigned char ipOctet[4]={0x00,0x00,0x00,0x00};
	unsigned short tempVal=0;
	while(iLoop<*(ipAddr+0))
	{
		if(*(a+iLoop)=='.')
		{
			if((tempVal>=0)&&(tempVal<=255))
			{
				*(ipOctet+dotCount)=(unsigned char)tempVal;
			}/*!> End of if; Octet >=0 and octet<=255 validation*/
			else
			{
				return 1;
			}/*!> End of Else*/
			
			dotCount++;tempVal=0;
			
		}/*!> End of if; Validating . Chars for octet validation*/
		else
		{
			if((*(a+iLoop)>=0x30)&&(*(a+iLoop)<=0x39))
			{
				tempVal=tempVal*10+(*(a+iLoop)-0x30);
			}/*!> End of if; numeric data validation*/
			else
			{
				break;
			}/*!> End of Else; Non-numeric data*/
		}/*!> End of Else; other chars than . chars*/
		iLoop++;																		
	}/*!> End of while; 16 chars IP Traversal*/
	
	*(a+iLoop)='\0';
	
	strcpy((char *)finalIP,(char *)ipAddr);
	
	if(tempVal>255)
	{
		return 1;
	}/*!> End of if; Last octat <=255 Validation*/
	
	*(ipOctet+dotCount)=(unsigned char)tempVal;

	if(dotCount!=3)
	{
		return 1;
	}/*!> End of if; exact . count in IP for 3*/
	
	if((*(ipOctet+0)==0)||(*(ipOctet+0)>254)||(*(ipOctet+1)>255)||(*(ipOctet+2)>255)||(*(ipOctet+3)>255))
	{
		return 1;
	}/*!> End of if validating all the octet of IP*/
	
	return 0;
}/*!> End of Function isValidIP(PUC ipAddr, PUC finalIP)*/

//////////////////////////////////////////////////////////////////////////
unsigned char isValidSubnetMask(unsigned char *ipAddr, unsigned char *finalIP)
{
	unsigned char *a=ipAddr+1;
	unsigned char dotCount=0,iLoop=0;
	unsigned char ipOctet[4]={0x00,0x00,0x00,0x00};
	unsigned short tempVal=0;
	while(iLoop<*(ipAddr+0))
	{
		if(*(a+iLoop)=='.')
		{
			if((tempVal>=0)&&(tempVal<=255))
			{
				*(ipOctet+dotCount)=(unsigned char)tempVal;
			}/*!> End of if; Octet >=0 and octet<=255 validation*/
			else
			{
				return 1;
			}/*!> End of Else*/
			
			dotCount++;tempVal=0;
			
		}/*!> End of if; Validating . Chars for octet validation*/
		else
		{
			if((*(a+iLoop)>=0x30)&&(*(a+iLoop)<=0x39))
			{
				tempVal=tempVal*10+(*(a+iLoop)-0x30);
			}/*!> End of if; numeric data validation*/
			else
			{
				break;
			}/*!> End of Else; Non-numeric data*/
		}/*!> End of Else; other chars than . chars*/
		iLoop++;																		
	}/*!> End of while; 16 chars IP Traversal*/
	
	*(a+iLoop)='\0';
	
	strcpy((char *)finalIP,(char *)ipAddr);
	
	*(ipOctet+dotCount)=(unsigned char)tempVal;

	if(dotCount!=3)
	{
		return 1;
	}/*!> End of if; exact . count in IP for 3*/
	
	return 0;
}/*!> End of Function isValidSubnetMask(PUC ipAddr, PUC finalIP)*/
///////////////////////////////////////////////////////////////////////////////
unsigned char check_domain_name( unsigned char *Buff){
	unsigned char icnt=0;
	unsigned char *Arry=Buff;
	if(*(Arry+0)!=0)
	{
		while(*(Arry+icnt))
		{
			if((*(Arry+icnt)<= 0x20) || (*(Arry+icnt) >=0x7f))
			{
				return 1;
			}/*!> End of if Part*/
			
			icnt++;
		}// End of while(*(Arry+icnt))
	}/*!> End of if(*(Arry+0)!=0)*/
	else
	{
		return 1;
	}/*!> End of Else part*/
	
	return 0;
}/*!> End of Function check_domain_name( PUC Buff)*/

/*
 ****************************************************************************************
~#  Function Name : printInstruction                                                    #~ 
~#  Parameters    : cmdChar: Configuration Setting Caption Decision as per Command char #~
~#  Return Type   : None                                                                #~
~#  Scope         : Global                                                              #~
~#  Functionality : Print Caption of Configuration Parameters along with current Value. #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #22/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/

void printInstruction(unsigned char cmdChar)
{
    unsigned char temp_buffer[64], ret_val=0x00;
  	char NA[]="---NA---";
	//setTimeout(60); 
	updateSystemConfigurations(0);
    switch(cmdChar){
        /*----------------------------------------------------------------------------*/
        /*!> GREETING MESSAGE TO ENTER IN CONFIG MODE */
        case CONFIG_GREETING:     
            sg_sprintf(LOG_GLOBAL_BUFFER, "###LogicLadder Technologies STD-V4 Configuration Console###\r\n\r\n\r\nPress ENTER to continue...");
            info_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> TOTAL NUMBER OF PORTS TO CONFIGURE*/
        case CONFIG_PORT_NUM:     
            sg_sprintf(LOG_GLOBAL_BUFFER, "Total Number of Ports |%d|",get_total_no_modbus_port());
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> ENABLE OR DISABLE MESSAGE ACKNOWLEDGMENT*/
        case CONFIG_ENABLE_ACK:    
            sg_sprintf(LOG_GLOBAL_BUFFER, "Enable Message Acknowledgment |%d|",get_message_ack_config());
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> ENABLE/DISABLE DEBUG LOGGING*/
        case CONFIG_DEBUG_LOGGING:
            sg_sprintf(LOG_GLOBAL_BUFFER, "Enable Debug Logging |%d|",get_debug_logging_config());
            echo_log(LOG_GLOBAL_BUFFER);
        break;
		/*----------------------------------------------------------------------------*/
		/*!> ENABLE/DISABLE PERSIST RUN HOUR*/
		case CONFIG_PERSIST_RUN_HOUR:
		sg_sprintf(LOG_GLOBAL_BUFFER, "Enable Persist Run Hour |%d|",get_log_run_hour_config());
		echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> ENABLE/DISABLE GENERATOR DATA TRANSMISSION */
		case CONFIG_GENERTOR_DATA_TRANSMIT:
		sg_sprintf(LOG_GLOBAL_BUFFER, "Enable Generator Data Transmission |%d|",get_generator_data_config());
		echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> GSM MODULE TYPE*/
		case CONFIG_GSM_MODULE_TYPE:
		ret_val=get_gsm_module_type_config();
		
		if(0x03 == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "GSM Module Type |3G|");
		}
		else if(0x04 == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "GSM Module Type |4G|");
		}
		else
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "GSM Module Type |UNKNOWN|");
		}
		
		echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> GSM SIM SLOT*/
		case CONFIG_GSM_SIM_SLOT:
		ret_val=get_gsm_sim_slot_config();
		
		if(0x00 == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "GSM SIM inserted in |1st| Slot");
		}
		else if(0x01 == ret_val)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "GSM SIM inserted in |2nd| Slot");
		}
		
		echo_log(LOG_GLOBAL_BUFFER);
		break;
        /*----------------------------------------------------------------------------*/
        /*!> ENABLE/DISABLE SSL*/
        case CONFIG_ENABLE_SSL:
            sg_sprintf(LOG_GLOBAL_BUFFER, "Enable SSL |%d|",get_ssl_config());
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> READ AFTER WRITE COUNT*/
        case CONFIG_READ_AFTER_WRITE_COUNT:
            sg_sprintf(LOG_GLOBAL_BUFFER, "Read after Write Count |%d|",get_read_after_write_config());
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> DEVICE LOCAL STATIC IP*/
        case CONFIG_LOCAL_STATIC_IP:
            memset(temp_buffer,0x00,64);
            ret_val=get_local_ip_address(temp_buffer);
            sg_sprintf(LOG_GLOBAL_BUFFER, "Device Local IP Address |%s|",(ret_val==0)?temp_buffer:NA);
            echo_log(LOG_GLOBAL_BUFFER);
        break;
		/*----------------------------------------------------------------------------*/
		/*!> DEVICE LOCAL GATEWAY IP*/
		case CONFIG_LOCAL_GATEWAY_IP:
		memset(temp_buffer,0x00,64);
		ret_val=get_local_gateway_ip(temp_buffer);
		sg_sprintf(LOG_GLOBAL_BUFFER, "Device Local Gateway IP |%s|",(ret_val==0)?temp_buffer:NA);
		echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> DEVICE LOCAL SUBNET MASK*/
		case CONFIG_LOCAL_SUBNET_MASK:
		memset(temp_buffer,0x00,64);
		ret_val=get_local_subnet_mask(temp_buffer);
		sg_sprintf(LOG_GLOBAL_BUFFER, "Device Local Subnet Mask |%s|",(ret_val==0)?temp_buffer:NA);
		echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> DEVICE DNS ADDRESS*/
		case CONFIG_LOCAL_DNS:
		memset(temp_buffer,0x00,64);
		ret_val=get_local_dns_address(temp_buffer);
		sg_sprintf(LOG_GLOBAL_BUFFER, "Device DNS Server Address |%s|",(ret_val==0)?temp_buffer:NA);
		echo_log(LOG_GLOBAL_BUFFER);
		break;
        /*----------------------------------------------------------------------------*/
        /*!> GSM MODULE IMEI NUMBER*/
        case CONFIG_DEVICE_IMEI_NO:
            memset(temp_buffer,0x00,64);
            ret_val=get_imei_no(temp_buffer);
            sg_sprintf(LOG_GLOBAL_BUFFER, "GSM Module IMEI Number |%s|",(ret_val==0)?temp_buffer:NA);
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> ACCESS POINT NAME FOR GPRS*/
        case CONFIG_APN:
            memset(temp_buffer,0x00,64);
            ret_val=get_apn(temp_buffer);
            sg_sprintf(LOG_GLOBAL_BUFFER, "Access Point Name |%s|",(ret_val==0)?temp_buffer:NA);
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> NTP SERVER ADDRESS*/
        case CONFIG_NTP_SERVER_ADDRESS:
            memset(temp_buffer,0x00,64);
            ret_val= get_ntp_server_address(temp_buffer);
            sg_sprintf(LOG_GLOBAL_BUFFER, "NTP Server Address |%s|",(ret_val==0)?temp_buffer:NA);
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PING REQUEST INTERVAL TIME IN SECONDS*/
        case CONFIG_PING_REQUEST_INTERVAL:
            sg_sprintf(LOG_GLOBAL_BUFFER, "Ping Request Interval |%d| Seconds",get_ping_req_interval_in_sec());
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> BUSINESS LOGIC INTERVAL TIME IN SECONDS*/
        case CONFIG_BUSINESS_LOGIC_INTERVAL:
            sg_sprintf(LOG_GLOBAL_BUFFER, "Data Polling Frequency |%d| Seconds",get_business_logic_interval_in_sec());
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> GPS LOCATION INTERVAL IN SECONDS*/
        case CONFIG_GPS_LOCATION_INTERVAL:
            sg_sprintf(LOG_GLOBAL_BUFFER, "Location Polling Frequency |%d| Seconds",get_gps_location_interval());
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 0 TYPE*/
        case CONFIG_PORT_0_TYPE:
            ret_val=get_port_config_status(PORT_0);

            if(0x55 == ret_val)
            {
                ret_val = get_port_type(PORT_0);

                if(0x00 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Type |%s|","RS-232");
                }
                else if(0x01 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Type |%s|","RS-485");
                }
                else if(0x02 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Type |%s|","TCP");
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Type |%s|",NA);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Type |%s|",NA);
            }
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 0 DELIMITER*/
        case CONFIG_PORT_0_DELIMITER:
            ret_val = get_port_config_status(PORT_0);

            if(0x55 == ret_val)
            {
                ret_val = get_port_delimiter(PORT_0);
                
                if(0x00 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Delimiter Value |%s|",NA);
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Delimiter Decimal Value |%d|",ret_val);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Delimiter Decimal Value |%s|",NA);
            }

            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 0 CONFIGURATION PARAMETERS*/
        case CONFIG_PORT_0_PARAMETER:
            ret_val = get_port_config_status(PORT_0);

            if(0x55 == ret_val)
            {
                memset(temp_buffer,0x00,64);
                ret_val = get_port_parameter(PORT_0, temp_buffer);
                
                if(0x01 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Parameter Value |%s|",NA);
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Parameter Value |%s|",temp_buffer);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "Port-0 Parameter Value |%s|",NA);
            }

            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 0 COMMAND*/
        case CONFIG_PORT_0_COMMAND:
            ret_val=get_port_config_status(PORT_0);
             if(0x55 == ret_val)
            {
                memset(temp_buffer,0x00,64);
                ret_val = get_modbus_command(PORT_0, temp_buffer);
                
                if(0x01 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "PORT-0 Device Commands |%s|",NA);
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "PORT-0 Device Commands |%s|",temp_buffer);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "PORT-0 Device Commands |%s|",NA);
            }
            
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 1 TYPE*/
        case CONFIG_PORT_1_TYPE:
            ret_val=get_port_config_status(PORT_1);
            if(0x55 == ret_val)
            {
                ret_val = get_port_type(PORT_1);

                if(0x00 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Type |%s|","RS-232");
                }
                else if(0x01 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Type |%s|","RS-485");
                }
                else if(0x02 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Type |%s|","TCP");
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Type |%s|",NA);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Type |%s|",NA);
            }
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 1 DELIMITER*/
        case CONFIG_PORT_1_DELIMITER:
            ret_val = get_port_config_status(PORT_1);

            if(0x55 == ret_val)
            {
                ret_val = get_port_delimiter(PORT_1);
                
                if(0x00 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Delimiter Value |%s|",NA);
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Delimiter Decimal Value |%d|",ret_val);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Delimiter Decimal Value |%s|",NA);
            }

            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 1 CONFIGURATION PARAMETER*/
        case CONFIG_PORT_1_PARAMETER:
            ret_val = get_port_config_status(PORT_1);

            if(0x55 == ret_val)
            {
                memset(temp_buffer,0x00,64);
                ret_val = get_port_parameter(PORT_1, temp_buffer);
                
                if(0x01 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Parameter Value |%s|",NA);
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Parameter Value |%s|",temp_buffer);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "Port-1 Parameter Value |%s|",NA);
            }

            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 1 COMMAND*/
        case CONFIG_PORT_1_COMMAND:
            ret_val=get_port_config_status(PORT_1);
             if(0x55 == ret_val)
            {
                memset(temp_buffer,0x00,64);
                ret_val = get_modbus_command(PORT_1, temp_buffer);
                
                if(0x01 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "PORT-1 Device Commands |%s|",NA);
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "PORT-1 Device Commands |%s|",temp_buffer);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "PORT-1 Device Commands |%s|",NA);
            }
            
            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 2 TYPE*/
        case CONFIG_PORT_2_TYPE:
            ret_val=get_port_config_status(PORT_2);
           
            if(0x55 == ret_val)
            {
                ret_val = get_port_type(PORT_2);

                if(0x00 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Type |%s|","RS-232");
                }
                else if(0x01 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Type |%s|","RS-485");
                }
                else if(0x02 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Type |%s|","TCP");
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Type |%s|",NA);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Type |%s|",NA);
            }

            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 2 DELIMITER*/
        case CONFIG_PORT_2_DELIMITER:
            ret_val = get_port_config_status(PORT_2);

            if(0x55 == ret_val)
            {
                ret_val = get_port_delimiter(PORT_2);

                if(0x00 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Delimiter Value |%s|",NA);
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Delimiter Decimal Value |%d|",ret_val);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Delimiter Decimal Value |%s|",NA);
            }

            echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 2 CONFIGURATION PARAMETER*/
        case CONFIG_PORT_2_PARAMETER:
            ret_val = get_port_config_status(PORT_2);

            if(0x55 == ret_val)
            {
                memset(temp_buffer,0x00,64);
                ret_val = get_port_parameter(PORT_2, temp_buffer);
                
                if(0x01 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Parameter Value |%s|",NA);
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Parameter Value |%s|",temp_buffer);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "Port-2 Parameter Value |%s|",NA);
            }
			
			echo_log(LOG_GLOBAL_BUFFER);
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 2 COMMAND*/
        case CONFIG_PORT_2_COMMAND:
            ret_val=get_port_config_status(PORT_2);
             if(0x55 == ret_val)
            {
                memset(temp_buffer,0x00,64);
                ret_val = get_modbus_command(PORT_2, temp_buffer);
                
                if(0x01 == ret_val)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "PORT-2 Device Commands |%s|",NA);
                }
                else
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "PORT-2 Device Commands |%s|",temp_buffer);
                }
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "PORT-2 Device Commands |%s|",NA);
            }
            
            echo_log(LOG_GLOBAL_BUFFER);
        break;
		/*----------------------------------------------------------------------------*/
		/*!> Device Password*/
		case CONFIG_DEVICE_PASSWORD:
			memset(temp_buffer,0x00,64);
			ret_val=get_device_password(temp_buffer);
			sg_sprintf(LOG_GLOBAL_BUFFER, "Device Password |%s|",(ret_val==0)?temp_buffer:NA);
			echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> InstaMsg Server Address*/
		case CONFIG_SERVER_ADDRESS:
			memset(temp_buffer,0x00,64);
			ret_val=get_instaMsg_server_address(temp_buffer);
			sg_sprintf(LOG_GLOBAL_BUFFER, "InstaMsg Server Address |%s|",(ret_val==0)?temp_buffer:NA);
			echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> InstaMsg Server Port Number*/
		case CONFIG_SERVER_PORT_NO:
			 sg_sprintf(LOG_GLOBAL_BUFFER, "InstaMsg Server Port Number |%d|",get_server_port_number());
			 echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server Address*/
		case CONFIG_FOTA_SERVER_ADDRESS:
		memset(temp_buffer,0x00,64);
			ret_val=get_fota_host_address(temp_buffer, sizeof(temp_buffer));
			sg_sprintf(LOG_GLOBAL_BUFFER, "Server Address for FoTA |%s|",(ret_val==0)?temp_buffer:NA);
			echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server Port Number*/
		case CONFIG_FOTA_SERVER_PORT_NO:
			sg_sprintf(LOG_GLOBAL_BUFFER, "Server Port Number for FoTA |%d|",get_fota_host_port_number());
			echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Firmware Name*/
		case CONFIG_FIRMWARE_NAME:
			memset(temp_buffer,0x00,64);
			ret_val=get_firmware_name(temp_buffer);
			sg_sprintf(LOG_GLOBAL_BUFFER, "Firmware Name for FoTA |%s|",(ret_val==0)?temp_buffer:NA);
			echo_log(LOG_GLOBAL_BUFFER);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> Instruction to format flash memory*/
		case CONFIG_FORMAT_MEMORY:
			sg_sprintf(LOG_GLOBAL_BUFFER, "Do You want to format the memory? [Y/N]" );
			echo_log(LOG_GLOBAL_BUFFER);
		break;
        /*----------------------------------------------------------------------------*/
        /*!> CONFIGURATION COMPLETION CONFIRMATION*/
        default:
            sg_sprintf(LOG_GLOBAL_BUFFER, "YUPPY...\r\nLogicLadder Device Version: |%s|, InstaMsg Version: |%s|, Configuration done Successfully...",DEVICE_VERSION,INSTAMSG_VERSION);
            info_log(LOG_GLOBAL_BUFFER);
			resetDevice();
        break;
    }    
}

/*
 ****************************************************************************************
~#  Function Name : setConfig                                                           #~ 
~#  Parameter1    : configParam: Configuration Parameter Location                       #~
~#  Parameter2    : configVal: Configuration Parameter Value to set                     #~
~#  Return Type   : None                                                                #~
~#  Scope         : Local                                                               #~
~#  Functionality : This function store the values supplied as configVal at the location#~
~#                  supplied in configParam.                                            #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #21/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/
void setConfig(unsigned char configParam,void *configVal)
{
	unsigned char	uc_temp=0;
	unsigned short	us_temp=0;
	unsigned char	*puc_temp;
	
    switch(configParam){
	    /*----------------------------------------------------------------------------*/
	    /*!> TOTAL NUMBER OF PORTS TO CONFIGURE*/
	    case CONFIG_PORT_NUM:
			uc_temp= (*(unsigned char *)configVal);
		    set_total_no_modbus_port(uc_temp);	    
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> ENABLE OR DISABLE MESSAGE ACKNOWLEDGMENT*/
	    case CONFIG_ENABLE_ACK:
			uc_temp= (*(unsigned char *)configVal);
			set_message_ack_config(uc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> ENABLE/DISABLE DEBUG LOGGING*/
	    case CONFIG_DEBUG_LOGGING:
			uc_temp= (*(unsigned char *)configVal);
			set_debug_logging_config(uc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> ENABLE/DISABLE SSL*/
	    case CONFIG_ENABLE_SSL:
			uc_temp= (*(unsigned char *)configVal);
			set_ssl_config(uc_temp);
	    break;
		/*----------------------------------------------------------------------------*/
		/*!> CONFIGURE GSM MODULE TYPE*/
		case CONFIG_GSM_MODULE_TYPE:
			uc_temp= (*(unsigned char *)configVal);
			set_gsm_module_type_config(uc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> CONFIGURE SIM SLOT*/
		case CONFIG_GSM_SIM_SLOT:
		uc_temp= (*(unsigned char *)configVal);
		set_gsm_sim_slot_config(uc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> ENABLE/DISABLE PERSIST RUN HOUR*/
		case CONFIG_PERSIST_RUN_HOUR:
			uc_temp= (*(unsigned char *)configVal);
			set_log_run_hour_config(uc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> ENABLE/DISABLE Generator Data Transmission*/
		case CONFIG_GENERTOR_DATA_TRANSMIT:
			uc_temp= (*(unsigned char *)configVal);
			set_generator_data_config(uc_temp);
		break;
	    /*----------------------------------------------------------------------------*/
	    /*!> READ AFTER WRITE COUNT*/
	    case CONFIG_READ_AFTER_WRITE_COUNT:
			uc_temp= (*(unsigned char *)configVal);
			set_read_after_write_config(uc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> DEVICE LOCAL STATIC IP*/
	    case CONFIG_LOCAL_STATIC_IP:
			puc_temp= ((unsigned char *)configVal);
			set_local_ip_address(puc_temp);
	    break;
		/*----------------------------------------------------------------------------*/
		/*!> DEVICE LOCAL GATEWAY IP*/
		case CONFIG_LOCAL_GATEWAY_IP:
			puc_temp= ((unsigned char *)configVal);
			set_local_gateway_ip(puc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> DEVICE LOCAL SUBNET MASK*/
		case CONFIG_LOCAL_SUBNET_MASK:
			puc_temp= ((unsigned char *)configVal);
			set_local_subnet_mask(puc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> DEVICE LOCAL DNS SERVER ADDRESS*/
		case CONFIG_LOCAL_DNS:
			puc_temp= ((unsigned char *)configVal);
			set_local_dns_address(puc_temp);
		break;
	    /*----------------------------------------------------------------------------*/
	    /*!> GSM MODULE IMEI NUMBER*/
	    case CONFIG_DEVICE_IMEI_NO:
			puc_temp= ((unsigned char *)configVal);
			set_imei_no(puc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> ACCESS POINT NAME FOR GPRS*/
	    case CONFIG_APN:
			puc_temp= ((unsigned char *)configVal);
			set_apn(puc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> NTP SERVER ADDRESS*/
	    case CONFIG_NTP_SERVER_ADDRESS:
			puc_temp= ((unsigned char *)configVal);
			set_ntp_server_address(puc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PING REQUEST INTERVAL TIME IN SECONDS*/
	    case CONFIG_PING_REQUEST_INTERVAL:
			us_temp= (*(unsigned short *)configVal);
			set_ping_req_interval_in_sec(us_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> BUSINESS LOGIC INTERVAL TIME IN SECONDS*/
	    case CONFIG_BUSINESS_LOGIC_INTERVAL:
			us_temp= (*(unsigned short *)configVal);
			set_business_logic_interval_in_sec(us_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> GPS LOCATION INTERVAL IN SECONDS*/
	    case CONFIG_GPS_LOCATION_INTERVAL:
			us_temp= (*(unsigned short *)configVal);
			set_gps_location_interval(us_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 0 TYPE*/
	    case CONFIG_PORT_0_TYPE:
			uc_temp= (*(unsigned char *)configVal);
			set_port_type(PORT_0,uc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 0 DELIMITER*/
	    case CONFIG_PORT_0_DELIMITER:
			uc_temp= (*(unsigned char *)configVal);
			set_port_delimiter(PORT_0,uc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 0 CONFIGURATION PARAMETERS*/
	    case CONFIG_PORT_0_PARAMETER:
			puc_temp= ((unsigned char *)configVal);
			set_port_parameter(PORT_0, puc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 0 COMMAND*/
	    case CONFIG_PORT_0_COMMAND:
			puc_temp= ((unsigned char *)configVal);
			set_modbus_command(PORT_0,puc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 1 TYPE*/
	    case CONFIG_PORT_1_TYPE:
			uc_temp= (*(unsigned char *)configVal);
			set_port_type(PORT_1,uc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 1 DELIMITER*/
	    case CONFIG_PORT_1_DELIMITER:
			uc_temp= (*(unsigned char *)configVal);
			set_port_delimiter(PORT_1,uc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 1 CONFIGURATION PARAMETER*/
	    case CONFIG_PORT_1_PARAMETER:
			puc_temp= ((unsigned char *)configVal);
			set_port_parameter(PORT_1, puc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 1 COMMAND*/
	    case CONFIG_PORT_1_COMMAND:
			puc_temp= ((unsigned char *)configVal);
			set_modbus_command(PORT_1, puc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 2 TYPE*/
	    case CONFIG_PORT_2_TYPE:
			uc_temp= (*(unsigned char *)configVal);
			set_port_type(PORT_2, uc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 2 DELIMITER*/
	    case CONFIG_PORT_2_DELIMITER:
			uc_temp= (*(unsigned char *)configVal);
			set_port_delimiter(PORT_2, uc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 2 CONFIGURATION PARAMETER*/
	    case CONFIG_PORT_2_PARAMETER:
			puc_temp= ((unsigned char *)configVal);
			set_port_parameter(PORT_2, puc_temp);
	    break;
	    /*----------------------------------------------------------------------------*/
	    /*!> PORT 2 COMMAND*/
	    case CONFIG_PORT_2_COMMAND:
			puc_temp= ((unsigned char *)configVal);
			set_modbus_command(PORT_1,puc_temp);
	    break;
		/*----------------------------------------------------------------------------*/
		/*!> Device Password*/
		case CONFIG_DEVICE_PASSWORD:
			puc_temp= ((unsigned char *)configVal);
			set_device_password(puc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> InstaMsg Server Address*/
		case CONFIG_SERVER_ADDRESS:
			puc_temp= ((unsigned char *)configVal);
			set_instaMsg_server_address(puc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> InstaMsg Server Port Number*/
		case CONFIG_SERVER_PORT_NO:
			us_temp= (*(unsigned short *)configVal);
			set_server_port_number(us_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Firmware Name*/
		case CONFIG_FIRMWARE_NAME:
			puc_temp= ((unsigned char *)configVal);
			set_firmware_name(puc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server Address*/
		case CONFIG_FOTA_SERVER_ADDRESS:
			puc_temp= ((unsigned char *)configVal);
			set_fota_host_address(puc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server IP*/
		case CONFIG_FOTA_SERVER_IP_ADDRESS:
		puc_temp= ((unsigned char *)configVal);
		set_fota_host_ip_address(puc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server Hostname*/
		case CONFIG_FOTA_SERVER_HOST_NAME:
		puc_temp= ((unsigned char *)configVal);
		set_fota_host_name(puc_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server Port Number*/
		case CONFIG_FOTA_SERVER_PORT_NO:
			us_temp= (*(unsigned short *)configVal);
			set_fota_host_port_number(us_temp);
		break;
		/*----------------------------------------------------------------------------*/
		/*!> Instruction to Format Flash Memory*/
		case CONFIG_FORMAT_MEMORY:
			updateSystemConfigurations(1);
		break;
    }
	
	// Write API to save data in secondary memory
	save_config_parameters();
}

/*
 ****************************************************************************************
~#  Function Name : setConfigVal                                                        #~ 
~#  Parameter1    : cmdChar: Configuration Parameter Number                             #~
~#  Parameter2    : buff: Parameter Value to set                                        #~
~#  Parameter3    : buffCounter: Configuration Parameter Length                         #~
~#  Return Type   : True in case successful, False otherwise                           #~
~#  Scope         : Global                                                              #~
~#  Functionality : This function store the values supplied as buff and length of val   #~
~#                  buffCounter at location supplied as cmdChar.                        #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #21/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/

unsigned char setConfigVal(unsigned char cmdChar,unsigned char *buff,unsigned char buffCounter){
    unsigned char locBuff[32];
    unsigned char retVal=FALSE;
    unsigned int number=0;
    memset(locBuff,0x00,32);
    extractData(buff,buffCounter,locBuff,DR,HS);
    switch(cmdChar){
        /*----------------------------------------------------------------------------*/
        /*!> GREETING MESSAGE TO ENTER IN CONFIG MODE */
        case CONFIG_GREETING:     
			retVal = true;
        break;
        /*----------------------------------------------------------------------------*/
        /*!> TOTAL NUMBER OF PORTS TO CONFIGURE*/
        case CONFIG_PORT_NUM:     
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=4)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_PORT_NUM,&number);
					retVal=true;
				}
			}
			
        break;
        /*----------------------------------------------------------------------------*/
        /*!> ENABLE OR DISABLE MESSAGE ACKNOWLEDGMENT*/
        case CONFIG_ENABLE_ACK:    
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=2)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_ENABLE_ACK,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> ENABLE/DISABLE DEBUG LOGGING*/
        case CONFIG_DEBUG_LOGGING:
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=2)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_DEBUG_LOGGING,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> ENABLE/DISABLE SSL*/
        case CONFIG_ENABLE_SSL:
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=2)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_ENABLE_SSL,&number);
					retVal=true;
				}
			}
        break;
		
		
		/*----------------------------------------------------------------------------*/
		/*!> CONFIGURE GSM MODULE TYPE*/
		case CONFIG_GSM_MODULE_TYPE:
			number= *(locBuff+1)-0x30;
				
			if((number<3) || (number>4))
			{
				retVal=false;
			}
			else
			{
				setConfig(CONFIG_GSM_MODULE_TYPE,&number);
				retVal=true;
			}
		break;
		/*----------------------------------------------------------------------------*/
		/*!> CONFIGURE GSM MODULE TYPE*/
		case CONFIG_GSM_SIM_SLOT:
			number= *(locBuff+1)-0x30;
		
			if(number>1)
			{
				retVal=false;
			}
			else
			{
				setConfig(CONFIG_GSM_SIM_SLOT,&number);
				retVal=true;
			}
			break;
		
		/*----------------------------------------------------------------------------*/
		/*!> ENABLE/DISABLE PERSIST RUN HOUR*/
		case CONFIG_PERSIST_RUN_HOUR:
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=2)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_PERSIST_RUN_HOUR,&number);
					retVal=true;
				}
			}
		break;
		/*----------------------------------------------------------------------------*/
		/*!> ENABLE/DISABLE GENERATOR DATA TRANSMISSION*/
		case CONFIG_GENERTOR_DATA_TRANSMIT:
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=2)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_GENERTOR_DATA_TRANSMIT,&number);
					retVal=true;
				}
			}
			break;
        /*----------------------------------------------------------------------------*/
        /*!> READ AFTER WRITE COUNT*/
        case CONFIG_READ_AFTER_WRITE_COUNT:
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=6)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_ENABLE_SSL,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> DEVICE LOCAL STATIC IP*/
        case CONFIG_LOCAL_STATIC_IP:
			memset(buff,0x00,32);
			if(isValidIP(locBuff,buff)==0)
			{
				if(*buff>15)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_LOCAL_STATIC_IP,locBuff+1);
					retVal=true;
				}
			}
        break;
		
		/*----------------------------------------------------------------------------*/
		/*!> DEVICE LOCAL GATEWAY IP*/
		case CONFIG_LOCAL_GATEWAY_IP:
			memset(buff,0x00,32);
			if(isValidIP(locBuff,buff)==0)
			{
				if(*buff>15)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_LOCAL_GATEWAY_IP,locBuff+1);
					retVal=true;
				}
			}
		break;
		/*----------------------------------------------------------------------------*/
		/*!> DEVICE LOCAL SUBNET MASK ADDRESS*/
		case CONFIG_LOCAL_SUBNET_MASK:
			memset(buff,0x00,32);
			if(isValidSubnetMask(locBuff,buff)==0)
			{
				if(*buff>15)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_LOCAL_SUBNET_MASK,locBuff+1);
					retVal=true;
				}
			}
		break;
		/*----------------------------------------------------------------------------*/
		/*!> DEVICE LOCAL DNS SERVER ADDRESS*/
		case CONFIG_LOCAL_DNS:
			memset(buff,0x00,32);
			if(isValidIP(locBuff,buff)==0)
			{
				if(*buff>15)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_LOCAL_DNS,locBuff+1);
					retVal=true;
				}
			}
		break;
        /*----------------------------------------------------------------------------*/
        /*!> GSM MODULE IMEI NUMBER*/
        case CONFIG_DEVICE_IMEI_NO:
			if(*locBuff<14)
			{
				retVal=false;
			}
			else
			{
				setConfig(CONFIG_DEVICE_IMEI_NO,locBuff+1);
				retVal=true;
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> ACCESS POINT NAME FOR GPRS*/
        case CONFIG_APN:
			setConfig(CONFIG_APN,locBuff+1);
			retVal=true;
        break;
        /*----------------------------------------------------------------------------*/
        /*!> NTP SERVER ADDRESS*/
        case CONFIG_NTP_SERVER_ADDRESS:
			if(check_domain_name(locBuff+1)==0)
			{
				setConfig(CONFIG_NTP_SERVER_ADDRESS,(locBuff+1));
				retVal=true;
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PING REQUEST INTERVAL TIME IN SECONDS*/
        case CONFIG_PING_REQUEST_INTERVAL:
			if(isNum(locBuff+1)==0)
			{
				sscanf((char *)(locBuff+1),"%d",&number);
				if((number>3600)||(number<20))
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_PING_REQUEST_INTERVAL,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> BUSINESS LOGIC INTERVAL TIME IN SECONDS*/
        case CONFIG_BUSINESS_LOGIC_INTERVAL:
			if(isNum(locBuff+1)==0)
			{
				sscanf((char *)(locBuff+1),"%d",&number);
				if((number>3600)||(number<20))
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_BUSINESS_LOGIC_INTERVAL,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> GPS LOCATION INTERVAL IN SECONDS*/
        case CONFIG_GPS_LOCATION_INTERVAL:
			if(isNum(locBuff+1)==0)
			{
				sscanf((char *)(locBuff+1),"%d",&number);
				if((number>3600)||(number<20))
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_GPS_LOCATION_INTERVAL,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 0 TYPE*/
        case CONFIG_PORT_0_TYPE:
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=3)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_PORT_0_TYPE,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 0 DELIMITER*/
        case CONFIG_PORT_0_DELIMITER:
			if(isNum(locBuff+1)==0)
			{
				sscanf((char *)(locBuff+1),"%d",&number);
				if(number>255)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_PORT_0_DELIMITER,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 0 CONFIGURATION PARAMETERS*/
        case CONFIG_PORT_0_PARAMETER:
			setConfig(CONFIG_PORT_0_PARAMETER,locBuff+1);
			retVal=true;
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 0 COMMAND*/
        case CONFIG_PORT_0_COMMAND:
			setConfig(CONFIG_PORT_0_PARAMETER,locBuff+1);
			retVal=true;
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 1 TYPE*/
        case CONFIG_PORT_1_TYPE:
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=3)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_PORT_1_TYPE,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 1 DELIMITER*/
        case CONFIG_PORT_1_DELIMITER:
			if(isNum(locBuff+1)==0)
			{
				sscanf((char *)(locBuff+1),"%d",&number);
				if(number>255)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_PORT_1_DELIMITER,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 1 CONFIGURATION PARAMETER*/
        case CONFIG_PORT_1_PARAMETER:
			setConfig(CONFIG_PORT_0_PARAMETER,locBuff+1);
			retVal=true;
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 1 COMMAND*/
        case CONFIG_PORT_1_COMMAND:
			setConfig(CONFIG_PORT_0_PARAMETER,locBuff+1);
			retVal=true;
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 2 TYPE*/
        case CONFIG_PORT_2_TYPE:
			if(isNum(locBuff+1)==0)
			{
				number= *(locBuff+1)-0x30;
				
				if(number>=3)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_PORT_2_TYPE,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 2 DELIMITER*/
        case CONFIG_PORT_2_DELIMITER:
			if(isNum(locBuff+1)==0)
			{
				sscanf((char *)(locBuff+1),"%d",&number);
				if(number>255)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_PORT_2_DELIMITER,&number);
					retVal=true;
				}
			}
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 2 CONFIGURATION PARAMETER*/
        case CONFIG_PORT_2_PARAMETER:
			setConfig(CONFIG_PORT_0_PARAMETER,locBuff+1);
			retVal=true;
        break;
        /*----------------------------------------------------------------------------*/
        /*!> PORT 2 COMMAND*/
        case CONFIG_PORT_2_COMMAND:
			setConfig(CONFIG_PORT_0_PARAMETER,locBuff+1);
			retVal=true;
        break;
		/*----------------------------------------------------------------------------*/
		/*!> Device Password*/
		case CONFIG_DEVICE_PASSWORD:
			setConfig(CONFIG_DEVICE_PASSWORD,locBuff+1);
			retVal=true;
		break;
		/*----------------------------------------------------------------------------*/
		/*!> InstaMsg Server Address*/
		case CONFIG_SERVER_ADDRESS:
			if(check_domain_name(locBuff+1)==0)
			{
				setConfig(CONFIG_SERVER_ADDRESS,(locBuff+1));
				retVal=true;
			}
		break;
		/*----------------------------------------------------------------------------*/
		/*!> InstaMsg Port Number*/
		case CONFIG_SERVER_PORT_NO:
			if(isNum(locBuff+1)==0)
			{
				sscanf((char *)(locBuff+1),"%d",&number);
				if(number>65567)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_SERVER_PORT_NO,&number);
					retVal=true;
				}
			}
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server Address*/
		case CONFIG_FOTA_SERVER_ADDRESS:
			if(check_domain_name(locBuff+1)==0)
			{
				setConfig(CONFIG_FOTA_SERVER_ADDRESS,(locBuff+1));
				retVal=true;
			}
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server Port Number*/
		case CONFIG_FOTA_SERVER_PORT_NO:
			if(isNum(locBuff+1)==0)
			{
				sscanf((char *)(locBuff+1),"%d",&number);
				if(number>65567)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_FOTA_SERVER_PORT_NO,&number);
					retVal=true;
				}
			}
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server Hostname*/
		case CONFIG_FOTA_SERVER_HOST_NAME:
			setConfig(CONFIG_FOTA_SERVER_ADDRESS,(locBuff+1));
			retVal=true;
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Server IP Address*/
		case CONFIG_FOTA_SERVER_IP_ADDRESS:
			memset(buff,0x00,32);
			if(isValidIP(locBuff,buff)==0)
			{
				if(*buff>15)
				{
					retVal=false;
				}
				else
				{
					setConfig(CONFIG_FOTA_SERVER_IP_ADDRESS,(locBuff+1));
					retVal=true;
				}
			}
		break;
		/*----------------------------------------------------------------------------*/
		/*!> FoTA Firmware Name*/
		case CONFIG_FIRMWARE_NAME:
			setConfig(CONFIG_FIRMWARE_NAME,locBuff+1);
			retVal=true;
		break;
		/*----------------------------------------------------------------------------*/
		/*!> Instruction to Format Flash Memory*/
		case CONFIG_FORMAT_MEMORY:
			if(('y'==*(locBuff+1))||('Y'==*(locBuff+1)))
			{
				setConfig(CONFIG_FORMAT_MEMORY,locBuff+1);
				retVal=true;
			}
			else
			{
				retVal = false;
			}
			
		break;
    }
	
    return retVal;
}

/*
 ****************************************************************************************
~#  Function Name : sysConfiguration                                                    #~ 
~#  Parameter2    : None                                                                #~
~#  Return Type   : None                                                                #~
~#  Scope         : Global                                                              #~
~#  Functionality : This function will Display the Configuration Caption and Trap the   #~
~#                  Character received from Serial Port and Write the Same in FLASH     #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #21/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/
void sysConfiguration( void ) {
    unsigned char cmdChar=0,getVal=0;
    unsigned char flagSend=TRUE,retVal=FALSE;
    unsigned char tempBuff[32];
	
    while(1)
	{
		if(gSysConsoleState == SYS_CONSOLE_STATE_CONFIG)
		{
            if(flagSend==TRUE)
			{
                flagSend=FALSE;
				/*!>Print the Configuration Parameter and current Value*/
                printInstruction(cmdChar);								
            }
			else if(flagSend==FALSE)
			{
                getVal=kbHit(tempBuff);
				/*!>In case Enter Key Pressed*/
                if(getVal==0xAA)
				{		
					/*!>Skip to next configuration parameter*/														
                    cmdChar++;							
                    flagSend=TRUE;
                }
				/*!>In case ESC Key pressed*/
				else if(getVal==0x55)													 
				{                                         
					/*!>Cancel the Configuration Process*/
                    sg_sprintf(LOG_GLOBAL_BUFFER,"\r\n\r\nConfiguration Process Canceled!!!"); 
					info_log(LOG_GLOBAL_BUFFER);
					/*!>RESET The Device*/
					resetDevice();												
                }
				/*In case of other than NEXT/CANCEL Request*/
                else if(getVal>=3)
				{																		
					/*!>Verify and store the config parameter in secondary memory*/  
                    retVal=setConfigVal(cmdChar,tempBuff,getVal);						
                    if(retVal==TRUE)
					{			
						/*!> If successfully stored in memory then move to next parameter*/	
                        cmdChar++;														
                        flagSend=TRUE;
                    }
					else															
					{          
						/*!>If Invalid Parameter Value; Repeat the same parameter.*/                                            
                        sg_sprintf(LOG_GLOBAL_BUFFER,"\r\nInvalid Parameter Supplied!!!\r\nPlease Try Again...");
                        info_log(LOG_GLOBAL_BUFFER);
                        flagSend=true;
                    }
                }
            }
        } else if (gSysConsoleState == SYS_CONSOLE_STATE_CONFIG_TIMEOUT) {
			break;
		}
    }/*End of While*/
}/*End of Function: sysConfiguration*/

void initConsole(void)
{	
	gSysConsoleState = SYS_CONSOLE_STATE_BYPASS;
	gIsConsoleTimerSet = 1;
	sg_sprintf(LOG_GLOBAL_BUFFER,"-----------------------FOR CONFIG MODE : Press u...-------------------");
	info_log(LOG_GLOBAL_BUFFER);
	ll_start_timer();
}