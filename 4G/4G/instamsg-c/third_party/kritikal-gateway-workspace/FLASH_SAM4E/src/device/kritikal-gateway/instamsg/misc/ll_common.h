/*
 * ll_common.h
 *
 * Created: 12/2/2020 9:58:39 AM
 *  Author: Kartik.Gupta
 */ 


#ifndef LL_COMMON_H_
#define LL_COMMON_H_

#if 1
#ifdef __cplusplus
extern "C" {
#endif
#ifndef TRUE
#define	TRUE				0x01
#endif

#ifndef FALSE
#define	FALSE				0x00
#endif

#define LF                  0x0A        /*!> Line Feed			*/
#define CR                  0x0D        /*!> Carriage Return	*/
#define EC                  0x1B        /*!> Escape				*/
#define DQ                  0x22        /*!> Double Quote "		*/
#define HS                  0x23        /*!> Hash #				*/
#define DR                  0x24        /*!> Dollar $			*/
#define CM                  0x2C        /*!> Comma ,			*/
#define CN                  0x3A        /*!> Colon :			*/
#define EQ                  0x3D        /*!> EQUAL To =			*/
#define RT                  0x40        /*!> At the Rate @		*/
#define US                  0x5F        /*!> UnderScor _		*/

#define CRLF                "\r\n"      /*!> CR and LF			*/
#define MAX_CPAS_OFF        60          // In case of CPAS Failed, Timeout to Switch on GSM Module

#define     increament(VAR,ITERATION,MAXVAL)            VAR=(VAR<=MAXVAL)?VAR+ITERATION:0;
#define     decrement(VAR,ITERATION,MINVAL)             VAR=(VAR>MINVAL)?VAR-ITERATION:0;
#define     decrementCond(VAR,ITERATION,MINVAL)         if(VAR>MINVAL){         \
															VAR=VAR-ITERATION;  \
														}else if(VAR==0){       \
															VAR=0;              \
														}else{                  \
															VAR=1;              \
														}


#define CMD_IDLE_STATE      0x2F        // GSM Module in IDLE Mode
#define CMD_BASE            0x00        // Base Command For GSM Module
#define REP_CMD             0x40        // Repeating Command Mode Like CSQ/CREG/CGRE with TCP Send/TCP Receive
#define POLL_CMD            0x50        // Repeating Command Mode Like CSQ/CREG/CGRE without TCP Commands in every 30 minutes
#define CMD_RD_SOC          0x60        // Read If there is any Unread Data 0n TCP Socket
#define CMD_COPS            0x70        // Manual Registration of Network.
#define CMD_SW_RST          0x80        // Soft Reset of GSM Module.

#define CONFIG_TIMEOUT		0x03

#define SYSINFO             0x00        // Displaying Current Configuration Value
#define SYSCONFIG           0x01        // In Device Configuration Mode
#define GSMINIT             0x02        // Displaying GSM Initialization
#define SYSRUN              0x04        // Device in Running Mode

typedef struct{
	unsigned char 	cpasFailed:1;
	unsigned char 	SMSReset:1;
	unsigned char 	CopsInProgress:2;
	unsigned char 	togglePacket:2;
	unsigned char 	FlasWrite:1;
	unsigned char 	exempCmd:1;
	unsigned char 	gmsBytes:1;
	unsigned char 	miscBytes:1;
	unsigned char 	gsmSend:1;
	unsigned char 	gsmInfo:1;
}STATUS_FLAG_T;

extern	STATUS_FLAG_T			Flag;
extern  volatile unsigned char  sysState;
extern  unsigned char			timeOut;
extern  unsigned short			cpasTOCounter;
extern	unsigned char			gsmBuff[1024];
extern  unsigned short			gsmBuffCntr;

extern volatile unsigned char	gsmTOCounter;
extern volatile unsigned char	smsTOCounter;
extern volatile unsigned char	pollCounter;
extern volatile unsigned char	polling;
extern volatile unsigned char	socReadCounter;
extern volatile unsigned char	csqCounter;
extern volatile unsigned char	sendFlag;
extern volatile unsigned char	cmdChar;

void			setGSMCommand   ( unsigned char );
unsigned char   getGSMCommand   ( void );
unsigned char   subString       ( unsigned char *, unsigned short, unsigned char*, unsigned char);
void			extractData     ( unsigned char *,unsigned char ,unsigned char *,unsigned char ,unsigned char );
unsigned char   isValidIP       ( unsigned char *,unsigned char * );
unsigned char   isValidSubnetMask( unsigned char *,unsigned char * );
unsigned char   CheckDNSArr     ( unsigned char * );
unsigned char   isMobNo         ( unsigned char *,unsigned char * );
unsigned char   isNum           ( unsigned char *Arry );
void			updGsmCMD       ( unsigned char , unsigned char * );
void			managedDelay    ( unsigned short );

/**
 * @}
 */
#ifdef __cplusplus
} // extern "C"
#endif

#endif

#endif /* LL_COMMON_H_ */