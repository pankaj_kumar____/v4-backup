/*
 * fwUpgradeStateMachine.c
 *
 * Created: 4/10/2020 5:05:22 PM
 *  Author: Kartik.Gupta
 */ 

#include "fwUpgradeStateMachine.h"
#include "fwUpgradeSmOperations.h"
#include "common.h"

//*****************************************************************************************//
/* < For debugging purpose*/
/*State Machine Names*/
const char* gStates[] = {
	[STATE_NA] = "NA",
	[STATE_AVAILABILITY_REQUEST] = "AVAILABILITY_REQUEST",
	[STATE_UPGRADE_VERIFICATION] = "UPGRADE_VERIFICATION",
	[STATE_DOWNLOAD_FIRMWARE] = "DOWNLOAD_FIRMWARE",
	[STATE_VALIDATE_FIRMWARE] = "VALIDATE_FIRMWARE",
	[STATE_UPDATE_FIRMWARE_CONFIG] = "UPDATE_FIRMWARE_CONFIG",
	[STATE_RESET_DEVICE] = "RESET_DEVICE",
	[STATE_EXIT] = "EXIT",
	[STATE_MAX] = "MAX"
};

/*Event Names*/
const char* gEvents[] = {
	[EVENT_NA] = "NA",
	[EVENT_SM_INIT] = "SM_INIT",
	[EVENT_SUCCESS] = "SUCCESS",
	[EVENT_FAILURE] = "FAILURE",
	[EVENT_UPGRADE_REQ] = "UPGRADE_REQ",
	[EVENT_UPGRADE_NOT_REQ] = "UPGRADE_NOT_REQ",
	[EVENT_MAX] = "Max"
};
//*************************************************************************************************//

/*Operation to be performed during state transition*/
typedef FwUpgradeEvents_e (*Operation)(void);

/*Structure for State Machine Configuration Data*/
typedef struct {
	FwUpgradeStates_e nextState;   /*Next State*/
	Operation operation;	/*Function to be executed before transitioning to next state*/
} StateMachine_t;

/*To maintain the current state*/
static FwUpgradeStates_e gCurrentState;

/*State Table for State Machine*/
const StateMachine_t gSm[STATE_MAX][EVENT_MAX]= {
	/*States                    	Events*/					/*NextState,                	Operation*/
	[STATE_AVAILABILITY_REQUEST]	[EVENT_SM_INIT]			= 	{STATE_AVAILABILITY_REQUEST,	fwAvailableRequest},
	[STATE_AVAILABILITY_REQUEST]	[EVENT_SUCCESS]			= 	{STATE_UPGRADE_VERIFICATION,	fwUpgradeVerification},
	[STATE_AVAILABILITY_REQUEST]	[EVENT_FAILURE]			= 	{STATE_EXIT,					fwUpgradeExit},
	[STATE_UPGRADE_VERIFICATION]	[EVENT_UPGRADE_REQ]		=	{STATE_DOWNLOAD_FIRMWARE,		fwDownload},
	[STATE_UPGRADE_VERIFICATION]	[EVENT_UPGRADE_NOT_REQ]	=	{STATE_EXIT,					fwUpgradeExit},
	[STATE_DOWNLOAD_FIRMWARE]		[EVENT_SUCCESS]			=	{STATE_VALIDATE_FIRMWARE,		fwValidate},
	[STATE_DOWNLOAD_FIRMWARE]		[EVENT_FAILURE]			=	{STATE_EXIT,					fwUpgradeExit},
	[STATE_VALIDATE_FIRMWARE]		[EVENT_SUCCESS]			=	{STATE_UPDATE_FIRMWARE_CONFIG,	fwUpdateConfig},
	[STATE_VALIDATE_FIRMWARE]		[EVENT_FAILURE]			=	{STATE_EXIT,					fwUpgradeExit},
	[STATE_UPDATE_FIRMWARE_CONFIG]	[EVENT_SUCCESS]			=	{STATE_RESET_DEVICE,			fwResetDevice},
	[STATE_UPDATE_FIRMWARE_CONFIG]	[EVENT_FAILURE]			=	{STATE_EXIT,					fwUpgradeExit},		
};


/* execute state machine*/
void executeStateMachine(FwUpgradeEvents_e evt)
{
	while (evt != EVENT_NA) {
		if (gSm[gCurrentState][evt].nextState > STATE_NA &&\
		gSm[gCurrentState][evt].nextState < STATE_MAX) {
			int currState = gCurrentState; /*temporarily hold current state*/
			int currEvent = evt; /*temporarily hold current evt*/
			gCurrentState = gSm[gCurrentState][evt].nextState;
			if (gSm[currState][evt].operation != NULL) {
				evt = gSm[currState][currEvent].operation();
			} else {
				evt = EVENT_NA;
			}
			sg_sprintf(LOG_GLOBAL_BUFFER, "[%s]-[%s]->[%s]==[%s]==>\r\n", gStates[currState], gEvents[currEvent], gStates[gCurrentState], gEvents[evt]);
			info_log(LOG_GLOBAL_BUFFER);
		} else {
			break;
		}
	}
}

/*initialize and execute the state machine*/
void executeFwUpgradeStateMachine(char *serverHostName, char *serverIpAddress, int port)
{
	int ret = initFwUpgradeSmOperations(serverHostName, serverIpAddress, port);
	if(OPER_STATE_SUCCESS != ret) {
		return;
	}
	gCurrentState = STATE_AVAILABILITY_REQUEST;
	executeStateMachine(EVENT_SM_INIT);
}