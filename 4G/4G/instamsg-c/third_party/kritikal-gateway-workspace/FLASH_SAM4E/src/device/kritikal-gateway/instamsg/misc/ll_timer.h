#if 0
/*
 * ll_timer.h
 *
 * Created: 9/12/2020
 *  Author: Kartik.Gupta
 */ 


#ifndef USERTIMER_H_
#define USERTIMER_H_

/* timer handler callback*/
typedef void (*Cb)(void);

/* @brief initialize the timer module
 * @param timer handler callback to be called when timer expired
 */
void ll_timerInit(Cb timerHandler);

/* @brief start the single shot timer
 * @param timeInSecond	value in second for timer to get run
 * @return 0 for success, 1 for failure
 */
int ll_timerStart(int timeInSecond);

/* @brief stop any timer already running
 */
void ll_timerStop(void);

/* @brief check if timer is running
 * @return 1 if running, 0 if not
 */
int ll_isTimerRunning(void);

/* 
 *	@brief get the remaining timer counter value of current running timer
 *	@return the counter value, -1 if no timer running
 */
int ll_getRemainingCountValue(void);

#endif /* USERTIMER_H_ */
#endif

#ifndef __LL_TIMER_H__
#define __LL_TIMER_H__

#ifdef __cplusplus
extern "C" {
#endif
/*==============================================================================
                                INCLUDE FILES
==============================================================================*/

/*==============================================================================
                      LOCAL DEFINITIONS AND TYPES : MACROS
==============================================================================*/
/*----------------------------------------------------------------------------*/
#define LL_TIMER_OFF	0x00
#define LL_TIMER_ON		0x01
/*----------------------------------------------------------------------------*/
typedef void (*LL_CALLBACK)(void);

/* @brief initialize the timer module
 * @param timer handler callback to be called when timer expired
 */
void ll_init_timer(LL_CALLBACK ll_api_handler);
/*----------------------------------------------------------------------------*/
/* @brief start the single shot timer
 * @param timeInSecond	value in second for timer to get run
 * @return 0 for success, 1 for failure
 */
int ll_start_timer( void );
/*----------------------------------------------------------------------------*/
/* @brief stop any timer already running
 */
void ll_stop_timer(void);
/*----------------------------------------------------------------------------*/
/* @brief check if timer is running
 * @return 1 if running, 0 if not
 */
int get_heartbeat_count_in_second(void);

/**
 * @}
 */

#ifdef __cplusplus
}//extern "C"
#endif

#endif //__LL_TIMER_H__