/*
 * ll_console.h
 *
 * Created: 12/2/2020 9:29:36 AM
 *  Author: Kartik.Gupta
 */ 


#ifndef LL_CONSOLE_H_
#define LL_CONSOLE_H_

#ifdef __cplusplus
extern "C" {
#endif

#define USER_CONSOLE_TIMEOUT				3	/* in seconds*/
#define SYS_CONSOLE_STATE_BYPASS			0	
#define SYS_CONSOLE_STATE_CONFIG			1
#define SYS_CONSOLE_STATE_CONFIG_TIMEOUT	2
		
void			initConsole			( void );
unsigned char   getTimeout          ( void );
void			sysConfiguration    ( void );
void			setConfigDefault    ( void );
void			printConfigValue    ( void );
void			setTimeout          ( unsigned char Val );
void			setConfig           ( unsigned char configParam, void *configVal );

extern char gIsConsoleTimerSet;
extern char gSysConsoleState;

/**
 * @}
 */
#ifdef __cplusplus
} // extern "C"
#endif


#endif /* LL_CONSOLE_H_ */