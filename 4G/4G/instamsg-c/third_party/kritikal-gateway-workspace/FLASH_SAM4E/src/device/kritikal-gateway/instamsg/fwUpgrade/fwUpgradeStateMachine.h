/*
 * fwUpgradeStateMachine.h
 *
 * Created: 4/10/2020 5:04:59 PM
 *  Author: Kartik.Gupta
 */ 


#ifndef FWUPGRADESTATEMACHINE_H_
#define FWUPGRADESTATEMACHINE_H_

/*Enum for various state machine states*/
typedef enum {
    STATE_NA = 0,	/*Not Used*/
	STATE_AVAILABILITY_REQUEST,	/* to send initial request to fetch firmware file info*/
	STATE_UPGRADE_VERIFICATION,	/* firmware upgrade required or not*/
	STATE_DOWNLOAD_FIRMWARE,	/* to download & save firmware file in chunks from server*/
	STATE_VALIDATE_FIRMWARE,	/* verify the integrity of firmware*/
	STATE_UPDATE_FIRMWARE_CONFIG,	/* update the new firmware file information*/
	STATE_RESET_DEVICE,	/* reset the device*/
	STATE_EXIT,	/* exit tasks*/
    STATE_MAX	/*Not Used*/  
} FwUpgradeStates_e;
	
/*Enum for various state machine events*/
typedef enum {
    EVENT_NA = 0,	/*Not Used*/
	EVENT_SM_INIT,
	EVENT_SUCCESS,
	EVENT_FAILURE,
	EVENT_UPGRADE_REQ,
	EVENT_UPGRADE_NOT_REQ,
    EVENT_MAX	/*Not Used*/
} FwUpgradeEvents_e;

/* @brief to execute the state machine
 */
void executeFwUpgradeStateMachine(char *serverHostName, char *serverIpAddress, int port);


#endif /* FWUPGRADESTATEMACHINE_H_ */