/*
 * common.h
 *
 * Created: 3/24/2020 2:16:15 PM
 *  Author: Kartik.Gupta
 */ 


#ifndef COMMON_H_
#define COMMON_H_

#include "../../../../instamsg-c/common/instamsg/driver/include/sg_mem.h"
#include "../../../../instamsg-c/common/instamsg/driver/include/sg_stdlib.h"
#include "../../../../instamsg-c/common/instamsg/driver/include/data_logger.h"
#include "../../../../instamsg-c/common/instamsg/driver/include/sg_stdlib.h"
#include "../../../../instamsg-c/common/instamsg/driver/include/log.h"
#include <asf.h>
#include "stdint.h"
#include <stdio.h>
#include <string.h>
#include <stddef.h>

typedef enum {
	OPER_STATE_NA = -1,
	OPER_STATE_SUCCESS,				/* return for success, rest codes to indicate error*/
	OPER_STATE_INVALID_BUFFER,
	OPER_STATE_INVALID_CONFIG,
	OPER_STATE_INVALID_FILE_SIZE,
	OPER_STATE_INVALID_OPERATION,
	OPER_STATE_FLASH_WRITE,
	OPER_STATE_FLASH_INIT,
	OPER_STATE_READ_DEV_ID,
	OPER_STATE_DEV_UNPROTECT,
	OPER_STATE_FLASH_READ,
	OPER_STATE_FLASH_ERASE,
	OPER_STATE_XMODEM_TRANSFER,
	OPER_STATE_SOCKET_INIT,
	OPER_STATE_INVALID_BUFF_SIZE,
	OPER_STATE_INVALID_CHUNK,
	OPER_STATE_SOCKET_REQUEST_WRITE,
	OPER_STATE_SOCKET_READ,
	OPER_STATE_SOCKET_READ_TIMEOUT,	
	OPER_STATE_HTTP_REQUEST,
	OPER_STATE_MAX,
} OperationStateCode_e;



#endif /* COMMON_H_ */