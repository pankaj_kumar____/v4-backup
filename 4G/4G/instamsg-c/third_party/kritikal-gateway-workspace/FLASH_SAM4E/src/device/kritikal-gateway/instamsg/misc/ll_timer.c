#if 0
/*
 * ll_timer.c
 *
 * Created: 4/1/2020 5:12:48 AM
 *  Author: Kartik.Gupta
 */ 

#include "ll_timer.h"
#include "tc.h"
#include "pmc.h"
#include "sysclk.h"
#include "../../../common/instamsg/driver/include/log.h"

// My #define's. In the example project most of these were in conf_board.h
#define TC			TC1
#define CHANNEL		0
#define ID_TC		ID_TC3
#define TC_Handler  TC3_Handler
#define TC_IRQn     TC3_IRQn
static char gIsTimerRunning;	/* flag to update whether timer is running*/
static unsigned int gTimerCountInSecond;	/*to maintain counts of current timer in seconds*/

static Cb cb = NULL;	/* application callback when timer expired*/

// Interrupt service routine
void TC_Handler(void)
{
	uint32_t dummy;
	
	/*must read status register to clear interrupt*/
	dummy = tc_get_status(TC,CHANNEL);
	UNUSED(dummy);
	
	if(0 != gTimerCountInSecond) {
		sg_sprintf(LOG_GLOBAL_BUFFER, "countdown: %d\r\n", gTimerCountInSecond);
		info_log(LOG_GLOBAL_BUFFER);
		gTimerCountInSecond--;
	}

	if(0 == gTimerCountInSecond) {
		/* in our appln we are using single shot timer*/
		tc_stop(TC, CHANNEL);
		gIsTimerRunning = 0;
		
		/* call callback*/
		if(NULL != cb) {
			cb();
		}
	}
}

void ll_timerInit(Cb appCallback)
{
	cb = appCallback;
	
	// Configure TC interrupts
	NVIC_DisableIRQ(TC_IRQn);
	NVIC_ClearPendingIRQ(TC_IRQn);
	NVIC_SetPriority(TC_IRQn,0);
	NVIC_EnableIRQ(TC_IRQn);
}

int ll_timerStart(int timeInSecond)
{
	// INPUTS:
	//	freq_desired	The desired rate at which to call the ISR, Hz
	unsigned int freq_desired = 1;
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();
	uint32_t counts;
	
	if(gIsTimerRunning) {
		return 1;	
	}
	
	gTimerCountInSecond = timeInSecond;
	
	// Configure PMC
	pmc_enable_periph_clk(ID_TC);

	// Configure TC for a 4Hz frequency and trigger on RC compare.
	tc_find_mck_divisor(
	(uint32_t)freq_desired,	// The desired frequency as a uint32.
	ul_sysclk,				// Master clock freq in Hz.
	&ul_div,				// Pointer to register where divisor will be stored.
	&ul_tcclks,				// Pointer to reg where clock selection number is stored.
	ul_sysclk);				// Board clock freq in Hz.
	
	tc_init(TC0, CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
		
	// Find the best estimate of counts, then write it to TC register C.
	counts = (ul_sysclk/ul_div)/freq_desired;
	tc_write_rc(TC0, 0, counts);
	
	// Enable interrupts for this TC, and start the TC.
	tc_enable_interrupt(TC, CHANNEL, TC_IER_CPCS);
	gIsTimerRunning = 1;
	tc_start(TC, CHANNEL);

	return 0;
}

void ll_timerStop(void)
{
	if(1 == gIsTimerRunning) {
		tc_stop(TC, CHANNEL);
		gTimerCountInSecond = 0;	
		gIsTimerRunning = 0;
	} 	
}

int ll_isTimerRunning(void)
{
	return gIsTimerRunning;
}

int ll_getRemainingCountValue(void)
{
	if(0 == gIsTimerRunning) {
		return -1;
	} else {
		return gTimerCountInSecond;
	}
}

#endif

/*==============================================================================
                                INCLUDE FILES
==============================================================================*/
#include "asf.h"
#include "ll_timer.h"
#include "tc.h"
#include "compiler.h"
#include "board.h"
#include "conf_board.h"
#include "sam4e8c.h"
/*******************************************************************************
 * Private macros/types/enumerations/variables
 ******************************************************************************/
#define LL_TC			TC1
#define LL_CHANNEL		0
#define LL_ID_TC		ID_TC3
#define LL_TC_Handler	TC3_Handler
#define LL_TC_IRQn		TC3_IRQn

/*!> Timer State, wheather it is running or not*/
static char timer_state = 0;	
/*!> MCU Running Count in Seconds */
static unsigned int ll_heartbeat_sec;	

static LL_CALLBACK ll_cb = NULL;	/* application callback when timer expired*/

/*----------------------------------------------------------------------------*/
/**
 * \brief	TC_Handler
 * \      : Timer_0 Interrupt Handler 
 * \param   NA
 * \return	NA
 * \info date       : 09-OCT-2020
 * \info author     : Ashutosh
 */
void TC3_Handler(void)
{
	uint32_t dummy;
	
	/*must read status register to clear interrupt*/
	dummy = tc_get_status(LL_TC,LL_CHANNEL);
	UNUSED(dummy);
	
	ll_heartbeat_sec++;
	ll_cb();
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	ll_init_timer
 * \      : Timer_1 Initialization Function 
 * \param   appCallback: Function Pointer of CALLBACK
 * \return	NA
 * \info date       : 09-OCT-2020
 * \info author     : Ashutosh
 */
void ll_init_timer(LL_CALLBACK apiCallBack)
{
	ll_cb = apiCallBack;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	ll_start_timer
 * \      : This function will start the TIMER 
 * \param   appCallback: Function Pointer of CALLBACK
 * \return	NA
 * \info date       : 09-OCT-2020
 * \info author     : Ashutosh
 */
int ll_start_timer( void )
{
	unsigned int freq_desired = 1;
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();
	uint32_t counts;
	
	if(LL_TIMER_ON == timer_state) 
	{
		return 1;	
	}
	
	ll_heartbeat_sec = 0;
	
	/*!> Configure PMC*/
	pmc_enable_periph_clk(LL_ID_TC);

	/*!> Configure TC for a 4Hz frequency and trigger on RC compare.*/
	tc_find_mck_divisor(
						(uint32_t)freq_desired,	/* The desired frequency as a uint32.*/
						ul_sysclk,				/* Master clock freq in Hz.*/
						&ul_div,				/* Pointer to register where divisor will be stored.*/
						&ul_tcclks,				/* Pointer to reg where clock selection number is stored.*/
						ul_sysclk);				/* Board clock freq in Hz.*/
	
	tc_init(LL_TC, LL_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
		
	/*!> Find the best estimate of counts, then write it to TC register C.*/
	counts = (ul_sysclk/ul_div)/freq_desired;
	tc_write_rc(LL_TC, 0, counts);
	
	/*!> Configure TC interrupts*/
//	NVIC_DisableIRQ(LL_TC_IRQn);
//	NVIC_ClearPendingIRQ(LL_TC_IRQn);
	NVIC_SetPriority(LL_TC_IRQn,0);
	NVIC_EnableIRQ(LL_TC_IRQn);
	/*!> Enable interrupts for this TC, and start the TC.*/
	tc_enable_interrupt(LL_TC, LL_CHANNEL, TC_IER_CPCS);
	timer_state = LL_TIMER_ON;
	tc_start(LL_TC, LL_CHANNEL);

	return 0;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	ll_stop_timer
 * \      : This function will stop the TIMER 
 * \param   NA
 * \return	NA
 * \info date       : 09-OCT-2020
 * \info author     : Ashutosh
 */
void ll_stop_timer(void)
{
	if(LL_TIMER_ON == timer_state) {
		tc_stop(LL_TC, LL_CHANNEL);
		ll_heartbeat_sec = 0;	
		timer_state = LL_TIMER_OFF;
	} 	
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_timer_state
 * \      : This function will return the state of timer, 0 for stopped, 1 for started
 * \param   NA
 * \return	NA
 * \info date       : 09-OCT-2020
 * \info author     : Ashutosh
 */
int get_timer_state(void)
{
	return timer_state;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_heartbeat_count_in_second
 * \      : This function will return running seconds of MCU
 * \param   NA
 * \return	NA
 * \info date       : 09-OCT-2020
 * \info author     : Ashutosh
 */
int get_heartbeat_count_in_second(void)
{
	if(LL_TIMER_OFF == timer_state) {
		return -1;
	} 
	else 
	{
		return ll_heartbeat_sec;
	}
}