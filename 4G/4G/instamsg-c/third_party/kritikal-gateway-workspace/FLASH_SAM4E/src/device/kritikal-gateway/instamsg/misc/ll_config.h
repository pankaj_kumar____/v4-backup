/*
 * ll_config.h
 *
 * Created: 12/2/2020 7:15:26 AM
 *  Author: Kartik.Gupta
 */ 


#ifndef LL_CONFIG_H_
#define LL_CONFIG_H_

#if 1
#ifdef __cplusplus
extern "C" {
#endif


/*==============================================================================
                                INCLUDE FILES
==============================================================================*/

/*==============================================================================
                      LOCAL DEFINITIONS AND TYPES : MACROS
==============================================================================*/
/*----------------------------------------------------------------------------*/
/*!@brief enum to define the status return type Temporary*/
#define MAX_PORT_ALLOWED	0x03
#define MAX_OFFLINE_PACKET	10000

#define MEMORY_FORMATTED	0xAA
/*==============================================================================
                     LOCAL DEFINITIONS AND TYPES : TYPEDEFS
==============================================================================*/
#define		CONFIG_GREETING							0x00
#define 	CONFIG_PORT_NUM  						CONFIG_GREETING					+ 1
#define 	CONFIG_ENABLE_ACK                    	CONFIG_PORT_NUM					+ 1
#define 	CONFIG_DEBUG_LOGGING					CONFIG_ENABLE_ACK				+ 1
#define 	CONFIG_ENABLE_SSL						CONFIG_DEBUG_LOGGING			+ 1
#define		CONFIG_GSM_MODULE_TYPE					CONFIG_ENABLE_SSL				+ 1
#define		CONFIG_GSM_SIM_SLOT						CONFIG_GSM_MODULE_TYPE			+ 1
#define		CONFIG_DEVICE_PASSWORD					CONFIG_GSM_SIM_SLOT				+ 1
#define		CONFIG_SERVER_ADDRESS					CONFIG_DEVICE_PASSWORD			+ 1
#define		CONFIG_SERVER_PORT_NO					CONFIG_SERVER_ADDRESS			+ 1
#define 	CONFIG_READ_AFTER_WRITE_COUNT			CONFIG_SERVER_PORT_NO			+ 1
#define		CONFIG_GENERTOR_DATA_TRANSMIT			CONFIG_READ_AFTER_WRITE_COUNT	+ 1
#define		CONFIG_PERSIST_RUN_HOUR					CONFIG_GENERTOR_DATA_TRANSMIT	+ 1
#define 	CONFIG_LOCAL_STATIC_IP					CONFIG_PERSIST_RUN_HOUR			+ 1
#define		CONFIG_LOCAL_GATEWAY_IP					CONFIG_LOCAL_STATIC_IP			+ 1
#define		CONFIG_LOCAL_SUBNET_MASK				CONFIG_LOCAL_GATEWAY_IP			+ 1
#define		CONFIG_LOCAL_DNS						CONFIG_LOCAL_SUBNET_MASK		+ 1
#define 	CONFIG_DEVICE_IMEI_NO					CONFIG_LOCAL_DNS				+ 1
#define 	CONFIG_APN								CONFIG_DEVICE_IMEI_NO			+ 1
#define 	CONFIG_NTP_SERVER_ADDRESS				CONFIG_APN						+ 1
#define 	CONFIG_PING_REQUEST_INTERVAL			CONFIG_NTP_SERVER_ADDRESS		+ 1
#define 	CONFIG_BUSINESS_LOGIC_INTERVAL			CONFIG_PING_REQUEST_INTERVAL	+ 1
#define 	CONFIG_GPS_LOCATION_INTERVAL			CONFIG_BUSINESS_LOGIC_INTERVAL	+ 1
#define		CONFIG_FOTA_SERVER_ADDRESS				CONFIG_GPS_LOCATION_INTERVAL	+ 1
#define		CONFIG_FOTA_SERVER_IP_ADDRESS			CONFIG_FOTA_SERVER_ADDRESS		+ 1
#define		CONFIG_FOTA_SERVER_HOST_NAME			CONFIG_FOTA_SERVER_IP_ADDRESS	+ 1
#define		CONFIG_FOTA_SERVER_PORT_NO				CONFIG_FOTA_SERVER_HOST_NAME	+ 1
#define		CONFIG_FIRMWARE_NAME					CONFIG_FOTA_SERVER_PORT_NO		+ 1
#define 	CONFIG_PORT_0_TYPE						CONFIG_FIRMWARE_NAME			+ 1
#define 	CONFIG_PORT_0_DELIMITER					CONFIG_PORT_0_TYPE				+ 1
#define 	CONFIG_PORT_0_PARAMETER					CONFIG_PORT_0_DELIMITER			+ 1
#define 	CONFIG_PORT_0_COMMAND					CONFIG_PORT_0_PARAMETER			+ 1
#define 	CONFIG_PORT_1_TYPE						CONFIG_PORT_0_COMMAND			+ 1
#define 	CONFIG_PORT_1_DELIMITER					CONFIG_PORT_1_TYPE				+ 1
#define 	CONFIG_PORT_1_PARAMETER					CONFIG_PORT_1_DELIMITER			+ 1
#define 	CONFIG_PORT_1_COMMAND					CONFIG_PORT_1_PARAMETER			+ 1
#define 	CONFIG_PORT_2_TYPE						CONFIG_PORT_1_COMMAND			+ 1
#define 	CONFIG_PORT_2_DELIMITER					CONFIG_PORT_2_TYPE				+ 1
#define 	CONFIG_PORT_2_PARAMETER					CONFIG_PORT_2_DELIMITER			+ 1
#define 	CONFIG_PORT_2_COMMAND					CONFIG_PORT_2_PARAMETER			+ 1
#define		CONFIG_FORMAT_MEMORY					CONFIG_PORT_2_COMMAND			+ 1
#define		CONFIG_LAST_COMMAND						CONFIG_FORMAT_MEMORY			+ 1

/*==============================================================================
                      LOCAL DEFINITIONS AND TYPES : ENUMS
==============================================================================*/

/*==============================================================================
                    LOCAL DEFINITIONS AND TYPES : STRUCTURES
==============================================================================*/

/*----------------------------------------------------------------------------*/
/*!@brief struct to define the MODBus port parameters
*/
typedef struct {
	unsigned char 		status;								/*!> Set if parameters present*/
	unsigned char 		port_type;							/*!> 0-RS232, 1-RS485 & 2 for TCP*/
	unsigned char 		port_delimiter;						/*!> Holds delimiter if present*/
	unsigned char 		port_parameter[24];					/*!> Port Config Parameters */
	unsigned char 		command[384];						/*!> Need to dynamically allocate to hold Modbus commands*/
}PORT_PARAMETER_S;

/*----------------------------------------------------------------------------*/
/*!@brief struct to hold config parameters*/
typedef struct{
	unsigned char 		memory_format_id;					/*!> Check if memory is virgin then format it*/
	unsigned char 		num_port;							/*!> Total number of modbus communication port*/
	unsigned char 		enable_message_ack;					/*!> Enable Message Acknowledgement configuration*/
	unsigned char 		enable_debug_logging;				/*!> Enable Debug Log configuration Parameter*/
	unsigned char 		enable_ssl;							/*!> Enable SSL Configuration*/
	unsigned char		compulsory_read_after_write_count;	/*!> MQTT: Read after writing the data*/ 
	unsigned char		enable_send_generator_data;			/*!> Enable/Disable Generator Data Packet Transmit */
	unsigned char		enable_log_persistant_run_hour;		/*!> Store Persistant Load Run Hour*/
	unsigned char		gsm_module_type;					/*!> Store GSM Module type*/
	unsigned char		gsm_sim_slot;						/*!> Store GSM SIM Slot*/
	unsigned char		device_password[32];				/*!> Device Password to get secret*/
	unsigned char		local_static_ip_add[16];			/*!> Device Local Static IP Address*/
	unsigned char		local_gateway_ip[16];				/*!> Device Local Gateway IP Address*/
	unsigned char 		local_subnet_mask[16];				/*!> Device Local Subnet Mask*/
	unsigned char		local_dns[16];						/*!> Device Local DNS Address*/
	unsigned char		device_imei_no[16];					/*!> GSM Module IMEI No*/
	unsigned char		gprs_apn[32];						/*!> APN*/
	unsigned char		ntp_server_add[32];					/*!> NTP Server Address*/
	unsigned char		data_server_add[32];				/*!> DATA Server Address*/
	unsigned char		fota_server_add[32];				/*!> FoTA Server Address*/
	unsigned char		fota_server_ip_add[16];				/*!> FOTA Server IP Address*/
	unsigned char		fota_server_hostname[32];			/*!> FoTA Server Hostname*/
	unsigned char		firmware_name[32];					/*!> Firmware Name*/
	unsigned char		device_secret_id[70];				/*!> Device Secret ID*/
	unsigned short		data_server_port_number;			/*!> Data Server Port Number*/
	unsigned short		fota_server_port_number;			/*!> FoTA Server Port Number*/
	unsigned short 		read_pointer;						/*!> Offline Data Storage Read Pointer*/
	unsigned short 		write_pointer;						/*!> Offline Data Storage Write Pointer*/
	unsigned short 		ping_request_interval;				/*!> Ping Request Interval in Second*/
	unsigned short 		business_logic_interval;			/*!> Data Polling Interval in Second*/
	unsigned short		gps_location_interval;				/*!> GPS Interval in Second*/
	PORT_PARAMETER_S 	modbus_port[MAX_PORT_ALLOWED];		/*!> MODBus Peripheral ID,ADD,Command*/
} CONFIG_S;


/*==============================================================================
                                INCLUDE FILES
==============================================================================*/

/*==============================================================================
                      LOCAL DEFINITIONS AND TYPES : MACROS
==============================================================================*/
/*----------------------------------------------------------------------------*/
/*!@MACRO for KEYWORD for JSON for MQTT */
#define JSON_PING_REQ_INTERVAL								"PING_REQ_INTERVAL"
#define JSON_BUSINESS_LOGIC_INTERVAL						"BUSINESS_LOGIC_INTERVAL"
#define JSON_ENABLE_MESSAGE_ACK								"ENABLE_MESSAGE_ACK"
#define JSON_DEBUG_LOGGING_ENABLED							"DEBUG_LOGGING_ENABLED"
#define JSON_SSL_ENABLE										"SSL_ENABLED"
#define JSON_SEND_GPS_LOCATION_INTERVAL						"SEND_GPS_LOCATION_INTERVAL"
#define JSON_COMPULSORY_SOCKET_READ_AFTER_WRITE_COUNT		"COMPULSORY_SOCKET_READ_AFTER_WRITE_COUNT"
#define JSON_NTP_SERVER										"NTP_SERVER"
#define JSON_OTA_SERVER_HOST_NAME							"OTA_SERVER_HOST_NAME"
#define JSON_OTA_SERVER_HOST_IP								"OTA_SERVER_HOST_IP"
#define JSON_OTA_SERVER_HOST_ADDRESS						"OTA_SERVER_HOST_ADDRESS"
#define JSON_OTA_SERVER_PORT_NUMBER							"OTA_SERVER_PORT_NUMBER"	
#define JSON_ETHERNET_STATIC_IP_PARAMS						"ETHERNET_STATIC_IP_PARAMS"
#define JSON_NUM_PORTS_IN_USE								"NUM_PORTS_IN_USE"
#define JSON_PORT_1_TYPE									"PORT_1_TYPE"
#define JSON_PORT_1_COMMANDS								"PORT_1_COMMANDS"
#define JSON_PORT_1_SERIAL_PARAMS							"PORT_1_SERIAL_PARAMS"
#define JSON_PORT_1_SERIAL_DELIMITER						"PORT_1_SERIAL_DELIMITER"
#define JSON_PORT_2_TYPE									"PORT_2_TYPE"
#define JSON_PORT_2_COMMANDS								"PORT_2_COMMANDS"
#define JSON_PORT_2_SERIAL_PARAMS							"PORT_2_SERIAL_PARAMS"
#define JSON_PORT_2_SERIAL_DELIMITER						"PORT_2_SERIAL_DELIMITER"
#define JSON_PORT_3_TYPE									"PORT_3_TYPE"
#define JSON_PORT_3_COMMANDS								"PORT_3_COMMANDS"
#define JSON_PORT_3_SERIAL_PARAMS							"PORT_3_SERIAL_PARAMS"
#define JSON_PORT_3_SERIAL_DELIMITER						"PORT_3_SERIAL_DELIMITER"
#define JSON_SECRET											"SECRET"
/*----------------------------------------------------------------------------*/
/*!@Application State-Machine Constant*/
#define NO_COM_CHANNEL_AVAIALABLE			0x00
#define NO_CONNECTION_WITH_SERVER			0x01
#define CONNECTED_THROUGH_ETHERNET			0x02
#define CONNECTED_THROUGH_GPRS				0x04

/*==============================================================================
                     LOCAL DEFINITIONS AND TYPES : TYPEDEFS
==============================================================================*/

/*==============================================================================
                      LOCAL DEFINITIONS AND TYPES : ENUMS
==============================================================================*/

/*==============================================================================
                    LOCAL DEFINITIONS AND TYPES : STRUCTURES
==============================================================================*/
typedef struct  
{
	unsigned char send_gsm_command:1;
	unsigned char send_config_command:1;
	unsigned char send_modbus_tcp_data:1;
	unsigned char send_modbus_rs232_data:1;
	unsigned char send_modbus_rs485_data:1;
	unsigned char send_ping_serv_req:1;
	unsigned char gps_poll_time:1;
	unsigned char get_rtc:1;
	unsigned char get_gsm_location:1;
	unsigned char connected_to_server:1;
	unsigned char con_via_ethernet:1;
	unsigned char con_via_gsm:1;
}LL_APP_STATUS_S;
/*==============================================================================
                              EXTERNAL DECLARATIONS
=============================================================================*/
extern LL_APP_STATUS_S ll_app_status;
/*==============================================================================
                           EXTERNAL FUNCTION PROTOTYPES
==============================================================================*/
/**
 * @brief	Load config from external storage and update on RAM 
 * @param	hardFormat, 1 if memory reset required, 0 if not required and let the system decide
 * @return	0 for success and 1 for failure
 * @info date       : 03-Dec-2020
 * @info author     : Kartik
 */
unsigned char updateSystemConfigurations(char hardFormat);

/*----------------------------------------------------------------------------*/
/**
 * @brief	get the current config
 * @param	NA
 * @return	ptr to config structure, NULL if failed
 * @info date       : 04-Dec-2020
 * @info author     : Kartik
 */
CONFIG_S* getCurrentConfig( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Memory Virginity Test 
 * @param	NA
 * @return	0 if memory formatted else 1
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_flash_memory_formatted_status( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Set Flash memory is formatted 
 * @param   flag_formatted: Formatted Flag
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_flash_memory_formatted( unsigned char flag_formatted );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Total Number of Modbus Port Configured
 * @param	NA
 * @return	Return Total Number of Modbus Port Configured
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_total_no_modbus_port( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Set Flash memory is formatted 
 * @param	no_of_ports: Total number of Modbus Ports
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_total_no_modbus_port( unsigned char no_of_ports);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get the message acknowledgment setting
 * @param	NA
 * @return	Return Message Acknowledgment is enable or disabled
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_message_ack_config( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Enable/Disable message acknowledgment settings
 * @param	config_message_ack: Set or Clear message acknowledgment settings
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_message_ack_config( unsigned char config_message_ack);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Debug Logging is enable or disabled
 * @param	NA
 * @return	Return Debug Logging is enable or disabled
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_debug_logging_config( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Enable/Disable Debug Logging Settings
 * @param	config_debug_log:  Set or Clear Debug Logging Settings
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_debug_logging_config( unsigned char config_debug_log);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get SSL is enable or disabled
 * @param	NA
 * @return	Return SSL is enabled or disabled
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_ssl_config( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Enable/Disable SSL Settings
 * @param	config_ssl:  Set or Clear SSL Feature Settings
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_ssl_config( unsigned char config_ssl);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Total no of Read attempt after write
 * @param	NA
 * @return	Return Total no of Read attempt after write
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_read_after_write_config( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Enable/Disable Read after Write Count Settings
 * @param	config_read_after_write:  Set or Clear Read after Write Count
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_read_after_write_config( unsigned char config_read_after_write);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Local IP Address
 * @param	local_ip_add: To hold the Local IP Address
 * @return	Return IP Address present of not: 0-Present otherwise 1
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_local_ip_address( unsigned char *local_ip_add );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update Local IP Address
 * @param	local_ip_add:  Update local IP Address
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_local_ip_address( unsigned char *local_ip_add);
/*----------------------------------------------------------------------------*/
/**
 * @brief	Get GSM Module IMEI Number
 * @param	imei_no: To hold the IMEI Number
 * @return	Return IMEI Number present of not: 0-Present otherwise 1
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_imei_no( unsigned char *imei_no );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update IMEI Number of GSM Module
 * @param	imei_no:  Update imei number
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_imei_no( unsigned char *imei_no);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get APN
 * @param	apn: To hold the APN
 * @return	Return APN present of not: 0-Present otherwise 1
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_apn( unsigned char *apn );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update Access Provider Name
 * @param	apn:  Update APN
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_apn( unsigned char *apn);
/*----------------------------------------------------------------------------*/
/**
 * @brief	Get NTP Server Address
 * @param	ntp_server_add: To hold the NTP Server Address
 * @return	Return NTP Server Address present of not: 0-Present otherwise 1
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_ntp_server_address( unsigned char *ntp_server_add );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update NTP Server Address
 * @param	ntp_server_add:  Update NTP Server Address
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_ntp_server_address( unsigned char *ntp_server_add);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Off line data Packet initial address 
 * @param	NA
 * @return	Base address of off line stored data in flash memory
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned short get_read_pointer( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Increase Data Read Pointer to next data packet
 * @param	NA
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned short increase_read_pointer( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Off line data Packet last address 
 * @param	NA
 * @return	last address of off line stored data in flash memory
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned short get_write_pointer( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Increase Data write Pointer to empty location
 * @param	NA
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned short increase_write_pointer( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get value of Ping Request Interval in Second
 * @param	NA
 * @return	Return SSL is enabled or disabled
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned short get_ping_req_interval_in_sec( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Configure Ping Request Interval in Seconds
 * @param	ping_req_sec:  Set No of Seconds in Ping Request Interval
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_ping_req_interval_in_sec( unsigned short ping_req_sec );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Business Logic Interval
 * @param	NA
 * @return	Return Business Logic Polling time in seconds
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned short get_business_logic_interval_in_sec( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Configure Business Logic Interval in seconds
 * @param	business_logic_polling_sec:  Set  Business Logic Interval in seconds
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_business_logic_interval_in_sec( unsigned short business_logic_polling_sec);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get GPS Location forwarding interval in seconds
 * @param	NA
 * @return	Return GPS Acquisition and Polling Time in Seconds.
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
unsigned short get_gps_location_interval( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Configure GPS Polling Interval time in seconds
 * @param	gps_polling_interval_sec:  Sets GPS Polling Interval
 * @return	NA
 * @info date       : 16-Sept-2020
 * @info author     : Ashutosh
 */
void set_gps_location_interval( unsigned short gps_polling_interval_sec);


/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Port Configuration Status in case Modbus command present or not
 * @param	port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @return	Return 0x55 in case Port Configuration is present otherwise return 0x00.
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_port_config_status( unsigned char port_num );

/*----------------------------------------------------------------------------*/
/**
 * @brief	It will set Status 0x55 if modus commands stored or 
 * @param	port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @return	NA
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
void set_port_config_status(  unsigned char port_num, unsigned char status );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Port Type it is RS432 or RS485 or TCP
 * @param	port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @return	Return 0x00-RS432, 0x01-RS485 and 0x02-TCP
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_port_type( unsigned char port_num );

/*----------------------------------------------------------------------------*/
/**
 * @brief	It will set Status 0x55 if modus commands stored or 
 * @Param   port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @param	port_type: Accept between 0x00 to 0x02 as port types
 * @return	NA
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
void set_port_type( unsigned char port_num, unsigned char port_type );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Stored Delimiter for Modbus Command
 * @param	port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @return	Return stored Delimiter for Modbus Command
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_port_delimiter( unsigned char port_num );

/*----------------------------------------------------------------------------*/
/**
 * @brief	It will set delimiter Value to detect EoF of any command response
 * @param   port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @param	port_delimiter: Set Port Delimiter
 * @return	NA
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
void set_port_delimiter( unsigned char port_num, unsigned char port_delimiter );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Port Parameter like baudrate, start/stop bit, priority/data bits.
 * @Param 	port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @param	port_config_param: To hold the Port Parameters
 * @return	Return Port Parameter present of not: 0-Present otherwise 1
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_port_parameter( unsigned char port_num, 
									unsigned char *port_config_param );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update Port Parameters in Device Flash Memory
 * @param 	port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @param	port_config_param:  Update Port Parameters
 * @return	NA
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
void set_port_parameter( unsigned char port_num, unsigned char *port_config_param);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Stored ModBus Command
 * @param 	port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @param	modbus_command: To hold the Stored ModBus Command
 * @return	Return Modbus Command present of not: 0-Present otherwise 1
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_modbus_command( unsigned char port_num, 
										unsigned char *modbus_command );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update Modbus Command(s) in device flash memory
 * @param 	port_num : Port Number 0x00- RS232, 0x01- RS485 & 0x02- TCP
 * @param	modbus_command:  Update Modbus Command(s)
 * @return	NA
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
void set_modbus_command( unsigned char port_num, unsigned char *modbus_command);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Configure Read Pointer
 * @param	read_pointer:  Set The Read Pointer to device flash memory
 * @return	NA
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
void set_read_pointer( unsigned short read_pointer );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Configure Write Pointer
 * @param	write_pointer:  Set The Write pointer to device flash memory
 * @return	NA
 * @info date       : 17-Sept-2020
 * @info author     : Ashutosh
 */
void set_write_pointer( unsigned short write_pointer );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get the Generator Data Transmission settings
 * @param	NA
 * @return	Return Generator Data Packet Transmission is enable or disabled
 * @info date       : 30-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_generator_data_config( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Enable/Disable Generator Data Transmission settings
 * @param	config_gen_data_transmit: Set or Clear Generator Data Transmission settings
 * @return	NA
 * @info date       : 30-Sept-2020
 * @info author     : Ashutosh
 */
void set_generator_data_config( unsigned char config_gen_data_transmit);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Run Hour Logging Settings
 * @param	NA
 * @return	Return Run Hour Logging is enable or disabled
 * @info date       : 30-Sept-2020
 * @info author     : Ashutosh
 */
unsigned char get_log_run_hour_config( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Enable/Disable Run Hour Logging settings
 * @param	config_run_hour: Set or Clear Logging Run Hour settings
 * @return	NA
 * @info date       : 30-Sept-2020
 * @info author     : Ashutosh
 */
void set_log_run_hour_config( unsigned char config_run_hour);

/*----------------------------------------------------------------------------*/
/**
 * @brief	GET GSM Module Type Settings
 * @param	NA
 * @return	Return GSM Module Type it is 3 for 3G or 4 for 4G
 * @info date       : 15-Oct-2020
 * @info author     : Ashutosh
 */
unsigned char get_gsm_module_type_config( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Configure it is 3G Supported GSM Module or 4G Supported
 * @param	gsm_module_type: GSM Module type settings
 * @return	NA
 * @info date       : 15-Oct-2020
 * @info author     : Ashutosh
 */
void set_gsm_module_type_config( unsigned char gsm_module_type);

/*----------------------------------------------------------------------------*/
/**
 * @brief	GET in which SIM Slot, SIM is inserted
 * @param	NA
 * @return	Return in which SIM Slot, SIM is inserted
 * @info date       : 21-Oct-2020
 * @info author     : Ashutosh
 */
unsigned char get_gsm_sim_slot_config( void );

/*----------------------------------------------------------------------------*/
/**
 * @brief	Configure in which SIM Slot, SIM is inserted
 * @param	gsm_sim_slot: SIM Slot Configuration
 * @return	NA
 * @info date       : 21-Oct-2020
 * @info author     : Ashutosh
 */
void set_gsm_sim_slot_config( unsigned char gsm_sim_slot);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Local Gateway IP Address
 * @param	local_gateway_ip: To hold the Local Gateway IP
 * @return	Return Gateway IP Address present or not: 0-Present otherwise 1
 * @info date       : 15-OCT-2020
 * @info author     : Ashutosh
 */
unsigned char get_local_gateway_ip( unsigned char *local_gateway_ip );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update Local Gateway IP Address
 * @param	local_gateway_ip:  Update local Gateway IP Address
 * @return	NA
 * @info date       : 15-Oct-2020
 * @info author     : Ashutosh
 */
void set_local_gateway_ip( unsigned char *local_gateway_ip);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Local DNS Address
 * @param	local_dns_add: To hold the Local DNS Address
 * @return	Return DNS Address present or not: 0-Present otherwise 1
 * @info date       : 15-OCT-2020
 * @info author     : Ashutosh
 */
unsigned char get_local_dns_address( unsigned char *local_dns_add );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update Local DNS  Address
 * @param	local_dns_add:  Update local DNS Address
 * @return	NA
 * @info date       : 15-Oct-2020
 * @info author     : Ashutosh
 */
void set_local_dns_address( unsigned char *local_dns_add);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Local Subnet Mask
 * @param	local_subnet_mask: To hold the Local Subnet MAsk
 * @return	Return Subnet Mask present or not: 0-Present otherwise 1
 * @info date       : 15-OCT-2020
 * @info author     : Ashutosh
 */
unsigned char get_local_subnet_mask( unsigned char *local_subnet_mask );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update Local Subnet Mask 
 * @param	local_subnet_mask:  Update local Subnet Mask
 * @return	NA
 * @info date       : 15-Oct-2020
 * @info author     : Ashutosh
 */
void set_local_subnet_mask( unsigned char *local_subnet_mask);
/*----------------------------------------------------------------------------*/
/**
 * @brief	InstaMsg Port Number Configured
 * @param	NA
 * @return	Return InstaMSG Configured Port Number
 * @info date       : 22-OCT-2020
 * @info author     : Ashutosh
 */
unsigned short get_server_port_number( void );
/*----------------------------------------------------------------------------*/
/**
 * @brief	Set InstaMSG Port Number
 * @param	port: InstaMSG Port Number
 * @return	NA
 * @info date       : 22-OCT-2020
 * @info author     : Ashutosh
 */
void set_server_port_number( unsigned short port);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get InstaMsg Server Address
 * @param	insta_msg_add: To hold the InstaMSG Server Address
 * @return	Return InstaMSG Server Address present or not: 0-Present otherwise 1
 * @info date       : 21-OCT-2020
 * @info author     : Ashutosh
 */
unsigned char get_instaMsg_server_address( unsigned char *insta_msg_add );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update InstaMsg Server Address
 * @param	insta_msg_add:  Update InstaMsg Server Address
 * @return	NA
 * @info date       : 21-Oct-2020
 * @info author     : Ashutosh
 */
void set_instaMsg_server_address( unsigned char *insta_msg_add);
/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Device Password
 * @param	dev_pwd: To hold Device Password
 * @return	Return IMEI Number present of not: 0-Present otherwise 1
 * @info date       : 21-OCT-2020
 * @info author     : Ashutosh
 */
unsigned char get_device_password( unsigned char *dev_pwd );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update Device Password
 * @param	dev_pwd:  Update Device Password
 * @return	NA
 * @info date       : 21-OCT-2020
 * @info author     : Ashutosh
 */
void set_device_password( unsigned char *dev_pwd);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get Server Address for FOTA
 * @param	host_address: To hold Server Address
 * @param   len: buffer size
 * @return	Return 0-Present otherwise 1
 * @info date       : 09-Nov-2020
 * @info author     : Ashutosh
 */
unsigned char get_fota_host_address( unsigned char *host_address, int len );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update Server Address for FOTA
 * @param	host_address:  Update Server Address
 * @return	NA
 * @info date       : 09-Nov-2020
 * @info author     : Ashutosh
 */
void set_fota_host_address( unsigned char *host_address);

/*----------------------------------------------------------------------------*/
/**
 * @brief	FoTA Port Number Configured
 * @param	NA
 * @return	Return FoTA Server Configured Port Number
 * @info date       : 09-NOV-2020
 * @info author     : Ashutosh
 */
unsigned short get_fota_host_port_number( void );
/*----------------------------------------------------------------------------*/
/**
 * @brief	Set FoTA Port Number
 * @param	port: Server Port Number
 * @return	NA
 * @info date       : 09-NOV-2020
 * @info author     : Ashutosh
 */
void set_fota_host_port_number( unsigned short port);
/**
 \ @brief	get_fota_host_name	
 \ @	  :	Get Fota server host name
 \ @param	fota_server_host_name: container to hold server host name
 \ @param	len: buffer size
 \ @param	0 if present, 1 if not
 \ @info date       : 4-DEC-2020
 \ @info author     : Kartik
 */
unsigned char get_fota_host_name( unsigned char *fota_server_host_name, int len);
/**
 \ @brief	get_fota_host_ip_address	
 \ @	  :	Get Fota server host ip address
 \ @param	fota_server_host_ip_address: container to hold server host ip address
 \ @param	len: buffer size
 \ @return  0 if present, 1 is not present
 \ @info date       : 4-DEC-2020
 \ @info author     : Kartik
 */
unsigned char get_fota_host_ip_address( unsigned char *fota_server_host_ip_address, int len);

/**
 \ @brief	set_fota_host_ip_address
 \ @	  : Set FoTA Host IP Address
 \ @param	host_ip_address: ptr to ip address
 \ @return	NA
 \ @info date       : 4-DEC-2020
 \ @info author     : Kartik
 */
void set_fota_host_ip_address(char *host_ip_address);

/**
 \ @brief	set_fota_host_name
 \ @	  : Set FoTA Host Name
 \ @param	hostname: ptr to hostname
 \ @return	NA
 \ @info date       : 4-DEC-2020
 \ @info author     : Kartik
 */
void set_fota_host_name(char *hostname);

/*----------------------------------------------------------------------------*/
/**
 * @brief	Get firmware name to download from FoTA Server
 * @param	fw_name: To hold Firmware name
 * @return	Return Firmware Name is present of not: 0-Present otherwise 1
 * @info date       : 09-Nov-2020
 * @info author     : Ashutosh
 */
unsigned char get_firmware_name( unsigned char *fw_name );

/*----------------------------------------------------------------------------*/
/**
 * @brief	To update firmware name to download from FoTA Server
 * @param	fw_name:  To Update Firmware name 
 * @return	NA
 * @info date       : 09-Nov-2020
 * @info author     : Ashutosh
 */
void set_firmware_name( unsigned char *fw_name);

/*----------------------------------------------------------------------------*/
void save_config_parameters( void );
/**
 * @}
 */
#ifdef __cplusplus
} // extern "C"
#endif

#endif


#endif /* LL_CONFIG_H_ */