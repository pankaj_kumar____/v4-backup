/*
 * ll_config.c
 *
 * Created: 12/2/2020 7:16:28 AM
 *  Author: Kartik.Gupta
 */ 

#if 1
/*==============================================================================
                                INCLUDE FILES
==============================================================================*/
#include "ll_config.h"
#include "ll_console.h"
#include "device_defines.h"
#include "../fwUpgrade/fwUpgrade.h"
#include "../../../../../../../../common/instamsg/driver/include/misc.h"
#include "../../../../../../../../common/instamsg/driver/include/log.h"
#include "../../../../../../../../device/kritikal-gateway/common/storage_utils.h"

#include <string.h>
/*******************************************************************************
 * Private macros/types/enumerations/variables
 ******************************************************************************/
typedef union{
    unsigned char config_data[sizeof(CONFIG_S)];
    CONFIG_S config_struct;
}CONFIG_U;


CONFIG_U ll_config;
LL_APP_STATUS_S ll_app_status;
/*******************************************************************************
 * Public types/enumerations/variables
 ******************************************************************************/

/*******************************************************************************
 * Private functions
 ******************************************************************************/
/*
 ****************************************************************************************
~#  Function Name : setConfigDefault                                                    #~ 
~#  Parameters    : None                                                                #~
~#  Return Type   : None                                                                #~
~#  Scope         : Global                                                              #~
~#  Functionality : This function store default values in the EEPROM Variables          #~
~#                                                                                      #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #21/08/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/
void setConfigDefault( void )
{
	int iLoop=0;
    sg_sprintf(LOG_GLOBAL_BUFFER, "No Config Data present in memory, Hence Setting it to default config!!!");
    info_log(LOG_GLOBAL_BUFFER);	
	/*!> Check if memory is virgin then format it*/
	ll_config.config_struct.memory_format_id=MEMORY_FORMATTED;					
	/*!> Total number of modbus communication port*/
	ll_config.config_struct.num_port=0x01;				
	/*!> Enable Message Acknowledgement configuration*/			
	ll_config.config_struct.enable_message_ack=0x00;		
	/*!> Enable Debug Log configuration Parameter*/
	ll_config.config_struct.enable_debug_logging=0x00;	
	/*!> Enable SSL Configuration*/
	ll_config.config_struct.enable_ssl=0x00;				
	/*!> MQTT: Read after writing the data*/
	ll_config.config_struct.compulsory_read_after_write_count=0x03;
	/*!> Enable/Disable Generator Data Packet Transmit */
	ll_config.config_struct.enable_send_generator_data=0x00;	
	/*!> Store Persistant Load Run Hour*/	
	ll_config.config_struct.enable_log_persistant_run_hour=0x00;	
	/*!> Store GSM Module type*/	
	ll_config.config_struct.gsm_module_type=0x00;	
	/*!> Store GSM SIM Slot*/
	ll_config.config_struct.gsm_sim_slot=0x00;	
	/*!> Device Password to get secret*/
	strcpy(ll_config.config_struct.device_password,"test");
	/*!> Device Local Static IP Address*/			
	strcpy(ll_config.config_struct.local_static_ip_add,"0.0.0.0");
	/*!> Device Local Gateway IP Address*/
	strcpy(ll_config.config_struct.local_gateway_ip,"0.0.0.0");
	/*!> Device Local Subnet Mask*/
	strcpy(ll_config.config_struct.local_subnet_mask,"0.0.0.0");		
	/*!> Device Local DNS Address*/	
	memset(ll_config.config_struct.local_dns,0x00,sizeof(ll_config.config_struct.local_dns));
	/*!> GSM Module IMEI No*/			
	memset(ll_config.config_struct.device_imei_no,0x00,sizeof(ll_config.config_struct.device_imei_no));	
	/*!> APN*/			
	memset(ll_config.config_struct.gprs_apn,0x00,sizeof(ll_config.config_struct.gprs_apn));	
	/*!> NTP Server Address*/	
	strcpy(ll_config.config_struct.ntp_server_add,DEFAULT_NTP_SERVER);		
	/*!> DATA Server Address*/
	strcpy(ll_config.config_struct.data_server_add,INSTAMSG_HOST);
	/*!> FOTA Server Address*/
	memset(ll_config.config_struct.fota_server_add,0x00,sizeof(ll_config.config_struct.fota_server_add));
	/*!> FOTA Server IP*/
	strcpy(ll_config.config_struct.fota_server_ip_add, FW_UPGRADE_DEFAULT_HOST_IP);
	/*!> FOTA Server Port number*/
	ll_config.config_struct.fota_server_port_number = FW_UPGRADE_DEFAULT_PORT;
	/*!> FOTA Server Host name*/
	strcpy(ll_config.config_struct.fota_server_hostname, FW_UPGRADE_DEFAULT_HOST_NAME);
	/*!> Device Secret ID*/			
	memset(ll_config.config_struct.device_secret_id,0x00,sizeof(ll_config.config_struct.device_secret_id));	

	//for testing
	strcpy(ll_config.config_struct.device_secret_id, "c270c520-57bc-11ea-bc3c-92e7bb4f11cc-da2f41cbae524714a82b303cd09adc9f");
	
	/*!> Data Server Port Number*/
	ll_config.config_struct.data_server_port_number=8883;	
	/*!> Offline Data Storage Read Pointer*/
	ll_config.config_struct.read_pointer=0x00;		
	/*!> Offline Data Storage Write Pointer*/		
	ll_config.config_struct.write_pointer=0x00;	
	/*!> Ping Request Interval in Second*/				
	ll_config.config_struct.ping_request_interval=180;	
	/*!> Data Polling Interval in Second*/
	ll_config.config_struct.business_logic_interval=60;			
	/*!> GPS Interval in Second*/
	ll_config.config_struct.gps_location_interval=180;	
				
	for(iLoop=0;iLoop<MAX_PORT_ALLOWED;iLoop++)   
	{
		/*!> Set if parameters present*/
		if(ll_config.config_struct.num_port >= iLoop + 1) {
			ll_config.config_struct.modbus_port[iLoop].status=0x55;
			/*!> 0-RS232, 1-RS485 & 2 for TCP*/
			ll_config.config_struct.modbus_port[iLoop].port_type=iLoop;
		} else {
			ll_config.config_struct.modbus_port[iLoop].port_type=0x55;
			ll_config.config_struct.modbus_port[iLoop].status=0x00;	
		}			
		/*!> Holds delimiter if present*/
		ll_config.config_struct.modbus_port[iLoop].port_delimiter=0x00;		
		/*!> Port Config Parameters */				
		memset(ll_config.config_struct.modbus_port[iLoop].port_parameter,0x00,sizeof(ll_config.config_struct.modbus_port[iLoop].port_parameter));			
		/*!> Need to dynamically allocate to hold Modbus commands*/
		memset(ll_config.config_struct.modbus_port[iLoop].command,0x00,sizeof(ll_config.config_struct.modbus_port[iLoop].command));						
	} 
		
}

/*******************************************************************************
 * Public functions
 ******************************************************************************/
/*----------------------------------------------------------------------------*/
/**
 * \brief	init_app_status_default
 * \	  : Application Flags Initial Value
 * \param	NA
 * \return	NA
 * \info date       : 25-Sept-2020
 * \info author     : Ashutosh
 */
void init_app_status_default( void )
{
	ll_app_status.get_gsm_location		= 0x00;
	ll_app_status.get_rtc				= 0x00;
	ll_app_status.gps_poll_time			= 0x00;
	ll_app_status.send_config_command	= 0x00;
	ll_app_status.send_gsm_command		= 0x00;
	ll_app_status.send_modbus_rs232_data= 0x00;
	ll_app_status.send_modbus_rs485_data= 0x00;
	ll_app_status.send_modbus_tcp_data	= 0x00;
	ll_app_status.connected_to_server	= 0x00;
	ll_app_status.con_via_ethernet		= 0x00;
	ll_app_status.con_via_gsm			= 0x00;
}
/*----------------------------------------------------------------------------*/
/**
 * \brief	get_flash_memory_formatted_status
 * \	  : Memory Virginity Test 
 * \param	NA
 * \return	0 if memory formatted else 1
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_flash_memory_formatted_status( void )
{
	unsigned char ret_val=0;

	if(MEMORY_FORMATTED == ll_config.config_struct.memory_format_id)
	{
		ret_val = 0x00;
	}
	else
	{
		ret_val = 0x01;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_flash_memory_formatted
 * \      : Set Flash memory is formatted 
 * \param   flag_formatted: Formatted Flag
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_flash_memory_formatted( unsigned char flag_formatted )
{
	ll_config.config_struct.memory_format_id = flag_formatted;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_total_no_modbus_port
 * \	  : Total Number of Modbus Port Configured
 * \param	NA
 * \return	Return Total Number of Modbus Port Configured
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_total_no_modbus_port( void )
{
	unsigned char ret_val=0;

	if(ll_config.config_struct.num_port > MAX_PORT_ALLOWED)
	{
		ret_val = ll_config.config_struct.num_port = 0x00;
	}
	else
	{
		ret_val = ll_config.config_struct.num_port;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_total_no_modbus_port
 * \	  : Set Flash memory is formatted 
 * \param	no_of_ports: Total number of Modbus Ports
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_total_no_modbus_port( unsigned char no_of_ports)
{
	if(no_of_ports<=MAX_PORT_ALLOWED)
	{
		ll_config.config_struct.num_port = no_of_ports;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_server_port_number
 * \	  : InstaMsg Port Number Configured
 * \param	NA
 * \return	Return InstaMSG Configured Port Number
 * \info date       : 22-OCT-2020
 * \info author     : Ashutosh
 */
unsigned short get_server_port_number( void )
{
	unsigned short ret_val=0;

	ret_val = ll_config.config_struct.data_server_port_number;

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_server_port_number
 * \	  : Set InstaMSG Port Number
 * \param	port: InstaMSG Port Number
 * \return	NA
 * \info date       : 22-OCT-2020
 * \info author     : Ashutosh
 */
void set_server_port_number( unsigned short port)
{
	ll_config.config_struct.data_server_port_number = port;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_total_no_modbus_port
 * \	  : Get the message acknowledgment setting
 * \param	NA
 * \return	Return Message Acknowledgment is enable or disabled
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_message_ack_config( void )
{
	unsigned char ret_val=0;

	if(0x01 == ll_config.config_struct.enable_message_ack)
	{
		ret_val = 0x01;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_message_ack_config
 * \	  : Enable/Disable message acknowledgment settings
 * \param	config_message_ack: Set or Clear message acknowledgment settings
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_message_ack_config( unsigned char config_message_ack)
{
	if(0x01 == config_message_ack)
	{
		ll_config.config_struct.enable_message_ack = 0x01;
	}
	else
	{
		ll_config.config_struct.enable_message_ack = 0x00;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_debug_logging_config
 * \	  : Get Debug Logging is enable or disabled
 * \param	NA
 * \return	Return Debug Logging is enable or disabled
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_debug_logging_config( void )
{
	unsigned char ret_val=0;

	if(0x01 == ll_config.config_struct.enable_debug_logging)
	{
		ret_val = 0x01;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_debug_logging_config	
 * \	  : Enable/Disable Debug Logging Settings
 * \param	config_debug_log:  Set or Clear Debug Logging Settings
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_debug_logging_config( unsigned char config_debug_log)
{
	if(0x01 == config_debug_log)
	{
		ll_config.config_struct.enable_debug_logging = 0x01;
	}
	else
	{
		ll_config.config_struct.enable_debug_logging = 0x00;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_ssl_config
 * \ 	  : Get SSL is enable or disabled
 * \param	NA
 * \return	Return SSL is enabled or disabled
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_ssl_config( void )
{
	unsigned char ret_val=0;

	if(0x01 == ll_config.config_struct.enable_ssl)
	{
		ret_val = 0x01;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_ssl_config
 * \ 	  : Enable/Disable SSL Settings
 * \param	config_ssl:  Set or Clear SSL Feature Settings
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_ssl_config( unsigned char config_ssl)
{
	if(0x01 == config_ssl)
	{
		ll_config.config_struct.enable_ssl = 0x01;
	}
	else
	{
		ll_config.config_struct.enable_ssl = 0x00;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_read_after_write_config
 * \ 	  : Get Total no of Read attempt after write
 * \param	NA
 * \return	Return Total no of Read attempt after write
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_read_after_write_config( void )
{
	unsigned char ret_val=0;

	if(0x00 <= ll_config.config_struct.compulsory_read_after_write_count)
	{
		ret_val = ll_config.config_struct.compulsory_read_after_write_count;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_read_after_write_config	
 * \	  : Enable/Disable Read after Write Count Settings
 * \param	config_read_after_write:  Set or Clear Read after Write Count
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_read_after_write_config( unsigned char config_read_after_write)
{
	if(0x00 == config_read_after_write)
	{
		ll_config.config_struct.compulsory_read_after_write_count = 0x00;
	}
	else
	{
		ll_config.config_struct.compulsory_read_after_write_count = config_read_after_write;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_local_ip_address
 * \	  : Get Local IP Address
 * \param	local_ip_add: To hold the Local IP Address
 * \return	Return IP Address present of not: 0-Present otherwise 1
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_local_ip_address( unsigned char *local_ip_add )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.local_static_ip_add[0])
	{
		return ret_val;
	}

	strcpy(local_ip_add, ll_config.config_struct.local_static_ip_add);
	ret_val = 0x00;

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_local_ip_address
 * \	  : To update Local IP Address
 * \param	local_ip_add:  Update local IP Address
 * \return	NA
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
void set_local_ip_address( unsigned char *local_ip_add)
{
	if(local_ip_add==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.local_static_ip_add, local_ip_add);
}

/*----------------------------------------------------------------------------*/
/**
 * @brief	get_instaMsg_server_address
 * \	  : Get InstaMsg Server Address
 * \param	insta_msg_add: To hold the InstaMSG Server Address
 * \return	Return InstaMSG Server Address present or not: 0-Present otherwise 1
 * \info date       : 21-OCT-2020
 * \info author     : Ashutosh
 */
unsigned char get_instaMsg_server_address( unsigned char *insta_msg_add )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.data_server_add[0])
	{
		return ret_val;
	}

	strcpy(insta_msg_add, ll_config.config_struct.data_server_add);
	ret_val = 0x00;

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_instaMsg_server_address
 * \	  : To update InstaMsg Server Address
 * \param	insta_msg_add:  Update InstaMsg Server Address
 * \return	NA
 * \info date       : 21-Oct-2020
 *\info author     : Ashutosh
 */
void set_instaMsg_server_address( unsigned char *insta_msg_add)
{
	if(insta_msg_add==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.data_server_add, insta_msg_add);
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_device_password
 * \	  : Get Device Password
 * \param	dev_pwd: To hold Device Password
 * \return	Return Device Password present of not: 0-Present otherwise 1
 * \info date       : 21-OCT-2020
 * \info author     : Ashutosh
 */
unsigned char get_device_password( unsigned char *dev_pwd )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.device_password[0])
	{
		return ret_val;
	}

	strcpy(dev_pwd, ll_config.config_struct.device_password);
	ret_val = 0x00;

	return ret_val;
}
/*----------------------------------------------------------------------------*/
/**
 * \brief	set_device_password
 * \	  : To update Device Password
 * \param	dev_pwd:  Update Device Password
 * \return	NA
 * \info date       : 21-OCT-2020
 * \info author     : Ashutosh
 */
void set_device_password( unsigned char *dev_pwd)
{
	if(dev_pwd==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.device_password, dev_pwd);
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_imei_no
 * \	  : Get GSM Module IMEI Number
 * \param	imei_no: To hold the IMEI Number
 * \return	Return IMEI Number present of not: 0-Present otherwise 1
 * \info date       : 16-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_imei_no( unsigned char *imei_no )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.device_imei_no[0])
	{
		return ret_val;
	}

	strcpy(imei_no, ll_config.config_struct.device_imei_no);
	ret_val = 0x00;

	return ret_val;
}
/*----------------------------------------------------------------------------*/
/**
 * \brief	set_imei_no
 * \	  : To update IMEI Number of GSM Module
 * \param	imei_no:  Update imei number
 * \return	NA
 * \info date       : 16-Sept-2020
 * \info author     : Ashutosh
 */
void set_imei_no( unsigned char *imei_no)
{
	if(imei_no==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.device_imei_no, imei_no);
}
/*----------------------------------------------------------------------------*/
/**
 * \brief	get_apn
 * \	  : Get APN
 * \param	apn: To hold the APN
 * \return	Return APN present of not: 0-Present otherwise 1
 * \info date       : 16-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_apn( unsigned char *apn )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.gprs_apn[0])
	{
		return ret_val;
	}

	strcpy(apn, ll_config.config_struct.gprs_apn);
	ret_val = 0x00;

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_apn
 * \	  : To update Access Provider Name
 * \param	apn:  Update APN
 * \return	NA
 * \info date       : 16-Sept-2020
 * \info author     : Ashutosh
 */
void set_apn( unsigned char *apn)
{
	if(apn==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.gprs_apn, apn);
}
/*----------------------------------------------------------------------------*/
/**
 * \brief	get_ntp_server_address
 * \	  : Get NTP Server Address
 * \param	ntp_server_add: To hold the NTP Server Address
 * \return	Return NTP Server Address present of not: 0-Present otherwise 1
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_ntp_server_address( unsigned char *ntp_server_add )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.ntp_server_add[0])
	{
		return ret_val;
	}

	strcpy(ntp_server_add, ll_config.config_struct.ntp_server_add);
	ret_val = 0x00;

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_ntp_server_address
 * \	  : To update NTP Server Address
 * \param	ntp_server_add:  Update NTP Server Address
 * \return	NA
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
void set_ntp_server_address( unsigned char *ntp_server_add)
{
	if(ntp_server_add==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.ntp_server_add, ntp_server_add);
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_read_pointer
 * \	  : Get Off line data Packet initial address 
 * \param	NA
 * \return	Base address of off line stored data in flash memory
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned short get_read_pointer( void )
{
	unsigned short ret_val=0;

	if(0x01 <= ll_config.config_struct.read_pointer)
	{
		ret_val = ll_config.config_struct.read_pointer;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_read_pointer
 * \	  : Configure Read Pointer
 * \param	read_pointer:  Set The Read Pointer to device flash memory
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_read_pointer( unsigned short read_pointer )
{
	if(MAX_OFFLINE_PACKET>read_pointer)
	{
		ll_config.config_struct.read_pointer = read_pointer;
	}
}
/*----------------------------------------------------------------------------*/
/**
 * \brief	increase_read_pointer
 * \ 	  : Increase Data Read Pointer to next data packet
 * \param	NA
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned short increase_read_pointer( void )
{
	unsigned short ret_val=0;

	if(++ll_config.config_struct.read_pointer == ll_config.config_struct.write_pointer)
	{
		ret_val = ll_config.config_struct.write_pointer = ll_config.config_struct.read_pointer = 0;
	}
	else if(ll_config.config_struct.read_pointer >= MAX_OFFLINE_PACKET)
	{
		ret_val = ll_config.config_struct.read_pointer = 0;
	}
	else
	{
		ret_val = ll_config.config_struct.read_pointer;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_write_pointer
 * \	  : Get Off line data Packet last address 
 * \param	NA
 * \return	last address of off line stored data in flash memory
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned short get_write_pointer( void )
{
	unsigned short ret_val=0;

	if(0x01 <= ll_config.config_struct.write_pointer)
	{
		ret_val = ll_config.config_struct.write_pointer;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_write_pointer
 * \ 	  : Configure Write Pointer
 * \param	write_pointer:  Set The Write pointer to device flash memory
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_write_pointer( unsigned short write_pointer )
{
	if(MAX_OFFLINE_PACKET>write_pointer)
	{
		ll_config.config_struct.write_pointer = write_pointer;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	increase_write_pointer
 * \	  : Increase Data write Pointer to empty location
 * \param	NA
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned short increase_write_pointer( void )
{
	unsigned short ret_val=0;

	if(++ll_config.config_struct.write_pointer>=MAX_OFFLINE_PACKET)
	{
		/*!> Write Pointer reached to MAX_ALLOWED_RANGE, it replace the old packet in FIFO Manner
		 **  Hence Set to 0*/
		ll_config.config_struct.write_pointer = 0x00;
		/*!> And as per erase_before_write problem, we need to shift read pointer to next sector
		 **  read_pointer+=4*/
		if(0x00 == ll_config.config_struct.read_pointer)
		{
			ll_config.config_struct.read_pointer = ll_config.config_struct.write_pointer+4;
		}
	}
	/*!> If Read and Write Pointers point same location, Read Pointer need to shift to next sector */
	else if (ll_config.config_struct.write_pointer == ll_config.config_struct.read_pointer)
	{
		ret_val=  ll_config.config_struct.write_pointer;
		ll_config.config_struct.read_pointer = ((unsigned short)(ll_config.config_struct.read_pointer/4)+1)*4;
		if(ll_config.config_struct.read_pointer >= MAX_OFFLINE_PACKET)
		{
			ll_config.config_struct.read_pointer = 0x00;
		}
	}
	else
	{
		ret_val = ll_config.config_struct.write_pointer;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_ping_req_interval_in_sec
 * \	  : Get value of Ping Request Interval in Second
 * \param	NA
 * \return	Return PING Request Interval in Seconds
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned short get_ping_req_interval_in_sec( void )
{
	unsigned short ret_val=0;

	if(0x01 <= ll_config.config_struct.ping_request_interval)
	{
		ret_val = ll_config.config_struct.ping_request_interval;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_ping_req_interval_in_sec
 * \ 	  : Configure Ping Request Interval in Seconds
 * \param	ping_req_sec:  Set No of Seconds in Ping Request Interval
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_ping_req_interval_in_sec( unsigned short ping_req_sec )
{
	ll_config.config_struct.ping_request_interval = ping_req_sec;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_business_logic_interval_in_sec
 * \	  : Get Business Logic Interval
 * \param	NA
 * \return	Return Business Logic Polling time in seconds
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned short get_business_logic_interval_in_sec( void )
{
	unsigned short ret_val=0;

	if(0x01 <= ll_config.config_struct.business_logic_interval)
	{
		ret_val = ll_config.config_struct.business_logic_interval;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_business_logic_interval_in_sec
 * \	  : Configure Business Logic Interval in seconds
 * \param	business_logic_polling_sec:  Set  Business Logic Interval in seconds
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_business_logic_interval_in_sec( unsigned short business_logic_polling_sec)
{
	ll_config.config_struct.business_logic_interval = business_logic_polling_sec;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_gps_location_interval
 * \	  : Get GPS Location forwarding interval in seconds
 * \param	NA
 * \return	Return GPS Acquisition and Polling Time in Seconds.
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned short get_gps_location_interval( void )
{
	unsigned short ret_val=0;

	if(0x01 <= ll_config.config_struct.gps_location_interval)
	{
		ret_val = ll_config.config_struct.gps_location_interval;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_gps_location_interval
 * \	  : Configure GPS Polling Interval time in seconds
 * \param	gps_polling_interval_sec:  Sets GPS Polling Interval
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_gps_location_interval( unsigned short gps_polling_interval_sec)
{
	ll_config.config_struct.gps_location_interval = gps_polling_interval_sec;
}


/*----------------------------------------------------------------------------*/
/**
 * \brief	get_port_config_status
 * \	  : Get Port Configuration Status in case Modbus command present or not
 * \param	NA
 * \return	Return 0x55 in case Port Configuration is present otherwise return 0x00.
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_port_config_status( unsigned char port_num )
{
	unsigned char ret_val=0;

	if(port_num>=MAX_PORT_ALLOWED)
	{
		return ret_val;
	}

	if(0x55==ll_config.config_struct.modbus_port[port_num].status)
	{
		ret_val=ll_config.config_struct.modbus_port[port_num].status;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_port_config_status
 * \	  : It will set Status 0x55 if Modbus commands stored or 
 * \param	NA
 * \return	NA
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
void set_port_config_status( unsigned char port_num, unsigned char status )
{
	if(port_num>=MAX_PORT_ALLOWED)
	{
		return;
	}

	ll_config.config_struct.modbus_port[port_num].status = status;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_port_type
 * \	  : Get Port Type it is RS432 or RS485 or TCP
 * \param	NA
 * \return	Return 0x00-RS232, 0x01-RS485 and 0x02-TCP, 0xFF in case of any error
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_port_type( unsigned char port_num )
{
	unsigned char ret_val=0xFF;

	if(port_num>=MAX_PORT_ALLOWED)
	{
		return ret_val;
	}

	if(0x03 > ll_config.config_struct.modbus_port[port_num].port_type)
	{
		ret_val=ll_config.config_struct.modbus_port[port_num].port_type;
	}
	
	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_port_type
 * \	  : It will set Status 0x55 if Modbus commands stored or 
 * \param	port_type: Accept between 0x00 to 0x02 as port types
 * \return	NA
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
void set_port_type( unsigned char port_num, unsigned char port_type )
{
	if(port_num>=MAX_PORT_ALLOWED)
	{
		return;
	}

	if((port_type >= 0x00) && (port_type < MAX_PORT_ALLOWED))
	{
		ll_config.config_struct.modbus_port[port_num].port_type = port_type;	
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_port_delimiter
 * \	  : Get Stored Delimiter for Modbus Command
 * \param	NA
 * \return	Return stored Delimiter for Modbus Command
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_port_delimiter( unsigned char port_num )
{
	unsigned char ret_val=0;

	ret_val=ll_config.config_struct.modbus_port[port_num].port_delimiter;
	
	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_port_delimiter
 * \	  : It will set Delimiter Value to detect EoF of any command response
 * \param	port_delimiter: Set Port Delimiter
 * \return	NA
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
void set_port_delimiter( unsigned char port_num, unsigned char port_delimiter )
{
	ll_config.config_struct.modbus_port[port_num].port_delimiter = port_delimiter;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_port_parameter
 * \	  : Get Port Parameter like baudrate, start/stop bit, priority/data bits etc
 * \param	port_config_param: To hold the Port Parameters
 * \return	Return Port Parameter present of not: 0-Present otherwise 1
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_port_parameter( unsigned char port_num, unsigned char *port_config_param )
{
	unsigned char ret_val = 0x01;

	if(port_num>=MAX_PORT_ALLOWED)
	{
		return ret_val;
	}

	if(ll_config.config_struct.modbus_port[port_num].port_parameter[0]==0x00)
	{
		return ret_val;
	}
	else
	{
		ret_val = 0x00;
		strcpy(port_config_param,ll_config.config_struct.modbus_port[port_num].port_parameter);
	}
	
	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_port_parameter
 * \ 	  : To update Port Parameters in Device Flash Memory
 * \param	port_config_param:  Update Port Parameters
 * \return	NA
 * \info date       : 18-Sept-2020
 * \info author     : Ashutosh
 */
void set_port_parameter( unsigned char port_num, unsigned char *port_config_param)
{
	if(port_num>=MAX_PORT_ALLOWED)
	{
		return;
	}

	if(port_config_param==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.modbus_port[port_num].port_parameter, port_config_param);
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_modbus_command
 * \	  : Get Stored ModBus Command
 * \param	modbus_command: To hold the Stored ModBus Command
 * \return	Return Modbus Command present of not: 0-Present otherwise 1
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_modbus_command( unsigned char port_num, unsigned char *modbus_command )
{
	unsigned char ret_val=1;

	if(port_num>=MAX_PORT_ALLOWED)
	{
		return ret_val;
	}

	if(ll_config.config_struct.modbus_port[port_num].command[0]==0x00)
	{
		return ret_val;
	}
	else
	{
		ret_val = 0x00;
		strcpy(modbus_command,ll_config.config_struct.modbus_port[port_num].command);
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_modbus_command
 * \	  : To update Modbus Command(s) in device flash memory
 * \param	modbus_command:  Update Modbus Command(s)
 * \return	NA
 * \info date       : 17-Sept-2020
 * \info author     : Ashutosh
 */
void set_modbus_command( unsigned char port_num, unsigned char *modbus_command)
{
	if(port_num>=MAX_PORT_ALLOWED)
	{
		return;
	}

	if(modbus_command==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.modbus_port[port_num].command, modbus_command);
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_generator_data_config
 * \	  : Get the Generator Data Transmission settings
 * \param	NA
 * \return	Return Generator Data Packet Transmission is enable or disabled
 * \info date       : 30-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_generator_data_config( void )
{
	unsigned char ret_val=0;

	if(0x01 == ll_config.config_struct.enable_send_generator_data)
	{
		ret_val = 0x01;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_generator_data_config
 * \	  : Enable/Disable Generator Data Transmission settings
 * \param	config_gen_data_transmit: Set or Clear Generator Data Transmission settings
 * \return	NA
 * \info date       : 30-Sept-2020
 * \info author     : Ashutosh
 */
void set_generator_data_config( unsigned char config_gen_data_transmit)
{
	if(0x01 == config_gen_data_transmit)
	{
		ll_config.config_struct.enable_send_generator_data = 0x01;
	}
	else
	{
		ll_config.config_struct.enable_send_generator_data = 0x00;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_log_run_hour_config
 * \	  : Get Run Hour Logging Settings
 * \param	NA
 * \return	Return Run Hour Logging is enable or disabled
 * \info date       : 30-Sept-2020
 * \info author     : Ashutosh
 */
unsigned char get_log_run_hour_config( void )
{
	unsigned char ret_val=0;

	if(0x01 == ll_config.config_struct.enable_log_persistant_run_hour)
	{
		ret_val = 0x01;
	}

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_log_run_hour_config
 * \	  : Enable/Disable Run Hour Logging settings
 * \param	config_run_hour: Set or Clear Logging Run Hour settings
 * \return	NA
 * \info date       : 30-Sept-2020
 * \info author     : Ashutosh
 */
void set_log_run_hour_config( unsigned char config_run_hour)
{
	if(0x01 == config_run_hour)
	{
		ll_config.config_struct.enable_log_persistant_run_hour = 0x01;
	}
	else
	{
		ll_config.config_struct.enable_log_persistant_run_hour = 0x00;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_gsm_module_type_config
 * \	  : GET GSM Module Type Settings
 * \param	NA
 * \return	Return GSM Module Type it is 3 for 3G or 4 for 4G
 * \info date       : 15-Oct-2020
 * \info author     : Ashutosh
 */
unsigned char get_gsm_module_type_config( void )
{
	if((0x03 == ll_config.config_struct.gsm_module_type) || 
	   (0x04 == ll_config.config_struct.gsm_module_type))
	{
		return ll_config.config_struct.gsm_module_type;
	}

	return 0x00;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_gsm_module_type_config
 * \	  : Configure it is 3G Supported GSM Module or 4G Supported
 * \param	gsm_module_type: GSM Module type settings
 * \return	NA
 * \info date       : 15-Oct-2020
 * \info author     : Ashutosh
 */
void set_gsm_module_type_config( unsigned char gsm_module_type)
{
	if((0x03 == gsm_module_type)||(0x04 == gsm_module_type))
	{
		ll_config.config_struct.gsm_module_type = gsm_module_type;
	}
	else
	{
		ll_config.config_struct.gsm_module_type = 0x00;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_gsm_sim_slot_config
 * \	  : GET SIM Slot in which SIM is inserted
 * \param	NA
 * \return	Return SIM Slot in which SIM is inserted
 * \info date       : 21-Oct-2020
 * \info author     : Ashutosh
 */
unsigned char get_gsm_sim_slot_config( void )
{
	if((0x00 == ll_config.config_struct.gsm_sim_slot) || 
	   (0x01 == ll_config.config_struct.gsm_sim_slot))
	{
		return ll_config.config_struct.gsm_sim_slot;
	}

	return 0x00;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_gsm_sim_slot_config
 * \	  : Configure which SIM slot, SIM is inserted
 * \param	gsm_sim_slot: GSM SIM Slot settings
 * \return	NA
 * \info date       : 21-Oct-2020
 * \info author     : Ashutosh
 */
void set_gsm_sim_slot_config( unsigned char gsm_sim_slot)
{
	if((0x00 == gsm_sim_slot)||(0x01 == gsm_sim_slot))
	{
		ll_config.config_struct.gsm_sim_slot = gsm_sim_slot;
	}
	else
	{
		ll_config.config_struct.gsm_sim_slot = 0x00;
	}
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_local_gateway_ip
 * \	  : Get Local Gateway IP Address
 * \param	local_gateway_ip: To hold the Local Gateway IP
 * \return	Return Gateway IP Address present or not: 0-Present otherwise 1
 * \info date       : 15-OCT-2020
 * \info author     : Ashutosh
 */
unsigned char get_local_gateway_ip( unsigned char *local_gateway_ip )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.local_gateway_ip[0])
	{
		return ret_val;
	}

	strcpy(local_gateway_ip, ll_config.config_struct.local_gateway_ip);
	ret_val = 0x00;

	return ret_val;
}
/*----------------------------------------------------------------------------*/
/**
 * \brief	set_local_gateway_ip
 * \	  : To update Local Gateway IP Address
 * \param	local_gateway_ip:  Update local Gateway IP Address
 * \return	NA
 * \info date       : 15-Oct-2020
 * \info author     : Ashutosh
 */
void set_local_gateway_ip( unsigned char *local_gateway_ip)
{
	if(local_gateway_ip==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.local_gateway_ip, local_gateway_ip);
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_local_dns_address
 * \	  : Get Local DNS Address
 * \param	local_dns_add: To hold the Local DNS Address
 * \return	Return DNS Address present or not: 0-Present otherwise 1
 * \info date       : 15-OCT-2020
 * \info author     : Ashutosh
 */
unsigned char get_local_dns_address( unsigned char *local_dns_add )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.local_dns[0])
	{
		return ret_val;
	}

	strcpy(local_dns_add, ll_config.config_struct.local_dns);
	ret_val = 0x00;

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_local_dns_address
 * \	  : To update Local DNS  Address
 * \param	local_dns_add:  Update local DNS Address
 * \return	NA
 * \info date       : 15-Oct-2020
 * \info author     : Ashutosh
 */
void set_local_dns_address( unsigned char *local_dns_add)
{
	if(local_dns_add==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.local_dns, local_dns_add);
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	get_local_subnet_mask
 * \	  : Get Local Subnet Mask
 * \param	local_subnet_mask: To hold the Local Subnet MAsk
 * \return	Return Subnet Mask present or not: 0-Present otherwise 1
 * \info date       : 15-OCT-2020
 * \info author     : Ashutosh
 */
unsigned char get_local_subnet_mask( unsigned char *local_subnet_mask )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.local_subnet_mask[0])
	{
		return ret_val;
	}

	strcpy(local_subnet_mask, ll_config.config_struct.local_subnet_mask);
	ret_val = 0x00;

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 * \brief	set_local_subnet_mask
 * \	  : To update Local Subnet Mask 
 * \param	local_subnet_mask:  Update local Subnet Mask
 * \return	NA
 * \info date       : 15-Oct-2020
 * \info author     : Ashutosh
 */
void set_local_subnet_mask( unsigned char *local_subnet_mask)
{
	if(local_subnet_mask==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.local_subnet_mask, local_subnet_mask);
}

/*----------------------------------------------------------------------------*/
/**
 \ @brief	get_fota_host_address	
 \ @		Get Server Address for FOTA
 \ @param	host_address: To hold Server Address
 \ @param	len: supplied buffer length
 \ @return	Return IMEI Number present of not: 0-Present otherwise 1
 \ @info date       : 09-Nov-2020
 \ @info author     : Ashutosh
 */
unsigned char get_fota_host_address( unsigned char *host_address, int len )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.fota_server_add[0])
	{
		return ret_val;
	}
	
	if(len < sizeof(ll_config.config_struct.fota_server_add)) {
		return ret_val;
	}

	strncpy(host_address, ll_config.config_struct.fota_server_add, sizeof(ll_config.config_struct.fota_server_add));
	ret_val = 0x00;

	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 \ @brief	set_fota_host_address	
 \ @	  : To update Server Address for FOTA
 \ @param	host_address:  Update Server Address
 \ @return	NA
 \ @info date       : 09-Nov-2020
 \ @info author     : Ashutosh
 */
void set_fota_host_address( unsigned char *host_address)
{
	if(host_address==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.fota_server_add, host_address);
}

/*----------------------------------------------------------------------------*/
/**
 \ @brief	get_fota_host_port_number
 \ @	  : FoTA Port Number Configured
 \ @param	NA
 \ @return	Return FoTA Server Configured Port Number
 \ @info date       : 09-NOV-2020
 \ @info author     : Ashutosh
 */
unsigned short get_fota_host_port_number( void )
{
	unsigned short ret_val=0;

	ret_val = ll_config.config_struct.fota_server_port_number;

	return ret_val;
}
/*----------------------------------------------------------------------------*/
/**
 \ @brief	set_fota_host_port_number
 \ @	  : Set FoTA Port Number
 \ @param	port: Server Port Number
 \ @return	NA
 \ @info date       : 09-NOV-2020
 \ @info author     : Ashutosh
 */
void set_fota_host_port_number( unsigned short port)
{
	ll_config.config_struct.fota_server_port_number = port;
}
/*----------------------------------------------------------------------------*/
/**
 \ @brief	get_fota_host_name	
 \ @	  :	Get Fota server host name
 \ @param	fota_server_host_name: container to hold server host name
 \ @param	len: buffer size
 \ @param	0 if present, 1 if not
 \ @info date       : 4-DEC-2020
 \ @info author     : Kartik
 */
unsigned char get_fota_host_name( unsigned char *fota_server_host_name, int len)
{
	unsigned char ret_val = 0x01;
	if(NULL == fota_server_host_name) {
		return ret_val;	
	}
	
	if(len < sizeof(ll_config.config_struct.fota_server_hostname)) {
		return ret_val;
	}
	
	if(0x00 == ll_config.config_struct.fota_server_hostname[0])
	{
		return ret_val;
	}
	
	strncpy(fota_server_host_name, ll_config.config_struct.fota_server_hostname, sizeof(ll_config.config_struct.fota_server_hostname));
	
	ret_val = 0x00;
	return ret_val;
}
/*----------------------------------------------------------------------------*/
/**
 \ @brief	set_fota_host_name
 \ @	  : Set FoTA Host Name
 \ @param	hostname: ptr to hostname
 \ @return	NA
 \ @info date       : 4-DEC-2020
 \ @info author     : Kartik
 */
void set_fota_host_name(char *hostname)
{
	if(hostname == NULL)
	{
		return;
	}
	
	strncpy(ll_config.config_struct.fota_server_hostname, hostname, sizeof(ll_config.config_struct.fota_server_hostname));
}
/*----------------------------------------------------------------------------*/
/**
 \ @brief	get_fota_host_ip_address	
 \ @	  :	Get Fota server host ip address
 \ @param	fota_server_host_ip_address: container to hold server host ip address
 \ @param	len: buffer size
 \ @return  0 if present, 1 is not present
 \ @info date       : 4-DEC-2020
 \ @info author     : Kartik
 */
unsigned char get_fota_host_ip_address( unsigned char *fota_server_host_ip_address, int len)
{
	unsigned char ret_val = 0x01;
	if(NULL == fota_server_host_ip_address) {
		return ret_val;	
	}
	
	if(len < sizeof(ll_config.config_struct.fota_server_ip_add)) {
		return ret_val;
	}
	
	if(0x00 == ll_config.config_struct.fota_server_ip_add[0])
	{
		return ret_val;
	}

	strncpy(fota_server_host_ip_address, ll_config.config_struct.fota_server_ip_add, sizeof(ll_config.config_struct.fota_server_ip_add));
	ret_val = 0x00;
	return ret_val;
}
/*----------------------------------------------------------------------------*/
/**
 \ @brief	set_fota_host_ip_address
 \ @	  : Set FoTA Host IP Address
 \ @param	host_ip_address: ptr to ip address
 \ @return	NA
 \ @info date       : 4-DEC-2020
 \ @info author     : Kartik
 */
void set_fota_host_ip_address(char *host_ip_address)
{
	if(host_ip_address == NULL)
	{
		return;
	}
	
	strncpy(ll_config.config_struct.fota_server_ip_add, host_ip_address, sizeof(ll_config.config_struct.fota_server_ip_add));
}

/*----------------------------------------------------------------------------*/
/**
 \ @brief	get_firmware_name
 \ @	  : Get firmware name to download from FoTA Server
 \ @param	fw_name: To hold Firmware name
 \ @return	Return Firmware Name is present of not: 0-Present otherwise 1
 \ @info date       : 09-Nov-2020
 \ @info author     : Ashutosh
 */
unsigned char get_firmware_name( unsigned char *fw_name )
{
	unsigned char ret_val = 0x01;

	if(0x00 == ll_config.config_struct.firmware_name[0])
	{
		return ret_val;
	}

	strcpy(fw_name, ll_config.config_struct.firmware_name);
	ret_val = 0x00;
	return ret_val;
}

/*----------------------------------------------------------------------------*/
/**
 \ @brief	set_firmware_name
 \ @	  : To update firmware name to download from FoTA Server
 \ @param	fw_name:  To Update Firmware name 
 \ @return	NA
 \ @info date       : 09-Nov-2020
 \ @info author     : Ashutosh
 */
void set_firmware_name( unsigned char *fw_name)
{
	if(fw_name==NULL)
	{
		return;
	}

	strcpy(ll_config.config_struct.firmware_name, fw_name);
}
/*----------------------------------------------------------------------------*/


void save_config_parameters( void )
{
	sg_sprintf(LOG_GLOBAL_BUFFER, "Storing Config Data in Persistent Memory !!!");
	info_log(LOG_GLOBAL_BUFFER);
	write_record_on_persistent_storage(CONFIG_RECORDS_LOWER_INDEX, ll_config.config_data, sizeof(ll_config.config_data), RECORD_TYPE_CONFIG);
}

static void get_config_parameters( char hardFormat)
{
	sg_sprintf(LOG_GLOBAL_BUFFER, "Fetching configurations from Flash Memory!!!");
	info_log(LOG_GLOBAL_BUFFER);
	
	read_record_from_persistent_storage(CONFIG_RECORDS_LOWER_INDEX, ll_config.config_data, sizeof(ll_config.config_data), RECORD_TYPE_CONFIG);
	
	if(hardFormat == 0x00)
	{
		
		if(get_flash_memory_formatted_status() != 0x00)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Memory is not formatted, So Formating the memory!!!");
			info_log(LOG_GLOBAL_BUFFER);
			ll_clear_flash_memory();
			setConfigDefault();
			save_config_parameters();
		}
		else
		{
			//for testing
			//setConfigDefault();
			//save_config_parameters();
			
			sg_sprintf(LOG_GLOBAL_BUFFER, "Memory Format Identifier Matched, No Need to format!!!");
			info_log(LOG_GLOBAL_BUFFER);
		}
		
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Memory is not formatted, So Formating the memory!!!");
		info_log(LOG_GLOBAL_BUFFER);
		ll_clear_flash_memory();
		setConfigDefault();
		save_config_parameters();
	}
}

unsigned char updateSystemConfigurations(char hardFormat)
{
	get_config_parameters(hardFormat);
	printConfigValue();
}

CONFIG_S* getCurrentConfig( void )
{
	return (&ll_config.config_struct);	
}

#endif