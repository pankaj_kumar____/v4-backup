/*
 * fwUpgradeSmOperations.c
 *
 * Created: 4/10/2020 5:06:20 PM
 *  Author: Kartik.Gupta
 */ 

#include "fwUpgradeSmOperations.h"
#include "httpUtils.h"
#include "internalFlash.h"
#include "externalFlash.h"
#include "md5Validation.h"
#include "common.h"
#include "userTimer.h"
#include "rstc.h"

#define MAX_TIMEOUT_COUNT_SOCKET_READ	40	/* 1 timeout is 1 sec as implemented in gsm socket read api, ethernet need to verify*/ 

static firmwareHeader_s gFirmwareFileInfo;	/*information of firmware executable file*/
static volatile char gHttpReadTimeout;	/*socket read timeout for non blocking read*/

static void setHttpTimeout(void)
{
	gHttpReadTimeout = 1;
}

static void clearHttpTimeout(void)
{
	gHttpReadTimeout = 0;	
}

static void timeoutHandler()
{
	/* raise timeout flag*/
	setHttpTimeout();	
}

int initFwUpgradeSmOperations(char *serverHostName, char *serverIpAddress, int port)
{	
	/* initialize external flash*/
	int rc = externalFlashInit();
	if(OPER_STATE_SUCCESS != rc) {
		info_log("error: external flash init failed\r\n");
		return OPER_STATE_FLASH_INIT;
	}
	
	/* initialize http utils module*/
	rc = initHttpUtils(serverHostName, serverIpAddress, port);
	if(rc != OPER_STATE_SUCCESS) {
		info_log("error: http init failed\r\n");
		return rc;
	}
	
	/* initialize the timer*/
	timerInit(timeoutHandler);
	
	return OPER_STATE_SUCCESS;
}

/* generate request and read data*/
/* return 0 for success, 1 for failure*/
static int writeReadHttpData(unsigned int chunkStart, unsigned int chunkBytes, unsigned char *fileBuff)
{
	char httpHeaderReceived = 0;
	int ret;
	unsigned int bytesReceivedInSingleRequest,		/* from non-blocking read api*/
						totalBytesReceived = 0,		/* header + file data*/
						fileBytesReceived = 0,		/* firmware file bytes*/
						remainingFileBytes = 0,		/* remaining chunk bytes of file, without header*/
						totalBytesToBeReceived = 0;	/* header + requested chunk bytes(file bytes)*/
						
	unsigned char readBuff[MAX_CHUNK_SIZE / 2];
	unsigned char dataBuff[MAX_CHUNK_SIZE];
	httpHeaderInfo_t httpInfo;
		
	memset(readBuff, '\0', sizeof(readBuff));
	memset(&httpInfo, '\0', sizeof(httpHeaderInfo_t));
	memset(dataBuff, '\0', sizeof(dataBuff));
	
	if(!fileBuff) {
		return 1;
	}
	
	ret = generateHttpChunkRequest(chunkStart, chunkBytes);
	if(ret != OPER_STATE_SUCCESS) {
		return 1;
	}
	
	clearHttpTimeout();
	
	/* start timer to monitor read timeout*/
	timerStart(MAX_TIMEOUT_COUNT_SOCKET_READ);
	
	while(1) {
		/* come out of loop when read timeout*/
		if(1 == gHttpReadTimeout) {
			info_log("timeout");
			return 1;
		}
		
		if(fileBytesReceived == chunkBytes) {
			timerStop();
			return 0;
		}
		
		/* its non blocking read call, will return immediately, api have its own delay*/
		ret = httpRead(readBuff, sizeof(readBuff), &bytesReceivedInSingleRequest);
		if(ret == OPER_STATE_SOCKET_READ) {
			/* read error*/
			timerStop();
			return 1;
		} else if(ret == OPER_STATE_SOCKET_READ_TIMEOUT || ret == OPER_STATE_SUCCESS) {	
			
			/* http header overflow detected*/
			if(0 == httpHeaderReceived && (totalBytesReceived > MAX_CHUNK_SIZE || \
					((totalBytesReceived == MAX_CHUNK_SIZE) && bytesReceivedInSingleRequest != 0))) {
				timerStop();
				return 1;
			}
			
			/*save in buffer whatever data is read*/
			if(0 == httpHeaderReceived) {
				memcpy(dataBuff + totalBytesReceived, readBuff, bytesReceivedInSingleRequest);
				totalBytesReceived = totalBytesReceived + bytesReceivedInSingleRequest;
			} else {
				memcpy(fileBuff + fileBytesReceived, readBuff, bytesReceivedInSingleRequest);
				fileBytesReceived = fileBytesReceived + bytesReceivedInSingleRequest;
// 				sg_sprintf(LOG_GLOBAL_BUFFER, "file bytes recv: %d", fileBytesReceived);
// 				info_log(LOG_GLOBAL_BUFFER);
			}
			memset(readBuff, '\0', sizeof(readBuff));
			
			/* if http header received successfully*/
			if(0 == httpHeaderReceived) {
				ret = parseHttpRespHeader(dataBuff, totalBytesReceived, &httpInfo);
				if(ret == OPER_STATE_SUCCESS) {
					httpHeaderReceived = 1;
					
					/* verify whether header info is as expected*/
					if(httpInfo.startChunkByte == chunkStart &&\
						httpInfo.finishChunkByte == (chunkStart + chunkBytes - 1) &&\
						httpInfo.contentLength == chunkBytes &&\
						httpInfo.httpRespCode == HTTP_CODE_PARTIAL_CONTENT) {
							/* copy any received file data in user buffer*/
							fileBytesReceived = httpInfo.httpRespFileLength;
							memcpy(fileBuff, dataBuff + httpInfo.httpRespHeaderLength, fileBytesReceived);
							memset(dataBuff, '\0', sizeof(dataBuff));
							memcpy(dataBuff, fileBuff, fileBytesReceived);
					} else {
						timerStop();
						return 1;
					}
				} else if (ret == OPER_STATE_HTTP_REQUEST) {
					/* received invalid request code*/
					timerStop();
					return 1;
				} else {
					/* http response header buffer incomplete --> read further*/
				}
			}
			
			/* go to read further*/
			continue;
		}
	}
	
	timerStop();
	return 0;
}

FwUpgradeEvents_e fwAvailableRequest(void)
{
	/* send the initial request to get the firmware file details*/
	int ret;
	unsigned char dataBuff[sizeof(firmwareHeader_s)];
	
	ret = writeReadHttpData(0, sizeof(firmwareHeader_s), dataBuff);
	if(ret != 0) {
		return EVENT_FAILURE;
	}
	
	memcpy(&gFirmwareFileInfo, dataBuff, sizeof(firmwareHeader_s));
	
	info_log("***Server Bin File Info***\r\n");
	for(int i = 0; i < 24; i++) {
		sg_sprintf(LOG_GLOBAL_BUFFER, "%02X", dataBuff[i]);
		info_log(LOG_GLOBAL_BUFFER);
	}
	
	return EVENT_SUCCESS;
}

FwUpgradeEvents_e fwUpgradeVerification(void)
{
	/* just for testing we are not comparing versions here, to compare, just comment the line*/
	//return EVENT_UPGRADE_REQ;	
		
	/* check for the current running firmware version*/
	firmwareHeader_s currentRunningFwInfo;
	int state;
	char fetchedFwVersion[5];
	char currentFwVersion[5];
	float currFwVer, fetchFwVer;
	
	state = getFirmwareHeader(&currentRunningFwInfo, 1);
	if(state != OPER_STATE_SUCCESS) {
		info_log("error: reading curr fw header");
		return EVENT_UPGRADE_NOT_REQ;
	}
	
	memset(fetchedFwVersion, '\0', sizeof(fetchedFwVersion));
	memset(currentFwVersion, '\0', sizeof(currentFwVersion));
	
	memcpy(fetchedFwVersion, gFirmwareFileInfo.firmwareVersion, 4);
	memcpy(currentFwVersion, currentRunningFwInfo.firmwareVersion, 4);

	currFwVer = atof(currentFwVersion);
	fetchFwVer = atof(fetchedFwVersion);
	
	if(fetchFwVer > currFwVer || (currFwVer == 99.9f && fetchFwVer == 0.01f)) { /* version rollover over exhaust*/
		return EVENT_UPGRADE_REQ;
	} else {
		return EVENT_UPGRADE_NOT_REQ;
	}
}

FwUpgradeEvents_e fwDownload(void)
{
	/* see the last downloaded chunk available from flash*/
	/* and start getting chunk accordingly*/
	
	lastFwUpdateInfo_s lastFwUpdateInfo;
	
	unsigned int startChunkNo, totalChunks, writtenChunks, remainingChunks, fallBackWrittenChunk, \
					totalBytes, writtenBytes, remainingBytes, nextRequestBytes, nextRequestStartByte;
	int ret;
	unsigned char dataBuff[MAX_CHUNK_SIZE];
	memset(dataBuff, '\0', sizeof(dataBuff));
	
	totalBytes = gFirmwareFileInfo.firmwareSize;
	totalChunks = totalBytes / MAX_CHUNK_SIZE;
	if(totalBytes % MAX_CHUNK_SIZE != 0) {
		totalChunks = totalChunks + 1;
	}
	
	ret = getLastFwUpdateInfo(&lastFwUpdateInfo);
	if(ret != OPER_STATE_SUCCESS) {
		startChunkNo = 1;
		writtenBytes = 0;
		remainingBytes = totalBytes;
	}
	
	for(int i = 0; i < 16; i++) {
		sg_sprintf(LOG_GLOBAL_BUFFER, "%02X || %02X", lastFwUpdateInfo.lastFwUpdateHeader.firmwareMd5Hash[i], \
																			gFirmwareFileInfo.firmwareMd5Hash[i]);
		info_log(LOG_GLOBAL_BUFFER);
	}

	/* check eligibility to resume download*/
	if(0 != memcmp(&gFirmwareFileInfo.firmwareMd5Hash, \
						&lastFwUpdateInfo.lastFwUpdateHeader.firmwareMd5Hash, 16)) {
		/* start from beginning*/
		startChunkNo = 1;
		writtenBytes = 0;
		remainingBytes = totalBytes;							
	} else {
		/* resume previous*/
		if(lastFwUpdateInfo.lastSuccessfullyWrittenChunk > totalChunks) {
			startChunkNo = 1;
			writtenBytes = 0;
			remainingBytes = totalBytes;	
		} else {
			startChunkNo = lastFwUpdateInfo.lastSuccessfullyWrittenChunk + 1;
		}
	}
	
// 	/* to test download firmware from beginning bypassing resumability*/ 
// 	startChunkNo = 1;
// 	writtenBytes = 0;
// 	remainingBytes = totalBytes;
//
	
	writtenChunks = startChunkNo - 1;
	if(writtenChunks == totalChunks) {
		return EVENT_SUCCESS;
	}
	
	fallBackWrittenChunk = writtenChunks - ((int)(writtenChunks) % EXTERNAL_FLASH_CHUNK_OFFSET);
	remainingChunks = totalChunks - fallBackWrittenChunk;
	
	writtenBytes = fallBackWrittenChunk * MAX_CHUNK_SIZE;
	remainingBytes = totalBytes - writtenBytes;
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Starting from: %d KB", fallBackWrittenChunk * MAX_CHUNK_SIZE);
	info_log(LOG_GLOBAL_BUFFER);
	
	for(int i = 0; i < remainingChunks; i++) {
		if(remainingBytes >= MAX_CHUNK_SIZE) {
			nextRequestBytes = MAX_CHUNK_SIZE;
		} else {
			nextRequestBytes = remainingBytes;
		}
		
		ret = writeReadHttpData(writtenBytes + sizeof(firmwareHeader_s), nextRequestBytes, dataBuff);
		if(ret != 0) {
			return EVENT_FAILURE;
		}
		
		if(0 == i) {
			ret = externalFlashAppWrite(dataBuff, gFirmwareFileInfo.firmwareSize, 1, 0, fallBackWrittenChunk);
		} else {
			ret = externalFlashAppWrite(dataBuff, gFirmwareFileInfo.firmwareSize, 0, 0, fallBackWrittenChunk);
		}
		
		if(ret != OPER_STATE_SUCCESS) {
			return EVENT_FAILURE;	
		}
		
		writtenBytes = writtenBytes + nextRequestBytes;
		remainingBytes = totalBytes - writtenBytes;
		
		/* update config*/
		lastFwUpdateInfo.lastSuccessfullyWrittenChunk = fallBackWrittenChunk + i + 1;
		memcpy(&lastFwUpdateInfo.lastFwUpdateHeader, &gFirmwareFileInfo, sizeof(firmwareHeader_s));
		if(OPER_STATE_SUCCESS != setLastFwUpdateInfo(&lastFwUpdateInfo)) {
			return EVENT_FAILURE;
		}
		
		sg_sprintf(LOG_GLOBAL_BUFFER, "Downloaded: %d Bytes || Remaining: %d Bytes", writtenBytes, remainingBytes);
		info_log(LOG_GLOBAL_BUFFER);
	}
	
	return EVENT_SUCCESS;
}

FwUpgradeEvents_e fwValidate(void)
{
	MD5_CTX md5Operation;
	char md5Value[16];
	char *calculatedMd5;
	unsigned int totalBytes, totalChunks;
	int ret;
	char fetchedMd5[16];
	unsigned char dataBuff[MAX_CHUNK_SIZE];
	
	memset(fetchedMd5, '\0', sizeof(fetchedMd5));
	memcpy(fetchedMd5, gFirmwareFileInfo.firmwareMd5Hash, 16);
	
	totalBytes = gFirmwareFileInfo.firmwareSize;
	totalChunks = totalBytes / MAX_CHUNK_SIZE;
	if(totalBytes % MAX_CHUNK_SIZE != 0) {
		totalChunks = totalChunks + 1;
	}
	
	for(int i = 0; i < totalChunks; i++) {
		if(i == 0) {
			ret = externalFlashAppRead(dataBuff, totalBytes, 1, 0);
			if(ret != OPER_STATE_SUCCESS) {
				return EVENT_FAILURE;
			}
			
			ret = calculateAndGetMd5Value(&md5Operation, dataBuff, totalBytes, 1, md5Value);
			if(ret != OPER_STATE_SUCCESS) {
				return EVENT_FAILURE;
			}		
		} else {
			ret = externalFlashAppRead(dataBuff, totalBytes, 0, 0);
			if(ret != OPER_STATE_SUCCESS) {
				return EVENT_FAILURE;
			}
			
			ret = calculateAndGetMd5Value(&md5Operation, dataBuff, totalBytes, 0, md5Value);
			if(ret != OPER_STATE_SUCCESS) {
				return EVENT_FAILURE;
			}
		}
	}
	
	calculatedMd5 = getLastMd5Value();
	if(NULL == calculatedMd5) {
		info_log("error: invalid buffer");
		return EVENT_FAILURE;
	}
	
	for(int i = 0; i < 16; i++) {
		if(calculatedMd5[i] != fetchedMd5[i]) {
			info_log("error: md5 validation failed");
			return EVENT_FAILURE;
		}
	}
	return EVENT_SUCCESS;
}

FwUpgradeEvents_e fwUpdateConfig(void)
{
	int state;
	int size = sizeof(exSysBootConfig_s);
	unsigned char config[size];
	exSysBootConfig_s *cfg;
	
	/* for efficiency read full config and set since setting 2 configurations in a go*/
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		info_log("error: reading boot config");
		return EVENT_FAILURE;
	}
	
	cfg = (exSysBootConfig_s*)config;
	
	/* update new firmware header info*/
	memcpy(&cfg->newFirmwareHeader, &gFirmwareFileInfo, sizeof(firmwareHeader_s));
	
	/* set the availability flag for update*/
	cfg->isNewFirmwareAvaliable = 1;
	
	state = setExternalBootConfig((void*)cfg, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		info_log("error: writing boot config");
		return EVENT_FAILURE;
	}
	
	return EVENT_SUCCESS;
}

FwUpgradeEvents_e fwResetDevice(void)
{
	/* restart the device*/
	rstc_start_software_reset(RSTC);
	while(1);
	
	/* program should not reach here*/
	return EVENT_NA;
}

FwUpgradeEvents_e fwUpgradeExit(void)
{
	/* to do any exit tasks if required*/
	
	/* de init http utils module*/
	deInitHttpUtils();
	
	/* else return EVENT_NA*/
	return EVENT_NA;
}
	