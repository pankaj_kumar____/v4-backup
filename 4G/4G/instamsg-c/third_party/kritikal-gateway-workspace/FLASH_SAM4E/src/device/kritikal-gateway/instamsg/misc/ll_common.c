/*
 * ll_common.c
 *
 * Created: 12/4/2020 4:39:34 PM
 *  Author: Kartik.Gupta
 */ 


/*
 ****************************************************************************************
~#  Function Name : extractData                                                         #~ 
~#  Parameter1    : buffPtr: String received from GSM/GPS Module                        #~
~#  Parameter2    : locBuffCntr : Length/Size of the aforesaid string                   #~
~#  Parameter3    : tempBuff : for storage of extracted Data                            #~
~#  Parameter4    : delimeter : Delimiter is a point to start extraction of data        #~
~#  Parameter5    : etx : End of extraction point, i.e. End of Text                     #~
~#  Return Type   : None                                                                #~
~#  Scope         : Local                                                               #~
~#  Functionality : This function will extract information from the buffer and check    #~
~#                  first occurrence of Delimiter and start the extraction till the etx  #~
~#                  occurs. And store the extracted data into tempBuffer.               #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~ 
~# Date: #06/09/2020#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/
void extractData(unsigned char *buffPtr,unsigned char locBuffCntr,
				unsigned char *tempBuff,unsigned char delimeter,unsigned char etx)
{
    unsigned char iLoop,locCntr=0,sCopy=0;
	
    for(iLoop=0;iLoop<locBuffCntr;iLoop++)
	{
		/*!> Search for the Delimiter*/
        if((sCopy==0)&&(*(buffPtr+iLoop)==delimeter))
		{   
			/*!> If Occurred then Mark Copy Flag And Continue the Loop for next Character*/  
            sCopy=1;                                            
            continue;                                          
        }
		
		/*!> Check for Copy Flag, If it is marked*/
        if(sCopy==1)
		{ 
			/*!> Ignore Blank Spaces in starting of Data*/                                          
            if((*(buffPtr+iLoop)!=0x20)||(locCntr>1))
			{   
				/*!> If ETX Occurred, then stop copying the Data*/       
                if(*(buffPtr+iLoop)==etx)
				{   
					/*!> Store the total size of extracted data in fixed location*/                   
                    *(tempBuff+0)=locCntr;   
					*(tempBuff+iLoop)='\0';                    
                    return;                                  
                }  
				
				/*!> Copy the Data*/
                *(tempBuff+1+locCntr)=*(buffPtr+iLoop);
				/*!> Increase the pointer of extracted data*/
                locCntr++;                                      
            }
        }
    }        
}