/*
 * md5Validation.h
 *
 * Created: 3/30/2020 3:39:36 AM
 *  Author: Kartik.Gupta
 */ 


#ifndef MD5VALIDATION_H_
#define MD5VALIDATION_H_

#include "md5.h"

/*brief function to pass on buffer chunk to calculate md5 hash value
 *@param - md5OperBuff, ptr to operation buffer for md5 calculation
 *@param - dataBuff, ptr to the buffer data 
			NOTE:	to avoid errorneous calculation, max buff size is restricted to  MAX_CHUNK_SIZE
 *@param - maximumDataSize, expected data size to write
 *@param - operStart, flag to indicate to begin md5 calculation, if it is set
 *@param - container, ptr to the final hash value buffer, (size should be atleast 16 bytes)
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int calculateAndGetMd5Value(MD5_CTX *md5OperBuff, unsigned char* dataBuff, unsigned int maxDataSize, \
								char operStart, unsigned char* container);

/*brief function to get latest calculated md5 hash value
 *@return ptr to md5 hash value, NULL if no valid md5 available 
 */
unsigned char* getLastMd5Value(void);

#endif /* MD5VALIDATION_H_ */