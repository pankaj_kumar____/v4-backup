#include "../../instamsg/driver/include/instamsg.h"
#include "../../instamsg/driver/include/misc.h"

#include "../../ioeye/include/serial.h"


static int onConnect()
{
    runSerialOnConnectProcedures();
    return SUCCESS;
}


static void coreLoopyBusinessLogicInitiatedBySelf()
{
    runSerialProcedures();
}


void release_app_resources()
{
    releaseSerialInitializations();
}


int main(int argc, char** argv)
{
#if FILE_LOGGING_ENABLED == 1
    globalSystemInit("instamsg.log");
#else
    globalSystemInit(NULL);
#endif

    runSerialInitializations();
    start(onConnect, NULL, NULL, coreLoopyBusinessLogicInitiatedBySelf, 60);

    return 0;
}
