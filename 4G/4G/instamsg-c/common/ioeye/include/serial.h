#ifndef IOEYE_MODBUS_COMMON
#define IOEYE_MODBUS_COMMON

#include "device_serial.h"

#define SERIAL                  "[SERIAL] "
#define SERIAL_ERROR            "[SERIAL_ERROR] "
#define PORT_TYPE_RS232			0
#define PORT_TYPE_RS485			1
#define PORT_TYPE_TCP			2
#define PORT_NAME_RS232			"rs232"
#define PORT_NAME_RS485			"rs485"
#define PORT_NAME_TCP			"tcp"

struct SimulatedModbus
{
    unsigned char isSimulatedDevice;
    char identifier[50];
    void (*valueConcatenatorFunc)(char *payload, void *arg);
    void *valueConcatenatorFuncArg;
};

extern Serial serialInterfaces[MAX_PORTS_ALLOWED];
extern char *simulatedModbusValuesCumulated;
extern int num_ports_in_use;

typedef void (*VALUE_CONCATENATOR)(char *payload, void *);

/*
 * Global-functions callable.
 */
void runSerialInitializations(void);
void releaseSerialInitializations(void);

void init_serial(void *ser, unsigned char isSimulatedDevice, const char *identifier, const char *serial_params_identifier,
                 const char *serial_delimiter_identifier, const char *modbus_tcp_ip_address_identifier,
                 VALUE_CONCATENATOR valueConcatenatorFunc, void *valueConcatenatorFuncArg);
int release_serial(void *ser);

void runSerialOnConnectProcedures(void);
void serialOnConnectProcedures(void *ser);

void runSerialProcedures(void);
void serialProcedures(void *ser);

void resetSimulatedModbusEnvironment(int numberOfSimulatedInterfaces, char *functionCode);
void flushSimulatedModbusEnvironment(int numberOfSimulatedInterfaces, char *functionCode);

/*
	/brief returns port type converted number
	/param: portName, rs232 - 0, rs485 - 1, tcp - 2
	/return: -1 for failure, else port type number
*/
char portTypeNameToNumberConverter(char *portName);

/*
	/brief converts port name from the port number
	/param: portType, 0=>rs232 , 1=>rs485, 2=>tcp
	/return: NULL for failure, else port type name
*/
char* portTypeNumberToNameConverter(char portType);

#define DIGITAL_INPUTS_OUTPUTS_FUNCTION_CODE            "01"
#define ANALOG_INPUTS_FUNCTION_CODE                     "04"


/*
 * Must not be called directly.
 * Instead must be called as ::
 *
 *      modbus->send_command_and_read_response_sync
 */
int serial_send_command_and_read_response_sync(Serial *serial,
                                               unsigned char *commandBytes,
                                               int commandBytesLength,
                                               unsigned char *responseByteBuffer,
                                               int *responseBytesLength,
											   unsigned char *error_code);


/*
 * Internally Used.
 * Must not be called by anyone.
 */
void connect_underlying_serial_medium_guaranteed(Serial *serial);
int release_underlying_serial_medium_guaranteed(Serial *serial);

void parse_serial_connection_params(char *params_string,
                                    int *speed,
                                    int *parity,
                                    int *odd_parity,
                                    int *chars,
                                    int *blocking,
                                    int *two_stop_bits,
                                    int *hardware_control);

void processCommand(char *command, Serial *serial);

#endif
