#include "./include/serial.h"

#include "../../instamsg/driver/include/log.h"
#include "../../instamsg/driver/include/instamsg.h"
#include "../../instamsg/driver/include/sg_mem.h"
#include "../../instamsg/driver/include/misc.h"
#include "../../instamsg/driver/include/watchdog.h"
#include "../../instamsg/driver/include/config.h"
#include "../../instamsg/driver/include/globals.h"
#include "../../instamsg/driver/include/hex.h"
#include "../../instamsg/driver/include/sg_stdlib.h"
#include "../../instamsg/driver/include/json.h"
#include "../../instamsg/driver/include/socket.h"
#include "../../instamsg/driver/include/time.h"
#include "../../instamsg/driver/include/payload.h"

#include <string.h>

#define COMMAND_HEX_STRING_DONT_CARE    NULL
#define PAYLOAD_DONT_CARE               NULL

extern char *serialCommandUnderProcess;
static char simulatedModbusCommand[25];

extern unsigned int errorCase;


Serial serialInterfaces[MAX_PORTS_ALLOWED];
char *simulatedModbusValuesCumulated;
char *stringified_ioeye_data_prefix;

static char modbusTcpIPAddress[30];

#define NUM_PORTS_IN_USE            "NUM_PORTS_IN_USE"

int num_ports_in_use;

static unsigned char isModbusDevice(Serial *ser)
{
    if((ser->isSimulatedDevice) == 0)
    {
        if(strlen(ser->serialDelimiter) == 0)
        {
            return 1;
        }
    }

    return 0;
}


static unsigned char isModbusOverTcp(Serial *ser)
{
    if(strcmp(ser->portType, PORT_NAME_WIFI) == 0)
    {
        return 1;
    }

    return 0;
}


static int isWriteFunctionCode(char *commandNibbles)
{
    if( (   (commandNibbles[2] == '0') && (commandNibbles[3] == '5')    ) ||
        (   (commandNibbles[2] == '0') && (commandNibbles[3] == '6')    ) ||
        (   (commandNibbles[2] == '0') && (commandNibbles[3] == 'F')    ) ||
        (   (commandNibbles[2] == '0') && (commandNibbles[3] == 'f')    ) ||
        (   (commandNibbles[2] == '1') && (commandNibbles[3] == '0')    ) )
    {
        return SUCCESS;
    }

    return FAILURE;
}


static unsigned char isCoilsCode(char *functionCode)
{
    if(
            (strcmp(functionCode, "01") == 0) ||
            (strcmp(functionCode, "02") == 0) ||
            (strcmp(functionCode, "05") == 0) ||
            (strcmp(functionCode, "0f") == 0) ||
            (strcmp(functionCode, "0F") == 0)
      )
    {
        return 1;
    }

    return 0;
}


static int getNumberOfCharacterBytes(int numberOfEntities, char *functionCode)
{
    int numberOfCharacterBytes = numberOfEntities * 4;
    if(isCoilsCode(functionCode) == 1)
    {
        numberOfCharacterBytes = ( (numberOfEntities / 8) * 2);
        if( (numberOfEntities % 8) != 0)
        {
            numberOfCharacterBytes = numberOfCharacterBytes + 2;
        }
    }

    return numberOfCharacterBytes;
}


/*
 * Function-Codes being handled ::
 *
 *  FC 1:   11 01 0013 0025 0E84
 *  FC 2:   11 02 00C4 0016 BAA9
 *  FC 3:   11 02 00C4 0016 BAA9
 *  FC 4:   11 04 0008 0001 B298
 */
static int validationCheck(char *commandNibbles)
{
    int i;
    int rc = FAILURE;

    const char* functionCodes[] = {
                                        "01",
                                        "02",
                                        "03",
                                        "04",
                                        "05",
                                        "06",
                                        "0F",
                                        "10"
                                    };

    int commandNibblesLength = strlen(commandNibbles);
    if(commandNibblesLength < 12)
    {
        sg_sprintf(LOG_GLOBAL_BUFFER, PROSTR("%sModbus-Command Length less than 12"), SERIAL_ERROR);
        error_log(LOG_GLOBAL_BUFFER);

        return FAILURE;
    }

    for(i = 0; i < sizeof(functionCodes); i++)
    {
        if( (commandNibbles[2] == functionCodes[i][0]) && (commandNibbles[3] == functionCodes[i][1]) )
        {
            rc = SUCCESS;
            break;
        }
    }

    if(rc == FAILURE)
    {
        sg_sprintf(LOG_GLOBAL_BUFFER, SERIAL_ERROR "Modbus-Command-Code [%c%c] not one of 01 02 03 04 05 06 0F 10 in command [%s]",
                               commandNibbles[2], commandNibbles[3], commandNibbles);
        error_log(LOG_GLOBAL_BUFFER);
    }

    return rc;
}


static unsigned long getExpectedModbusResponseLength(Serial *serial, char *commandNibbles)
{
    int rc;
    unsigned long i = 0;
    short offset = 0;
    unsigned char isModbusTcp = 0;

    if(isModbusOverTcp(serial) == 1)
    {
        isModbusTcp = 1;
        offset = 12;    /* 6 additional bytes (!2 character-bytes) in Modbus-TCP Request/Response */
    }

    commandNibbles = commandNibbles + offset;

    rc = validationCheck(commandNibbles);
    if(rc != SUCCESS)
    {
        return rc;
    }

    if(isWriteFunctionCode(commandNibbles) == SUCCESS)
    {
        return (8 + (offset / 2));
    }


    /*
     * The 9, 10, 11, 12 nibbles contain the number of registers to be read.
     */
    i = i + (16 * 16 * 16 * getIntValueOfHexChar(commandNibbles[8]));
    i = i + (16 * 16 *      getIntValueOfHexChar(commandNibbles[9]));
    i = i + (16 *           getIntValueOfHexChar(commandNibbles[10]));
    i = i + (               getIntValueOfHexChar(commandNibbles[11]));

    {
        char function_code[3] = {0};

        function_code[0] = commandNibbles[2];
        function_code[1] = commandNibbles[3];

        i = getNumberOfCharacterBytes(i, function_code) / 2;
    }

    i = i + 3;      /* Id, Code, Bytes-Counter-Byte in the beginning*/

    if(isModbusTcp == 0)
    {
        i = i + 2;      /* 2 bytes for CRC in the end */
    }
    else
    {
        i = i + 6;      /* 6 bytes in the beginning; no CRC in the end  */
    }

    return i;
}


static void fillPrefixIndices(Serial *serial, char *commandNibbles, int *prefixStartIndex, int *prefixEndIndex)
{
    int rc = FAILURE;
    short offset = 0;

    if(isModbusOverTcp(serial) == 1)
    {
        offset = 12;
    }

    commandNibbles = commandNibbles + offset;

    rc = validationCheck(commandNibbles);
    if(rc != SUCCESS)
    {
        *prefixStartIndex = -1;
        *prefixEndIndex = -2;

        return;
    }

    *prefixStartIndex = 4 + offset;
    *prefixEndIndex = 7 + offset;
}


static void appendModbusCRC16(char *hexifiedModbusResponse, int maxLength)
{
    int i, pos;
    int len = strlen(hexifiedModbusResponse);
    unsigned int crc = 0xFFFF;


    if((len + 5) >= maxLength)
    {
        error_log(SERIAL_ERROR "Not enough space to fill-in CRC .. returning back");
        return;
    }

    sg_sprintf(LOG_GLOBAL_BUFFER, SERIAL "Hexified-Modbus-Response before adding CRC = %s", hexifiedModbusResponse);
    debug_log(LOG_GLOBAL_BUFFER);

    /*
     * First calculate the integer-CRC-value.
     */
    for (pos = 0; pos < len; pos = pos + 2)
    {
        unsigned int byteValue = 0;
        byteValue = byteValue + (16 * getIntValueOfHexChar(hexifiedModbusResponse[pos]));
        byteValue = byteValue + (1  * getIntValueOfHexChar(hexifiedModbusResponse[pos + 1]));

        crc ^= (unsigned int) byteValue;

        for (i = 8; i != 0; i--)
        {
            if ((crc & 0x0001) != 0)
            {
                crc >>= 1;
                crc ^= 0xA001;
            }
            else
            {
                crc >>= 1;
            }
        }
    }

    {
        char hexCRC[5] = {0};
        sg_sprintf(hexCRC, "%x", crc);
        addPaddingIfRequired(hexCRC, sizeof(hexCRC) - 1);


        /*
         * Finally, add the CRC in modbus-response (lower-byte first).
         */
        hexifiedModbusResponse[len]     = hexCRC[2];
        hexifiedModbusResponse[len + 1] = hexCRC[3];
        hexifiedModbusResponse[len + 2] = hexCRC[0];
        hexifiedModbusResponse[len + 3] = hexCRC[1];
    }

    sg_sprintf(LOG_GLOBAL_BUFFER, SERIAL "Hexified-Modbus-Response after  adding CRC = %s", hexifiedModbusResponse);
    debug_log(LOG_GLOBAL_BUFFER);
}


static void sendSerialErrorNotificationToServer(const char *errorType)
{
    memset(messageBuffer, 0, sizeof(messageBuffer));
    sg_sprintf(messageBuffer, PROSTR("%s Response for energy-meter-command [%s]"), errorType, serialCommandUnderProcess);

    publish(NOTIFICATION_TOPIC,
            messageBuffer,
            QOS0,
            0,
            NULL,
            MQTT_RESULT_HANDLER_TIMEOUT,
            1);

    memset(messageBuffer, 0, sizeof(messageBuffer));
}


struct serialErrorPOJO
{
    unsigned char *responseByteBuffer;
    int responseLength;
    Serial *serial;

    unsigned char noResponseCase;
};


static void* processSerialFailureCasesIfAtAll(void *arg)
{
    struct serialErrorPOJO *pojo = (struct serialErrorPOJO *) arg;
    pojo->noResponseCase = 0;

	
    if(1)
    {
        if(1)
        {
            /*
             * Check if we did not receive ANNNY bytes.
             * In that case, we presume that the device is not present, and we can move on to the next command.
             */
			
            unsigned char noResponseCase = 1;
            {
                int i;
                for(i = 0; i < pojo->responseLength; i++)
                {
                    if((pojo->responseByteBuffer)[i] != 0)
                    {
                        noResponseCase = 0;
                        break;
                    }
                }
            }

            if(noResponseCase == 1)
            {
                pojo->noResponseCase = 1;
            }
            else if(pojo->serial->serialCommands != NULL)
            {
                sendSerialErrorNotificationToServer(PROSTR("INVALID"));
                exitApp(0);
            }
        }
    }

    return NULL;
}


extern unsigned char rebootPending;
/*
static int request_response_over_modbus_tcp(Serial *serial,
                                            unsigned char *commandBytes,
                                            int commandBytesLength,
                                            unsigned char *responseByteBuffer,
                                            int *responseBytesLength)
{
    int rc = FAILURE;
    int bytes_read = 0;
    int serial_delimiter = 0;

    if((serial->modbusSocket).socketCorrupted == 1)
    {
        sg_sprintf(LOG_GLOBAL_BUFFER, "Modbus-TCP-Socket not-connected/failed for [%s].. marking this as no-response ...", serial->identifier);
        error_log(LOG_GLOBAL_BUFFER);

        while(time_fine_for_time_limit_function() == 1)
        {
            startAndCountdownTimer(1, 0);
        }

        goto exit;
    }

    rc = (serial->modbusSocket).write(&(serial->modbusSocket), commandBytes, commandBytesLength);
    if(rc == FAILURE)
    {
        (serial->modbusSocket).socketCorrupted = 1;
        rebootPending = 1;

        while(time_fine_for_time_limit_function() == 1)
        {
            startAndCountdownTimer(1, 0);
        }

        goto exit;
    }

    if((*responseBytesLength) == 0)
    {
        serial_delimiter = sg_atoi(serial->serialDelimiter);
    }

    {
        while(time_fine_for_time_limit_function() == 1)
        {
            if(serial_delimiter != 0)
            {
                if(bytes_read >= 1)
                {
                    if(responseByteBuffer[bytes_read - 1] == serial_delimiter)
                    {
                        break;
                    }
                }
            }
            else
            {
                if(bytes_read == (*responseBytesLength))
                {
                    break;
                }
            }


            rc = (serial->modbusSocket).read(&(serial->modbusSocket), responseByteBuffer + bytes_read, 1, 0);
            if(rc == SUCCESS)
            {
                bytes_read = bytes_read + 1;
            }
            else if(rc == SOCKET_READ_TIMEOUT)
            {
                startAndCountdownTimer(1, 0);
                continue;
            }
            else if(rc == FAILURE)
            {
                (serial->modbusSocket).socketCorrupted = 1;
                rebootPending = 1;

                startAndCountdownTimer(1, 0);
                continue;
            }
        }
    }

exit:
    *responseBytesLength = bytes_read;
    rc = SUCCESS;

    return rc;
}
*/
static int request_response_over_modbus_tcp(Serial *serial,
											unsigned char *commandBytes,
											int commandBytesLength,
											unsigned char *responseByteBuffer,
											int *responseBytesLength,
											unsigned char *error_no)
{
	int rc = FAILURE;
	int bytes_read = 0;
	int serial_delimiter = 0;
	unsigned char retryCounter=0;
	unsigned char readByte=0;
	*error_no = 0;
	
	external_wathdog_init();
	
	if((serial->modbusSocket).socketCorrupted == 1)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Modbus-TCP-Socket not-connected/failed for [%s].. marking this as no-response ...", serial->identifier);
		error_log(LOG_GLOBAL_BUFFER);

		*error_no = 105;
		goto exit;
	}
	while(++retryCounter <= 3)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER,"Sending Command on MODBus TCP, Attempt [%d]", retryCounter);
		error_log(LOG_GLOBAL_BUFFER);

		rc = (serial->modbusSocket).write(&(serial->modbusSocket), commandBytes, commandBytesLength);
		if(rc == FAILURE)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER,"Sending Command on MODBus TCP, Failed !!!");
			error_log(LOG_GLOBAL_BUFFER);
			
			*error_no = 105;
			goto exit;
		}

		if((*responseBytesLength) == 0)
		{
			serial_delimiter = sg_atoi(serial->serialDelimiter);
		}

		{
			while(time_fine_for_time_limit_function() == 1)
			{
				rc = (serial->modbusSocket).read(&(serial->modbusSocket), &readByte, 1, 0);

				if(SUCCESS == rc)
				{
					responseByteBuffer[bytes_read]= readByte;
					bytes_read++;
					if((*responseBytesLength)>0)
					{
						if((*responseBytesLength)<=bytes_read)
						{
							*error_no=0;
							goto exit;
						}
					}
					else
					{
						if(responseByteBuffer[bytes_read - 1] == serial_delimiter)
						{
							*error_no=0;
							goto exit;
						}
					}
				}
				else if(SOCKET_READ_TIMEOUT == rc)
				{
					break;
				}
				else if(FAILURE == rc)
				{
					(serial->modbusSocket).socketCorrupted = 1;
					*error_no = 105;

					goto exit;
				}
			}
		}
	}

	exit:
	
	external_watchdog_feed();
	
	if(0 == *error_no)
	{
		if(0 == bytes_read)
		{
			*error_no = 101;
		}
		else if(*responseBytesLength > bytes_read)
		{
			*error_no = 104;
		}
	}
	*responseBytesLength = bytes_read;
	rc = SUCCESS;

	return rc;
}

static void fillSerialCommandResponseIntoMessageBufferForSerialDevice(char *buf, char *commandHexString, Serial *serial,
                                                                      char *modbusTcpIPAddress)
{
    int rc = FAILURE;
    unsigned char *responseByteBuffer = NULL;
    int offset;

    {
        int prefixStartIndex = -1, prefixEndIndex = -2, i;
        int responseLength = 0;
		unsigned char error_case = 0;

        if(isModbusDevice(serial) == 1)
        {
            responseLength = getExpectedModbusResponseLength(serial, commandHexString);
            if(responseLength == FAILURE)
            {
                sg_sprintf(LOG_GLOBAL_BUFFER,
                          "Some problem occurred while processing modbus-command [%s]. Not continuing in this cycle", commandHexString);
                error_log(LOG_GLOBAL_BUFFER);

                goto exit;
            }

            fillPrefixIndices(serial, commandHexString, &prefixStartIndex, &prefixEndIndex);
        }

        if(responseLength == 0)
        {
            responseByteBuffer = (unsigned char*) sg_malloc(MAX_BUFFER_SIZE);
        }
        else
        {
            responseByteBuffer = (unsigned char*) sg_malloc(responseLength);
        }
        if(responseByteBuffer == NULL)
        {
            sg_sprintf(LOG_GLOBAL_BUFFER, "Could not allocate memory for serial-response-buffer :(");
            error_log(LOG_GLOBAL_BUFFER);

            goto exit;
        }
        if(responseLength == 0)
        {
            memset(responseByteBuffer, 0, MAX_BUFFER_SIZE);
        }
        else
        {
            memset(responseByteBuffer, 0, responseLength);
        }

        if(isModbusOverTcp(serial) == 1)
        {
            if((serial->modbusSocket).socketCorrupted == 0)
            {
                /*
                 * Close the earlier socket.
                 */
#if ENG_DEBUG
				sg_sprintf(LOG_GLOBAL_BUFFER, "Releasing old Socket....");
				error_log(LOG_GLOBAL_BUFFER);
#endif
				external_wathdog_init();
                mb_release_socket_simple_guaranteed(&(serial->modbusSocket));
				(serial->modbusSocket).socketCorrupted = 1;
				external_watchdog_feed();
            }
			
/*			if((serial->modbusSocket).transitionBuffer != NULL)
			{
				sg_free((serial->modbusSocket).transitionBuffer);
			}
			
*/            
            if(1)
            {
				external_wathdog_init();
				sg_sprintf(LOG_GLOBAL_BUFFER, "Initializing new Socket....");
				error_log(LOG_GLOBAL_BUFFER);
                strcpy(serial->hostAddress, modbusTcpIPAddress);
	            init__modbus_slave_socket(&(serial->modbusSocket), modbusTcpIPAddress, 502, SOCKET_TCP, 0);
				external_watchdog_feed();
            }
        }

        sg_sprintf(LOG_GLOBAL_BUFFER, "Processing MODBus-Command [%s]", commandHexString);
        info_log(LOG_GLOBAL_BUFFER);

        RESET_GLOBAL_BUFFER;
        getByteStreamFromHexString(commandHexString, GLOBAL_BUFFER);


        stringified_ioeye_data_prefix = NULL;

        if(isModbusOverTcp(serial) == 1)
        {
			watchdog_reset_and_enable(SERIAL_RESPONSE_TIMEOUT_SECS, "Getting-TCP-MODBus-Response", 0);
            rc = request_response_over_modbus_tcp(           serial,
                                                             GLOBAL_BUFFER,
                                                             strlen(commandHexString) / 2,
                                                             responseByteBuffer,
                                                             &responseLength,
                                                             &error_case);
        }
        else
        {
			watchdog_reset_and_enable(SERIAL_RESPONSE_TIMEOUT_SECS, "Getting-SERIAL-Response", 0);
            rc = serial->send_command_and_read_response_sync(serial,
                                                             GLOBAL_BUFFER,
                                                             strlen(commandHexString) / 2,
                                                             responseByteBuffer,
                                                             &responseLength,
															 &error_case);
        }
		watchdog_disable(NULL, NULL);
		if(error_case>100)
		{
			errorCase=error_case-100;
			goto exit;
		}


        if(rc != SUCCESS)
        {
			if(isModbusOverTcp(serial) == 1)
			{
				sg_sprintf(LOG_GLOBAL_BUFFER, "Problem occurred while fetching TCP MODBus-response... not proceeding further");
			}
			else
			{
				sg_sprintf(LOG_GLOBAL_BUFFER, "Problem occurred while fetching SERIAL-response... not proceeding further");
			}
            error_log(LOG_GLOBAL_BUFFER);

            goto exit;
        }


        /*
         * Now, start filling the final serial-response.
         * We always, irrespective of the port-type, fill hexified-response.
         */
        RESET_GLOBAL_BUFFER;


        if(1)
        {
            /*
             * Fill in the prefix, only for Modbus-Ports.
             */
            for(i = prefixStartIndex; i <= prefixEndIndex; i++)
            {
                char byte[2] = {0};
                sg_sprintf(byte, "%c", commandHexString[i]);

                strcat((char*)GLOBAL_BUFFER, byte);
            }
        }

#if 0
        /*
         * Display the stringified-response for logging
         */
        if(serial->deviceType == DELIM_TERMINATED)
        {
            sg_sprintf(LOG_GLOBAL_BUFFER, "Stringified-response :: [%s]", responseByteBuffer);
            info_log(LOG_GLOBAL_BUFFER);
        }
#endif
        /*
         * Fill-in the serial-response-nibbles.
         */
        for(i = 0; i < responseLength; i++)
        {
            char hex[3] = {0};
            sg_sprintf(hex, "%x", responseByteBuffer[i]);

            if(responseByteBuffer[i] <= 0x0F)
            {
                strcat((char*)GLOBAL_BUFFER, "0");
            }
            strcat((char*)GLOBAL_BUFFER, hex);
        }

        sg_sprintf(LOG_GLOBAL_BUFFER, "Modbus-Command [%s], Modbus-Response [%s]", commandHexString, (char*)GLOBAL_BUFFER);
        debug_log(LOG_GLOBAL_BUFFER);

        if(isModbusDevice(serial) == 1)
        {
            /*
             * Do validation-checks in the response (only required for modbus-type ports).
             *
             * a)
             * Check if the first two-bytes/four-nibbles are in accordance.
             * That is, check if the slave-id and function-code are there in place, as expected by the modbus-command.
             */
            offset = prefixEndIndex - prefixStartIndex + 1;

            {
 /*               int commandHexStringOffset = 0;
                if(isModbusOverTcp(serial) == 1)
                {
                    commandHexStringOffset = 12;
                }

                if(strncmp((char*)GLOBAL_BUFFER + offset + commandHexStringOffset, commandHexString + commandHexStringOffset, 4) != 0)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "Slave-Id/Function-Code not proper in the response [%s] for command [%s] ",
                                                  ((char*)GLOBAL_BUFFER) + offset, commandHexString);
                    error_log(LOG_GLOBAL_BUFFER);

                    exitApp(0);
                }
*/
            }

            /*
             * b)
             * Check the CRC.
             */
            if(isModbusOverTcp(serial) == 0)
            {
                char crcBackup[5] = {0};
                int totalLength = strlen((char*) GLOBAL_BUFFER);

                for(i = 0; i < 4; i++)
                {
                    char *crcPointer = ((char*)GLOBAL_BUFFER) + totalLength - 4 + i;

                    crcBackup[i] = *crcPointer; /* Take the backup of this CRC character */
                    *crcPointer = 0;            /* Mark this as null-terminator. */
                }

                appendModbusCRC16((char*)GLOBAL_BUFFER + offset, sizeof(GLOBAL_BUFFER) - offset);
                if(strncmp((char*)GLOBAL_BUFFER + totalLength - 4, crcBackup, 4) != 0)
                {
                    sg_sprintf(LOG_GLOBAL_BUFFER, "CRC not proper in the response [%s] for command [%s]",
                                                  ((char*)GLOBAL_BUFFER) + offset, commandHexString);
                    error_log(LOG_GLOBAL_BUFFER);

                    errorCase=2;
                    goto exit;
                }
            }
        }

        /*
         * Pre-pend the prefix, if any
         */
        if(stringified_ioeye_data_prefix != NULL)
        {
            strcat(buf, stringified_ioeye_data_prefix);
        }

        /*
         * Finally, add the serial-response field in the total-XMLized-response to be sent to the server.
         */
        strcat(buf, (char*) GLOBAL_BUFFER);
    }

exit:
	if(isModbusOverTcp(serial) == 1)
    {
        /*
            * Close the earlier socket.
            */
		if((serial->modbusSocket).socketCorrupted == 0)
		{
			mb_release_socket_simple_guaranteed(&(serial->modbusSocket));
			(serial->modbusSocket).socketCorrupted = 1;
		}
/*
		if((serial->modbusSocket).transitionBuffer != NULL)
		{
			sg_free((serial->modbusSocket).transitionBuffer);
		}
*/
    }
	
    if(responseByteBuffer)
        sg_free(responseByteBuffer);
}


static void fillModbusCommandResponseIntoMessageBufferForSimulatedDevice(char *messageBuffer, char *payloadValues,
                                                                         int numberOfSimulatedInterfaces, char *functionCode)
{
    RESET_GLOBAL_BUFFER;

    strcat((char*)GLOBAL_BUFFER, "FF");
    strcat((char*)GLOBAL_BUFFER, functionCode);

    {
        /*
         * We calculate the number of bytes to follow.
         */
        char numberOfBytesToFollow[3] = {0};
        sg_sprintf(numberOfBytesToFollow, "%x", getNumberOfCharacterBytes(numberOfSimulatedInterfaces, functionCode) / 2);
        addPaddingIfRequired(numberOfBytesToFollow, sizeof(numberOfBytesToFollow) - 1);

        strcat((char*)GLOBAL_BUFFER, numberOfBytesToFollow);
    }
    strcat((char*)GLOBAL_BUFFER, payloadValues);

    appendModbusCRC16((char*)GLOBAL_BUFFER, sizeof(GLOBAL_BUFFER));

	/*
     * Pre-pend the prefix, if any
	 */
    if(stringified_ioeye_data_prefix != NULL)
    {
        strcat(messageBuffer, stringified_ioeye_data_prefix);
	}

    /*
     * Fill the prefix, containing the initial register-value.
     */
    strcat(messageBuffer, "0000");

    /*
     * Fill the simulated-modbus-response.
     */
    strcat(messageBuffer, (char*)GLOBAL_BUFFER);
}















void init_serial(void *ser, unsigned char isSimulatedDevice, const char *identifier, const char *serial_params_identifier,
                 const char *serial_delimiter_identifier, const char *port_type_identifier,
                 VALUE_CONCATENATOR valueConcatenatorFunc, void *valueConcatenatorFuncArg)
{
#if SINGULAR_PORT_NUMBERING == 1
    static unsigned int portNumber = 0;
#else
    static unsigned int modbusTcpPortAddress = 0;
#endif

    unsigned int *portNumberToBeUsed = NULL;

    if(isSimulatedDevice == 1)
    {
        SimulatedModbus *modbus = (SimulatedModbus *) ser;

        modbus->isSimulatedDevice = isSimulatedDevice;

        memset(modbus->identifier, 0, sizeof(modbus->identifier));
        strcpy(modbus->identifier, identifier);

        modbus->valueConcatenatorFunc = valueConcatenatorFunc;
        modbus->valueConcatenatorFuncArg = valueConcatenatorFuncArg;
    }
    else
    {
        static unsigned char serialNumberCounter = 0;
        int rc = FAILURE;

        Serial *serial = (Serial *) ser;
        serialNumberCounter++;

        serial->isSimulatedDevice = isSimulatedDevice;

        memset(serial->identifier, 0, sizeof(serial->identifier));
        strcpy(serial->identifier, identifier);

        memset(serial->serial_params_identifier, 0, sizeof(serial->serial_params_identifier));
        strcpy(serial->serial_params_identifier, serial_params_identifier);

        memset(serial->serial_delimiter_identifier, 0, sizeof(serial->serial_delimiter_identifier));
        strcpy(serial->serial_delimiter_identifier, serial_delimiter_identifier);

        memset(serial->port_type_identifier, 0, sizeof(serial->port_type_identifier));
        strcpy(serial->port_type_identifier, port_type_identifier);

	    serial->send_command_and_read_response_sync = serial_send_command_and_read_response_sync;

        /*
         * Load the serial-commands.
         */
        memset(serial->serialCommands, 0, sizeof(serial->serialCommands));
        RESET_GLOBAL_BUFFER;

        rc = get_config_value_from_persistent_storage(serial->identifier, (char*)GLOBAL_BUFFER, sizeof(GLOBAL_BUFFER));
        if(rc == SUCCESS)
        {
            /*getJsonKeyValueIfPresent((char*)GLOBAL_BUFFER, CONFIG_VALUE_KEY, serial->serialCommands);*/
			strcpy(serial->serialCommands, GLOBAL_BUFFER);
        }

        /*
         * Load the serial-connection params.
         */
        memset(serial->serialParams, 0, sizeof(serial->serialParams));
        RESET_GLOBAL_BUFFER;

        rc = get_config_value_from_persistent_storage(serial->serial_params_identifier, (char*)GLOBAL_BUFFER, sizeof(GLOBAL_BUFFER));
        if(rc == SUCCESS)
        {
            /*getJsonKeyValueIfPresent((char*)GLOBAL_BUFFER, CONFIG_VALUE_KEY, serial->serialParams);*/
			strcpy(serial->serialParams, GLOBAL_BUFFER);
        }
        else
        {
#ifdef DEFAULT_BAUD_RATE
            strcpy(serial->serialParams, DEFAULT_BAUD_RATE);
#else
            strcpy(serial->serialParams, "9600,0,0,8,1,0,0");
#endif
        }

        /*
         * Load the serial-connection delimiter.
         */
        memset(serial->serialDelimiter, 0, sizeof(serial->serialDelimiter));
        RESET_GLOBAL_BUFFER;

        rc = get_config_value_from_persistent_storage(serial->serial_delimiter_identifier, (char*)GLOBAL_BUFFER, sizeof(GLOBAL_BUFFER));
        if(rc == SUCCESS)
        {
            /*getJsonKeyValueIfPresent((char*)GLOBAL_BUFFER, CONFIG_VALUE_KEY, serial->serialDelimiter);*/
			strcpy(serial->serialDelimiter, GLOBAL_BUFFER);
        }

        /*
         * Load the port-type.
         */
        memset(serial->portType, 0, sizeof(serial->portType));
        RESET_GLOBAL_BUFFER;

        rc = get_config_value_from_persistent_storage(serial->port_type_identifier, (char*)GLOBAL_BUFFER, sizeof(GLOBAL_BUFFER));
        if(rc == SUCCESS)
        {
            /*getJsonKeyValueIfPresent((char*)GLOBAL_BUFFER, CONFIG_VALUE_KEY, serial->portType);*/
			strcpy(serial->portType, GLOBAL_BUFFER);
        }

        memset(serial->portName, 0, sizeof(serial->portName));
        memset(serial->portAddress, 0, sizeof(serial->portAddress));
        memset(serial->hostAddress, 0, sizeof(serial->hostAddress));
        memset(serial->hostPort, 0, sizeof(serial->hostPort));

        if(isModbusOverTcp(serial) == 1)
        {

#if SINGULAR_PORT_NUMBERING == 1
            portNumberToBeUsed = &portNumber;
#else
            portNumberToBeUsed = &modbusTcpPortAddress;
#endif

            (serial->modbusSocket).socketCorrupted = 1;

            strcpy(serial->portName, PORT_NAME_WIFI);
            sg_sprintf(serial->portAddress, "%u", *portNumberToBeUsed);
            strcpy(serial->hostPort, "502");

            *portNumberToBeUsed = *portNumberToBeUsed + 1;

        }
        else if(strlen(serial->serialCommands) > 0)
        {
#if SINGULAR_PORT_NUMBERING == 1
            portNumberToBeUsed = &portNumber;
			
#endif

            strcpy(serial->portName, PORT_NAME_COM);
            sg_sprintf(serial->portAddress, "%u", (strcmp(serial->portType, "rs232") == 0)?0:1);
			
            strcpy(serial->hostAddress, "");
            strcpy(serial->hostPort, "");

            //*portNumberToBeUsed = *portNumberToBeUsed + 1;

            connect_underlying_serial_medium_guaranteed(serial);
        }
    }
}


static char serial_identifier[50];
static char serial_params_identifier[50];
static char serial_delimiter_identifier[50];
static char port_type_identifier[50];
void runSerialInitializations()
{
    int i = 0;
    int rc = FAILURE;

    RESET_GLOBAL_BUFFER;

    rc = get_config_value_from_persistent_storage(NUM_PORTS_IN_USE, (char*)GLOBAL_BUFFER, sizeof(GLOBAL_BUFFER));
    if(rc == SUCCESS)
    {
        /*char small[3];
        memset(small, 0, sizeof(small));

        getJsonKeyValueIfPresent((char*)GLOBAL_BUFFER, CONFIG_VALUE_KEY, small);
        num_ports_in_use = sg_atoi(small);
		*/
		num_ports_in_use = sg_atoi(GLOBAL_BUFFER);
    }
    else
    {
        num_ports_in_use = 1;
    }

    sg_sprintf(LOG_GLOBAL_BUFFER, "\n\nNUM_PORTS_IN_USE ====> [%u]\n\n", num_ports_in_use);
    info_log(LOG_GLOBAL_BUFFER);

    for(i = 0; i < num_ports_in_use; i++)
    {
        memset(serial_identifier, 0, sizeof(serial_identifier));
        memset(serial_params_identifier, 0, sizeof(serial_params_identifier));
        memset(serial_delimiter_identifier, 0, sizeof(serial_delimiter_identifier));
        memset(port_type_identifier, 0, sizeof(port_type_identifier));

        sg_sprintf(serial_identifier, "PORT_%u_COMMANDS", i + 1);
        sg_sprintf(serial_params_identifier, "PORT_%u_SERIAL_PARAMS", i + 1);
        sg_sprintf(serial_delimiter_identifier, "PORT_%u_SERIAL_DELIMITER", i + 1);
        sg_sprintf(port_type_identifier, "PORT_%u_TYPE", i + 1);

        init_serial(&(serialInterfaces[i]), 0, serial_identifier, serial_params_identifier, serial_delimiter_identifier,
                    port_type_identifier, NULL, NULL);
    }
}


int release_serial(void *ser)
{
    int rc = FAILURE;
    unsigned char isSimulatedDevice = *((unsigned char *) ser);

    if(isSimulatedDevice == 0)
    {
        Serial *serial = (Serial *) ser;

        rc = release_underlying_serial_medium_guaranteed(serial);
        if(rc == SUCCESS)
        {
            sg_sprintf(LOG_GLOBAL_BUFFER, PROSTR("%s serial-interface closed successfully."), serial->identifier);
            info_log(LOG_GLOBAL_BUFFER);
        }
        else
        {
            sg_sprintf(LOG_GLOBAL_BUFFER, PROSTR("%s serial-interface COULD NOT BE CLOSED :("), serial->identifier);
            error_log(LOG_GLOBAL_BUFFER);
        }
    }

    return rc;
}


void releaseSerialInitializations()
{
    int i = 0;
    for(i = 0; i < num_ports_in_use; i++)
    {
        release_serial(&(serialInterfaces[i]));
    }
}


static void loadSerialCommandsIfNotAlready(Serial *serial, unsigned char force)
{
    if((serial->commandsLoaded == 0) || (force == 1))
    {
        registerEditableConfig(serial->serialCommands,
                               serial->identifier,
                               CONFIG_STRING,
                               "",
                               "",
							   1);
        registerEditableConfig(serial->serialParams,
                               serial->serial_params_identifier,
                               CONFIG_STRING,
                               "",
                               "",
							   1);
        registerEditableConfig(serial->serialDelimiter,
                               serial->serial_delimiter_identifier,
                               CONFIG_STRING,
                               "",
                               "",
							   1);
        registerEditableConfig(serial->portType,
                               serial->port_type_identifier,
                               CONFIG_STRING,
                               "",
                               "",
							   1);

        serial->commandsLoaded = 1;
    }
}


void serialOnConnectProcedures(void *ser)
{
    unsigned char isSimulatedDevice = *((unsigned char *) ser);

    if(isSimulatedDevice == 0)
    {
        Serial *serial = (Serial *) ser;
        loadSerialCommandsIfNotAlready(serial, 1);
    }
}


void runSerialOnConnectProcedures()
{
    int i = 0;

    registerEditableConfig(&num_ports_in_use,
                           NUM_PORTS_IN_USE,
                           CONFIG_INT,
                           PROSTR("1"),
                           PROSTR(""),
						   1);

    if(num_ports_in_use > MAX_PORTS_ALLOWED)
    {
        num_ports_in_use = MAX_PORTS_ALLOWED;
    }

    for(i = 0; i < num_ports_in_use; i++)
    {
        serialOnConnectProcedures(&(serialInterfaces[i]));
    }
}


static void processCommand2(char *command, Serial *serial, char *modbusTcpIPAddress)
{
    serialCommandUnderProcess = command;

    pre_process_payload();
    fillSerialCommandResponseIntoMessageBufferForSerialDevice(messageBuffer, command, serial, modbusTcpIPAddress);

    assignPortInfoToStructure(&portInfoArgument, serial->portName, serial->portAddress, serial->hostAddress, serial->hostPort);
    post_process_payload(errorCase, add_port_info, &portInfoArgument);
}


void processCommand(char *command, Serial *serial)
{
    processCommand2(command, serial, (char*) "");
}


void serialProcedures(void *ser)
{
    unsigned char isSimulatedDevice = *((unsigned char *) ser);
    char *temporaryCopy = NULL;

    if(isSimulatedDevice == 0)
    {
        Serial *serial = (Serial *) ser;

        loadSerialCommandsIfNotAlready(serial, 0);
        if(strlen(serial->serialCommands) > 0)
        {
            temporaryCopy = (char*) sg_malloc(sizeof(serial->serialCommands) + 1);
            if(temporaryCopy == NULL)
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, SERIAL_ERROR "Could not allocate temporary-memory for tokenizing serial-commands");
                error_log(LOG_GLOBAL_BUFFER);

                goto exit;
            }
            memset(temporaryCopy, 0, sizeof(serial->serialCommands) + 1);

            strcpy(temporaryCopy, serial->serialCommands);

            memset(modbusTcpIPAddress, 0, sizeof(modbusTcpIPAddress));
            serialCommandUnderProcess = strtok(temporaryCopy, ",");

            while(serialCommandUnderProcess != NULL)
            {
                if(isModbusOverTcp(serial) == 1)
                {
                    char *colon = strstr(serialCommandUnderProcess, ":");

                    get_nth_token_thread_safe(serialCommandUnderProcess, ':', 1, modbusTcpIPAddress, 1);
                    serialCommandUnderProcess = colon + 1;
                }

                processCommand2(serialCommandUnderProcess, serial, modbusTcpIPAddress);
                serialCommandUnderProcess = strtok(NULL, ",");
            }
        }
        else
        {
            sg_sprintf(LOG_GLOBAL_BUFFER,
                       SERIAL_ERROR "No commands to execute for field [%s] !!!; "
                       "please fill-in some commands on the InstaMsg-Server for this device", serial->identifier);
            info_log(LOG_GLOBAL_BUFFER);
        }
    }
    else
    {
        if(1)
        {
            SimulatedModbus *modbus = (SimulatedModbus *) ser;

            sg_sprintf(LOG_GLOBAL_BUFFER, "Processing simulated-modbus-device [%s]", modbus->identifier);
            info_log(LOG_GLOBAL_BUFFER);

            modbus->valueConcatenatorFunc(simulatedModbusValuesCumulated, modbus->valueConcatenatorFuncArg);
        }
    }

exit:
    if(temporaryCopy)
        sg_free(temporaryCopy);
}


void runSerialProcedures()
{
    int i = 0;
    for(i = 0; i < num_ports_in_use; i++)
    {
        serialProcedures(&(serialInterfaces[i]));
    }
}


void resetSimulatedModbusEnvironment(int numberOfSimulatedInterfaces, char *functionCode)
{
    stringified_ioeye_data_prefix = NULL;

    simulatedModbusValuesCumulated = (char*) sg_malloc(getNumberOfCharacterBytes(numberOfSimulatedInterfaces, functionCode) + 1);
    if(simulatedModbusValuesCumulated == NULL)
    {
        sg_sprintf(LOG_GLOBAL_BUFFER, "Could not allocate bytes for simulated modbus-devices.. rebooting device");
        error_log(LOG_GLOBAL_BUFFER);

        exitApp(0);
    }
    memset(simulatedModbusValuesCumulated, 0, getNumberOfCharacterBytes(numberOfSimulatedInterfaces, functionCode) + 1);
}


void flushSimulatedModbusEnvironment(int numberOfSimulatedInterfaces, char *functionCode)
{
    {
        /*
         * Calculated the simulated-modbus command.
         */
        memset(simulatedModbusCommand, 0, sizeof(simulatedModbusCommand));

        strcat(simulatedModbusCommand, "FF");
        strcat(simulatedModbusCommand, functionCode);
        strcat(simulatedModbusCommand, "0000");

        {
            char numRegisters[5] = {0};
            sg_sprintf(numRegisters, "%x", numberOfSimulatedInterfaces);
            addPaddingIfRequired(numRegisters, sizeof(numRegisters) - 1);

            strcat(simulatedModbusCommand, numRegisters);
        }

        appendModbusCRC16(simulatedModbusCommand, sizeof(simulatedModbusCommand));
    }

    serialCommandUnderProcess = simulatedModbusCommand;

    pre_process_payload();
    fillModbusCommandResponseIntoMessageBufferForSimulatedDevice(messageBuffer, simulatedModbusValuesCumulated,
                                                                 numberOfSimulatedInterfaces, functionCode);

    assignPortInfoToStructure(&portInfoArgument, (char*) PORT_NAME_SIMULATED, (char*) "00", (char*) "", (char*) "");
    post_process_payload(errorCase, add_port_info, &portInfoArgument);

    if(simulatedModbusValuesCumulated)
    {
        sg_free(simulatedModbusValuesCumulated);
    }
}

char portTypeNameToNumberConverter(char *portName)
{
	if(!portName) {
		return -1;
	}	
	
	if(0 == strcmp(portName, PORT_NAME_RS232)) {
		return PORT_TYPE_RS232;	
	} else if(0 == strcmp(portName, PORT_NAME_RS485)) {
		return PORT_TYPE_RS485;
	} else if (0 == strcmp(portName, PORT_NAME_TCP)) {
		return PORT_TYPE_TCP;
	} else {
		return -1;
	}
}

char* portTypeNumberToNameConverter(char portType)
{
	if(PORT_TYPE_RS232 == portType) {
		return PORT_NAME_RS232;
	} else if(PORT_TYPE_RS485 == portType) {
		return PORT_NAME_RS485;
	} else if (PORT_TYPE_TCP == portType) {
		return PORT_NAME_TCP;
	} else {
		return NULL;
	}
}


