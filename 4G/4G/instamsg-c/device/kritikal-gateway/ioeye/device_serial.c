#include "device_serial.h"

#include <asf.h>

#include "../../ioeye/include/serial.h"

#include "../../../common/instamsg/driver/include/watchdog.h"
#include "../../../common/instamsg/driver/include/sg_stdlib.h"
#include "../../../common/instamsg/driver/include/log.h"
#include "../../../common/instamsg/driver/include/misc.h"


static volatile unsigned char *responseBuffer;
static volatile int bytesToRead;
static volatile int responseBytesSoFar;

static volatile unsigned char readResponse;

static volatile Serial *global_serial;
static volatile unsigned char *global_commandBytes;
static volatile int global_commandBytesLength;
static volatile unsigned char global_serial_delimiter;

static volatile int maxTriesForSerialResponse;
static volatile unsigned char retryCounter=0;


/*
 * This method initializes and connects to the serial-interface.
 */
static unsigned int baudRate;

static unsigned int config_parity;
static unsigned int config_odd_parity;
static unsigned int config_chars;
static unsigned int config_blocking;
static unsigned int config_two_stop_bits;
static unsigned int config_hardware_control;

static unsigned int device_chars;
static unsigned int device_parity;
static unsigned int device_stop_bits;

#define RS232		"rs232"
#define RS484		"rs485"


/** PDC data packet. */
pdc_packet_t g_st_packet;

/** Pointer to PDC register base. */
Pdc *g_p_pdc;


#define CHECK_RECV_BYTES																			\
		if(bytesToRead > 0)																			\
        {																							\
            /*																						\
             * Fixed-bytes case.																	\
             */																						\
            if(responseBytesSoFar == bytesToRead)													\
            {																						\
                readResponse = 0;																	\
            }																						\
        }																							\
		else if(responseBuffer[responseBytesSoFar - 1] == global_serial_delimiter)					\
        {																							\
            /*																						\
             * Delimiter case.																		\
             */																						\
            if((responseBytesSoFar == 1) && (maxTriesForSerialResponse < 3))						\
            {																						\
                /*																					\
                 * Handle when empty response is received.											\
                 */																					\
                maxTriesForSerialResponse++;														\
                responseBytesSoFar = 0;																\
																									\
				sendBytes();																		\
            }																						\
            else                                                                                    \
            {																						\
                /*																					\
                 * We are done.																		\
                 */																					\
                readResponse = 0;																	\
            }																						\
        }

static void sendBytes()
{
	/*
	* Remember, this is a synchronous function.
	*/
	if(strcmp(((Serial*)global_serial)->portType, RS232) == 0)
	{
		int i = 0;
		for(i = 0; i < global_commandBytesLength; i++)
		{
			unsigned int temp;
			
			while(usart_is_tx_ready((Usart *) USART0) == 0)
			{
			}

			for(temp = 0xA00; temp != 0; temp--)
			{
			}
			
			temp = 0;
			temp = global_commandBytes[i];
			
			usart_write((Usart *) USART0, temp);
		}
	}
	else
	{
		ioport_set_pin_level(uC_RE_DE_GPIO, uC_RE_DE_ACTIVE_LEVEL);
		//startAndCountdownTimer(1, 0);
		
		g_st_packet.ul_addr = (uint32_t)responseBuffer;
		g_st_packet.ul_size = MAX_BUFFER_SIZE;
		pdc_rx_init(g_p_pdc, &g_st_packet, NULL);

		usart_enable_interrupt((Usart *) USART1, US_IER_ENDTX);
		
		g_st_packet.ul_addr = (uint32_t)global_commandBytes;
		g_st_packet.ul_size = global_commandBytesLength;
		pdc_tx_init(g_p_pdc, &g_st_packet, NULL);
		
		startAndCountdownTimer(1, 0);
		
		if( ( MAX_BUFFER_SIZE - pdc_read_rx_counter(g_p_pdc))>0)
		{
			readResponse= 0;
		}
		else
		{
			startAndCountdownTimer(1, 0);
			
			if( ( MAX_BUFFER_SIZE - pdc_read_rx_counter(g_p_pdc))>0)
			{
				readResponse= 0;
			}
			else
			{
				startAndCountdownTimer(1, 0);

				if( ( MAX_BUFFER_SIZE - pdc_read_rx_counter(g_p_pdc))>0)
				{
					readResponse= 0;
				}
				else
				{
					readResponse= 1;
				}		
			}
		}
	}
}


void USART1_Handler()
{
	uint32_t ul_status = usart_get_status((Usart *) USART1);

	if ((ul_status & US_CSR_ENDTX) && (ul_status & US_CSR_TXEMPTY) && (ul_status & US_CSR_TXBUFE))
	{
		usart_disable_interrupt((Usart *) USART1, US_IDR_ENDTX);		
		ioport_set_pin_level(uC_RE_DE_GPIO, uC_RE_DE_INACTIVE_LEVEL);
	}
}


void USART0_Handler()
{
	if ( (usart_get_status((Usart*) USART0)) & US_CSR_RXRDY)
	{
		uint32_t c = 0;
		usart_read((Usart *) USART0, &c);
		
		responseBuffer[responseBytesSoFar] = c;

        if(responseBuffer[0] == 0)
        {
            responseBytesSoFar = 0;
            return;
        }

        responseBytesSoFar++;
		if(bytesToRead>0)
		{
			if(responseBytesSoFar==bytesToRead)
			{
				readResponse= 0;
		
			}
		}
		else if(responseBuffer[responseBytesSoFar - 1] == global_serial_delimiter)
		{
			if(responseBytesSoFar>=2)
			{
				readResponse= 0;
			}
		}
//		CHECK_RECV_BYTES
    }
}


void connect_underlying_serial_medium_guaranteed(Serial *serial)
{
	baudRate = 9600;
	device_chars = 	US_MR_CHRL_7_BIT;
	device_parity = US_MR_PAR_EVEN;
	device_stop_bits = US_MR_NBSTOP_2_BIT;
	
	if(strlen(serial->serialParams) == 0)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Using default-parameters for serial-port ....");
		info_log(LOG_GLOBAL_BUFFER);		
	}
	else
	{
		char small[10];

		memset(small, 0, sizeof(small));
		get_nth_token_thread_safe(serial->serialParams, ',', 1, small, 1);
		baudRate = sg_atoi(small);
		
		memset(small, 0, sizeof(small));
		get_nth_token_thread_safe(serial->serialParams, ',', 2, small, 1);
		config_parity = sg_atoi(small);
		
		memset(small, 0, sizeof(small));
		get_nth_token_thread_safe(serial->serialParams, ',', 3, small, 1);
		config_odd_parity = sg_atoi(small);
		
		memset(small, 0, sizeof(small));
		get_nth_token_thread_safe(serial->serialParams, ',', 4, small, 1);
		config_chars = sg_atoi(small);
		
		memset(small, 0, sizeof(small));
		get_nth_token_thread_safe(serial->serialParams, ',', 5, small, 1);
		config_blocking = sg_atoi(small);
		
		memset(small, 0, sizeof(small));
		get_nth_token_thread_safe(serial->serialParams, ',', 6, small, 1);
		config_two_stop_bits = sg_atoi(small);
		
		memset(small, 0, sizeof(small));
		get_nth_token_thread_safe(serial->serialParams, ',', 7, small, 1);
		config_hardware_control = sg_atoi(small);	
		
		
		if(config_chars == 5)
		{
			device_chars = US_MR_CHRL_5_BIT;
		}
		else if(config_chars == 6)
		{
			device_chars = US_MR_CHRL_6_BIT;
		}
		else if(config_chars == 7)
		{
			device_chars = US_MR_CHRL_7_BIT;
		}
		else if(config_chars == 8)
		{
			device_chars = US_MR_CHRL_8_BIT;
		}
		
		
		
		if(config_parity == 0)
		{
			device_parity = US_MR_PAR_NO;
		}
		else
		{
			if(config_odd_parity == 1)
			{
				device_parity = US_MR_PAR_ODD;
			}
			else
			{
				device_parity = US_MR_PAR_EVEN;
			}
		}
		
		
		if(config_two_stop_bits == 1)
		{
			device_stop_bits = US_MR_NBSTOP_2_BIT;
		}
		else
		{
			device_stop_bits = US_MR_NBSTOP_1_BIT;
		}
	}
	
	
	{
		const sam_usart_opt_t uart_serial_options = {
			.baudrate		= baudRate,
			.char_length	= device_chars,
			.parity_type	= device_parity,
			.stop_bits		= device_stop_bits,
			.channel_mode	= US_MR_CHMODE_NORMAL,
			.irda_filter	= 0
		};


		if(strcmp(serial->portType, RS232) == 0)
		{		
			usart_init_rs232((Usart *) USART0, &uart_serial_options, sysclk_get_peripheral_hz());
		
			usart_enable_tx((Usart *) USART0);
			usart_enable_rx((Usart *) USART0);
		
			usart_enable_interrupt((Usart *) USART0, US_IER_RXRDY);
			NVIC_EnableIRQ(USART0_IRQn);
		}
		else
		{			
			/* Enable the peripheral clock in the PMC. */
			sysclk_enable_peripheral_clock(ID_USART1);
			
			usart_init_rs485((Usart *) USART1, &uart_serial_options, sysclk_get_peripheral_hz());
			
			/* Enable TX & RX function. */
			usart_enable_tx((Usart *) USART1);
			usart_enable_rx((Usart *) USART1);
			
			/* Configure and enable interrupt of USART. */
			NVIC_EnableIRQ(USART1_IRQn);
			
			g_p_pdc = usart_get_pdc_base((Usart *) USART1);
			pdc_enable_transfer(g_p_pdc, PERIPH_PTCR_RXTEN | PERIPH_PTCR_TXTEN);
		}
	} 
}


/*
 * This method ::
 *
 * a)
 * Sends the command to the serial-interface.
 *
 * b)
 * Response is received in "responseByteBuffer", while the following method returns 1 ::
 *
 *                                          time_fine_for_time_limit_function()
 *
 * Number of bytes to-be-actually read is determined from the following ::
 *
 *      * If #*responseBytesLength > 0, then #*responseBytesLength bytes are read.
 *
 *      * Else the command-response is delimited by a delimiter, and the device needs
 *        to handle it appropriately, AND *responseBytesLength MUST BE SET APPROPRIATELY.
 *
 *
 *
 * The function must return exactly one of the following ::
 *
 * *
 * SUCCESS, if everything went fine.
 *
 * *
 * FAILURE, else.
 */
int serial_send_command_and_read_response_sync(Serial *serial,
                                               unsigned char *commandBytes,
                                               int commandBytesLength,
                                               unsigned char *responseByteBuffer,
                                               int *responseBytesLength,
											   unsigned char *error_code)
{
	
	usart_disable_interrupt((Usart *) USART1, US_IDR_ENDTX);

	retryCounter = 0;

	while(++retryCounter <= 3)
	{
		global_serial = serial;
		global_commandBytes = commandBytes;
		global_commandBytesLength = commandBytesLength;
		global_serial_delimiter = sg_atoi(serial->serialDelimiter);

		responseBuffer = responseByteBuffer;
		bytesToRead = *responseBytesLength;
		responseBytesSoFar = 0;

		readResponse = 1;
		sg_sprintf(LOG_GLOBAL_BUFFER, "Sending Command Attempt <%d>",retryCounter);
		error_log(LOG_GLOBAL_BUFFER);
		
		external_wathdog_init();
		
		sendBytes();
		
		if(strcmp(serial->portType, RS232) == 0)
		{
			if(readResponse == 1)
			{
				startAndCountdownTimer(1, 0);
			}
			else
			{
				break;
			}
			
			if(readResponse == 1)
			{
				startAndCountdownTimer(1, 0);
			}
			else
			{
				break;
			}
			
			
			if(readResponse == 1)
			{
				startAndCountdownTimer(1, 0);
			}
			else
			{
				break;
			}
		}
		else if(strcmp(serial->portType, RS232) != 0)
		{
			responseBytesSoFar = MAX_BUFFER_SIZE - pdc_read_rx_counter(g_p_pdc);
#if ENG_DEBUG
			sg_sprintf(LOG_GLOBAL_BUFFER, "Response Length=%d Expected=%d",responseBytesSoFar ,bytesToRead);
			info_log(LOG_GLOBAL_BUFFER);
#endif

			if(responseBytesSoFar>0)
			{
				readResponse=0;
				break;
			}
		}
	}
	
	if(readResponse==1)
	{
		*error_code=101;						/*!> No Bytes Received */
	}
	else if(readResponse==0)
	{
		if(bytesToRead>0)						/*!> Without Delimeter*/
		{
			if(responseBytesSoFar<bytesToRead)
			{
				if(responseBuffer[1]&0x80)
				{
					*error_code=103;			/*!> Modbus Exception */
				}
				else
				{
					*error_code=104;			/*!> Less Data then Expected */
				}
			}
			else
			{
				*error_code = 0;
			}
		}
	}
	*responseBytesLength=responseBytesSoFar;
	readResponse = 0;
	
	external_watchdog_feed();
	return SUCCESS;

    /*
     * Stop reading from the interrupt-handler.
     */
    readResponse = 0;
	return SUCCESS;
}
/*******************
int serial_send_command_and_read_response_sync(Serial *serial,
                                               unsigned char *commandBytes,
                                               int commandBytesLength,
                                               unsigned char *responseByteBuffer,
                                               int *responseBytesLength,
											   unsigned char *error_code)
{
	
	usart_disable_interrupt((Usart *) USART1, US_IDR_ENDTX);

	retryCounter = 0;

	if(strcmp(serial->portType, RS232) == 0)
    {
		while(++retryCounter <= 3)
		{
			global_serial = serial;
			global_commandBytes = commandBytes;
			global_commandBytesLength = commandBytesLength;
			global_serial_delimiter = sg_atoi(serial->serialDelimiter);

			responseBuffer = responseByteBuffer;
			bytesToRead = *responseBytesLength;
			responseBytesSoFar = 0;

			readResponse = 1;
			sg_sprintf(LOG_GLOBAL_BUFFER, "Sending Command Attempt <%d>",retryCounter);
			error_log(LOG_GLOBAL_BUFFER);
			sendBytes();
			
			if(readResponse == 1)
			{
				startAndCountdownTimer(1, 0);
			}
			else
			{
				break;
			}
			if(readResponse == 1)
			{
				startAndCountdownTimer(1, 0);
			}
			else
			{
				break;
			}
			if(readResponse == 1)
			{
				startAndCountdownTimer(1, 0);
			}
			else
			{
				break;
			}
		}
		
		if(readResponse==1)
		{
			*error_code=101;						/*!> No Bytes Received */
/*		}
		else if(readResponse==0)
		{
			if(bytesToRead>0)						/*!> Without Delimeter*/
/*			{
				if(responseBytesSoFar<bytesToRead)
				{
					if(responseBuffer[1]&0x80)
					{
						*error_code=103;			/*!> Modbus Exception */
/*					}
					else
					{
						*error_code=104;			/*!> Less Data then Expected */
/*					}
				}
				else
				{
					*error_code = 0;
				}
			}
		}
		*responseBytesLength=responseBytesSoFar;
		readResponse = 0;
		return SUCCESS;
	}
	else
	{
		global_serial = serial;
		global_commandBytes = commandBytes;
		global_commandBytesLength = commandBytesLength;
		global_serial_delimiter = sg_atoi(serial->serialDelimiter);

		responseBuffer = responseByteBuffer;
		bytesToRead = *responseBytesLength;
		responseBytesSoFar = 0;

		readResponse = 1;

		sendBytes();
		
		while(1)
		{
			if((readResponse != 1) || (time_fine_for_time_limit_function() != 1))
			{
				break;
			}	
			
			startAndCountdownTimer(1, 0);
			
			responseBytesSoFar = MAX_BUFFER_SIZE - pdc_read_rx_counter(g_p_pdc);
			CHECK_RECV_BYTES
		}		
	}
	

    if(readResponse == 0)
    {
        *responseBytesLength = responseBytesSoFar;
    }

    /*
     * Stop reading from the interrupt-handler.
     */
/*    readResponse = 0;
	return SUCCESS;
}
*/


/*
 * This method cleans up the serial-interface.
 *
 * Returns SUCCESS, if the interface was closed successfully.
 *         FAILURE, if the interface could not be closed successfully.
 */
int release_underlying_serial_medium_guaranteed(Serial *serial)
{
    return SUCCESS;
}
