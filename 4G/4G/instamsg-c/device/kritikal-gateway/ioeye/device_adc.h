#ifndef IOEYE_ADC
#define IOEYE_ADC

#include "spi_master.h"

typedef struct ADC ADC;

struct ADC
{
    /* ============================= THIS SECTION MUST NOT BE TEMPERED ==================================== */
    long (*read_value_sync)(ADC *adc);
    /* ============================= THIS SECTION MUST NOT BE TEMPERED ==================================== */



    /* ============================= ANY EXTRA FIELDS GO HERE ============================================= */
    /* ============================= ANY EXTRA FIELDS GO HERE ============================================= */
};


#define MAXAdc_SPI_MODULE			SPI
#define MAXAdc_CS					0

/* SPI master speed in Hz */
#define MaxAdc_SPI_MASTER_SPEED		1000000UL


/*------------------SEQ register-----------*/
#define SEQ_REG_CMD			0xD0					// Sequence register address

#define SEQ_REG_CH0_DATA	0x02                     // channel 0, MUX delay enable
#define SEQ_REG_CH1_DATA	0x22					 //same like channel 0
#define SEQ_REG_CH2_DATA	0x42
#define SEQ_REG_CH3_DATA	0x62
#define SEQ_REG_CH4_DATA	0x82
#define SEQ_REG_CH5_DATA	0xA2

#define SEQ_REG_SELF_CALIB_CH0_DATA 0x01			// channel 0 , RYDB bit set
#define SEQ_REG_SELF_CALIB_CH1_DATA 0x21			// channel 1 , RYDB bit set
#define SEQ_REG_SELF_CALIB_CH2_DATA 0x41			// similarly all
#define SEQ_REG_SELF_CALIB_CH3_DATA 0x61
#define SEQ_REG_SELF_CALIB_CH4_DATA 0x81
#define SEQ_REG_SELF_CALIB_CH5_DATA 0xA1

/*------------------Delay register-----------*/
#define DELAY_REG_CMD	0xCA						// delay register address

#define GPO_DELAY		0x00
#define MUX_DELAY		0xF0					//960 MicroSec.

/*------------------Control registers-----------*/
#define CTRL1_REG_CMD		0xC2					// control 1 register address
#define CTRL1_REG_DATA		0x8D

#define CTRL2_REG_CMD		0xC4					// control 2 register address
#define CTRL2_REG_DATA		0x20

#define CTRL3_REG_CMD		0xC6					// control 3 register address
#define CTRL3_REG_DATA		0x5C

#define CTRL1_REG_SELF_CALIB_DATA	0x05			// offset binary format, continuous conversion
#define CTRL2_REG_SELF_CALIB_DATA	0x20			// INT_LDO_EN,PGA_EN=0,gain=0
#define CTRL3_REG_SELF_CALIB_DATA	0x0C			// no system gain, no self calibration gain

/*------------------DATA registers Address-----------*/
#define DATA_REG0_CMD		0xDD
#define DATA_REG1_CMD		0xDF
#define DATA_REG2_CMD		0xE1
#define DATA_REG3_CMD		0xE3
#define DATA_REG4_CMD		0xE5
#define DATA_REG5_CMD		0xE7


#define GPIO_CTRL_CMD		0xC8					// GPIO control register address
#define GPIO_CTRL_DATA		0xD8


#define START_CONVERSION	0xBE
#define DUMMY_BYTE			0x00

#define STATUS_REG_CMD		0xC1					// STATUS register address

#define SELF_CALIB_CMD		0xA0
#define POWER_DOWN_CMD		0x90

#include <stdint.h>


void adc_spi_init(void);

#include <stdint.h>
#include "spi.h"

/** MAX11253 operation status, each operation returns one of the following status */
typedef enum maxAdc_status {
	MAX_SUCCESS = 0,		/** Current operation successful */
	MAX_ERROR_INIT,			 /** Initialization error:*/
	MAX_ERROR_NOT_FOUND,	 /** The specific SerialADC Not found  */
	MAX_ERROR_WRITE,		/** Write error returned by the SerialADC */
	MAX_ERROR_BUSY,			/** Current operation failed, SerialADC is busy */
	MAX_ERROR_SPI,			/** SPI transfer failed */
	MAX_ERROR				/** Current operation failed */
} maxAdc_status_t;



#define Current_INTERCEPT		3992.501493207
#define Current_SLOPE			0.30012504240806

#define Vol_INTERCEPT			23.413718640855
#define Vol_SLOPE				0.56302779414741

#define  THRESHOLD_VOLT			0.500
#define  THRESHOLD_CURRENT		4.0
#define  MAX_SAMPLE				2048
#define	 MicroAMP_TO_MilliAMP	1000
#define	 MilliVolt_TO_VOLT		1000


#define channel0 0
#define channel1 1
#define channel2 2
#define channel3 3
#define channel4 4
#define channel5 5

#define CRTL_REG1 1
#define CRTL_REG3 3
#define CRTL_REG2 2


#define STATUS_REG_SIZE 4
extern uint8_t STATUS_REG[STATUS_REG_SIZE];

extern struct spi_device MAXADC_DEVICE1;

void read_adc(uint8_t channel, short *value);

#endif
