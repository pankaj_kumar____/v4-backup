#include "device_defines.h"

#include "./spi_utils.h"
#include "../ioeye/device_adc.h"

#include <asf.h>
#include "conf_board.h"

#include <string.h>

#include "../../../common/instamsg/driver/include/log.h"
#include "../../../common/instamsg/driver/include/globals.h"
#include "../../../common/instamsg/driver/include/misc.h"


const char* chosenPeripheral;




void switch_spi(const char* peripeheral, unsigned char force)
{
	if(strcmp(chosenPeripheral, peripeheral) != 0)
	{
	}
	else if(force == 0)
	{
		return;
	}
	
	//sg_sprintf(LOG_GLOBAL_BUFFER, "Selecting SPI for [%s]", peripeheral); removing msg showing selecting spi for flash
	//info_log(LOG_GLOBAL_BUFFER);
	
	if(strcmp(peripeheral, FLASH_PERIPHERAL) == 0)
	{
		at25_status_t status = AT25_ERROR;

		at25dfx_initialize();
		
		/* Set the SerialFlash active */
		at25dfx_set_mem_active(AT25DFX_MEM_ID);
		
		/* Check if the SerialFlash is valid */
		status = at25dfx_mem_check() ;
		if (status != AT25_SUCCESS)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Could not validate flash-memory, resetting device");
			error_log(LOG_GLOBAL_BUFFER);
			
			resetDevice();
		}
		
		/* Unprotect the chip */
		status = 	at25dfx_protect_chip(AT25_TYPE_UNPROTECT);
		if (status != AT25_SUCCESS)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Could not unprotect flash-memory, resetting device");
			error_log(LOG_GLOBAL_BUFFER);
			
			resetDevice();
		}
	}
	else if(strcmp(peripeheral, ADC_PERIPHERAL) == 0)
	{
		adc_spi_init();
	}
	
	startAndCountdownTimer(1, 0);
	chosenPeripheral = peripeheral;
}