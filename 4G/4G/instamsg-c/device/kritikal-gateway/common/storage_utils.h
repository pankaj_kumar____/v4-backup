#ifndef DEVICE_STORAGE_UTILS
#define DEVICE_STORAGE_UTILS

#include "../../src/device/kritikal-gateway/instamsg/fwUpgrade/externalFlash.h"

/*
 * Only these need to be changed if the persistent-storage capacity/sizes are changed.
 */
#define MEMORY_SIZE_BYTES                       ((16 * 1024) - EXTERNAL_FLASH_SIZE_USED) * 1024
#define MAX_RECORD_SIZE_BYTES                   4096
#define MAX_CONFIG_RECORD_SIZE_BYTES			2048
#define MBR_INDEX								0
#define MAX_RECORDS								((MEMORY_SIZE_BYTES) / (MAX_RECORD_SIZE_BYTES) )
#define CONFIG_RECORDS_LOWER_INDEX              (MBR_INDEX + 1)
#define CONFIG_RECORDS_UPPER_INDEX				CONFIG_RECORDS_LOWER_INDEX
#define DATA_RECORDS_LOWER_INDEX                (CONFIG_RECORDS_UPPER_INDEX + 1)
#define DATA_RECORDS_UPPER_INDEX                (MAX_RECORDS - 4)
#define NO_MORE_RECORD_TO_READ                  (MAX_RECORDS - 2)
#define FIRST_RECORD_NUMBER_VALUE               DATA_RECORDS_LOWER_INDEX


typedef enum {
	RECORD_TYPE_NA = -1,
	RECORD_TYPE_CONFIG,
	RECORD_TYPE_DATA,
	RECORD_TYPE_MAX
} recordType_e;

int is_record_valid(unsigned short recordNumber);
void mark_record_invalid(unsigned short recordNumber);
int read_record_from_persistent_storage(unsigned short recordNumber, char *buffer, int maxBufferLength, recordType_e recordType);
int write_record_on_persistent_storage(unsigned short recordNumber, const char *record, int recordLen, recordType_e recordType);


void ll_display_config_val(void);
void ll_clear_flash_memory(void);
void ll_init_persistent_storage(void);

#endif
