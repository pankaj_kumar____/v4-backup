#include "./storage_utils.h"
#include "./spi_utils.h"
#include <string.h>
#include <asf.h>
#include "conf_board.h"
#include "../../../common/instamsg/driver/include/log.h"
#include "../../../common/instamsg/driver/include/globals.h"
#include "../../../common/instamsg/driver/include/misc.h"

#define INVALID_SIZE														0
#define OPERATING_SECTOR_SIZE										       MAX_CONFIG_RECORD_SIZE_BYTES

static unsigned char tempBuffer[OPERATING_SECTOR_SIZE];
/*
 *
 * ALL DEVICE-SPECIFIC CODE GOES HERE NOW ...
 *
 */


/**********************************************************************************************************
 **************************   BOUNDARY WALL BETWEEN LOWER AND MIDDLE LAYER ********************************
 *********************************************************************************************************/

static void write_record_at_address(unsigned int address, unsigned char *buffer, int len)
{
	at25_status_t status = AT25_ERROR;
	external_wathdog_init();
	
	/* Erase the block before write */
	if(0x00==address%4096)
	{
		status = at25dfx_erase_block(address);
		if (status != AT25_SUCCESS)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Could not erase flash-memory-block, resetting device");
			error_log(LOG_GLOBAL_BUFFER);
			resetDevice();	
		}
	}
	delay_us(200);

	/* Write the data to the SerialFlash */
	status = at25dfx_write(buffer, len, address);
	external_watchdog_feed();
	if (status != AT25_SUCCESS)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not write flash-memory-block, resetting device");
		error_log(LOG_GLOBAL_BUFFER);
		resetDevice();	
	}
}


static void read_record_at_address(unsigned int address, unsigned char *buffer, int len)
{		
	at25_status_t status = AT25_ERROR;
	external_wathdog_init();

	status = at25dfx_read(buffer, len, address);
	external_watchdog_feed();
	if (status != AT25_SUCCESS)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not read flash-memory-block, resetting device");
		error_log(LOG_GLOBAL_BUFFER);
		resetDevice();
	}
}


static void write_record(int recordNumber, unsigned char *buffer, unsigned short len, recordType_e recordType)
{
    unsigned char *tmp = tempBuffer;
	unsigned lengthLimit = 0;
	
	if(recordType == RECORD_TYPE_CONFIG) {
		lengthLimit = OPERATING_SECTOR_SIZE;		/* for config we are reading complete sector */
	} else if (recordType == RECORD_TYPE_DATA) {
		lengthLimit = OPERATING_SECTOR_SIZE - 2;	/* for data two bytes reserved for record length */
	}
	
	if(len > lengthLimit) {
		sg_sprintf(LOG_GLOBAL_BUFFER, "Maximum allowed size exceeded");
		error_log(LOG_GLOBAL_BUFFER);
		resetDevice();
	}

    /*
     * Write the length of the record in two bytes itself at the start of record.
     */
	if(recordType == RECORD_TYPE_CONFIG) {
		memcpy(tmp, buffer, len);
		write_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, tmp, len);
	} else if (recordType == RECORD_TYPE_DATA) {
		int i = 2, j = 0;
		unsigned char *ptr = (unsigned char*) &len;

		tmp[0] = *ptr;
		tmp[1] = *(ptr + 1);

		for(i = 2, j = 0; j < len; i++, j++)
		{
			tmp[i] = buffer[j];
		}
		
		write_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, tmp, len + 2);
	}
}

static unsigned short read_record(int recordNumber, unsigned char *buffer, int bufferLength, recordType_e recordType)
{
    unsigned char *tmp = tempBuffer;
    unsigned short len = 0;
	
	if(recordType == RECORD_TYPE_CONFIG) {
		read_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, tmp, bufferLength);
		memcpy(buffer, tmp, bufferLength);	
		return bufferLength;			
	} else if (recordType == RECORD_TYPE_DATA) {
		/*
		 * Read the length of the record, stored in the first two bytes of the record itself.
		 */
		unsigned char *ptr = (unsigned char*) &len;
		
        read_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, tmp, 2);
        *ptr        = tmp[0];
        *(ptr + 1)  = tmp[1];
		
		if(len <= OPERATING_SECTOR_SIZE - 2) {
			read_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, tmp, len + 2);
			memcpy(buffer, tmp + 2, len);
		}
		return len;
	}
}

static unsigned short getRecordSize(unsigned short recordNumber)
{
    unsigned short c;
    unsigned char *ptr = (unsigned char*) &c;
	
	if(recordNumber == CONFIG_RECORDS_LOWER_INDEX) {
		return MAX_CONFIG_RECORD_SIZE_BYTES;		
	} else {
		read_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, (unsigned char*) LOG_GLOBAL_BUFFER, 2);
	
		*ptr        = ((unsigned char*) LOG_GLOBAL_BUFFER)[0];
		*(ptr + 1) 	= ((unsigned char*) LOG_GLOBAL_BUFFER)[1];
	
		memset(LOG_GLOBAL_BUFFER, 0, sizeof(LOG_GLOBAL_BUFFER));
		return c;
	}
}

int is_record_valid(unsigned short recordNumber)
{
	if(recordNumber > CONFIG_RECORDS_LOWER_INDEX) {
		if(getRecordSize(recordNumber) == INVALID_SIZE)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Record Number [%u] Failed !!!", recordNumber);
			debug_log(LOG_GLOBAL_BUFFER);
		    return FAILURE;
		}
		sg_sprintf(LOG_GLOBAL_BUFFER, "Record Number [%u] Passed !!!", recordNumber);
		debug_log(LOG_GLOBAL_BUFFER);
	}
    return SUCCESS;
}


void mark_record_invalid(unsigned short recordNumber)
{
    unsigned char tmp[1] = {0};
	if(recordNumber <= CONFIG_RECORDS_LOWER_INDEX) {
		return;
	}
    write_record(recordNumber, tmp, INVALID_SIZE, RECORD_TYPE_DATA);
}

int read_record_from_persistent_storage(unsigned short recordNumber, char *buffer, int maxBufferLength, recordType_e recordType)
{
    read_record(recordNumber, (unsigned char*)buffer,maxBufferLength,  recordType);
    return SUCCESS;
}

void ll_display_config_val( void )
{
	
}

int write_record_on_persistent_storage(unsigned short recordNumber, const char *record, int recordLen, recordType_e recordType)
{
    write_record(recordNumber, (unsigned char *)record, recordLen, recordType);	
    return SUCCESS;
}

void ll_clear_flash_memory(void)
{
	sg_sprintf(LOG_GLOBAL_BUFFER, "Formating Flash Memory...");
	info_log(LOG_GLOBAL_BUFFER);
	
	for(int i = MBR_INDEX; i < MAX_RECORDS - 1; i += 4)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Erasing Page [%u to %u]", i, i + 3);
		info_log(LOG_GLOBAL_BUFFER);
		at25dfx_erase_block(i * MAX_RECORD_SIZE_BYTES);
		delay_us(50);
	}
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Formating Flash Memory Completed!!!");
	info_log(LOG_GLOBAL_BUFFER);	
}

void ll_init_persistent_storage(void)
{
	switch_spi(FLASH_PERIPHERAL, 0);
}