/*******************************************************************************
 * Contributors:
 *
 *      Ajay Garg <ajay.garg@sensegrow.com>
 *
 *******************************************************************************/



#include "../../../common/instamsg/driver/include/globals.h"
#include "../../../common/instamsg/driver/include/log.h"
#include "../../../common/instamsg/driver/include/data_logger.h"
#include "../../../common/instamsg/driver/include/misc.h"
#include "../common/storage_utils.h"

#include <string.h>
#include <asf.h>

#define DATA_FILE_NAME					"data"
#define BETWEEN_FILE					"between"

unsigned char sdCardPresent;
static char tempBuffer[MAX_BUFFER_SIZE];

static void log_error_message(const char *method, const char *step)
{
	sg_sprintf(LOG_GLOBAL_BUFFER, "Error while [%s] in [%s], resetting device", step, method);
	error_log(LOG_GLOBAL_BUFFER);	
	
	resetDevice();
}


static void deleteFileIfExists(const char *path, unsigned char doSuccessLogging)
{
	FRESULT res = f_unlink(path);
	if(res == FR_NO_FILE)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "[%s] does not exist, so cant be deleted", path);
		info_log(LOG_GLOBAL_BUFFER);
	}
	else if(res == FR_OK)
	{
		if(doSuccessLogging == 1)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Successfully deleted [%s]", path);
			info_log(LOG_GLOBAL_BUFFER);
		}
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not delete file [%s] due to unknown reason :(, resetting device", path);
		error_log(LOG_GLOBAL_BUFFER);
		
		resetDevice();
	}
}


static void renameFile(const char *oldPath, const char *newPath, unsigned char doSuccessLogging)
{
	FRESULT res = FAILURE;
	if(1)
	{
		res = f_rename(oldPath, newPath);
		if(res == FR_NO_FILE)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "%sCould not move file from [%s] to [%s], as [%s] does not exist",
										   DATA_LOGGING_ERROR, oldPath, newPath, oldPath);
			error_log(LOG_GLOBAL_BUFFER);
		}
		else if(res == FR_EXIST)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "%sCould not move file from [%s] to [%s], as [%s] already exists",
										   DATA_LOGGING_ERROR, oldPath, newPath, newPath);
			error_log(LOG_GLOBAL_BUFFER);
		}
		else if(res == FR_OK)
		{
			if(doSuccessLogging == 1)
			{
				sg_sprintf(LOG_GLOBAL_BUFFER, "%sSuccessfully moved file from [%s] to [%s]", DATA_LOGGING_ERROR, oldPath, newPath);
				info_log(LOG_GLOBAL_BUFFER);	
			}			
		}
		else
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "%sCould not move file from [%s] to [%s] due to unknown reason, resetting device",
										  DATA_LOGGING_ERROR, oldPath, newPath);
			error_log(LOG_GLOBAL_BUFFER);	
			
			resetDevice();		
		}
	}
}


static void closeFileOrReset(FIL *fp)
{
	if(f_close(fp) != FR_OK)
	{
		log_error_message("closeFileOrReset", "f_close");
	}
}


static int openFileInReadingMode(FIL *fp, const char *filePath)
{
	int rc = FAILURE;
	
	FRESULT res = f_open(fp, filePath, FA_OPEN_EXISTING | FA_READ);
	if(res == FR_NO_FILE)
	{
		rc = FAILURE;
	}
	else if(res == FR_OK)
	{
		rc = SUCCESS;
	}
	else
	{
		log_error_message(filePath, "f_open in openFileInReadingMode");
	}
	
	return rc;
}


static unsigned long getFileSizeLocal(const char *filePath)
{
	FIL fp;
	unsigned long size = 0;
	
	int res = openFileInReadingMode(&fp, filePath);	
	if(res == SUCCESS)
	{
		size = fp.fsize;
		closeFileOrReset(&fp);
	}
	
	return size;
}


static int sg_createEmptyFile(const char *filePath)
{
	int rc = FAILURE;
	
	FIL fp;
	FRESULT res;

	res = f_open(&fp, filePath, FA_CREATE_ALWAYS | FA_WRITE);
	if(res == FR_OK)
	{
		closeFileOrReset(&fp);
		rc = SUCCESS;
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not open file %s in write-mode", filePath);
		error_log(LOG_GLOBAL_BUFFER);
	}

	return rc;
}


static void sg_readLine(FIL *fp, char *buffer, int maxBufferLength)
{
    memset(buffer, 0, maxBufferLength);

    if(1)
    {
		if(f_eof(fp) == 1)
		{
		}
		else
		{
			f_gets(buffer, maxBufferLength, fp);
		}
    }

    /*
     * Remove any trailing \r, if any.
     */
    while(1)
    {
		{
			if(strlen(buffer) < 1)
			{
				break;
			}
		}
		
        int lastIndex = strlen(buffer) - 1;
        if(buffer[lastIndex] == '\r')
        {
            buffer[lastIndex] = 0;
        }
        else
        {
            break;
        }
    }
	
	while(1)
    {
		{
			if(strlen(buffer) < 1)
			{
				break;
			}
		}
		
        int lastIndex = strlen(buffer) - 1;
        if(buffer[lastIndex] == '\n')
        {
            buffer[lastIndex] = 0;
        }
        else
        {
            break;
        }
    }
}


static void writeBufferToFileOrReset(FIL *fp, char *buffer, unsigned int len)
{
	FRESULT res;
	unsigned int bytesWritten;
	
	res = f_write(fp, buffer, len, &bytesWritten);
	if(res == FR_OK)
	{
		if(bytesWritten == len)
		{
			/*
			* We are done
			*/			
		}
		else
		{
			log_error_message("writeBufferToFileOrReset", "f_write, invalid bytes written");
		}
	}
	else
	{
		log_error_message("writeBufferToFileOrReset", "f_write, invalid result-code");
	}
}


static int sg_appendLine(const char *filePath, const char *buffer)
{
	int rc = FAILURE;
	
	FRESULT res;
	FIL fp;
	
	unsigned long currentSize = getFileSizeLocal(filePath);
	if(currentSize > 0)
	{
		res = f_open(&fp, filePath, FA_OPEN_EXISTING | FA_WRITE);
	}
	else
	{
		res = f_open(&fp, filePath, FA_CREATE_ALWAYS | FA_WRITE);
	}
	
	if(res == FR_OK)
	{
		if(f_lseek(&fp, fp.fsize) == FR_OK)
		{
			writeBufferToFileOrReset(&fp, (char*) buffer, strlen(buffer));
			
			{
				char small[2] = {0};
				small[0] = '\n';
				
				writeBufferToFileOrReset(&fp, small, 1);
			}
			
			closeFileOrReset(&fp);
			rc = SUCCESS;
		}
		else
		{
			log_error_message("sg_appendLine", "f_lseek");
		}
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not open file %s in append-mode", filePath);
		error_log(LOG_GLOBAL_BUFFER);
	}

	return rc;
}


static int sg_copyFile(const char *oldPath, const char *newPath)
{
	FIL fp;

	int res = openFileInReadingMode(&fp, oldPath);
	if(res == SUCCESS)
	{
		if(1)
		{
			while(1)
			{
				memset(tempBuffer, 0, sizeof(tempBuffer));
				sg_readLine(&fp, tempBuffer, sizeof(tempBuffer));

				if(strlen(tempBuffer) == 0)
				{
					break;
				}
				else
				{
					sg_appendLine(newPath, tempBuffer);
				}
			}
		}
		
		closeFileOrReset(&fp);
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not open [%s] for copying to [%s]", oldPath, newPath);
		error_log(LOG_GLOBAL_BUFFER);
	}

	return SUCCESS;
}


static void write_next_read_and_write_record_number_values(unsigned short readNumber, unsigned short writeNumber)
{
    if(writeNumber == (DATA_RECORDS_UPPER_INDEX + 1))
    {
        writeNumber = FIRST_RECORD_NUMBER_VALUE;
    }
    if(readNumber == (DATA_RECORDS_UPPER_INDEX + 1))
    {
        readNumber = FIRST_RECORD_NUMBER_VALUE;
	}
}


static void display_persistent_storage_info(const char *medium)
{
	startAndCountdownTimer(1, 0);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "\n\nMedium being used for data-logging ==> [%s]\n\n", medium);
	info_log(LOG_GLOBAL_BUFFER);
	
	startAndCountdownTimer(1, 0);
}


static unsigned char sd_card_init_done;
FATFS fs;

static void init_sd_card()
{
	Ctrl_status status;
	unsigned char cardDetected = 0;
	
	if(sd_card_init_done == 1)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "SD-Card already initialized ..");
		info_log(LOG_GLOBAL_BUFFER);
		
		return;
	}
	

	sd_mmc_init();
	
	while(1)
	{
		status = sd_mmc_test_unit_ready(0);
		if(status == CTRL_FAIL)
		{
			break;
		}
		else if(status == CTRL_GOOD)
		{
			cardDetected = 1;
			break;
		}
		
		sg_sprintf(LOG_GLOBAL_BUFFER, "Waiting for SD-Card detection to pass/fail");
		info_log(LOG_GLOBAL_BUFFER);
		
		startAndCountdownTimer(1, 0);
	}
	
	
	if(cardDetected == 1)
	{
		memset(&fs, 0, sizeof(FATFS));
		if(f_mount(LUN_ID_SD_MMC_0_MEM, &fs) == FR_OK)
		{
			sdCardPresent = 1;
			
			renameFile(BETWEEN_FILE, DATA_FILE_NAME, 1);
			deleteFileIfExists(BETWEEN_FILE, 1);
			
			deleteFileIfExists(SYSTEM_WIDE_TEMP_FILE, 1);
		}
		else
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "SD-Card detected, but could not be mounted :(");
			error_log(LOG_GLOBAL_BUFFER);
		}
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "SD-Card not detected.");
		info_log(LOG_GLOBAL_BUFFER);
	}
}


/*
 * This method initializes the data-logger-interface for the device.
 */
void init_data_logger()
{
	init_sd_card();
	
	if(sdCardPresent == 1)
	{
		display_persistent_storage_info("SD");	
	}
	else
	{
		display_persistent_storage_info("FLASH");
	}
}


static void save_record_to_flash_persistent_storage(char *record)
{
    unsigned short nextReadNumber, nextWriteNumber;

    //read_next_read_and_write_record_number_values(&nextReadNumber, &nextWriteNumber);
    write_record_on_persistent_storage(nextWriteNumber, record, strlen(record), RECORD_TYPE_DATA);

    /*
     * If till this point, there was no record to be read, so point the read-pointer to the just-written record.
     */
    if(nextReadNumber == NO_MORE_RECORD_TO_READ)
    {
        nextReadNumber = nextWriteNumber;
    }

    nextWriteNumber++;
    write_next_read_and_write_record_number_values(nextReadNumber, nextWriteNumber);
}


static void updateDataFile()
{
	renameFile(DATA_FILE_NAME, BETWEEN_FILE, 0);
	renameFile(SYSTEM_WIDE_TEMP_FILE, DATA_FILE_NAME, 0);
	
	deleteFileIfExists(BETWEEN_FILE, 0);
}


static void save_record_to_sd_persistent_storage(char *record)
{
	FIL fp;
	unsigned long currentFileSize = 0;
	
    while(1)
    {
        currentFileSize = getFileSizeLocal(DATA_FILE_NAME);
		
		sg_sprintf(LOG_GLOBAL_BUFFER, "Current data-log-file size = [%lu]", currentFileSize);
		info_log(LOG_GLOBAL_BUFFER);

        if(currentFileSize < MAX_DATA_LOGGER_SIZE_BYTES)
        {
            break;
        }
        else
        {
            /*
             * Remove the first (oldest) record.
             */
            unsigned char firstLineIgnored = 0;

            sg_sprintf(LOG_GLOBAL_BUFFER, "%sRemoving the oldest record ...., current size [%lu]", DATA_LOGGING, currentFileSize);
            error_log(LOG_GLOBAL_BUFFER);

            if(openFileInReadingMode(&fp, DATA_FILE_NAME) == SUCCESS)
            {
                while(1)
                {
                    memset(tempBuffer, 0, sizeof(tempBuffer));
                    sg_readLine(&fp, tempBuffer, sizeof(tempBuffer));

                    if(strlen(tempBuffer) == 0)
                    {
                        break;
                    }
                    else if(firstLineIgnored == 1)
                    {
                        sg_appendLine(SYSTEM_WIDE_TEMP_FILE, tempBuffer);
                    }

                    firstLineIgnored = 1;
                }
				
				closeFileOrReset(&fp);
				updateDataFile();
            }
            else
            {
                sg_sprintf(LOG_GLOBAL_BUFFER, "%sCould not open file [%s] for reading", DATA_LOGGING_ERROR, DATA_FILE_NAME);
                error_log(LOG_GLOBAL_BUFFER);
            }
        }
    }

    sg_copyFile(DATA_FILE_NAME, SYSTEM_WIDE_TEMP_FILE);
    sg_appendLine(SYSTEM_WIDE_TEMP_FILE, record);
	
    updateDataFile();
}


/*
 * This method saves the record on the device.
 *
 * If and when the device-storage becomes full, the device MUST delete the oldest record, and instead replace
 * it with the current record. That way, we will maintain a rolling-data-logger.
 */
void save_record_to_persistent_storage(char *record)
{
	if(sdCardPresent == 0)
	{
		save_record_to_flash_persistent_storage(record);
	}
	else
	{
		save_record_to_sd_persistent_storage(record);
	}
}


#define NO_RECORD_TO_READ_CODE                                                              \
    sg_sprintf(LOG_GLOBAL_BUFFER, "No more records to read from persistent-storage.");      \
    info_log(LOG_GLOBAL_BUFFER);                                                            \
                                                                                            \
    nextReadNumber = NO_MORE_RECORD_TO_READ;                                                \
    rc = FAILURE;                                                                           \
                                                                                            \
    goto exit;

static int get_next_record_from_flash_persistent_storage(char *buffer, int maxLength)
{
    int rc;
    unsigned short nextReadNumber, nextWriteNumber;

    //read_next_read_and_write_record_number_values(&nextReadNumber, &nextWriteNumber);
	if(nextReadNumber == NO_MORE_RECORD_TO_READ)
    {
        NO_RECORD_TO_READ_CODE
    }

    /*
     * Check if the record is valid, and proceed only if it is.
     */
    if(is_record_valid(nextReadNumber) == FAILURE)
    {
        NO_RECORD_TO_READ_CODE
    }

    /*
     * If we reach till here, read the actual record.
     */
    read_record_from_persistent_storage(nextReadNumber, buffer, maxLength, RECORD_TYPE_DATA);

    /*
     * Mark the just-read record invalid.
     */
    mark_record_invalid(nextReadNumber);

    /*
     * Finally, move the read-pointer, to (try and ) read the next-record.
     */
    nextReadNumber++;
    rc = SUCCESS;

exit:
    write_next_read_and_write_record_number_values(nextReadNumber, nextWriteNumber);
    return rc;
}


static int get_next_record_from_sd_persistent_storage(char *buffer, int maxLength)
{
    int rc = FAILURE;
    unsigned char lineRead = 0;
	
	FIL file_object;
	
	if(openFileInReadingMode(&file_object, DATA_FILE_NAME) == FAILURE)
    {
        sg_sprintf(LOG_GLOBAL_BUFFER, "%sCould not open data-log-file [%s] for reading", DATA_LOGGING_ERROR, DATA_FILE_NAME);
        error_log(LOG_GLOBAL_BUFFER);

        goto exit;
    }
	
	{	
        long currentFileSize = getFileSizeLocal(DATA_FILE_NAME);
        
        sg_sprintf(LOG_GLOBAL_BUFFER, "Current data-log-file size = [%lu]", currentFileSize);
        info_log(LOG_GLOBAL_BUFFER);
	}

    sg_createEmptyFile(SYSTEM_WIDE_TEMP_FILE);

    while(1)
    {
        memset(tempBuffer, 0, sizeof(tempBuffer));
        sg_readLine(&file_object, tempBuffer, sizeof(tempBuffer));

        if(strlen(tempBuffer) == 0)
        {
            /*
             * Nothing else was left to read.
             */
			closeFileOrReset(&file_object);
            goto exit;
        }
        else
        {
            if(lineRead == 0)
            {
                /*
                 * This is the data we want.
                 */
                memcpy(buffer, tempBuffer, strlen(tempBuffer));
                lineRead = 1;

                rc = SUCCESS;
            }
            else
            {
                /*
                 * Copy to temp-file.
                 */
                sg_appendLine(SYSTEM_WIDE_TEMP_FILE, tempBuffer);
            }
        }
    }

    {
        unsigned int i = 0;
        unsigned char onlyWhiteSpace = 1;

        for (i = strlen(buffer) - 1; i >= 0; i--)
        {
            if ((buffer[i] != ' ') && (buffer[i] != '\r') && (buffer[i] != '\n'))
            {
                onlyWhiteSpace = 0;
                break;
            }
        }

        if (onlyWhiteSpace == 1)
        {
            buffer[0] = 0;
        }
    }	
	
	closeFileOrReset(&file_object);
	
exit:

	updateDataFile();
    return rc;
}


/*
 * The method returns the next available record.
 * If a record is available, following must be done ::
 *
 * 1)
 * The record must be deleted from the storage-medium (so as not to duplicate-fetch this record later).
 *
 * 2)
 * Then actually return the record.
 *
 * Obviously, there is a rare chance that step 1) is finished, but step 2) could not run to completion.
 * That would result in a data-loss, but we are ok with it, because we don't want to send duplicate-records to InstaMsg-Server.
 *
 * We could have done step 2) first and then step 1), but in that scenario, we could have landed in a scenario where step 2)
 * was done but step 1) could not be completed. That could have caused duplicate-data on InstaMsg-Server, but we don't want
 * that.
 *
 *
 * One of the following statuses must be returned ::
 *
 * a)
 * SUCCESS, if a record is successfully returned.
 *
 * b)
 * FAILURE, if no record is available.
 */
int get_next_record_from_persistent_storage(char *buffer, int maxLength)
{
	if(sdCardPresent == 0)
	{
		return get_next_record_from_flash_persistent_storage(buffer, maxLength);
	}
	else
	{
		return get_next_record_from_sd_persistent_storage(buffer, maxLength);
	}
}


/*
 * This method releases the data-logger, just before the system is going for a reset.
 */
void release_data_logger()
{
}
