/*******************************************************************************
 * Contributors:
 *
 *      Ajay Garg <ajay.garg@sensegrow.com>
 *
 *******************************************************************************/




#include "../../../common/instamsg/driver/include/globals.h"
#include "../../../common/instamsg/driver/include/log.h"
#include "../../../common/instamsg/driver/include/config.h"
#include "../../../common/instamsg/driver/include/sg_mem.h"
#include "../../../common/instamsg/driver/include/json.h"
#include "../common/storage_utils.h"
#include "../../../device/kritikal-gateway/instamsg/misc/ll_config.h"
#include "../../../device/kritikal-gateway/instamsg/misc/ll_console.h"
#include "../../../common/ioeye/include/serial.h"
//#include "sg_stdlib.h"

#include <string.h>

static char tempBuffer[MAX_BUFFER_SIZE];

#define INVALID_RECORD_NUMBER                 -1

#if 0
static int get_config_value_from_persistent_storage_and_delete_if_asked(const char *key, char *buffer, int maxBufferLength,
                                                                        unsigned char deleteConfig, int ignoreRecordNumber)
{
    int rc = FAILURE;
    int i;
    char *jsonKey = NULL;

    jsonKey = (char*) sg_malloc(MAX_BUFFER_SIZE);
    if(jsonKey == NULL)
    {
        sg_sprintf(LOG_GLOBAL_BUFFER, "%sCould not allocate memory in config.. not proceeding", CONFIG_ERROR);
        error_log(LOG_GLOBAL_BUFFER);

        goto exit;
    }

    for(i = CONFIG_RECORDS_LOWER_INDEX; i <= CONFIG_RECORDS_UPPER_INDEX; i++)
    {
        if(is_record_valid(i) == SUCCESS)
        {
            memset(tempBuffer, 0, sizeof(tempBuffer));
            read_record_from_persistent_storage(i, tempBuffer, sizeof(tempBuffer), RECORD_TYPE_CONFIG);

            if(strlen(tempBuffer) == 0)
            {
                continue;
            }
            else
            {
                memset(jsonKey, 0, MAX_BUFFER_SIZE);
                getJsonKeyValueIfPresent(tempBuffer, CONFIG_KEY_KEY, jsonKey);

                if(strcmp(jsonKey, key) == 0)
                {
                    if(deleteConfig == 0)
                    {
                        int i;
                        for(i = 0; i < strlen(tempBuffer); i++)
                        {
                            buffer[i] = tempBuffer[i];
                        }
                    }
                    else
                    {
                        if(i != ignoreRecordNumber)
                        {
                            mark_record_invalid(i);
                        }
                    }

                    rc = SUCCESS;

                    /*
                     * We keep looking further, just in case there are multiple-values of the config.
                     * If BYYY CHHHANCCE anyone stores multiple values, following will happen ::
                     *
                     * a)
                     * For getting config, the LAST config (for the particular key) will be picked.
                     *
                     * b)
                     * For deleting config, ALL configs (for the particular key) will be deleted.
                     */
                }
                else
                {
                    continue;
                }
            }
        }
    }

exit:
    if(jsonKey != NULL)
    {
        sg_free(jsonKey);
    }

    return rc;
}
#endif

/*
 * This method initializes the Config-Interface for the device.
 */
void init_config()
{
    ll_init_persistent_storage();
	updateSystemConfigurations(0);
}


/*
 * This method fills in the JSONified-config-value for "key" into "buffer".
 *
 * It returns the following ::
 *
 * SUCCESS ==> If a config with the specified "key" is found.
 * FAILURE ==> If no config with the specified "key" is found.
 */
int get_config_value_from_persistent_storage(const char *key, char *buffer, int maxBufferLength)
{
	int rc = SUCCESS;
	CONFIG_S* currConfig = getCurrentConfig();
	char portType;
	char *portName;
	
	memset(buffer, '\0', maxBufferLength);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Getting Key: %s", key);
	info_log(LOG_GLOBAL_BUFFER);
	
	if(0 == strcmp(key, JSON_PING_REQ_INTERVAL)) {
		sg_sprintf(buffer, "%d", currConfig->ping_request_interval);
	} else if(0 == strcmp(key, JSON_BUSINESS_LOGIC_INTERVAL)) {
		sg_sprintf(buffer, "%d", currConfig->business_logic_interval);
	} else if(0 == strcmp(key, JSON_ENABLE_MESSAGE_ACK)) {
		sg_sprintf(buffer, "%d", currConfig->enable_message_ack);
	} else if(0 == strcmp(key, JSON_DEBUG_LOGGING_ENABLED)) {
		/*
			// commented due to bug 
			sg_sprintf(LOG_GLOBAL_BUFFER, "curr config: %d", currConfig->enable_debug_logging);
			info_log(LOG_GLOBAL_BUFFER);
			currConfig->enable_debug_logging = 0x30;
			sg_sprintf(buffer, "%d", currConfig->enable_debug_logging);
			sg_sprintf(LOG_GLOBAL_BUFFER, "%s", buffer);
			info_log(LOG_GLOBAL_BUFFER);
		*/
	} else if(0 == strcmp(key, JSON_SSL_ENABLE)) {
		sg_sprintf(buffer, "%d", currConfig->enable_ssl);
	} else if(0 == strcmp(key, JSON_SEND_GPS_LOCATION_INTERVAL)) {
		sg_sprintf(buffer, "%d", currConfig->gps_location_interval);
	} else if(0 == strcmp(key, JSON_COMPULSORY_SOCKET_READ_AFTER_WRITE_COUNT)) {
		sg_sprintf(buffer, "%d", currConfig->compulsory_read_after_write_count);
	} else if(0 == strcmp(key, JSON_NTP_SERVER)) {
		strcpy(buffer, currConfig->ntp_server_add);
	} else if(0 == strcmp(key, JSON_OTA_SERVER_HOST_NAME)) {
		strcpy(buffer, currConfig->fota_server_hostname);
	} else if(0 == strcmp(key, JSON_OTA_SERVER_HOST_IP)) {
		strcpy(buffer, currConfig->fota_server_ip_add);
	} else if(0 == strcmp(key, JSON_OTA_SERVER_HOST_ADDRESS)) {
		strcpy(buffer, currConfig->fota_server_add);
	} else if(0 == strcmp(key, JSON_OTA_SERVER_PORT_NUMBER)) {
		sg_sprintf(buffer, "%d", currConfig->fota_server_port_number);
	} else if(0 == strcmp(key, JSON_ETHERNET_STATIC_IP_PARAMS)){
		//if(strlen())
	} else if(0 == strcmp(key, JSON_NUM_PORTS_IN_USE)) {
		sg_sprintf(buffer, "%d", currConfig->num_port);
	} else if(0 == strcmp(key, JSON_PORT_1_COMMANDS)) {
		strcpy(buffer, currConfig->modbus_port[0].command);
	} else if(0 == strcmp(key, JSON_PORT_1_SERIAL_DELIMITER)) {
		sg_sprintf(buffer, "%d", currConfig->modbus_port[0].port_delimiter);
	} else if(0 == strcmp(key, JSON_PORT_1_SERIAL_PARAMS)) {
		strcpy(buffer, currConfig->modbus_port[0].port_parameter);
	} else if(0 == strcmp(key, JSON_PORT_1_TYPE)) {
		portName = portTypeNumberToNameConverter(currConfig->modbus_port[0].port_type);
		if(NULL != portName) {
			strcpy(buffer, portName);
		} else {
			rc = FAILURE;
		}
	} else if(0 == strcmp(key, JSON_PORT_2_COMMANDS)) {
		strcpy(buffer, currConfig->modbus_port[1].command);
	} else if(0 == strcmp(key, JSON_PORT_2_SERIAL_DELIMITER)) {
		sg_sprintf(buffer, "%d", currConfig->modbus_port[1].port_delimiter);
	} else if(0 == strcmp(key, JSON_PORT_2_SERIAL_PARAMS)) {
		strcpy(buffer, currConfig->modbus_port[1].port_parameter);
	} else if(0 == strcmp(key, JSON_PORT_2_TYPE)) {
		portName = portTypeNumberToNameConverter(currConfig->modbus_port[1].port_type);
		if(NULL != portName) {
			strcpy(buffer, portName);
		} else {
			rc = FAILURE;
		}
	} else if(0 == strcmp(key, JSON_PORT_3_COMMANDS)) {
		strcpy(buffer, currConfig->modbus_port[2].command);
	} else if(0 == strcmp(key, JSON_PORT_3_SERIAL_DELIMITER)) {
		sg_sprintf(buffer, "%d", currConfig->modbus_port[2].port_delimiter);
	} else if(0 == strcmp(key, JSON_PORT_3_SERIAL_PARAMS)) {
		strcpy(buffer, currConfig->modbus_port[2].port_parameter);
	} else if(0 == strcmp(key, JSON_PORT_3_TYPE)) {
		portName = portTypeNumberToNameConverter(currConfig->modbus_port[2].port_type);
		if(NULL != portName) {
			strcpy(buffer, portName);
		} else {
			rc = FAILURE;
		}
	} else if(0 == strcmp(key, JSON_SECRET)) {
		strcpy(buffer, currConfig->device_secret_id);
	} else {
		rc = FAILURE;
	}
	
	return rc;
	
	/*
		return get_config_value_from_persistent_storage_and_delete_if_asked(key, buffer, maxBufferLength, 0, INVALID_RECORD_NUMBER);
	*/
}

/*
 ****************************************************************************************
~#  Function Name : subString                                                           #~ 
~#  Parameters    :                                                                     #~
~#                :  str    -> Source String                                            #~
~#                :  Count  -> Source String Size                                       #~
~#                :  subStr -> Sub String to find in source string                      #~
~#                :  subLen -> Sub String Size                                          #~
~#  Return Type   : SUCCESS if Match Found, FAILURE otherwise.                          #~
~#  Scope         : Global                                                              #~
~#  Functionality : This function will Check that a given subString is present in       #~
~#                  String or not on the basis of bot string and subString Sizes        #~
~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~##~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~                                                                  
~# Date: #20/11/2019#                                    Written by: #Ashutosh#         #~
*****************************************************************************************
*/
uint8_t subString(char *str, unsigned short Count,char *subStr,unsigned short subLen) {
	uint8_t iLoop=0,jLoop=0;															// Array Index/Loop
	uint8_t sComp=0;																	// Start Compare Process
	for(iLoop=0;iLoop<Count;iLoop++){										            // Loop till string termination symbol
		if((sComp==0)&&(*(str+iLoop)==*subStr)){										// If subString first character matched
            sComp=1;jLoop=0;                                                            // SET Flag to start the comparison 
        }
        if(sComp==1){
            if(jLoop<subLen){
                if(*(str+iLoop) != *(subStr+jLoop)){                                    // Compare character by character, If it fails then clear the comparison Flag
                    sComp=0;jLoop=0;
                }else{
                    jLoop++;                                                            // Otherwise increase the pointer to the next character to match
                }
            }
            if(jLoop>=subLen)                                                           // If matched complete substring then return SUCCESS
                return SUCCESS;
        }/* End of Compare Start IF */
	}/* End of For*/	
    return FAILURE;                                                                     // Return FAILURE otherwise
}

/*
 * This method saves the JSONified-config-value for "key" onto persistent-storage.
 * The example value is of the form ::
 *
 *      {'key' : 'key_value', 'type' : '1', 'val' : 'value', 'desc' : 'description for this config'}
 *
 *
 * Note that for the 'type' field :
 *
 *      '0' denotes that the key-type is of STRING
 *      '1' denotes that the key-type is of INTEGER (although it is stored in stringified-form in 'val' field).
 *
 * It returns the following ::
 *
 * SUCCESS ==> If the config was successfully saved.
 * FAILURE ==> If the config could not be saved.
 */
int save_config_value_on_persistent_storage(const char *key, const char *value, unsigned char logging)
{
    char isSaveConfigRequired = 0;
	int rc = FAILURE;
	CONFIG_S* currConfig = getCurrentConfig();
	char portType;
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Key: %s, Value: %s", key, value);
	info_log(LOG_GLOBAL_BUFFER);
	
	if(0 == strcmp(key, JSON_PING_REQ_INTERVAL)) {
		if(atoi(value) != currConfig->ping_request_interval) {
			isSaveConfigRequired = 1;
			currConfig->ping_request_interval = atoi(value);					
		}
	} else if(0 == strcmp(key, JSON_BUSINESS_LOGIC_INTERVAL)) {
		if(atoi(value) != currConfig->business_logic_interval) {
			isSaveConfigRequired = 1;
			currConfig->business_logic_interval = atoi(value);
		}
	} else if(0 == strcmp(key, JSON_ENABLE_MESSAGE_ACK)) {
		if(atoi(value) != currConfig->enable_message_ack) {
			isSaveConfigRequired = 1;
			currConfig->enable_message_ack = atoi(value);
		}
	} else if(0 == strcmp(key, JSON_DEBUG_LOGGING_ENABLED)) {
		if(atoi(value) != currConfig->enable_debug_logging) {
			isSaveConfigRequired = 1;
			currConfig->enable_debug_logging = atoi(value);
		}
	} else if(0 == strcmp(key, JSON_SSL_ENABLE)) {
		if(atoi(value) != currConfig->enable_ssl) {
			isSaveConfigRequired = 1;
			currConfig->enable_ssl = atoi(value);
		}
	} else if(0 == strcmp(key, JSON_SEND_GPS_LOCATION_INTERVAL)) {
		if(atoi(value) != currConfig->gps_location_interval) {
			isSaveConfigRequired = 1;
			currConfig->gps_location_interval = atoi(value);
		}
	} else if(0 == strcmp(key, JSON_COMPULSORY_SOCKET_READ_AFTER_WRITE_COUNT)) {
		if(atoi(value) != currConfig->compulsory_read_after_write_count) {
			isSaveConfigRequired = 1;	
			currConfig->compulsory_read_after_write_count = atoi(value);	
		}
	} else if(0 == strcmp(key, JSON_NTP_SERVER)) {
		if(0 != strcmp(value, currConfig->ntp_server_add)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->ntp_server_add, value, sizeof(currConfig->ntp_server_add));
		}
	} else if(0 == strcmp(key, JSON_OTA_SERVER_HOST_NAME)) {
		if(0 != strcmp(value, currConfig->fota_server_hostname)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->fota_server_hostname, value, sizeof(currConfig->fota_server_hostname));
		}
	} else if(0 == strcmp(key, JSON_OTA_SERVER_HOST_IP)) {
		if(0 != strcmp(value, currConfig->fota_server_ip_add)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->fota_server_ip_add, value, sizeof(currConfig->fota_server_ip_add));
		}
	} else if(0 == strcmp(key, JSON_OTA_SERVER_HOST_ADDRESS)) {
		if(0 != strcmp(value, currConfig->fota_server_add)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->fota_server_add, value, sizeof(currConfig->fota_server_add));
		}
	} else if(0 == strcmp(key, JSON_OTA_SERVER_PORT_NUMBER)) {
		if(atoi(value) != currConfig->fota_server_port_number) {
			isSaveConfigRequired = 1;
			currConfig->fota_server_port_number = atoi(value);	
		}
	} else if(0 == strcmp(key, JSON_ETHERNET_STATIC_IP_PARAMS)) {
		if(strlen(value) > 0) {
			//get_nth_token_thread_safe(value, ',', 1, currConfig->local_static_ip_add, 1);
			//get_nth_token_thread_safe(value, ',', 2, currConfig->local_subnet_mask, 1);
			//get_nth_token_thread_safe(value, ',', 3, currConfig->local_gateway_ip, 1);
		}				
	} else if(0 == strcmp(key, JSON_NUM_PORTS_IN_USE)) {
		if(atoi(value) != currConfig->num_port) {
			isSaveConfigRequired = 1;
			currConfig->num_port = atoi(value);	
		}
	} else if(0 == strcmp(key, JSON_PORT_1_COMMANDS)) {
		if(0 != strcmp(value, currConfig->modbus_port[0].command)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->modbus_port[0].command, value, sizeof(currConfig->modbus_port[0].command));
		}
	} else if(0 == strcmp(key, JSON_PORT_1_SERIAL_DELIMITER)) {
		if(atoi(value) != currConfig->modbus_port[0].port_delimiter) {
			isSaveConfigRequired = 1;
			currConfig->modbus_port[0].port_delimiter = atoi(value);		
		}
	} else if(0 == strcmp(key, JSON_PORT_1_SERIAL_PARAMS)) {
		if(0 != strcmp(value, currConfig->modbus_port[0].port_parameter)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->modbus_port[0].port_parameter, value, sizeof(currConfig->modbus_port[0].port_parameter));
		}
	} else if(0 == strcmp(key, JSON_PORT_1_TYPE)) {
		portType = portTypeNameToNumberConverter(value);
		if(portType != currConfig->modbus_port[0].port_type) {
			isSaveConfigRequired = 1;
			currConfig->modbus_port[0].port_type = portType;
		}
	} else if(0 == strcmp(key, JSON_PORT_2_COMMANDS)) {
		if(0 != strcmp(value, currConfig->modbus_port[1].command)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->modbus_port[1].command, value, sizeof(currConfig->modbus_port[1].command));
		}
	} else if(0 == strcmp(key, JSON_PORT_2_SERIAL_DELIMITER)) {
		if(atoi(value) != currConfig->modbus_port[1].port_delimiter) {
			isSaveConfigRequired = 1;
			currConfig->modbus_port[1].port_delimiter = atoi(value);
		}
	} else if(0 == strcmp(key, JSON_PORT_2_SERIAL_PARAMS)) {
		if(0 != strcmp(value, currConfig->modbus_port[1].port_parameter)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->modbus_port[1].port_parameter, value, sizeof(currConfig->modbus_port[1].port_parameter));
		}
	} else if(0 == strcmp(key, JSON_PORT_2_TYPE)) {
		portType = portTypeNameToNumberConverter(value);
		if(portType != currConfig->modbus_port[1].port_type) {
			isSaveConfigRequired = 1;
			currConfig->modbus_port[1].port_type = portType;
		}
	} else if(0 == strcmp(key, JSON_PORT_3_COMMANDS)) {
		if(0 != strcmp(value, currConfig->modbus_port[2].command)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->modbus_port[2].command, value, sizeof(currConfig->modbus_port[2].command));
		}
	} else if(0 == strcmp(key, JSON_PORT_3_SERIAL_DELIMITER)) {
		if(atoi(value) != currConfig->modbus_port[2].port_delimiter) {
			isSaveConfigRequired = 1;
			currConfig->modbus_port[2].port_delimiter = atoi(value);
		}
	} else if(0 == strcmp(key, JSON_PORT_3_SERIAL_PARAMS)) {
		if(0 != strcmp(value, currConfig->modbus_port[2].port_parameter)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->modbus_port[2].port_parameter, value, sizeof(currConfig->modbus_port[2].port_parameter));
		}
	} else if(0 == strcmp(key, JSON_PORT_3_TYPE)) {
		portType = portTypeNameToNumberConverter(value);
		if(portType != currConfig->modbus_port[2].port_type) {
			isSaveConfigRequired = 1;
			currConfig->modbus_port[2].port_type = portType;
		}
	} else  if(0 == strcmp(key, JSON_SECRET)) {
		if(0 != strcmp(value, currConfig->device_secret_id)) {
			isSaveConfigRequired = 1;
			strncpy(currConfig->device_secret_id, value, sizeof(currConfig->device_secret_id));
		}
	} else {
		isSaveConfigRequired = -1;
	}
	
	if(1 == isSaveConfigRequired) {
		/* config is changed*/
		save_config_parameters();
		rc = SUCCESS;
	} else if(-1 == isSaveConfigRequired) {
		/* config key not found*/
		rc = FAILURE;	
	} else if(0 == isSaveConfigRequired) {
		/*config is already same*/	
	}	rc = SUCCESS;
	
	if(rc == SUCCESS) {
		sg_sprintf(LOG_GLOBAL_BUFFER, "Config Updated Successfully !!!");
		info_log(LOG_GLOBAL_BUFFER);
	} else {
		sg_sprintf(LOG_GLOBAL_BUFFER, "Config Updation Failed!!!");
		info_log(LOG_GLOBAL_BUFFER);
	}
}

/*
 * This method deletes the JSONified-config-value for "key" (if at all it exists).
 *
 * It returns the following ::
 *
 * SUCCESS ==> If a config with the specified "key" was found and deleted successfully.
 * FAILURE ==> In every other case.
 */
int delete_config_value_from_persistent_storage(const char *key)
{
	/*	
		return get_config_value_from_persistent_storage_and_delete_if_asked(key, NULL, 0, 1, INVALID_RECORD_NUMBER);
	*/
}


/*
 * This method releases the config, just before the system is going for a reset.
 */
void release_config()
{
}
