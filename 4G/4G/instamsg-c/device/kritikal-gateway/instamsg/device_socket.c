#include "device_defines.h"

#include "./device_socket.h"

#include "../modem/socket/simcom-5360.h"
#include "../driver/include/socket.h"
#include "../driver/include/watchdog.h"
#include "../driver/include/at.h"
#include "../driver/include/sg_stdlib.h"
#include "../driver/include/config.h"

#include "../driver/include/time.h"

#include "lwip/api.h"

#include "lwip/ip_addr.h"
#include "lwip/dns.h"

#if (NTP_TIME_SYNC_PRESENT == 1) || (GPS_TIME_SYNC_PRESENT == 1) || (GSM_TIME_SYNC_PRESENT == 1)
static DateParams dateParams;
#endif 

extern unsigned int bytes_sent_over_wire;
extern unsigned int bytes_received_over_wire;

volatile unsigned char ethernetInited;
ip_addr_t reverse_ip(ip_addr_t ip);
unsigned char extract_date_time_cclk(DateParams *tm, char *date_time_string);

#if GSM_INTERFACE_ENABLED == 1
/*
 * This method returns the *****LATEST****** sms, which contains the desired substring, while the following returns 1 ::
 *
 *                                      time_fine_for_time_limit_function()
 *
 * Note that "{" are sometimes not processed correctly by some SIMs, so a prefix-match (which
 * otherwise is a stronger check) is not being done.
 *
 * Please note that this method is called by Instamsg-application, *****BEFORE***** calling
 * "connect_underlying_socket_medium_try_once".
 */
void get_latest_sms_containing_substring(SG_Socket *socket, char *buffer, const char *substring)
{
    simcom_5360_get_latest_sms_containing_substring(socket, buffer, substring);
}
#endif


/*
 * This method tries to establish the socket/socket to "socket->host" on "socket->port".
 *
 * If the connection is successful, then the following must be done by the device-implementation ::
 *                          socket->socketCorrupted = 0;
 *
 * Setting the above value will let InstaMsg know that the connection can be used fine for writing/reading.
 */
extern uint32_t g_ip_mode;
extern char sms[200];

static unsigned gprsModeChecked;

static char localBuffer1[100];
static char localBuffer2[100];

ip_addr_t reverse_ip(ip_addr_t ip)
{
	ip_addr_t ret_ip;
	unsigned char ip1,ip2,ip3,ip4;

	ip4=((ip.addr)>>24) & 0xff;
	ip3=((ip.addr)>>16) & 0xff;
	ip2=((ip.addr)>>8) & 0xff;
	ip1=(ip.addr) & 0xff;

	ret_ip.addr = ((unsigned int)(((ip1) & 0xff) << 24) | (((ip2) & 0xff) << 16) | (((ip3) & 0xff) << 8)  | ((ip4) & 0xff));
	sg_sprintf(LOG_GLOBAL_BUFFER,"Resolved DNS IP Address [%d.%d.%d.%d]",ip1,ip2,ip3,ip4);
	info_log(LOG_GLOBAL_BUFFER);
	return ret_ip;
}
void connect_underlying_modbus_slave_socket_try_once(SG_Socket* socket)
{
	enum netconn_type type = -1;

	sg_sprintf(LOG_GLOBAL_BUFFER, "Connecting MODBUS Slave!!!");
	error_log(LOG_GLOBAL_BUFFER);

	type = NETCONN_TCP;
			
	socket->transitionBufferSize = 2 * MAX_BUFFER_SIZE;
	
	if(socket->transitionBuffer == NULL)
	{
		socket->transitionBuffer = (unsigned char *) sg_malloc(socket->transitionBufferSize);
	}
	if(socket->transitionBuffer == NULL)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not allocate transition-buffer, no point living");
		error_log(LOG_GLOBAL_BUFFER);
			
		resetDevice();
	}
		
	socket->netConnSocket = netconn_new(type);
	if(socket->netConnSocket == NULL)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "%sCould not allocate new connection-object", SOCKET_ERROR_STR);
		error_log(LOG_GLOBAL_BUFFER);
	}
	else
	{
		err_t res = -1;
			
		{
			int f = 0, s = 0, t = 0, fo = 0;
				
			get_ip_address_tokens(socket->host, &f, &s, &t, &fo);
			IP4_ADDR(&(socket->ipAddress), f, s, t, fo);
		}					 
		external_wathdog_init();	
		res = netconn_connect(socket->netConnSocket, &(socket->ipAddress), socket->port);
		if(res != ERR_OK)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Connection to TCP MODBus Slave, Failed!!!");
            info_log(LOG_GLOBAL_BUFFER);
            return;
		}
		else
		{
			netconn_set_recvtimeout(socket->netConnSocket, 1000);
			sg_sprintf(LOG_GLOBAL_BUFFER, "Connection to TCP MODBus Slave, Succeeded !!!");
			info_log(LOG_GLOBAL_BUFFER);
			socket->internalStatus = ERR_OK;
			socket->socketCorrupted = 0;
		}
		external_watchdog_feed();
	}

	sg_sprintf(LOG_GLOBAL_BUFFER, "%s-ETHERNET-SOCKET UNDERLYING_MEDIUM INITIATED FOR HOST = [%s], PORT = [%d].",
				socket->type, socket->host, socket->port);
	info_log(LOG_GLOBAL_BUFFER);
}

unsigned char extract_date_time_cclk(DateParams *tm, char *date_time_string)
{
	int i=0;
	unsigned char flagCopy=0;
	char offset_sign=0;
	int offset=0;
	external_watchdog_feed();
	for(i=0;i<strlen(date_time_string);i++)
	{
		if(0x00==flagCopy)
		{
			if(*(date_time_string+i)==0x22)
			{
				flagCopy=1;
			}
			continue;
		}
		else if(flagCopy>0x00)
		{
			if(*(date_time_string+i)==0x22)
			{
				break;
			}
			else if((*(date_time_string+i)<0x30)||(*(date_time_string+i)>0x39))
			{
				continue;
			}
			
			if(1 == flagCopy)
			{
				tm->tm_year = *(date_time_string+i)-0x30;
				flagCopy=2;
			}
			else if(2 == flagCopy)
			{
				tm->tm_year=tm->tm_year*10 + *(date_time_string+i)-0x30;
				flagCopy=3;
			}
			else if(3 == flagCopy)
			{
				tm->tm_mon=*(date_time_string+i)-0x30;
				flagCopy=4;
			}
			else if(4 == flagCopy)
			{
				tm->tm_mon=tm->tm_mon*10 + *(date_time_string+i)-0x30;
				flagCopy=5;
			}
			else if(5 == flagCopy)
			{
				tm->tm_mday=*(date_time_string+i)-0x30;
				flagCopy=6;
			}
			else if(6 == flagCopy)
			{
				tm->tm_mday=tm->tm_mday*10 + *(date_time_string+i)-0x30;
				flagCopy=7;
			}
			else if(7 == flagCopy)
			{
				tm->tm_hour=*(date_time_string+i)-0x30;
				flagCopy=8;
			}
			else if(8 == flagCopy)
			{
				tm->tm_hour=tm->tm_hour*10 + *(date_time_string+i)-0x30;
				flagCopy=9;
			}
			else if(9 == flagCopy)
			{
				tm->tm_min=*(date_time_string+i)-0x30;
				flagCopy=10;
			}
			else if(10 == flagCopy)
			{
				tm->tm_min=tm->tm_min*10 + *(date_time_string+i)-0x30;
				flagCopy=11;
			}
			else if(11 == flagCopy)
			{
				tm->tm_sec=*(date_time_string+i)-0x30;
				flagCopy=12;
			}
			else if(12 == flagCopy)
			{
				tm->tm_sec=tm->tm_sec*10 + *(date_time_string+i)-0x30;
				offset_sign=*(date_time_string+i+1);
				offset=*(date_time_string+i+2)-0x30;
				offset=offset*10 + *(date_time_string+i+3)-0x30;
				if(offset_sign=='+')
				{
					tm->offset= -900*offset;
				}
				else
				{
					tm->offset= 900*offset;
				}
				
				flagCopy=13;
			}
			else if(13 == flagCopy)
			{
				return 1;
			}
			else
			{
				return 1;
			}
		}
	}
	external_watchdog_feed();
	return 1;
}

/*
 * To convert into UTC Time from GSM Time
 */

const unsigned short days[4][12] =
{
	{   0,  31,  60,  91, 121, 152, 182, 213, 244, 274, 305, 335},
	{ 366, 397, 425, 456, 486, 517, 547, 578, 609, 639, 670, 700},
	{ 731, 762, 790, 821, 851, 882, 912, 943, 974,1004,1035,1065},
	{1096,1127,1155,1186,1216,1247,1277,1308,1339,1369,1400,1430},
};


static unsigned long convert_date_time_to_epoch(DateParams * date_time)
{
	unsigned int epoch=0;
	unsigned int second = date_time->tm_sec;  // 0-59
	unsigned int minute = date_time->tm_min;  // 0-59
	unsigned int hour   = date_time->tm_hour;    // 0-23
	unsigned int day    = date_time->tm_mday-1;   // 0-30
	unsigned int month  = date_time->tm_mon-1; // 0-11
	unsigned int year   = date_time->tm_year;    // 0-99
	epoch= (((year/4*(365*4+1)+days[year%4][month]+day)*24+hour)*60+minute)*60+second;
	return epoch;
}

static void convert_epoch_to_date_time(DateParams * date_time,unsigned long epoch)
{
	unsigned int year;
	unsigned int years;
	unsigned int month;
	epoch=epoch+date_time->offset;
	
	date_time->tm_sec	= epoch%60; epoch /= 60;
	date_time->tm_min	= epoch%60; epoch /= 60;
	date_time->tm_hour	= epoch%24; epoch /= 24;
	
	years = epoch / (365 * 4 + 1) * 4;
	epoch %= 365 * 4 + 1;
	
	for (year=3; year>0; year--)
	{
		if (epoch >= days[year][0])
		break;
	}

	
	for (month=11; month>0; month--)
	{
		if (epoch >= days[year][month])
		break;
	}

	date_time->tm_year  = years+year;
	date_time->tm_mon	= month+1;
	date_time->tm_mday  = epoch-days[year][month]+1;
}

void test_sim_slot_0_select( void )
{
	char command[64]={0x00};
		char simSlotBuffer[64]={0x00};
	sg_sprintf(command, "AT+CGSETV=44,%u,1\r", 0);
								
	sg_sprintf(LOG_GLOBAL_BUFFER, "Firing command [%s]", command);
	info_log(LOG_GLOBAL_BUFFER);
									
	run_simple_at_command_and_get_output(command, strlen(command), simSlotBuffer, sizeof(simSlotBuffer), OK_DELIMITER, 1, 1);
																			 
	/*
	Save the toggled index on config.
	*/
									
	memset(simSlotBuffer, 0, sizeof(simSlotBuffer));
	{
		char small[4] = {0};
		sg_sprintf(small, "%u", 0);
		
		save_config_value_on_persistent_storage("SIM_SLOT_INDEX", small, 0);
	}
}
void llt_delay(unsigned int count)
{
	while(--count>0);
}
void connect_socket_through_gprs(SG_Socket* socket)
{
	unsigned long epoch=0;
	char data[128]={0x00};

	if(strcmp(socket->type, SOCKET_UDP) != 0)
	{

		if(simAvailabilityCheck()!=SUCCESS)
		{
			external_wathdog_init();
			switch_sim_slot(0);
			if(simAvailabilityCheck()!=SUCCESS)
			{
				external_watchdog_feed();
				return;
			}
			external_watchdog_feed();
		}
		external_wathdog_init();
		fetch_gprs_params_from_sms(socket);
		external_watchdog_feed();
		if(gprsModeChecked == 0)
		{
			char *mode = socket->gsmMode;
			char *command = (char*) "AT+BND?\r";
		
			memset(localBuffer2, 0, sizeof(localBuffer2));
			sg_sprintf(localBuffer2, "+BND: %s", mode);
		
			if(strlen(mode) > 0)
			{
				external_wathdog_init();
			
				run_simple_at_command_and_get_output(command, strlen(command), localBuffer1, sizeof(localBuffer1),
				OK_DELIMITER, 1, 1);
				if(strstr(localBuffer1, localBuffer2) != NULL)
				{
					sg_sprintf(LOG_GLOBAL_BUFFER, "Successfully found required mode [%s] in [%s]",
					localBuffer2, localBuffer1);
					info_log((LOG_GLOBAL_BUFFER));
				
					gprsModeChecked = 1;
				}
				else
				{
					sg_sprintf(LOG_GLOBAL_BUFFER, "FAILED to find required mode [%s] in [%s]",
					localBuffer2, localBuffer1);
				
					error_log((LOG_GLOBAL_BUFFER));
				
					memset(localBuffer1, 0, sizeof(localBuffer1));
					sg_sprintf(localBuffer1, "AT+BND=%s\r", mode);
				
					memset(localBuffer2, 0, sizeof(localBuffer2));
				
					run_simple_at_command_and_get_output(localBuffer1, strlen(localBuffer1),
					localBuffer2, sizeof(localBuffer2),
					OK_DELIMITER, 1, 1);
					sg_sprintf(LOG_GLOBAL_BUFFER, "%s fired successfully", localBuffer1);
					info_log(LOG_GLOBAL_BUFFER);
				
					resetDevice();
				}
				external_watchdog_feed();
			}
		}
		external_wathdog_init();
		
		sg_sprintf(LOG_GLOBAL_BUFFER, "Setting  Time Zone...");
		info_log(LOG_GLOBAL_BUFFER);
		simcom_5360_set_timezone_update ( );
		
		watchdog_reset_and_enable(SOCKET_CONNECTION_SOLITARY_ATTEMPT_MAX_ALLOWED_TIME_SECONDS, "TRYING-SOCKET-CONNECTION-SOLITARY-ATTEMPT", 1);
		external_watchdog_feed(); 
	}
	external_wathdog_init();
	simcom_5360_connect_underlying_socket_medium_try_once(socket);
	external_watchdog_feed();

	watchdog_disable(NULL, NULL);
	
	if(strcmp(socket->type, SOCKET_UDP) != 0)
	{
		char databuff[48]={0x00};
		int i=0;
		external_wathdog_init();
		while(1)
		{
			simcom_5360_get_network_time(databuff);
			extract_date_time_cclk(&dateParams, databuff);
			if(++i>3)
			{
				break;
			}
			if(dateParams.tm_year!=80)
			{
				break;
			}
			else
			{
				startAndCountdownTimer(2, 0);
			}
		}
		if(i>=3)
		{
			flagGetNTPtime = 0;
			sg_sprintf(LOG_GLOBAL_BUFFER, "GSM Date and Time Sync Failed!!!");
			error_log(LOG_GLOBAL_BUFFER);
//				resetDevice();
		}
		else
		{
			print_date_info(&dateParams, "UTC");
			/************* Commented because we recieved UTC Time on GSM **************
			epoch=convert_date_time_to_epoch(&dateParams);
			convert_epoch_to_date_time(&dateParams,epoch);
			print_date_info(&dateParams, "UTC");
			**************************************************************************/
			sync_system_clock(&dateParams, 0);
			flagGetNTPtime = 1;
		}
		
		external_watchdog_feed();
	}
}


void connect_underlying_socket_medium_try_once(SG_Socket* socket)
{
	ip_addr_t dnsServerIp;
	socket->transitionBuffer = NULL;

	if(g_ip_mode == 3)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "\n\n\nEthernet-Interface SUCCESSFULLY initiated !!!\n\n\n");
		info_log(LOG_GLOBAL_BUFFER);
		
		ethernetInited = 1;
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "\n\nEthernet-Interface FAILED, or no cable has been plugged, status = [%lu]\n\n", g_ip_mode);
		error_log(LOG_GLOBAL_BUFFER);
	}
	
	if(ethernetInited == 1)
	{
		enum netconn_type type = -1;
		if(strcmp(socket->type, SOCKET_TCP) == 0)
		{
			type = NETCONN_TCP;
			
			if(strcmp(socket->host, INSTAMSG_HOST) == 0)
			{
				/*
				* To handle RSA-certificate
				*/
				socket->transitionBufferSize = 2 * MAX_TCP_PACKET_SIZE;
			}
			else
			{
				/*
				* Modbus-TCP
				*/
				socket->transitionBufferSize = 2 * MAX_BUFFER_SIZE;
			}
		}
		else if(strcmp(socket->type, SOCKET_UDP) == 0)
		{
			type = NETCONN_UDP;
			socket->transitionBufferSize = 100;
		}
		
		socket->transitionBuffer = (unsigned char *) sg_malloc(socket->transitionBufferSize);
		if(socket->transitionBuffer == NULL)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Could not allocate transition-buffer, no point living");
			error_log(LOG_GLOBAL_BUFFER);
			
			resetDevice();
		}
		
		socket->netConnSocket = netconn_new(type);
		
		if(socket->netConnSocket == NULL)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "%sCould not allocate new connection-object", SOCKET_ERROR_STR);
			error_log(LOG_GLOBAL_BUFFER);
		}
		else
		{
			err_t res = -1;
			int f = 8, s = 8, t = 8, fo = 8;
			if(type != NETCONN_UDP)
			{
				IP4_ADDR( &dnsServerIp, f, s, t, fo);
				dns_setserver(1, &dnsServerIp);
				f = 0, s = 0, t = 0, fo = 0;
				
				external_wathdog_init();
				
				res = netconn_gethostbyname(socket->host, &socket->ipAddress);
			
				if(res==ERR_OK)
				{
					reverse_ip(socket->ipAddress);
					sg_sprintf(LOG_GLOBAL_BUFFER, "%s","DNS Resolved !!! Now Connecting");
					info_log(LOG_GLOBAL_BUFFER);
					res = netconn_connect(socket->netConnSocket, &(socket->ipAddress), socket->port);
				}
				else
				{					
					sg_sprintf(LOG_GLOBAL_BUFFER, "Could not resolved DNS!!! Error Code [%d]",res);
					info_log(LOG_GLOBAL_BUFFER);
					sg_sprintf(LOG_GLOBAL_BUFFER, "Now Connecting on GPRS...");
					info_log(LOG_GLOBAL_BUFFER);
					ethernetInited = 0;
					connect_socket_through_gprs(socket);
					
					return;
				}
				external_watchdog_feed();
				if(res != ERR_OK)
				{
					sg_sprintf(LOG_GLOBAL_BUFFER, SOCKET_NOT_AVAILABLE);
					info_log(LOG_GLOBAL_BUFFER);
					sg_sprintf(LOG_GLOBAL_BUFFER, "Now Connecting on GPRS...");
					info_log(LOG_GLOBAL_BUFFER);
					ethernetInited = 0;
					external_wathdog_init();
					
					connect_socket_through_gprs(socket);
					
					external_watchdog_feed();
					return;
				}
				else
				{
					
					netconn_set_recvtimeout(socket->netConnSocket, 1000);
					sg_sprintf(LOG_GLOBAL_BUFFER, "Connection Status Code: [%d], Connection Established Successfully !!!",res);
					info_log(LOG_GLOBAL_BUFFER);
					socket->internalStatus = ERR_OK;
					socket->socketCorrupted = 0;
				}
			}
			else
			{
				external_wathdog_init();
				res = netconn_gethostbyname(socket->host, &socket->ipAddress);
				external_watchdog_feed();
				if(res==ERR_OK)
				{
					external_wathdog_init();
					reverse_ip(socket->ipAddress);
					sg_sprintf(LOG_GLOBAL_BUFFER, "%s","NTP DNS Resolved !!! Now Connecting");
					info_log(LOG_GLOBAL_BUFFER);
					res = netconn_connect(socket->netConnSocket, &(socket->ipAddress), socket->port);
					external_watchdog_feed();
				}
				else
				{
					sg_sprintf(LOG_GLOBAL_BUFFER, "NTP DNS Resolution Failed!!!");
					info_log(LOG_GLOBAL_BUFFER);
					return;
				}
				if(ERR_OK == res)
				{
					netconn_set_recvtimeout(socket->netConnSocket, 1000);
					sg_sprintf(LOG_GLOBAL_BUFFER, "UDP Connection Status Code: [%d], Connection Established Successfully !!!",res);
					info_log(LOG_GLOBAL_BUFFER);
					socket->internalStatus = ERR_OK;
					socket->socketCorrupted = 0;
				}
				else
				{
					socket->socketCorrupted = 1;
					return;
				}
			}
		}

		sg_sprintf(LOG_GLOBAL_BUFFER, "%s-ETHERNET-SOCKET UNDERLYING_MEDIUM INITIATED FOR HOST = [%s], PORT = [%d].",
				   socket->type, socket->host, socket->port);
		info_log(LOG_GLOBAL_BUFFER);
	}
	else
	{
/*
		if(strcmp(socket->type, SOCKET_UDP) == 0)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "TRYING TO FETCH USING GSM !!!");
			info_log(LOG_GLOBAL_BUFFER);
			socket->socketCorrupted = 0;
		}
		else
		*/
		{
			
			external_wathdog_init();
			
//			test_sim_slot_0_select();
			ethernetInited = 0;
			connect_socket_through_gprs(socket);
			external_watchdog_feed();
		}
	}
}


/*
 * This method reads "len" bytes from socket into "buffer".
 *
 * Exactly one of the cases must hold ::
 *
 * a)
 * "guaranteed" is 1.
 * So, this "read" must behave as a blocking-read.
 *
 * Also, exactly "len" bytes are read successfully.
 * So, SUCCESS must be returned.
 *
 *                      OR
 *
 * b)
 * "guaranteed" is 1.
 * So, this "read" must bahave as a blocking-read.
 *
 * However, an error occurs while reading.
 * So, FAILURE must be returned immediately (i.e. no socket-reinstantiation must be done in this method).
 *
 *                      OR
 *
 * c)
 * "guaranteed" is 0.
 * So, this "read" must behave as a non-blocking read.
 *
 * Also, no bytes could be read in SOCKET_READ_TIMEOUT_SECS seconds (defined in "globals.h").
 * So, SOCKET_READ_TIMEOUT must be returned immediately.
 *
 *                      OR
 *
 * d)
 * "guaranteed" is 0.
 * So, this "read" must behave as a non-blocking read.
 *
 * Also, exactly "len" bytes are successfully read.
 * So, SUCCESS must be returned.
 *
 *                      OR
 *
 * e)
 * "guaranteed" is 0.
 * So, this "read" must behave as a non-blocking read.
 *
 * However, an error occurs while reading.
 * So, FAILURE must be returned immediately (i.e. no socket-reinstantiation must be done in this method).
 */


static void run_one_lwip_socket_read_cycle(SG_Socket *socket, unsigned char resetTransitionBuffer)
{
	 struct netbuf *inbuf;
	 u16_t buflen;
	 err_t res = ERR_VAL;
	 
	 
	 if(socket->internalStatus == ERR_VAL)
	 {
		 return;
	 }
	 
	 if(resetTransitionBuffer == 1)
	 {
		 socket->readerIndex = 0;
		 socket->writerIndex = 0;
		 
		 socket->transitionBufferTotalDataAvailable = 0;
	 }
	
	 res = netconn_recv(socket->netConnSocket, &inbuf);
	 if(res == ERR_TIMEOUT)
	 {		 		 
		 /* Nothing to do */
	 }
	 else if(res == ERR_OK)
	 {
		 buflen = inbuf->ptr->len;
		 if(buflen > (socket->transitionBufferSize - socket->transitionBufferTotalDataAvailable))
		 {
			 sg_sprintf(LOG_GLOBAL_BUFFER, "%sSocket-Transition Buffer Overflow occurred, due to received packet-size [%d]", SOCKET_ERROR_STR, buflen);
			 error_log(LOG_GLOBAL_BUFFER);
			 		 
			 socket->internalStatus = ERR_VAL;
			 return;
		 }
	 
		 memcpy(socket->transitionBuffer + socket->writerIndex, inbuf->ptr->payload, buflen);
		 
		 socket->writerIndex = socket->writerIndex + buflen;
		 socket->transitionBufferTotalDataAvailable = socket->transitionBufferTotalDataAvailable + buflen;		 
	 }
	 else
	 {	
		 socket->internalStatus = ERR_VAL;
	 }
	 
	 netbuf_delete(inbuf);
}

int mb_socket_read(SG_Socket* socket, unsigned char* buffer, int len, unsigned char guaranteed)
{
	if(len == 0)
	{
		return SUCCESS;
	}
		
	while(1)
	{
		if(socket->transitionBufferTotalDataAvailable == 0)
		{
			run_one_lwip_socket_read_cycle(socket, 1);
		}
			
		if(socket->internalStatus == ERR_VAL)
		{
			return FAILURE;							/* Case b) and e) */
		}
		else if(socket->transitionBufferTotalDataAvailable >= len)
		{
			memcpy(buffer, socket->transitionBuffer + socket->readerIndex, len);
				
			socket->readerIndex = socket->readerIndex + len;
			socket->transitionBufferTotalDataAvailable = socket->transitionBufferTotalDataAvailable - len;
				
			socket->bytes_received = len;
			bytes_received_over_wire = bytes_received_over_wire + len;
				
			return SUCCESS;							/* Case a) and d) */
		}
		else
		{
			if(guaranteed == 0)
			{
				return SOCKET_READ_TIMEOUT;			/* Case c) */
			}
			else
			{
				run_one_lwip_socket_read_cycle(socket, 0);
			}
		}
	}
}

int socket_read(SG_Socket* socket, unsigned char* buffer, int len, unsigned char guaranteed)
{
	if(ethernetInited == 1)
	{
		if(len == 0)
		{
			return SUCCESS;
		}
		
		external_wathdog_init();
		
		while(1)
		{
			if(socket->transitionBufferTotalDataAvailable == 0)
			{
				run_one_lwip_socket_read_cycle(socket, 1);
			}
		
			if(socket->internalStatus == ERR_VAL)
			{	
				external_watchdog_feed();	
				return FAILURE;							/* Case b) and e) */
			}
			else if(socket->transitionBufferTotalDataAvailable >= len)
			{
				memcpy(buffer, socket->transitionBuffer + socket->readerIndex, len);
			
				socket->readerIndex = socket->readerIndex + len;
				socket->transitionBufferTotalDataAvailable = socket->transitionBufferTotalDataAvailable - len;
	
				socket->bytes_received = len;					
                bytes_received_over_wire = bytes_received_over_wire + len;
				
				external_watchdog_feed();
				return SUCCESS;							/* Case a) and d) */
			}
			else
			{
				if(guaranteed == 0)
				{	
					/* these lines are added to test ethernet file get for firmware upgrade*///--> need to be decide how to fix the read issue
					memcpy(buffer, socket->transitionBuffer + socket->readerIndex, socket->transitionBufferTotalDataAvailable);
					
					socket->readerIndex = socket->readerIndex + socket->transitionBufferTotalDataAvailable;
					socket->bytes_received = socket->transitionBufferTotalDataAvailable;
					bytes_received_over_wire = bytes_received_over_wire + socket->transitionBufferTotalDataAvailable;
					socket->transitionBufferTotalDataAvailable = 0;
					/*------------------------------------------------------------------------*/
					
					external_watchdog_feed();
								
					return SOCKET_READ_TIMEOUT;			/* Case c) */
				}
				else
				{					
					run_one_lwip_socket_read_cycle(socket, 0);	
				}
			}
		}
		external_watchdog_feed();
	}
	else
	{
		return simcom_5360_socket_read(socket, buffer, len, guaranteed);
	}
}


/*
 * This method writes first "len" bytes from "buffer" onto the socket.
 *
 * This is a blocking function. So, either of the following must hold true ::
 *
 * a)
 * All "len" bytes are written.
 * In this case, SUCCESS must be returned.
 *
 *                      OR
 * b)
 * An error occurred while writing.
 * In this case, FAILURE must be returned immediately (i.e. no socket-reinstantiation must be done in this method).
 */
int socket_write(SG_Socket* socket, unsigned char* buffer, int len)
{
	if(ethernetInited == 1)
	{
		err_t res = ERR_VAL;
		
		if(socket->type == SOCKET_TCP)
		{
			unsigned int bytes_written = 0;
			external_wathdog_init();
			res = netconn_write_partly(socket->netConnSocket, buffer, len, NETCONN_COPY | NETCONN_DONTBLOCK, &bytes_written);
			if((res == ERR_OK) && (bytes_written == len))
			{				
                bytes_sent_over_wire = bytes_sent_over_wire + len;
				external_watchdog_feed();
				return SUCCESS;
			}
			else
			{		
				external_watchdog_feed();
					
				return FAILURE;
			}
			external_watchdog_feed();
		}
		else
		{
			struct netbuf *buf;
			unsigned char *data;
			int i = 0;
			external_wathdog_init();
			
			buf = netbuf_new();
			data = netbuf_alloc(buf, len);
			for(i = 0; i < len; i++)
			{
				data[i] = buffer[i];
			}
			
			res = netconn_send(socket->netConnSocket, buf);
			netbuf_delete(buf);
			
			external_watchdog_feed();
			
			if(res == ERR_OK)
			{
                bytes_sent_over_wire = bytes_sent_over_wire + len;
				return SUCCESS;
			}
			else
			{	
				return FAILURE;
			}			
		}    
	}
	else
	{
		/*
		buffer[len]=0x1b;
		len++;
		*/
		return simcom_5360_socket_write(socket, buffer, len);
		/*
		if(strcmp(socket->type, SOCKET_TCP) == 0)
		{
			return simcom_5360_socket_write(socket, buffer, len);
		}
		else if(strcmp(socket->type, SOCKET_UDP) == 0)
		{
			return simcom_5360_socket_write_udp(socket, buffer, len);
		}
		*/
	}
}

int mb_socket_write(SG_Socket* socket, unsigned char* buffer, int len)
{
	err_t res = ERR_VAL;
		
	if(socket->type == SOCKET_TCP)
	{
		unsigned int bytes_written = 0;
		
		res = netconn_write_partly(socket->netConnSocket, buffer, len, NETCONN_COPY | NETCONN_DONTBLOCK, &bytes_written);
		if((res == ERR_OK) && (bytes_written == len))
		{
			bytes_sent_over_wire = bytes_sent_over_wire + len;
			return SUCCESS;
		}
		else
		{
			return FAILURE;
		}
	}
	else
	{
		struct netbuf *buf;
		unsigned char *data;
		int i = 0;
		
		buf = netbuf_new();
		data = netbuf_alloc(buf, len);
		for(i = 0; i < len; i++)
		{
			data[i] = buffer[i];
		}
		
		res = netconn_send(socket->netConnSocket, buf);
		netbuf_delete(buf);
		
		if(res == ERR_OK)
		{
			bytes_sent_over_wire = bytes_sent_over_wire + len;
			return SUCCESS;
		}
		else
		{
			return FAILURE;
		}
	}
}

/*
 * This method does the cleaning up (for eg. closing a socket) when the socket is cleaned up.
 * But if it is ok to re-connect without releasing the underlying-system-resource, then this can be left empty.
 *
 * Note that this method MUST DO """ONLY""" per-socket level cleanup, NO GLOBAL-LEVEL CLEANING/REINIT MUST BE DONE.
 */
void release_underlying_socket_medium_guaranteed(SG_Socket* socket)
{
	if(ethernetInited == 1)
	{
		waitBeforeReboot();
		resetDevice();
	}
	else
	{
		simcom_5360_release_underlying_socket_medium_guaranteed(socket);
	}
}

void mb_release_socket_simple_guaranteed(SG_Socket* socket)
{
	err_t status = ERR_VAL;
	
	status = netconn_close(socket->netConnSocket);
	if(status != ERR_OK)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not close connection [%s] over [%u]", socket->host, socket->port);
		error_log(LOG_GLOBAL_BUFFER);
	}
	else
	{
#if ENG_DEBUG
		sg_sprintf(LOG_GLOBAL_BUFFER, "Successfully closed connection [%s] over [%u]", socket->host, socket->port);
		error_log(LOG_GLOBAL_BUFFER);
#endif
	}
	
	status = netconn_delete(socket->netConnSocket);
	if(status != ERR_OK)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not delete connection [%s] over [%u]", socket->host, socket->port);
		error_log(LOG_GLOBAL_BUFFER);
	}
	else
	{
#if ENG_DEBUG
		sg_sprintf(LOG_GLOBAL_BUFFER, "Successfully deleted connection [%s] over [%u]", socket->host, socket->port);
		error_log(LOG_GLOBAL_BUFFER);
#endif
	}
	/*
	if(socket->transitionBuffer != NULL)
	{
		sg_free(socket->transitionBuffer);
		socket->transitionBuffer = NULL;
	}
	*/
}

void release_socket_simple_guaranteed(SG_Socket* socket)
{
	if(ethernetInited == 1)
	{
		err_t status = ERR_VAL;
		
		status = netconn_close(socket->netConnSocket);
		if(status != ERR_OK)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Could not close connection [%s] over [%u]", socket->host, socket->port);
			error_log(LOG_GLOBAL_BUFFER);
		}
		else
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Successfully closed connection [%s] over [%u]", socket->host, socket->port);
			error_log(LOG_GLOBAL_BUFFER);
		}
		
		status = netconn_delete(socket->netConnSocket);
		if(status != ERR_OK)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Could not delete connection [%s] over [%u]", socket->host, socket->port);
			error_log(LOG_GLOBAL_BUFFER);
		}
		else
		{
#if ENG_DEBUG
			sg_sprintf(LOG_GLOBAL_BUFFER, "Successfully deleted connection [%s] over [%u]", socket->host, socket->port);
			error_log(LOG_GLOBAL_BUFFER);
#endif
		}
	}
	
	if(socket->transitionBuffer != NULL)
	{
		sg_free(socket->transitionBuffer);
		socket->transitionBuffer = NULL;
	}
}


#if (SSL_ENABLED == 1) || (SOCKET_SSL_ENABLED == 1)
/*
 * This method loads the client-certificate into buffer.
 */
void load_client_certificate_into_buffer(char *cert_buffer, int maxLength)
{
	if(ethernetInited == 1)
	{
	}
	else
	{
		simcom_5360_load_client_certificate_into_buffer(cert_buffer, maxLength);
	}
}


/*
 * This method saves the client-certificate onto the device in a persistent manner.
 */
void save_client_certificate_from_buffer(char *cert_buffer)
{
	if(ethernetInited == 1)
	{
	}
	else
	{
		simcom_5360_save_client_certificate_from_buffer(cert_buffer);
	}
}


/*
 * This method loads the client-private-key into buffer.
 */
void load_client_private_key_into_buffer(char *private_key_buffer, int maxLength)
{
	if(ethernetInited == 1)
	{
	}
	else
	{
		simcom_5360_load_client_private_key_into_buffer(private_key_buffer, maxLength);
	}
}


/*
 * This method saves the client-private-key onto the device in a persistent manner.
 */
void save_client_private_key_from_buffer(char *private_key_buffer)
{
	if(ethernetInited == 1)
	{
	}
	else
	{
		simcom_5360_save_client_private_key_from_buffer(private_key_buffer);
	}
}

#endif
