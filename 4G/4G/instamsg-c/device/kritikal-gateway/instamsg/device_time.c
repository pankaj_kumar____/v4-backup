/*******************************************************************************
 * Contributors:
 *
 *      Ajay Garg <ajay.garg@sensegrow.com>
 *
 *******************************************************************************/

#include "../driver/include/socket.h"
#include "../driver/include/sg_stdlib.h"
#include "../driver/include/time.h"
#include "../driver/include/log.h"
#include "../driver/include/at.h"
#include "../driver/include/misc.h"

#include <asf.h>

#include <delay.h>
#include <task.h>

#include <portmacro.h>

#define BOARD_TWI_INSTANCE			TWI0
#define RTC_SLAVE_ADDRESS			0x6F
#define RTC_CONTINUE_RW_DELAY		50

unsigned int time_offset = 0x00;

enum status_code rtc_read_regs(uint8_t reg_addr, uint8_t *read_buffer, uint8_t length);
enum status_code rtc_write_regs(uint8_t reg_addr, uint8_t *write_buffer, uint8_t length);
uint8_t convertToBcd(uint8_t byteDecimal);
uint8_t convertFromBcd(uint8_t byteBCD);

/**
 * \brief Read registers content from RTC device.
 *
 * \param reg_addr    Start register address.
 * \param read_buffer Pointer to byte array for read data.
 * \param length      Read data length.
 *
 * \retval STATUS_OK  If read successful.
 * \retval ERR_IO_ERROR If communication failed with the device.
 */
enum status_code rtc_read_regs(uint8_t reg_addr, uint8_t *read_buffer,uint8_t length)
{	
	/*
	 * Delay between each write or read cycle between TWI Stop and TWI Start.
	 */
	delay_us(RTC_CONTINUE_RW_DELAY);

	/*
	 * Read data from RTC device register
	 */
	twi_package_t packet_rd = {
		.addr[0]      = reg_addr,       /* TWI slave memory address */
		.addr_length = sizeof(uint8_t),             /* TWI slave memory address data size */
		.chip        = RTC_SLAVE_ADDRESS,/* TWI slave bus address */
		.buffer      = (void *)read_buffer, /* Transfer data buffer */
		.length      = length         /* Transfer data size (bytes) */
	};

	if (twi_master_read(BOARD_TWI_INSTANCE, &packet_rd) != STATUS_OK) {
		return ERR_IO_ERROR;
	}

	return STATUS_OK;
}

/**
 * \brief Write content to RTC device registers.
 *
 * \param reg_addr     Start register address.
 * \param write_buffer Pointer to byte array for write data.
 * \param length       Write data length.
 *
 * \retval STATUS_OK  If write successful.
 * \retval ERR_IO_ERROR If communication failed with the device.
 */
enum status_code rtc_write_regs(uint8_t reg_addr, uint8_t *write_buffer, uint8_t length)
{
	/*
	 * Delay between each write or read cycle between TWI Stop and TWI Start.
	 */
	delay_us(RTC_CONTINUE_RW_DELAY);

	/*
	 * Write data to RTC device registers
	 */
	twi_package_t packet_wr = {
		.addr[0]      = reg_addr,					/* TWI slave memory address */
		.addr_length  = sizeof(uint8_t),			/* TWI slave memory address data size */
		.chip         = RTC_SLAVE_ADDRESS,			/* TWI slave bus address */
		.buffer       = (void *)write_buffer,		/* Transfer data buffer */
		.length       = length						/* Transfer data size (bytes) */
	};

	/* Perform a write access */
	if (twi_master_write(BOARD_TWI_INSTANCE, &packet_wr) != STATUS_OK) {
		return ERR_IO_ERROR;
	}

	return STATUS_OK;
}

/*
 * This method does the global-level-initialization for time (if any).
 */
void init_global_timer()
{
	if(1)
	{
		/* TWI master initialization options. */
		twi_master_options_t twi_opt;

		memset((void *)&twi_opt, 0, sizeof(twi_master_options_t));
		twi_opt.speed = 100000;    /* 100KHz for I2C speed */

		/* Initialize the TWI master driver. */
		twi_master_setup(BOARD_TWI_INSTANCE, &twi_opt);		
	}
}


/*
 * This method returns the minimum-delay achievable via this device.
 */
unsigned long getMinimumDelayPossibleInMicroSeconds()
{
    return 1000000;
}


/*
 * This method ACTUALLY causes the current-device to go to sleep for the minimum-delay possible.
 */
void minimumDelay()
{
	vTaskDelay(1000 / portTICK_RATE_MS);
}


/*
 * This method returns the current-tick/timestamp.
 *
 * Note that the value returned by this method is used on a "relative basis" since the device reset.
 * So, this method may return either of following ::
 *
 *      * Either the absolute timestamp (as returned by the linux-command "date +%s"), or
 *      * Either the number of seconds that have passed (at the time of calling this method) since device reset.
 */
volatile unsigned long timestamp = 0;
unsigned long getCurrentTick()
{
    return timestamp;
}


#if GSM_TIME_SYNC_PRESENT == 1
/*
 * Returns the current-timestamp, the original of which was returned via GSM.
 * Returns 0 in case no informaton is received from GSM (yet).
 */
unsigned long get_GSM_timestamp()
{
#error "Function not implemented."
}
#endif


/*
 * Syncs the system-clock, using the structure "DateParams" as defined in common/instamsg/driver/include/time.h
 *
 * At the time of calling this method the variable "dateParams" is already filled in by the appropriate values
 * of the current time in UTC.
 *
 * Returns SUCCESS on successful-syncing.
 * Else returns FAILURE.
 */

uint8_t convertToBcd(uint8_t byteDecimal)
{
	uint8_t retVal;
	retVal= (byteDecimal / 10) << 4 | (byteDecimal % 10);
	return retVal;
}

uint8_t convertFromBcd(uint8_t byteBCD)
{	
	uint8_t byteMSB = 0;
	uint8_t byteLSB = 0;
	uint8_t retVal;
	byteMSB      = (byteBCD & 0xF0) >> 4;
	byteLSB      = (byteBCD & 0x0F);
	
	retVal=((byteMSB * 10) + byteLSB);
	return       retVal;
}


enum rtc_reg_map {
	RTCSEC = 0,
	RTCMIN,
	RTCHOUR,
	RTCWKDAY,
	RTCDATE,
	RTCMTH,
	RTCYEAR,
	CONTROL,
};
int get_days_in_month(int month, int year)
{
	int dayTable[]={31,28,31,30,31,30,31,31,30,31,30,31};
	if(2==month)
	{
		year=year+2000;
		if (year % 4 == 0) {
			if (year % 100 == 0) 
			{
				if (year % 400 == 0)
				{
					return 29;
				}
				else
				{
					return 28;
				}

			} 
			else
			{
				return 29;	
			}
		} 
		else
		{
			return 28;
		}
	}
	return dayTable[(month-1)];
}
int day_minus(DateParams *dateParams, int day)
{
	
}

int hour_minus(DateParams *dateParams, int hrs)
{
	if(dateParams->tm_hour<hrs)
	{
		dateParams->tm_hour=dateParams->tm_hour+(24-hrs);
		day_minus(dateParams,1);
	}
	else
	{
		dateParams->tm_hour-=hrs;
	}
}

int convert_ist_to_utc(DateParams *dateParams)
{
	if(dateParams->tm_min<30)
	{
		dateParams->tm_min=dateParams->tm_min+30;
		hour_minus(dateParams,6);
	}
	else
	{
		dateParams->tm_min -=30;
		hour_minus(dateParams,5);
	}	
}
uint8_t timeStamp[8];
int sync_system_clock(DateParams *dateParams, unsigned long seconds)
{
	enum status_code ret;
	uint8_t rtc_12hour, rtc_osc_start_bit;

	timeStamp[0]  = convertToBcd(dateParams->tm_sec);
	timeStamp[1]  = convertToBcd(dateParams->tm_min);
	timeStamp[2]  = convertToBcd(dateParams->tm_hour);
	timeStamp[3]  = convertToBcd(dateParams->tm_wday);
	timeStamp[4]  = convertToBcd(dateParams->tm_mday);
	timeStamp[5]  = convertToBcd(dateParams->tm_mon);
	timeStamp[6]  = convertToBcd(dateParams->tm_year);

	setTimezoneOffset(0);
	
	ret = rtc_write_regs(RTCSEC, timeStamp, 7);
	if(ret != STATUS_OK)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Unable to write Date & Time to RTC");
		error_log(LOG_GLOBAL_BUFFER);
		
		resetDevice();
	}
	
	rtc_read_regs(RTCHOUR, &rtc_12hour, 1);
	if(ret != STATUS_OK)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Unable to read hour-register");
		error_log(LOG_GLOBAL_BUFFER);
		
		resetDevice();
	}
				
	rtc_12hour |= (0 << 6);
	ret = rtc_write_regs(RTCHOUR, &rtc_12hour, 1);			  /* set 24 hour format */
	if(ret != STATUS_OK)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Unable to set 24-hour format");
		error_log(LOG_GLOBAL_BUFFER);
				
		resetDevice();
	}
	
	ret = rtc_read_regs(RTCSEC, &rtc_osc_start_bit, 1);		
	if(ret != STATUS_OK)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Unable to read second-register");
		error_log(LOG_GLOBAL_BUFFER);
		
		resetDevice();		
	}	
	
	rtc_osc_start_bit |= (1 << 7);
	ret= rtc_write_regs(RTCSEC, &rtc_osc_start_bit, 1);		 // set Oscillator enable bit and start external RTC Oscillator 32.768KHz
	if(ret != STATUS_OK)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Unable to set oscillator-enable-bit");
		error_log(LOG_GLOBAL_BUFFER);
		
		resetDevice();
	}
	
	return SUCCESS;
}


/*
 * This method gets the time in the following-format
 *
 *          YYYYMMDD4HHMMSS
 *          201507304155546
 *
 * The time can be in any timezone (the timzone is taken care of appriately by "getTimezoneOffset" method).
 */

extern unsigned char rebootPending;
static void getRequiredSizeStringifiedDigit(char *buffer, int bufferSize, int digit)
{
	memset(buffer, 0, bufferSize);
	
	if(digit < 10)
	{
		sg_sprintf(buffer, "0%u", digit);
	}
	else
	{
		sg_sprintf(buffer, "%u", digit);
	}
}

void getTimeInDesiredFormat(char *buffer, int maxBufferLength)
{	
	enum status_code ret;
	char temp[6] = {0};
	/*
	int month=0,year=0,isLeapYear=0;
	*/
	ret = rtc_read_regs(RTCSEC, timeStamp, 7);

	if(ret != STATUS_OK)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Unable to read time from RTC");
		error_log(LOG_GLOBAL_BUFFER);
		
		resetDevice();
	}
	/*	
	year=convertFromBcd(timeStamp[6]);
	*/
	getRequiredSizeStringifiedDigit(temp, sizeof(temp), 2000 + (convertFromBcd(timeStamp[6] & 0xff)));
	strcat(buffer, temp);
	/*
	month= convertFromBcd(timeStamp[5]);
	if (year % 4 == 0) {
		if (year % 100 == 0) 
		{
			// the year is a leap year if it is divisible by 400.
			if (year % 400 == 0)
			{
				isLeapYear=1;
			}
		} 
		else
		{
			isLeapYear=1;
		}
	} 
	
	if((month>20)&&(1==isLeapYear))
	{
		month-=20;
	}
	*/
	getRequiredSizeStringifiedDigit(temp, sizeof(temp), convertFromBcd(timeStamp[5] & 0x1f));
	strcat(buffer, temp);
	
	getRequiredSizeStringifiedDigit(temp, sizeof(temp), convertFromBcd(timeStamp[4] & 0x3f));
	strcat(buffer, temp);
	
	strcat(buffer, "4");
	
	getRequiredSizeStringifiedDigit(temp, sizeof(temp), convertFromBcd(timeStamp[2] & 0x3f));
	strcat(buffer, temp);

	getRequiredSizeStringifiedDigit(temp, sizeof(temp), convertFromBcd(timeStamp[1] & 0x7F));
	strcat(buffer, temp);

	getRequiredSizeStringifiedDigit(temp, sizeof(temp), convertFromBcd(timeStamp[0] & 0x7F));
	strcat(buffer, temp);
}


/*
 * This method gets the timezone-offset for this device.
 */
void getTimezoneOffset(char *buffer, int maxBufferLength)
{
	sprintf(buffer, "%d",time_offset);
}

void setTimezoneOffset(int seconds)
{
	time_offset=seconds;
}



/* added (Date - 27/4/20) by KSPL */

#define SERVER_UPGRADE_REQUEST_TIME		2	//configure periodic time to check server in hrs

static volatile char gAlarmTriggeredStatus;	/* will raise flag on rtc alarm trigger*/

void RTC_Handler(void)
{
	unsigned int Hour = 0, Minute = 0, Second = 0;
	unsigned int ul_status = rtc_get_status(RTC);
	
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		/* Disable RTC interrupt */
		rtc_disable_interrupt(RTC, RTC_IDR_ALRDIS);

		rtc_get_time(RTC, &Hour, &Minute, &Second);
		sg_sprintf(LOG_GLOBAL_BUFFER, "\n\r[Time: %02u:%02u:%02u]", Hour, Minute, Second);
		info_log(LOG_GLOBAL_BUFFER);
		
		info_log("RTC Alarm Triggered\r\n");
		gAlarmTriggeredStatus = 1;
		
		Hour = Hour + SERVER_UPGRADE_REQUEST_TIME;
		if(Hour >= 24) {
			Hour = Hour - 24;
		}
		
		rtc_clear_time_alarm(RTC);
		rtc_set_time_alarm(RTC, 1, Hour, 1, Minute, 1, Second);
		
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		rtc_enable_interrupt(RTC, RTC_IER_ALREN);
	}
}

int initRTCAlarm()
{
	unsigned int Hour, Minute, Second = 0;
	
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);
	rtc_enable_interrupt(RTC, RTC_IER_ALREN);
	
	rtc_clear_time_alarm(RTC);
	
	rtc_get_time(RTC, &Hour, &Minute, &Second);
	sg_sprintf(LOG_GLOBAL_BUFFER, "\n\r[Time: %02u:%02u:%02u]", Hour, Minute, Second);
	info_log(LOG_GLOBAL_BUFFER);
	
	Hour = Hour + SERVER_UPGRADE_REQUEST_TIME;
	if(Hour >= 24) {
		Hour = Hour - 24;
	}
	
	if(rtc_set_time_alarm(RTC, 1, Hour, 1, Minute, 1, Second)) {
		return 1;
	}
	
	return 0;
}

/* /brief get the last alarm status, also it will get clear on reading it*/
char getLastTriggeredAlarmStatus()
{
	char gCurrentStatus = gAlarmTriggeredStatus;
	gAlarmTriggeredStatus = 0;
	return gCurrentStatus;
}
