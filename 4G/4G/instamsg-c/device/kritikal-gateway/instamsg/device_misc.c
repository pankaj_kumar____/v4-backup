#include "device_defines.h"

#include "../driver/include/misc.h"
#include "../driver/include/at.h"
#include "../driver/include/sg_stdlib.h"
#include "../driver/include/log.h"
#include "../driver/include/instamsg.h"
#include "../driver/include/watchdog.h"

#include "../common/spi_utils.h"


#include <asf.h>

#include <string.h>
#include "tc.h"

#include "rstc.h"

#include "lwip/api.h"

extern volatile unsigned char ethernetInited;



/*
 * Utility-function that resets the device, bringing it into a clean, fresh state.
 */

void do_resetDevice(void);
void persistRunHours(void);
void do_resetDevice()
{
	switch_spi(FLASH_PERIPHERAL, 0);
	persistRunHours();
	
	rstc_start_software_reset(RSTC);
	while(1);
}


void resetDevice()
{
	do_resetDevice();
}


/*
 * This method does the initialization, that is needed on a global-basis.
 * All code in this method runs right at the beginning (followed by logger-initialization).
 *
 * THIS METHOD IS GENERALLY EXPECTED TO BE EMPTY, AS THE INITIALIZATION SHOULD HAPPEN ON
 * PER-MODULE BASIS. ONLY IF SOMETHING IS REALLY NEEDED RIGHT AT THE START (WHICH IS THEN
 * POTENTIALLY USED BY MORE THAN ONE MODULE), SHOULD SOMETHING BE REQUIRED TO COME HERE.
 */
void bootstrapInit()
{		
}


static void addKeyValue(char *buffer, const char *key, const char *splitter, const char *defaultValue, const char *command,
                        unsigned char addComma)
{
    if(addComma == 1)
    {
        strcat(buffer, ", ");
    }

    strcat(buffer, "'");
    strcat(buffer, key);
    strcat(buffer, "' : '");

    RESET_GLOBAL_BUFFER;
    run_simple_at_command_and_get_output(command, strlen(command), (char*) GLOBAL_BUFFER, sizeof(GLOBAL_BUFFER), OK_DELIMITER, 0, 1);

    if(splitter != NULL)
    {
        /*
         * Attach the splitted-value.
         */
        char *starter = strstr((char*)GLOBAL_BUFFER, splitter);
        if(starter != NULL)
        {
            /*
             * Ignore the starting double-quote.
             */
            if(starter[1] == '"')
            {
                starter = starter + 1;
            }

            strcat(buffer, starter + 1);

            /*
             * Ignore the ending double-quote.
             */
            if(buffer[strlen(buffer) - 1] == '"')
            {
                buffer[strlen(buffer) - 1] = 0;
            }
        }
        else
        {
            if(defaultValue != NULL)
            {
                strcat(buffer, defaultValue);
            }
        }
    }
    else
    {
        /*
         * Attach direct, complete value.
         */
        strcat(buffer, (char*)GLOBAL_BUFFER);
    }

    strcat(buffer, "'");
}


#define SIGNAL_STRENGTH_CMD     "AT+CSQ\r"
static void addSignalStrength(char *buffer, unsigned char addComma)
{
    if(addComma == 1)
    {
        strcat(buffer, ", ");
    }

    RESET_GLOBAL_BUFFER;

    while(1)
    {
        run_simple_at_command_and_get_output(SIGNAL_STRENGTH_CMD, strlen(SIGNAL_STRENGTH_CMD),
                                            (char*) GLOBAL_BUFFER, sizeof(GLOBAL_BUFFER), OK_DELIMITER, 0, 1);
        if(sg_strnstr( (char*) GLOBAL_BUFFER, "+CSQ", strlen( (char*) GLOBAL_BUFFER) ) == ((char*) GLOBAL_BUFFER))
        {
            break;
        }
    }

    sg_sprintf(LOG_GLOBAL_BUFFER, "Signal-Strength AT-Command Raw-Output = [%s]", GLOBAL_BUFFER);
    debug_log(LOG_GLOBAL_BUFFER);

    {
        /*
         * At this point, we have the output as "+CSQ: 15,0"
         */
        char *firstValueStart = (char*)GLOBAL_BUFFER + strlen("+CSQ: ");
        char *finder = strstr(firstValueStart, ",");

        int intValue = -1;

        char intString[6] = {0};
        memcpy(intString, firstValueStart, finder - firstValueStart);

        intValue = sg_atoi(intString);
        intValue = -113 + (2 * intValue);

        memset(intString, 0, sizeof(intString));
        sg_sprintf(intString, "%d", intValue);

        strcat(buffer, "'signal_strength' : '");
        strcat(buffer, intString);
        strcat(buffer, "'");
    }
}


/*
 * This method returns the client-network-data, in simple JSON form, of type ::
 *
 * {'method' : 'value', 'ip_address' : 'value', 'antina_status' : 'value', 'signal_strength' : 'value'}
 */
void get_client_session_data(char *messageBuffer, int maxBufferLength)
{
    strcat(messageBuffer, "{");
    addSignalStrength(messageBuffer, 0);
    strcat(messageBuffer, "}");
}


/*
 * This method returns the client-network-data, in simple JSON form, of type ::
 *
 * {'imei' : 'value', 'serial_number' : 'value', 'model' : 'value', 'firmware_version' : 'value', 'manufacturer' : 'value', 'imsi' : 'value', 'client_version' : 'value'}
 */
void get_client_metadata(char *messageBuffer, int maxBufferLength)
{
    /*
     * Start the JSON-Dict.
     */
    strcat(messageBuffer, "{");

    addKeyValue(messageBuffer, "imei", NULL, NULL, "AT+CGSN\r", 0);
    addKeyValue(messageBuffer, "serial_number", NULL, NULL, "AT+CGSN\r", 1);

    strcat(messageBuffer, ", 'client_version' : '");
    strcat(messageBuffer, INSTAMSG_VERSION);
    strcat(messageBuffer, "_");
    strcat(messageBuffer, DEVICE_VERSION);
    strcat(messageBuffer, "'");


    /*
     * Terminate the JSON-Dict.
     */
    strcat(messageBuffer, "}");

    sg_sprintf(LOG_GLOBAL_BUFFER, "Client-Metadata = [%s]", messageBuffer);
    debug_log(LOG_GLOBAL_BUFFER);
}


/*
 * This method returns the client-network-data, in simple JSON form, of type ::
 *
 * {'antina_status' : 'value', 'signal_strength' : 'value'}
 */
void get_network_data(char *messageBuffer, int maxBufferLength)
{
    strcat(messageBuffer, "{");
    addSignalStrength(messageBuffer, 0);
    strcat(messageBuffer, "}");
}


/*
 * This method gets the device-manufacturer.
 */
void get_manufacturer(char *messagebuffer, int maxbufferlength)
{
    strcpy(messagebuffer, "LogicLadder");
}


/*
 * This method returns the univerally-unique-identifier for this device.
 */
void get_device_uuid(char *buffer, int maxbufferlength)
{
	const char *command_1 = "ATE0\r";
	const char *command_2 = "AT+CGSN\r";
	
	extern unsigned char modemSetToEchoOffMode;
		
	char *tmp = (char*) sg_malloc(MAX_BUFFER_SIZE);
	
	if(tmp == NULL)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not malloc for fetching UUID");
		error_log(LOG_GLOBAL_BUFFER);
				
		return;
	}
	memset(buffer, 0, maxbufferlength);
	memset(tmp, 0, MAX_BUFFER_SIZE);
	
	if(0 == modemSetToEchoOffMode) {
		run_simple_at_command_and_get_output(command_1, strlen(command_1), tmp, MAX_BUFFER_SIZE, OK_DELIMITER, 0, 0);
		modemSetToEchoOffMode = 1;
	}
	
	while(1)
	{
		
		run_simple_at_command_and_get_output(command_2, strlen(command_2), tmp, MAX_BUFFER_SIZE, OK_DELIMITER, 0, 1);
		if(strlen(tmp) == 15)
		{
			strcpy(buffer, tmp);
			break;
		}
	}

	sg_free(tmp);
}


/*
 * This method returns the ip-address of this device.
 */
void get_device_ip_address(char *buffer, int maxbufferlength)
{
}


#if GSM_INTERFACE_ENABLED == 1
#else
/*
 * This method returns the provisioning-pin for this device.
 */
void get_prov_pin_for_non_gsm_devices(char *buffer, int maxbufferlength)
{
}
#endif


#if SSL_ENABLED == 1

char uuid[20];
int wc_GenerateSeed(OS_Seed* os, byte* output, word32 sz)
{
	/*
	TODO: Use the GMAC when the chip is ready.
	*/
	return 0;
}


int _gettimeofday( struct timeval *tv, void *tzvp );
int _gettimeofday( struct timeval *tv, void *tzvp )
{
	uint64_t t = getCurrentTick();
	tv->tv_sec = t;
	tv->tv_usec = 0;
	
	return 0;
}

#endif
