#include "device_defines.h"

#if AT_INTERFACE_ENABLED == 1

#include "../driver/include/at.h"

#include "../modem/socket/simcom-5360.h"

#include <asf.h>

static volatile unsigned char firstZeroIgnored;
void UART0_Handler()
{
	uint32_t dw_status = uart_get_status((Uart*) UART0);
	
	if (dw_status & UART_SR_RXRDY)
	{
		unsigned char c = 0;
		
		if(firstZeroIgnored == 0)
		{
			uart_read((Uart*) UART0, &c);
			firstZeroIgnored = 1;
			return;

			
		}
		uart_read((Uart*) UART0, &c);
		ADD_DATA_TO_CIRCULAR_BUFFER
	}
}


/*
 * Initializes the AT-interface.
 */

void init_at_interface()
{
	usart_serial_options_t uart_serial_options = {
		.baudrate =   115200,
		.charlength = US_MR_CHRL_8_BIT,
		.paritytype = US_MR_PAR_NO,
		.stopbits   = US_MR_NBSTOP_1_BIT
			
	};
	
	usart_serial_init((Usart*) UART0, &uart_serial_options);
	
	reset_circular_buffer();
	
	uart_enable_interrupt((Uart*) UART0, UART_IER_RXRDY);
	NVIC_EnableIRQ(UART0_IRQn);
	
	startAndCountdownTimer(5, 1); //changed from 20 to 5
}


/*
 * This command fires the "command" (of length "len") on AT-interface, and gets the response in "usefulOutput"
 * as soon as "delimiter" suffix arrives.
 *
 * If the above does not happen exactly within 10 seconds, the system is reset.
 */
void do_fire_at_command_and_get_output(const char *command, int len, char *usefulOutput, const char *delimiter)
{
	usart_serial_write_packet((Usart*) UART0, (unsigned char*) command, len);
	WAIT_FOR_SIMCOM_5360_MODEM_RESPONSE
}


/*
 * Releases the AT-interface.
 */

void release_at_interface()
{
}
#endif
