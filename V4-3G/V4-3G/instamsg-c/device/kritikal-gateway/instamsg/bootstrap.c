#include <asf.h>

#include <string.h>

#include "./ethernet.h"

#include "../../../common/instamsg/driver/include/misc.h"
#include "../../../common/instamsg/driver/include/sg_stdlib.h"
#include "../../../common/instamsg/driver/include/log.h"
#include "../../../common/instamsg/driver/include/config.h"
#include "../../../common/instamsg/driver/include/json.h"
#include "../../../common/instamsg/driver/include/watchdog.h"
#include "../common/storage_utils.h"
#include "lwip/api.h"
#include "lwip/ip_addr.h"
#include "tc.h"
#include "compiler.h"
#include "board.h"
#include "conf_board.h"
#include "sam4e8c.h"
						
volatile char mainTaskStatus = WATCHDOG_STATUS_SLEEP;
volatile char ethernetTaskStatus = WATCHDOG_STATUS_SLEEP;
int globalWatchdogCounter = 0;

long int timerCounter=0;
unsigned char flagTimer=0;
volatile int wdt_reset_Counter=0;
volatile unsigned char wdt_reset_flag = 0;

xTaskHandle	 TaskFunction_Init_id;
xTaskHandle  ethernetTask_id;
//xTaskHandle  watchdogTask_id;
//char ipTest[20] = "";

volatile unsigned char flagTemp=0;

int main2(int argc, char** argv);
int main(void);

void external_wathdog_init (void)
{
	wdt_reset_flag = 1;
	wdt_reset_Counter = 190;
}
void external_watchdog_feed(void)
{
	wdt_reset_flag = 0;
	wdt_reset_Counter =0;
}

static void TaskFunction_Init(void *pvParameters)
{
	/* Just to avoid compiler warnings. */
	UNUSED(pvParameters);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "<< STARTING MAIN TASK >>");
	info_log(LOG_GLOBAL_BUFFER);
	
	main2(0, NULL);
	for(;;)
	{
		vTaskDelay(1000);
	}
	vTaskDelete(TaskFunction_Init_id);
}

void loadRunHours(void);

extern uint32_t g_ip_mode;

char staticIp[50];
char staticSubnetMask[50];
char staticGateway[50];
char staticDns[50];

char dns_ip_addr_ascii[50] = "8.8.8.8";	//default with google dns

static char ethernetValue[200];

static void EthernetTask_Init(void *pvParameters)
{
	/* Just to avoid compiler warnings. */
	UNUSED(pvParameters);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "<< STARTING ETHERNET TASK >>");
	info_log(LOG_GLOBAL_BUFFER);
	
	init_config();
	loadRunHours();
	
	/*
	* Keep the DHCP-mode as default.
	*/	
	g_ip_mode = 2;
	
	{
		int rc = FAILURE;
		
		memset(messageBuffer, 0, sizeof(messageBuffer));
		
		rc = get_config_value_from_persistent_storage(STATIC_IP_PARAMS, messageBuffer, sizeof(messageBuffer));
		if(rc == SUCCESS)
		{
			memset(ethernetValue, 0, sizeof(ethernetValue));
			getJsonKeyValueIfPresent(messageBuffer, CONFIG_VALUE_KEY, ethernetValue);
			
			/*
			sg_sprintf(LOG_GLOBAL_BUFFER, "static params: %s", ethernetValue);
			info_log(LOG_GLOBAL_BUFFER);
			*/
			
			#if 0
			ethernetValue[0] = 1; //to test static IP
			#endif
			
			if(strlen(ethernetValue) > 0)
			{
				get_nth_token_thread_safe(ethernetValue, ',', 1, staticIp, 1);
				get_nth_token_thread_safe(ethernetValue, ',', 2, staticSubnetMask, 1);
				get_nth_token_thread_safe(ethernetValue, ',', 3, staticGateway, 1);
				get_nth_token_thread_safe(ethernetValue, ',', 4, staticDns, 1);
				
				#if 0
				strcpy(staticIp ,"192.168.137.34");
				strcpy(staticSubnetMask,"255.255.255.0");
				strcpy(staticGateway ,"192.168.137.1");
				strcpy(staticDns,"192.168.137.1");	//google
				#endif
				
				strncpy(dns_ip_addr_ascii, staticDns, strlen(staticDns));
				
				sg_sprintf(LOG_GLOBAL_BUFFER, "\r\n\r\nFor Ethernet, Static-IP = [%s], Static-Subnet = [%s], Static-Gateway = [%s], Static-DNS = [%s]\r\n\r\n",
					                           staticIp, staticSubnetMask, staticGateway, staticDns);
				info_log(LOG_GLOBAL_BUFFER);
				
				g_ip_mode = 1;
			}
		}
		
		if(g_ip_mode == 2)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "\r\n\r\nStarting Ethernet in DHCP-Mode ..\r\n\r\n");
			info_log(LOG_GLOBAL_BUFFER);
		}
	}

	init_ethernet();
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "\r\n\r\nEthernet Initialized\r\n\r\n");
	info_log(LOG_GLOBAL_BUFFER);
	
	for(;;)
	{
		ethernetTaskStatus = WATCHDOG_STATUS_ALIVE;
		vTaskDelay(1000);
		
		#if 0
		sg_sprintf(LOG_GLOBAL_BUFFER, "DHCP Assigned IP: %s", ipTest);
		info_log(LOG_GLOBAL_BUFFER);
		#endif
	}
	vTaskDelete(ethernetTask_id);
}

void TC3_Handler(void)							// Used as Watchdog timer handler
{
	if ((tc_get_status(TC1, 0) & TC_SR_CPCS) == TC_SR_CPCS)
	{
		if(++timerCounter >= 145)
		{
			if(((0<wdt_reset_Counter)&&(0x01 == wdt_reset_flag))||(0x00 == wdt_reset_flag))
			{
				timerCounter = 0;
				globalWatchdogCounter++;
				if(globalWatchdogCounter >= GLOBAL_WATCHDOG_COUNT) 
				{
					/*info_log("<<<---global health check--->>>\r\n");*/
					globalWatchdogCounter = 0;
					
					/* if task status is dead, then reboot*/
					if(mainTaskStatus == WATCHDOG_STATUS_DEAD || \
						ethernetTaskStatus == WATCHDOG_STATUS_DEAD) {
							tc_stop(TC1, 0);
							fastResetDevice();
					}
					
					/* mark live task as dead if alive */
					if(mainTaskStatus == WATCHDOG_STATUS_ALIVE) {
						mainTaskStatus = WATCHDOG_STATUS_DEAD;
					}
					
					if(ethernetTaskStatus == WATCHDOG_STATUS_ALIVE) {
						ethernetTaskStatus = WATCHDOG_STATUS_DEAD;
					}	
				}
				ioport_set_pin_level(WDT_IP_GPIO,flagTimer);
				flagTimer^=1;
				wdt_reset_Counter=(wdt_reset_Counter>0)?wdt_reset_Counter-1:wdt_reset_Counter;
			}
		}
	}
}
//220--79:30--75: 200--86:180--96:120--151:

void configure_timer_for_hardwareWatchDog()
{
	pmc_enable_periph_clk(ID_TC3);		// Warning TC number is the channel not TC number.
	tc_init(TC1, 0,						// Init timer counter 1 channel 0.
	TC_CMR_WAVE |				// Waveform Mode is enabled.
	TC_CMR_CPCTRG |				// UP mode with automatic trigger on RC Compare
	TC_CMR_TCCLKS_TIMER_CLOCK2	// Use slow clock to avoid overflow. TC_CMR_TCCLKS_TIMER_CLOCK5
	);

	tc_write_rc(TC1, 0, 0xffff);			// Load the highest possible value into TC. ffff--27379,32768--54789 //600=1.2sec --2995600

	/* Configure TC interrupts for TC TC_CHANNEL_CAPTURE only */
	NVIC_SetPriority(TC3_IRQn, 0);		//  TC3 channel 0 IRQ
	NVIC_EnableIRQ(TC3_IRQn);
	tc_enable_interrupt(TC1, 0, TC_IER_CPCS);
	tc_start(TC1, 0);					// Start Timer counter 1 channel 0.
}

void watchdog_init()
{
	/*
	 * External-Watchdog
	 */
	ioport_set_pin_dir(WDT_IP_GPIO, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(WDT_IP_GPIO, WDT_IP_INACTIVE_LEVEL);
	delay_ms(10);
	configure_timer_for_hardwareWatchDog();
}

int main(void)
{
	long rc = pdPASS;
	
	sysclk_init();
	NVIC_SetPriorityGrouping(0);
	/* Initialize IOPORTs */
	ioport_init();
	watchdog_init();
	board_init();	
	SysTick_Config(sysclk_get_cpu_hz() / 1000);	
	bootstrapInit();
	debugLoggingEnabled = 1;
	currentLogLevel = LOG_LEVEL;
	init_serial_logger();

#if 1
	rc = xTaskCreate(TaskFunction_Init,
				(const signed char *const) "MAIN",
				1024,
				NULL,
				tskIDLE_PRIORITY + 2,
				&TaskFunction_Init_id);
	if(rc == pdPASS)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "\r\nMain-Task created successfully");
		info_log(LOG_GLOBAL_BUFFER);				
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Main-Task creation failed with code [%d]", (int) rc);
		info_log(LOG_GLOBAL_BUFFER);
	}
	
#endif

#if 1
	rc = xTaskCreate(EthernetTask_Init,
				(const signed char *const) "ETHERNET",
				256,
				NULL,
				tskIDLE_PRIORITY + 3,
				&ethernetTask_id);
	if(rc == pdPASS)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Ethernet-Task created successfully");
		info_log(LOG_GLOBAL_BUFFER);
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Ethernet-Task creation failed with code [%d]", (int) rc);
		info_log(LOG_GLOBAL_BUFFER);
	}
#endif
				
	vTaskStartScheduler();	// scheduler start for task switching
	for (;;)
	{
	}
	return 0;
}


void vApplicationMallocFailedHook(void);
void vApplicationMallocFailedHook(void)
{
	/**
	 * vApplicationMallocFailedHook() will only be called if
	 * configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.
	 * It is a hook function that will get called if a call to
	 * pvPortMalloc() fails. pvPortMalloc() is called internally by
	 * the kernel whenever a task, queue, timer or semaphore is created.
	 * It is also called by various parts of the demo application.
	 * If heap_1.c or heap_2.c are used, then the size of the heap
	 * available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	 * FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can
	 * be used to query the size of free heap space that remains
	 * (although it does not provide information on how the remaining heap
	 * might be fragmented).
	 */
	sg_sprintf(LOG_GLOBAL_BUFFER, "vApplicationMallocFailedHook hit");
	error_log(LOG_GLOBAL_BUFFER);
	
	taskDISABLE_INTERRUPTS();
	for (;;) {
	}
}

void vApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName);
void vApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName)
{
	(void) pcTaskName;
	(void) pxTask;

	/**
	 * Run time stack overflow checking is performed if
	 * configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.
	 * This hook function is called if a stack overflow is
	 * detected.
	 */
	sg_sprintf(LOG_GLOBAL_BUFFER, "vApplicationStackOverflowHook hit for [%s]", pcTaskName);
	error_log(LOG_GLOBAL_BUFFER);
	
	taskDISABLE_INTERRUPTS();
	for (;;) {
	}
}