#ifndef IOEYE_MODBUS_COMMON
#define IOEYE_MODBUS_COMMON

#include "device_serial.h"

#define SERIAL                  "[SERIAL] "
#define SERIAL_ERROR            "[SERIAL_ERROR] "

struct SimulatedModbus
{
    unsigned char isSimulatedDevice;
    char identifier[50];
    void (*valueConcatenatorFunc)(char *payload, void *arg);
    void *valueConcatenatorFuncArg;
};

extern Serial serialInterfaces[MAX_PORTS_ALLOWED];
extern char *simulatedModbusValuesCumulated;
extern int num_ports_in_use;

typedef void (*VALUE_CONCATENATOR)(char *payload, void *);

/*
 * Global-functions callable.
 */
void runSerialInitializations(void);
void releaseSerialInitializations(void);

void init_serial(void *ser, unsigned char isSimulatedDevice, const char *identifier, const char *serial_params_identifier,
                 const char *serial_delimiter_identifier, const char *modbus_tcp_ip_address_identifier,
                 VALUE_CONCATENATOR valueConcatenatorFunc, void *valueConcatenatorFuncArg);
int release_serial(void *ser);

void runSerialOnConnectProcedures(void);
void serialOnConnectProcedures(void *ser);

void runSerialProcedures(void);
void serialProcedures(void *ser);

void resetSimulatedModbusEnvironment(int numberOfSimulatedInterfaces, char *functionCode);
void flushSimulatedModbusEnvironment(int numberOfSimulatedInterfaces, char *functionCode);


#define DIGITAL_INPUTS_OUTPUTS_FUNCTION_CODE            "01"
#define ANALOG_INPUTS_FUNCTION_CODE                     "04"


/*
 * Must not be called directly.
 * Instead must be called as ::
 *
 *      modbus->send_command_and_read_response_sync
 */
int serial_send_command_and_read_response_sync(Serial *serial,
                                               unsigned char *commandBytes,
                                               int commandBytesLength,
                                               unsigned char *responseByteBuffer,
                                               int *responseBytesLength,
											   unsigned char *error_code);


/*
 * Internally Used.
 * Must not be called by anyone.
 */
void connect_underlying_serial_medium_guaranteed(Serial *serial);
int release_underlying_serial_medium_guaranteed(Serial *serial);

void parse_serial_connection_params(char *params_string,
                                    int *speed,
                                    int *parity,
                                    int *odd_parity,
                                    int *chars,
                                    int *blocking,
                                    int *two_stop_bits,
                                    int *hardware_control);

void processCommand(char *command, Serial *serial);

#endif
