/*
 * xmodem.h
 *
 * Created: 3/19/2020 11:29:19 AM
 *  Author: Ashutosh
 */ 

#ifndef XMODEM_H_INCLUDED
#define XMODEM_H_INCLUDED

#include "compiler.h"
#include "serial.h"

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond

/**
 * \defgroup common_services_xmodem_group XMODEM transfer protocol service
 *
 * The XMODEM transfer protocol service provides function to receive/send a file
 * from USART/UART using XMODEM protocol.
 *
 * @{
 */

#define		XMODEM_CHUNK_SIZE		128		/* chunk size of xmodem protocol*/

/* @brief to init tell whether file transfer ready to begin
 * Note: its blocking call to wait for xmodem uart ready
 * @return 1, if going to start
 *		   2, if going on
 *		   0, none	
 */
int isXmodemFileTransferredStarted();

uint32_t xmodem_receive_file(unsigned int fileSize);
void xmodem_send_file(int8_t *p_buffer, uint32_t ul_length);

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond

#endif /* XMODEM_H_INCLUDED */