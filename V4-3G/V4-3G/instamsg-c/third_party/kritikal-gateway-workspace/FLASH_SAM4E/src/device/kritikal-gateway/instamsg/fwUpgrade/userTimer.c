/*
 * userTimer.c
 *
 * Created: 4/1/2020 5:12:48 AM
 *  Author: Kartik.Gupta
 */ 

#include "common.h"
#include "userTimer.h"
#include "tc.h"

// My #define's. In the example project most of these were in conf_board.h
#define TC			TC0
#define CHANNEL		0
#define ID_TC		ID_TC0
#define TC_Handler  TC0_Handler
#define TC_IRQn     TC0_IRQn
static char gIsTimerRunning;	/* flag to update whether timer is running*/
static unsigned int gTimerCountInSecond;	/*to maintain counts of current timer in seconds*/

static Cb cb = NULL;	/* application callback when timer expired*/

// Interrupt service routine
void TC_Handler(void)
{
	uint32_t dummy;
	
	/*must read status register to clear interrupt*/
	dummy = tc_get_status(TC,CHANNEL);
	UNUSED(dummy);
	
	if(0 != gTimerCountInSecond) {
		gTimerCountInSecond--;
	}

	if(0 == gTimerCountInSecond) {
		/* in our appln we are using single shot timer*/
		tc_stop(TC, CHANNEL);
		gIsTimerRunning = 0;
		
		/* call callback*/
		if(NULL != cb) {
			cb();
		}
	}
}

void timerInit(Cb appCallback)
{
	cb = appCallback;
	
	// Configure TC interrupts
	NVIC_DisableIRQ(TC_IRQn);
	NVIC_ClearPendingIRQ(TC_IRQn);
	NVIC_SetPriority(TC_IRQn,0);
	NVIC_EnableIRQ(TC_IRQn);
}

int timerStart(int timeInSecond)
{
	// INPUTS:
	//	freq_desired	The desired rate at which to call the ISR, Hz
	unsigned int freq_desired = 1;
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();
	uint32_t counts;
	
	if(gIsTimerRunning) {
		return 1;	
	}
	
	gTimerCountInSecond = timeInSecond;
	
	// Configure PMC
	pmc_enable_periph_clk(ID_TC);

	// Configure TC for a 4Hz frequency and trigger on RC compare.
	tc_find_mck_divisor(
	(uint32_t)freq_desired,	// The desired frequency as a uint32.
	ul_sysclk,				// Master clock freq in Hz.
	&ul_div,				// Pointer to register where divisor will be stored.
	&ul_tcclks,				// Pointer to reg where clock selection number is stored.
	ul_sysclk);				// Board clock freq in Hz.
	
	tc_init(TC0, CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
		
	// Find the best estimate of counts, then write it to TC register C.
	counts = (ul_sysclk/ul_div)/freq_desired;
	tc_write_rc(TC0, 0, counts);
	
	// Enable interrupts for this TC, and start the TC.
	tc_enable_interrupt(TC, CHANNEL, TC_IER_CPCS);
	gIsTimerRunning = 1;
	tc_start(TC, CHANNEL);

	return 0;
}

void timerStop(void)
{
	if(1 == gIsTimerRunning) {
		tc_stop(TC, CHANNEL);
		gTimerCountInSecond = 0;	
		gIsTimerRunning = 0;
	} 	
}

int isTimerRunning(void)
{
	return gIsTimerRunning;
}

