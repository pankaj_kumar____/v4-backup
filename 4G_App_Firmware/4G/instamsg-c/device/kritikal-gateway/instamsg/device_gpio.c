/*******************************************************************************
 * Contributors:
 *
 *      Ajay Garg <ajay.garg@sensegrow.com>
 *
 *******************************************************************************/

#include "device_defines.h"

#include "../driver/include/gpio.h"
#include "../driver/include/json.h"
#include "../driver/include/log.h"
#include "../driver/include/config.h"
#include "../driver/include/hex.h"

#include <string.h>


#if SEND_GPIO_INFORMATION == 1

/*
 * Device-implementors need to fill in data for GPIO-pins.
 *
 * For example, if there are 3 Digital-Input pins with status on, on, off; and 2 Digital-Output pins with status off, on,
 * the data will be of form ::
 *
 * GPIO/DI001,DI011,DI020,DO000,DO011
 *
 */
static unsigned char pinOrientationLoaded;

static char pinPrefix[4][10];

static char previousData[10];
static char currentData[10];
static char previousBufferData[200];

extern char gpioPinOrientation[GPIO_PIN_ORIENTATION_CONFIG_LENGTH];
extern unsigned char sendGpioData;

#define PREVIOUS_GPIO_DATA      "PREVIOUS_GPIO_DATA"
static char gpioState[1024];

static void addStatus(unsigned int pin, char *buffer, unsigned char lowValue)
{
	if(ioport_get_pin_level(pin) == lowValue)
	{
		strcat(buffer, "0");
	}
	else
	{
		strcat(buffer, "1");
	}
}


static void getCurrentData(char *buffer)
{
	addStatus(GPIO_uC_ISO_DI1, buffer, 1);
	addStatus(GPIO_uC_ISO_DI2, buffer, 1);
	addStatus(uC_DO1_GPIO, buffer, 0);
	addStatus(uC_DO1_GPIO, buffer, 0);
}

void fill_dio_data(char *buffer, int maxBufferLength)
{
	int i = 0;
	
    if(pinOrientationLoaded == 0)
    {
	    if(sendGpioData == 0)
	    {
		    sg_sprintf(LOG_GLOBAL_BUFFER, "%sPin-Orientation not known, no data can be sent", DIO_ERROR);
		    error_log(LOG_GLOBAL_BUFFER);

		    return;
	    }

        for(i = 0; i < 4; i++)
        {
            strcpy(pinPrefix[i], "");

            {
                char small[3] = {0};

                if(i <= 1)
                {
                    /*
                     * DIs are from 00 to 01
                     */
                    sg_sprintf(small, "%u", i);
                }
                else
                {
                    /*
                     * DOs are from 00 to 01
                     */
                    sg_sprintf(small, "%u", i - 2);
                }

                addPaddingIfRequired(small, sizeof(small) - 1);
                fillPinPrefix(gpioPinOrientation, i + 1, pinPrefix[i], small);
            }
        }
		
        /*
         * Retrieve the last changed data.
         */
        memset(gpioState, 0, sizeof(gpioState));
        i = get_config_value_from_persistent_storage(PREVIOUS_GPIO_DATA, gpioState, sizeof(gpioState));
        if(i == SUCCESS)
        {
            memset(previousData, 0, sizeof(previousData));
            getJsonKeyValueIfPresent(gpioState, CONFIG_VALUE_KEY, previousData);
        }

        if(strlen(previousData) == 0)
        {
            strcpy(previousData, "0000");
        }

        sg_sprintf(LOG_GLOBAL_BUFFER, "%sPrevious Data loaded from config ==> [%s]", DIO, previousData);
        info_log(LOG_GLOBAL_BUFFER);

        memset(previousBufferData, 0, sizeof(previousBufferData));
        strcpy(previousBufferData, "");

	    pinOrientationLoaded = 1;
    }

    if(sendGpioData == 0)
    {
	    return;
    }

    memset(currentData, 0, sizeof(currentData));
    getCurrentData(currentData);


    if(strcmp(previousData, currentData) != 0)
    {
        /*
         * Fill in the data-buffer
         */
        strcat(buffer, "GPIO/");
        for(i = 0; i < 8; i++)
        {
            if(previousData[i] != currentData[i])
            {
                strcat(buffer, pinPrefix[i]);

                {
                    char small[2] = {0};
                    sg_sprintf(small, "%c", currentData[i]);
                    strcat(buffer, small);
                }

                strcat(buffer, ",");
            }
        }
        buffer[strlen(buffer) - 1] = 0;     /* Remove the last comma */


        /*
         * Store the latest changed data, to take reset into consideration.
         */
        RESET_GLOBAL_BUFFER;

        generate_config_json((char*) GLOBAL_BUFFER, PREVIOUS_GPIO_DATA, CONFIG_STRING, currentData, "");
        save_config_value_on_persistent_storage(PREVIOUS_GPIO_DATA, (char*) GLOBAL_BUFFER, 0);

        /*
         * Make a backup of latest changed data in-memory for further comparison.
         */
         memset(previousData, 0, sizeof(previousData));
         strcpy(previousData, currentData);

         memset(previousBufferData, 0, sizeof(previousBufferData));
         strcpy(previousBufferData, buffer);
     }
     else
     {
		strcpy(buffer, previousBufferData);     /* To satisfy the framework */
     }
}

#endif


void performDigitalOutputAction(const char *action)
{
    if(1)
    {
	    if(strcmp(action, "GPIO/DO000") == 0)
	    {
			ioport_set_pin_level(uC_DO1_GPIO, uC_DO_INACTIVE_LEVEL);
	    }
	    else if(strcmp(action, "GPIO/DO001") == 0)
	    {
			ioport_set_pin_level(uC_DO1_GPIO, uC_DO_ACTIVE_LEVEL);
	    }
	    else if(strcmp(action, "GPIO/DO010") == 0)
	    {
			ioport_set_pin_level(uC_DO2_GPIO, uC_DO_INACTIVE_LEVEL);
	    }
	    else if(strcmp(action, "GPIO/DO011") == 0)
	    {
			ioport_set_pin_level(uC_DO2_GPIO, uC_DO_ACTIVE_LEVEL);
	    }
    }
}
