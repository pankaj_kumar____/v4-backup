/*******************************************************************************
 * Contributors:
 *
 *      Ajay Garg <ajay.garg@sensegrow.com>
 *
 *******************************************************************************/

#include "../driver/include/globals.h"
#include "../driver/include/serial_logger.h"

#include <asf.h>
#include "conf_board.h"
#include "ioport.h"

/*
 * This method MUST connect the underlying medium (even if it means to retry continuously).
 */
void init_serial_logger()
{
	usart_serial_options_t uart_serial_options = {
		 .baudrate =   115200,
		 .charlength = US_MR_CHRL_8_BIT,
		 .paritytype = US_MR_PAR_NO,
		 .stopbits   = US_MR_NBSTOP_1_BIT
		 
	};
	
	usart_serial_init(((Usart*) CONSOLE_UART), &uart_serial_options);
}


/*
 * This method writes first "len" bytes from "buffer" onto the serial-logger-interface.
 *
 * This is a blocking function. So, either of the following must hold true ::
 *
 * a)
 * All "len" bytes are written.
 * In this case, SUCCESS must be returned.
 *
 *                      OR
 * b)
 * An error occurred while writing.
 * In this case, FAILURE must be returned immediately.
 */
int serial_logger_write(unsigned char* buffer, int len)
{
	usart_serial_write_packet((Usart*) CONSOLE_UART, buffer, len);
	return SUCCESS;
}


/*
 * This method MUST release the underlying medium (even if it means to retry continuously).
 * But if it is ok to re-connect without releasing the underlying-system-resource, then this can be left empty.
 */
void release_serial_logger()
{
}


