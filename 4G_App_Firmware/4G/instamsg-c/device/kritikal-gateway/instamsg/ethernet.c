/**
 * \file
 *
 * \brief Ethernet management for LwIP (threaded mode).
 *
 * Copyright (c) 2013-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <string.h>
#include "board.h"
#include "ethernet.h"
#if SAM3XA
# include "ethernet_phy.h"
# include "emac.h"
#elif SAM4E
# include "ethernet_phy.h"
# include "gmac.h"
#elif SAM4S
# include "ksz8851snl.h"
# include "ksz8851snl_reg.h"
# include "netif/sam_spi_ksz8851snl.h"
#else
# error Unsupported chip type
#endif
#include "sysclk.h"
/* lwIP includes */
#include "lwip/sys.h"
#include "lwip/api.h"
#include "lwip/tcp.h"
#include "lwip/tcpip.h"
#include "lwip/memp.h"
#include "lwip/dhcp.h"
#include "lwip/dns.h"
#include "lwip/stats.h"
#include "lwip/init.h"
#include "lwip/ip_frag.h"
#if ( (LWIP_VERSION) == ((1U << 24) | (3U << 16) | (2U << 8) | (LWIP_VERSION_RC)) )
#include "netif/loopif.h"
#else
#include "lwip/inet.h"
#include "lwip/tcp_impl.h"
#endif
#include "netif/etharp.h"

#include "./ethernet.h"

#include "../driver/include/socket.h"

/* Global variable containing MAC Config (hw addr, IP, GW, ...) */
struct netif gs_net_if;


uint32_t g_ip_mode = 2; // 0 unselected, 1 static, 2 dhcp.
int8_t g_c_ipconfig[25];

extern char staticIp[50];
extern char staticSubnetMask[50];
extern char staticGateway[50];

/**
 * \brief Configure ethernet interface.
 */
extern err_t ethernetif_init(struct netif *netif);
static void ethernet_configure_interface(void)
{
	struct ip_addr x_ip_addr, x_net_mask, x_gateway;

	if (g_ip_mode == 2) {
		/* DHCP mode. */
		x_ip_addr.addr = 0;
		x_net_mask.addr = 0;                       
	}
	else {
		/* Fixed IP mode. */
		/* Default IP addr */
		int first = 0, second = 0, third = 0, fourth = 0;
		
		get_ip_address_tokens(staticIp, &first, &second, &third, &fourth);
		IP4_ADDR(&x_ip_addr, first, second, third, fourth);
		
		/* Default subnet mask. */
		get_ip_address_tokens(staticSubnetMask, &first, &second, &third, &fourth);
		IP4_ADDR(&x_net_mask, first, second, third, fourth);

		/* Default gateway addr. */
		get_ip_address_tokens(staticGateway, &first, &second, &third, &fourth);
		IP4_ADDR(&x_gateway, first, second, third, fourth);
	}

	/* Add data to netif. */
	/* Use ethernet_input as input method for standalone lwIP mode. */
	/* Use tcpip_input as input method for threaded lwIP mode. */
	if (NULL == netif_add(&gs_net_if, &x_ip_addr, &x_net_mask, &x_gateway, NULL,
			ethernetif_init, tcpip_input)) {
		LWIP_ASSERT("NULL == netif_add", 0);
	}

	/* Make it the default interface. */
	netif_set_default(&gs_net_if);

	/* Setup callback function for netif status change. */
	netif_set_status_callback(&gs_net_if, status_callback);

	/* Bring it up. */
	if (g_ip_mode == 2) {
		/* DHCP mode. */
		if (ERR_OK != dhcp_start(&gs_net_if)) {
			LWIP_ASSERT("ERR_OK != dhcp_start", 0);
		}
	}
	else {
		/* Static mode. */
		netif_set_up(&gs_net_if);
	}
}

/**
 * \brief Initialize ethernet stask.
 */
void init_ethernet(void)
{
	/* Initialize lwIP. */
	/* Call tcpip_init for threaded lwIP mode. */
	tcpip_init(NULL, NULL);

	/* Set hw and IP parameters, initialize MAC too */
	ethernet_configure_interface();
}

/**
 * \brief Status callback used to print assigned IP address.
 *
 * \param netif Instance to network interface.
 */
void status_callback(struct netif *netif)
{
	if (netif_is_up(netif)) {
		strcat((char*)g_c_ipconfig, inet_ntoa(*(struct in_addr *)&(netif->ip_addr)));
		sg_sprintf(LOG_GLOBAL_BUFFER, "DHCP Assigned IP: %s", g_c_ipconfig);
		info_log(LOG_GLOBAL_BUFFER);
		for(int i = 0; i < 6; i++) {
			sg_sprintf(LOG_GLOBAL_BUFFER, "%02X", netif->hwaddr[i]);
			info_log(LOG_GLOBAL_BUFFER);	
		}
		
		//extern char ipTest[20];
		//strcpy(ipTest, g_c_ipconfig);
		g_ip_mode = 3;
	}
}
