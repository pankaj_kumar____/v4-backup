/**
 * \file
 *
 * \brief SAM4E-EK board init.
 *
 * Copyright (c) 2012-2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include "compiler.h"
#include "board.h"
#include "conf_board.h"
#include "ioport.h"
#include "delay.h"

#include "device_defines.h"

#include "../../../common/instamsg/driver/include/globals.h"


/**
 * \brief Set peripheral mode for IOPORT pins.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param port IOPORT port to configure
 * \param masks IOPORT pin masks to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 */
#define ioport_set_port_peripheral_mode(port, masks, mode) \
	do {\
		ioport_set_port_mode(port, masks, mode);\
		ioport_disable_port(port, masks);\
	} while (0)

/**
 * \brief Set peripheral mode for one single IOPORT pin.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param pin IOPORT pin to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 */
#define ioport_set_pin_peripheral_mode(pin, mode) \
	do {\
		ioport_set_pin_mode(pin, mode);\
		ioport_disable_pin(pin);\
	} while (0)

/**
 * \brief Set input mode for one single IOPORT pin.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param pin IOPORT pin to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 * \param sense Sense for interrupt detection (\ref ioport_sense)
 */
#define ioport_set_pin_input_mode(pin, mode, sense) \
	do {\
		ioport_set_pin_dir(pin, IOPORT_DIR_INPUT);\
		ioport_set_pin_mode(pin, mode);\
		ioport_set_pin_sense_mode(pin, sense);\
	} while (0)


#define POWERKEY_GPIO		 PIO_PA18_IDX
#define POWERKEY_ACTIVE		 (IOPORT_PIN_LEVEL_HIGH)
#define POWERKEY_INACTIVE	 (IOPORT_PIN_LEVEL_LOW)

#define SIM_SEL_GPIO		 PIO_PD22_IDX
#define SIM_SEL_ACTIVE		 (IOPORT_PIN_LEVEL_HIGH)
#define SIM_SEL_INACTIVE	 (IOPORT_PIN_LEVEL_LOW)

#define GPIO_DI_FLAGS        (IOPORT_MODE_PULLUP | IOPORT_MODE_DEBOUNCE)
#define GPIO_DI_SENSE        (IOPORT_SENSE_BOTHEDGES)

#define GSM_GPS_Reset_GPIO           (PIO_PA19_IDX)
#define GSM_GPS_Reset_ACTIVE		 (IOPORT_PIN_LEVEL_HIGH)
#define GSM_GPS_Reset_INACTIVE       (IOPORT_PIN_LEVEL_LOW)


volatile unsigned char digitalPinsInitialized;
void delay_second(unsigned int seconds)
{
	long milliSeconds=seconds*1000;
	unsigned char flagHiLow = 1;
	while(milliSeconds>0)
	{
		ioport_set_pin_level(WDT_IP_GPIO, flagHiLow);
		flagHiLow^=1;
		milliSeconds-=200;
		delay_ms(200);
	}
}
void board_init(void)
{
	#ifndef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
	#endif

	/*
	* DI
	*/

    ioport_set_pin_input_mode(GPIO_uC_ISO_DI1, GPIO_DI_FLAGS, GPIO_DI_SENSE);
    ioport_set_pin_input_mode(GPIO_uC_ISO_DI2, GPIO_DI_FLAGS, GPIO_DI_SENSE);	
	
	/*
	* DO
	*/
    ioport_set_pin_dir(uC_DO1_GPIO, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(uC_DO1_GPIO, uC_DO_INACTIVE_LEVEL);

    ioport_set_pin_dir(uC_DO2_GPIO, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(uC_DO2_GPIO, uC_DO_INACTIVE_LEVEL);
	
	digitalPinsInitialized = 1;

	/*
	* Serial-Logger
	*/
	ioport_set_port_peripheral_mode(PINS_UART1_PORT, PINS_UART1, PINS_UART1_FLAGS);
	
	/*
	* AT-Interface/Modem
	*/
	ioport_set_port_peripheral_mode(PINS_UART0_PORT, PINS_UART0, PINS_UART0_FLAGS);
	sysclk_enable_peripheral_clock(ID_USART0);
	ioport_set_pin_peripheral_mode(PIN_USART0_RXD_IDX, PIN_USART0_RXD_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_USART0_TXD_IDX, PIN_USART0_TXD_FLAGS);
	
	delay_second(1);
	ioport_set_pin_dir(GSM_GPS_Reset_GPIO, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(GSM_GPS_Reset_GPIO, GSM_GPS_Reset_INACTIVE);
	ioport_set_pin_dir(POWERKEY_GPIO, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(POWERKEY_GPIO,POWERKEY_ACTIVE);
	delay_ms(250);
	ioport_set_pin_level(POWERKEY_GPIO,POWERKEY_INACTIVE);
	//delay_ms(2700);
	delay_second(3);
	
	/*
	* SIM Selection
	*/
	
	ioport_set_pin_dir(SIM_SEL_GPIO, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SIM_SEL_GPIO, SIM_SEL_INACTIVE);
	
#if 1
	/*
	* ADC
	*/
	//ioport_set_pin_dir(RSTB_GPIO, IOPORT_DIR_OUTPUT);	/* This causes hang, anyways this is not required */

	ioport_set_pin_dir(LED_VDC1_GPIO, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(LED_VDC2_GPIO, IOPORT_DIR_OUTPUT);

	//ioport_set_pin_input_mode(RDYB_GPIO, RDYB_FLAGS, RDYB_GPIO_SENSE); /* This causes hang too, not required too */
#endif

	
	/*
	* Flash, ADC
	*/
	ioport_set_pin_peripheral_mode(SPI_MISO_GPIO, SPI_MISO_FLAGS);
	ioport_set_pin_peripheral_mode(SPI_MOSI_GPIO, SPI_MOSI_FLAGS);
	ioport_set_pin_peripheral_mode(SPI_SPCK_GPIO, SPI_SPCK_FLAGS);
	
	ioport_set_pin_peripheral_mode(SPI_NPCS0_GPIO, SPI_NPCS0_FLAGS);
	ioport_set_pin_peripheral_mode(SPI_NPCS1_PB14_GPIO, SPI_NPCS1_PB14_FLAGS);
	
	
	/*
	* SD
	*/
	ioport_set_pin_peripheral_mode(PIN_HSMCI_MCCDA_GPIO, PIN_HSMCI_MCCDA_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_HSMCI_MCCK_GPIO, PIN_HSMCI_MCCK_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_HSMCI_MCDA0_GPIO, PIN_HSMCI_MCDA0_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_HSMCI_MCDA1_GPIO, PIN_HSMCI_MCDA1_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_HSMCI_MCDA2_GPIO, PIN_HSMCI_MCDA2_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_HSMCI_MCDA3_GPIO, PIN_HSMCI_MCDA3_FLAGS);
	delay_second(1);
	
	/*
	* RS-485
	*/
    ioport_set_pin_dir(uC_RE_DE_GPIO, IOPORT_DIR_OUTPUT);
    ioport_set_pin_peripheral_mode(PIN_USART1_RXD_IDX, PIN_USART1_RXD_FLAGS);
    ioport_set_pin_peripheral_mode(PIN_USART1_TXD_IDX, PIN_USART1_TXD_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_USART1_RTS_IDX, PIN_USART1_RTS_FLAGS);
	delay_second(1);
	
	/*
	* Ethernet
	*/
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_RXC_IDX, PIN_KSZ8051MNL_RXC_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_TXC_IDX, PIN_KSZ8051MNL_TXC_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_TXEN_IDX, PIN_KSZ8051MNL_TXEN_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_TXD3_IDX, PIN_KSZ8051MNL_TXD3_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_TXD2_IDX, PIN_KSZ8051MNL_TXD2_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_TXD1_IDX,	PIN_KSZ8051MNL_TXD1_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_TXD0_IDX,	PIN_KSZ8051MNL_TXD0_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_RXD3_IDX,	PIN_KSZ8051MNL_RXD3_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_RXD2_IDX,	PIN_KSZ8051MNL_RXD2_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_RXD1_IDX,	PIN_KSZ8051MNL_RXD1_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_RXD0_IDX,	PIN_KSZ8051MNL_RXD0_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_RXER_IDX,	PIN_KSZ8051MNL_RXER_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_RXDV_IDX,	PIN_KSZ8051MNL_RXDV_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_CRS_IDX, PIN_KSZ8051MNL_CRS_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_COL_IDX, PIN_KSZ8051MNL_COL_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_MDC_IDX, PIN_KSZ8051MNL_MDC_FLAGS);
	ioport_set_pin_peripheral_mode(PIN_KSZ8051MNL_MDIO_IDX, PIN_KSZ8051MNL_MDIO_FLAGS);
	ioport_set_pin_dir(PIN_KSZ8051MNL_INTRP_IDX, IOPORT_DIR_INPUT);
	delay_second(1);
	
	/*
	RTC
	*/
	ioport_set_pin_peripheral_mode(TWI0_DATA_GPIO, TWI0_DATA_FLAGS);
	ioport_set_pin_peripheral_mode(TWI0_CLK_GPIO, TWI0_CLK_FLAGS);
	
	delay_second(1);
}

void toggle_sim_slot(unsigned char index)
{
	if(0x00 == index)
	{
		ioport_set_pin_level(SIM_SEL_GPIO, SIM_SEL_INACTIVE);
	}
	else
	{
		ioport_set_pin_level(SIM_SEL_GPIO, SIM_SEL_ACTIVE);
	}
}