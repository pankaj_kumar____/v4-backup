/*
 * fwUpgrade.h
 *
 * Created: 4/6/2020 5:55:52 PM
 *  Author: Kartik.Gupta
 */ 


#ifndef FWUPGRADE_H_
#define FWUPGRADE_H_

#define	FW_UPGRADE_DEFAULT_HOST_IP		"3.15.201.154"
#define FW_UPGRADE_PORT					80
#define FW_UPGRADE_DEFAULT_HOST_NAME	"ip-172-31-44-156"
//#define FW_BIN_FILE_NAME				"v5_firmware.bin"	/* change binary name to v5_firmware.bin once for legacy*/
//#define FW_BIN_FILE_NAME				"test_v4_4g_firmware.bin"
#define FW_BIN_FILE_NAME				"v4_4g_firmware.bin"

/* @brief 1) the function will check for any new firmware available at server
 *		  2) get the binary file from server
 *		  3) validate file
 *        3) update it on external flash
 *        4) reset the device for upgrade
 * @param serverHostName, host name of OTA server
 * @param serverIpAddress, ip address of OTA server
 * return - not required, if success device will be reset, on failure the function will be exited to resume normal operations
 */
void beginFwUpgrade(char *serverHostName, char *serverIpAddress);

/*
 *	@brief update the boot configurations once the application run successfully
 */
void updateSuccessfulFwUpgradeConfigurations(void);

#endif /* FWUPGRADE_H_ */