/*
 * httpUtils.c
 *
 * Created: 4/11/2020 9:02:39 PM
 *  Author: Kartik.Gupta
 */ 

#include "httpUtils.h"
#include "fwUpgrade.h"
#include "internalFlash.h"
#include "common.h"
#include "../../../../instamsg-c/common/instamsg/driver/include/socket.h"

#define HTTP_MAX_CMD_SIZE	256		/* http command max length in bytes*/

static char *gServerHostName = NULL;
static char *gServerIpAddress = NULL;

static SG_Socket gSocketOperation;	/* socket operation structure instance*/

int initHttpUtils(char *serverHostName, char *serverIpAddress)
{
	/* reset structure*/
	memset(&gSocketOperation, '\0', sizeof(SG_Socket));
	
	if(NULL == serverIpAddress) {
		gServerIpAddress = FW_UPGRADE_DEFAULT_HOST_IP;
	} else {
		gServerIpAddress = serverIpAddress;
	}
	
	if(NULL == serverHostName) {
		gServerHostName = FW_UPGRADE_DEFAULT_HOST_NAME;
	} else {
		gServerHostName = serverHostName;
	}
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "OTA Server hostname: %s, IP: %s", gServerHostName, gServerIpAddress);
	info_log(LOG_GLOBAL_BUFFER);
	
	init_socket(&gSocketOperation, gServerIpAddress, FW_UPGRADE_PORT, SOCKET_TCP, 1);
	
	/* check for socket initialization failure*/
	if(gSocketOperation.socketCorrupted == 1) {
		sg_sprintf(LOG_GLOBAL_BUFFER, "FAILED to Connect on either channel!!!");
		error_log(LOG_GLOBAL_BUFFER);
		return OPER_STATE_SOCKET_INIT;
	}
	
	return OPER_STATE_SUCCESS;
}

int parseHttpRespHeader(char *dataBuff, int size, httpHeaderInfo_t* httpInfo)
{
	/* sample header content*/
	/*
	char dataBuff[]  = "HTTP/1.1 206 Partial Content\r\n\
	Date: Thu, 09 Apr 2020 08:30:40 GMT\r\n\
	Server: Apache/2.4.29 (Ubuntu)\r\n\
	Last-Modified: Wed, 08 Apr 2020 11:11:59 GMT\r\n\
	ETag: \"4e51-5a2c591a678f1\"\r\n\
	Accept-Ranges: bytes\r\n\
	Content-Length: 513\r\n\
	Content-Range: bytes 0-512/20049\r\n\
	Content-Type: application/octet-stream\r\n\r\nMsg";
	*/
	
	char headerSeparator[5] = {'\r', '\n', '\r', '\n', '\0'};
	char codeSeparator[12] = "HTTP/1.1 ";
	char contentLengthSeparator[20] = "Content-Length: ";
	char contentRangeSeparator[24] = "Content-Range: bytes ";
	char fileSizeSeparator = '/';
	char chunkSeparator = '-';
	char tempHttpRespBuffer[MAX_CHUNK_SIZE + 1];
	
	char *httpRespEndPoint, *httpResponseCodeAddr, *httpContentLengthAddr, *httpContentRangeAddr;
	char httpRespCode[4], httpContentLength[12], httpChunkStart[12], httpChunkFinish[12], httpFileSize[12], httpContentRange[40];
		
	char currByte = 0x00;
	int byteCounter = 0, i = 0;
	
	memset(httpInfo, '\0', sizeof(httpHeaderInfo_t));
	
	if(NULL == httpInfo || NULL == dataBuff) {
		return OPER_STATE_INVALID_BUFFER;	
	}
	
	if(size > MAX_CHUNK_SIZE || size <= 0) {
		return OPER_STATE_INVALID_BUFF_SIZE;
	}
	
	/* take buffer backup in temporary storage*/
	memset(tempHttpRespBuffer, '\0', MAX_CHUNK_SIZE + 1);
	memcpy(tempHttpRespBuffer, dataBuff, size);
	tempHttpRespBuffer[size] = '\0';
	
		
	//info_log(tempHttpRespBuffer);
		
	/* locate substring where header separator present*/
	httpRespEndPoint = strstr(tempHttpRespBuffer, (char*)headerSeparator);
	if(NULL == httpRespEndPoint) {
		return OPER_STATE_INVALID_BUFFER;
	}
	
	/* pointer to the data apart from the http header---->just for understanding(its not required)*/
	/*char *httpResponseMessagePtr = httpRespEndPoint + strlen(headerSeparator);*/
	
	/* separate out the http status code*/
	httpResponseCodeAddr = strstr(tempHttpRespBuffer, codeSeparator) + strlen(codeSeparator);
	if(NULL == httpResponseCodeAddr) {
		return OPER_STATE_INVALID_BUFFER;
	}
	memset(httpRespCode, '\0', sizeof(httpRespCode));
	
	strncpy(httpRespCode, httpResponseCodeAddr, 3);
	httpInfo->httpRespCode = atoi(httpRespCode);
	
	if(httpInfo->httpRespCode != HTTP_CODE_PARTIAL_CONTENT) {
		return OPER_STATE_HTTP_REQUEST;	
	}
	
	/* separate out the content length*/
	httpContentLengthAddr = strstr(tempHttpRespBuffer, contentLengthSeparator) + strlen(contentLengthSeparator);
	if(NULL == httpContentLengthAddr) {
		return OPER_STATE_INVALID_BUFFER;
	}
	memset(httpContentLength, '\0', sizeof(httpContentLength));
	
	while(1) {
		currByte = httpContentLengthAddr[byteCounter];
		if(currByte == '\r') {
			httpContentLength[byteCounter] = '\0';
			byteCounter = 0;
			currByte = 0x00;
			break;
		} else {
			httpContentLength[byteCounter] = currByte;
		}
		byteCounter++;
	}

	/* separate out the content range*/
	httpContentRangeAddr = strstr(tempHttpRespBuffer, contentRangeSeparator) + strlen(contentRangeSeparator);
	if(NULL == httpContentRangeAddr) {
		return OPER_STATE_INVALID_BUFFER;
	}
	
	memset(httpChunkStart, '\0', sizeof(httpChunkStart));
	memset(httpChunkFinish, '\0', sizeof(httpChunkFinish));
	memset(httpFileSize, '\0', sizeof(httpFileSize));

	while(1) {
		currByte = httpContentRangeAddr[byteCounter];
		if(currByte == '\n') {
			httpContentRange[byteCounter] = '\0';
			byteCounter = 0;
			currByte = 0x00;
			break;
		} else {
			httpContentRange[byteCounter] = currByte;
		}
		byteCounter++;
	}
	
	currByte = httpContentRange[byteCounter];
	while(currByte != chunkSeparator) {
		httpChunkStart[byteCounter] = currByte;
		byteCounter++;
		currByte = httpContentRange[byteCounter];
	}
	httpChunkStart[byteCounter] = '\0';
	
	currByte = httpContentRange[++byteCounter];
	while(currByte != fileSizeSeparator) {
		httpChunkFinish[i] = currByte;
		byteCounter++;
		i++;
		currByte = httpContentRange[byteCounter];
	}
	httpChunkFinish[i] = '\0';

	currByte = httpContentRange[++byteCounter];
	i = 0;

	while(currByte != '\r') {
		httpFileSize[i] = currByte;
		byteCounter++;
		i++;
		currByte = httpContentRange[byteCounter];
	}
	httpFileSize[i] = '\0';

	httpInfo->httpRespHeaderLength =  httpRespEndPoint - tempHttpRespBuffer + strlen(headerSeparator);
	httpInfo->httpRespFileLength = size - httpInfo->httpRespHeaderLength;
	httpInfo->contentLength = atoi(httpContentLength);
	httpInfo->startChunkByte = atoi(httpChunkStart);
	httpInfo->finishChunkByte = atoi(httpChunkFinish);
	httpInfo->fileSize = atoi(httpFileSize);
	
#if 1
	info_log("------------------------------------------------------------\r\n");
	sg_sprintf(LOG_GLOBAL_BUFFER, "http code: %d", httpInfo->httpRespCode);
	info_log(LOG_GLOBAL_BUFFER);
	sg_sprintf(LOG_GLOBAL_BUFFER, "header len: %d, message len: %d", \
								httpInfo->httpRespHeaderLength, httpInfo->httpRespFileLength);
	info_log(LOG_GLOBAL_BUFFER);
	sg_sprintf(LOG_GLOBAL_BUFFER, "content length: %d", httpInfo->contentLength);
	info_log(LOG_GLOBAL_BUFFER);
	sg_sprintf(LOG_GLOBAL_BUFFER, "start: %d, finish: %d, size: %d", httpInfo->startChunkByte, httpInfo->finishChunkByte, httpInfo->fileSize);
	info_log(LOG_GLOBAL_BUFFER);
	info_log("------------------------------------------------------------\r\n");
#endif

	return OPER_STATE_SUCCESS;
}

int generateHttpChunkRequest(unsigned int chunkStart, unsigned int totalBytes)
{
	char httpReq[HTTP_MAX_CMD_SIZE];
	unsigned int chunkFinish;
	int ret;
	
	if(0 == totalBytes || totalBytes > MAX_CHUNK_SIZE) {
		return OPER_STATE_INVALID_CHUNK;	
	}
	
	chunkFinish = chunkStart + totalBytes - 1;
	
	/* prepare command*/
	sprintf(httpReq, "GET http://%s/%s HTTP/1.1\r\nHost: %s\r\nRange: bytes=%d-%d\r\n\r\n", \
				gServerIpAddress, FW_BIN_FILE_NAME, gServerHostName, chunkStart, chunkFinish);
	info_log(httpReq);
	
	/*send request*/
	ret = gSocketOperation.write(&gSocketOperation, httpReq, strlen(httpReq));
	if(ret != SUCCESS) {
		return OPER_STATE_SOCKET_REQUEST_WRITE;
	}
	
	return OPER_STATE_SUCCESS;
}

int httpRead(char *dataBuff, int size, int* bytesReceived)
{
	if(!dataBuff || !bytesReceived) {
		return OPER_STATE_INVALID_BUFFER;
	}
	
	if(0 == size || size > MAX_CHUNK_SIZE) {
		return OPER_STATE_INVALID_CHUNK;
	}
	
	/* non blocking read*/
	int rc = gSocketOperation.read(&gSocketOperation, dataBuff, size, 0);
	*bytesReceived = gSocketOperation.bytes_received;
	
	if(rc != SUCCESS) {
		if(rc == SOCKET_READ_TIMEOUT) {
			/* if not all bytes read, but may be some or none are read*/
			return OPER_STATE_SOCKET_READ_TIMEOUT;
		} else {
			return OPER_STATE_SOCKET_READ;
		}
	}
	return OPER_STATE_SUCCESS;
}

void deInitHttpUtils(void)
{
	release_socket_simple_guaranteed(&gSocketOperation);
}