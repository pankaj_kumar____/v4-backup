#ifndef GENERATOR_MONITORING_APIS
#define GENERATOR_MONITORING_APIS

void modbusProceduresForGeneratorMonitoring();
void modbusOnConnectProceduresForGeneratorMonitoring();
void persistRunHours();
void init_generator_monitoring();
void persistRunHours();

#endif
