#include "../../instamsg/driver/include/instamsg.h"
#include "../../instamsg/driver/include/misc.h"
#include "../../instamsg/driver/include/sg_stdlib.h"
#include "../../instamsg/driver/include/config.h"
#include "../../instamsg/driver/include/json.h"
#include "../../instamsg/driver/include/hex.h"

#include "../../ioeye/include/serial.h"
#include "../../../device/kritikal-gateway/instamsg/fwUpgrade/fwUpgrade.h"

volatile short runHours1;
volatile short runHours2;
volatile short generator1Running;
volatile short generator2Running;

struct SimulatedModbus adcInterface;

static int onConnect()
{
    runSerialOnConnectProcedures();
    return SUCCESS;
}


static void coreLoopyBusinessLogicInitiatedBySelf()
{
	resetSimulatedModbusEnvironment(12, (char*) "04");
    serialProcedures(&adcInterface);
	flushSimulatedModbusEnvironment(12, (char*) "04");

    runSerialProcedures();
}


void release_app_resources()
{
    releaseSerialInitializations();
}


static char jsonString[500];
#define RUN_HOURS_1		"RUN_HOURS_1"
#define RUN_HOURS_2		"RUN_HOURS_2"

static void persistRunHour(const char *key, short value)
{
	int rc = FAILURE;
	
	char small[10] = {0};
	sg_sprintf(small, "%d", value);
	
	memset(jsonString, 0, sizeof(jsonString));
	generate_config_json(jsonString, key, CONFIG_INT, small, "");
	
	rc = save_config_value_on_persistent_storage(key, jsonString, 0);
	if(rc == SUCCESS)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Successfully persisted [%s] = [%u]", key, value);
		info_log(LOG_GLOBAL_BUFFER);
	}
	else
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Failed to persist [%s] = [%u]", key, value);
		info_log(LOG_GLOBAL_BUFFER);
	}
}

void persistRunHours(void);
void persistRunHours()
{
	persistRunHour(RUN_HOURS_1, runHours1);
	persistRunHour(RUN_HOURS_2, runHours2);	
}


static void loadIntegerValue(const char *key, volatile short* value)
{
	int rc = FAILURE;
	
	memset(jsonString, 0, sizeof(jsonString));
	
	rc = get_config_value_from_persistent_storage(key, jsonString, sizeof(jsonString));
	
	if(rc == SUCCESS)
	{
		char small[10] = {0};
		getJsonKeyValueIfPresent(jsonString, CONFIG_VALUE_KEY, small);
		
		*value = sg_atoi(small);
	}	
	else
	{
		*value = 0;
	}
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "Loaded [%s] = [%u]", key, *value);
	info_log(LOG_GLOBAL_BUFFER);
}


void loadRunHours(void);
void loadRunHours()
{
	loadIntegerValue(RUN_HOURS_1, &runHours1);
	loadIntegerValue(RUN_HOURS_2, &runHours2);
}


void read_adc(uint8_t channel, short *value);


static void appendData(char *payload, short *value)
{
	char stringified_data[5] = {0};

	sg_sprintf(stringified_data, "%x", *value);
	addPaddingIfRequired(stringified_data, sizeof(stringified_data) - 1);
	strcat(payload, stringified_data);
}


static short previousValues[6];
static void processADCData(char *payload, uint8_t channel)
{
	short value = 0;

	read_adc(channel, &value);
	previousValues[channel] = value;
	appendData(payload, &value);
}


void getADCsData(char *payload, void *arg);
void getADCsData(char *payload, void *arg)
{
	short temp = 0;
	
	processADCData(payload, 3);
	processADCData(payload, 2);
	processADCData(payload, 1);
	processADCData(payload, 0);

	processADCData(payload, 5);
	processADCData(payload, 4);

	appendData(payload, (short*) &generator1Running);
	appendData(payload, (short*) &generator2Running);

	appendData(payload, (short*) &runHours1);
	appendData(payload, (short*) &runHours2);
	
	temp = ioport_get_pin_level(uC_DO1_GPIO);
	appendData(payload, &temp);
	
	temp = ioport_get_pin_level(uC_DO2_GPIO);
	appendData(payload, &temp);
}


int main2(int argc, char** argv);
int main2(int argc, char** argv)
{	
#if FILE_LOGGING_ENABLED == 1
    globalSystemInit("instamsg.log");
#else
    globalSystemInit(NULL);
#endif
    init_serial(&adcInterface, 1, "ADC_INTERFACE", NULL, NULL, NULL, (VALUE_CONCATENATOR) getADCsData, NULL);
	{
		int i = 0;
		for(i = 0; i < 6; i++)
		{
			previousValues[i] = 0;
		}
	}
    runSerialInitializations();
	
	/* this function need to be placed after verifying our application boot up successfully with new firmware, to decide rollback accurately*/
	updateSuccessfulFwUpgradeConfigurations();
	
    start(onConnect, NULL, NULL, coreLoopyBusinessLogicInitiatedBySelf, 60);
    return 0;
}
