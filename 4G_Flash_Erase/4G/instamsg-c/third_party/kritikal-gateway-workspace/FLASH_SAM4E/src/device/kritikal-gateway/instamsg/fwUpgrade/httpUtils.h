/*
 * httpUtils.h
 *
 * Created: 4/11/2020 9:02:03 PM
 *  Author: Kartik.Gupta
 */ 


#ifndef HTTPUTILS_H_
#define HTTPUTILS_H_

#define HTTP_CODE_PARTIAL_CONTENT	206	/* http response code to get file in chunks*/

/* structure having parsed header information for http structure*/
typedef struct {
	int httpRespCode;
	int httpRespHeaderLength;
	int httpRespFileLength;
	int contentLength;
	int startChunkByte;
	int finishChunkByte;
	int fileSize;
} httpHeaderInfo_t;

/* /brief to initialize the http module, like socket initialization tasks..
 * /return : OPER_STATE_SUCCESS for success, else state error code
 */
int initHttpUtils(char *serverHostName, char *serverIpAddress);

/*
 *	/brief : parse the header for each request and update the relevant information of the response
 *  /param : httpInfo ptr to headerInfo_t structure instance, its container need to get filled
 *  /param : dataBuff, ptr to the data buffer
 *  /param : size, buffer size
 *  return OPER_STATE_SUCCESS for success, else state error code
 */
int parseHttpRespHeader(char *dataBuff, int size, httpHeaderInfo_t* httpInfo);

/*
 * /brief prepare and send http command to get file
 * /param: chunk start, start byte of chunk
 * /param: totalBytes, total bytes from chunk start
 * /return: OPER_STATE_SUCCESS, else state error code	
 */
int generateHttpChunkRequest(unsigned int chunkStart, unsigned int totalBytes);

/*
 *	/brief wrapper function to read the requested http data over socket
 *	/param: dataBuff, container to fetch data
 *  /param: size, size of the data to read
 *	/param: bytesReceived, container to fetch the actual bytes read
 *	/return: OPER_STATE_SUCCESS for success, else state error code
 */
int httpRead(char *dataBuff, int size, int* bytesReceived);

/*
 *	/brief deinitialize the http utils module, eg. releasing socket
 */
void deInitHttpUtils(void);

#endif /* HTTPUTILS_H_ */