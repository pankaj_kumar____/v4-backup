/*
 * externalFlash.h
 *
 * Created: 3/28/2020 11:40:34 PM
 *  Author: Kartik.Gupta
 */ 


#ifndef EXTERNALFLASH_H_
#define EXTERNALFLASH_H_

/* Memory Used
Device Part - AT25DFX_321
AT25DFX_DEV_ID                     0x1840EF//(WinBond) //0x154001(Cypress)
#define AT25DFX_SIZE               (16 * 1024 * 1024)
#define AT25DFX_BLOCK_SIZE         (64 * 1024)
*/


#define EXTERNAL_FLASH_START_ADDR		0xE00000	/* address where our backup application presents*/
#define EXTERNAL_FLASH_FINISH_ADDR		0x1000000	/* total 16MB*/

/* size of internal flash available for application in bytes*/
#define EXTERNAL_FLASH_APP_SIZE_MAX		INTERNAL_FLASH_APP_SIZE_MAX

#define EXTERNAL_FLASH_MAX_APP_SPACE	512			/* maximum space reserved for each app regions in KB*/
#define EXTERNAL_FLASH_ERASE_BLOCK_SIZE	4			/* block size to erase in KB*/

/* to consider chunks in multiple of 4K to perform write in case of resumable download*/
#define EXTERNAL_FLASH_CHUNK_OFFSET		(EXTERNAL_FLASH_ERASE_BLOCK_SIZE * 1024 / MAX_CHUNK_SIZE)

#define	EXTERNAL_FLASH_BACKUP_APP_START_ADDR	EXTERNAL_FLASH_START_ADDR	/* older backup application*/
#define EXTERNAL_FLASH_NEW_APP_START_ADDR		0xE80000		/* where we are storing updated image*/

#define EXTERNAL_FLASH_SYS_BOOT_CONFIG_START_ADDR	0xF00000	/* boot config / fw info data address*/
#define EXTERNAL_FLASH_DNLD_BOOT_CONFIG_START_ADDR	0xF01000	/* config for downloading firmware, if any*/

#define UNUSED_SPACE_ADDR						0xF02000	/* free space start address*/
#define EXTERNAL_FLASH_SIZE_USED				((512 * 2) + (4 * 2) + 1016)	/* in KB(APP + BOOTCONFIG + SPARE)*/ 

/* external boot configurations type*/
typedef enum {
	CONFIG_TYPE_NA = -1,
	CONFIG_TYPE_SYSTEM,
	CONFIG_TYPE_DOWNLOAD,
	CONFIG_TYPE_MAX,
} configType_e;

/* structure having details of our firmware binary file*/
typedef struct {
	char firmwareVersion[4];
	char firmwareMd5Hash[16];
	unsigned int firmwareSize;
} firmwareHeader_s;

/* structure having details of last firmware upgrade*/
typedef struct {
	firmwareHeader_s lastFwUpdateHeader;
	unsigned int lastSuccessfullyWrittenChunk;	/*0 if file to get from starting chunk*/
} lastFwUpdateInfo_s;

/* structure having configurations related to system firmware*/
typedef struct {
	unsigned char isNewFirmwareAvaliable;		/*true if available*/
	unsigned char isRollBackRequested;			/*true if roll back to previous version requested*/
	unsigned char previousBackupStatus;			/*1 if backup of internal already taken, 0 otherwise*/
	firmwareHeader_s currentFirmwareHeader;
	firmwareHeader_s newFirmwareHeader;
} exSysBootConfig_s;

/* structure having configurations related to downloading firmware*/
typedef struct {
	lastFwUpdateInfo_s lastFwUpdateInfo;
} exDnldBootConfig_s;

/*brief initialize the external flash memory
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int externalFlashInit(void);

/*brief write data buffer to external flash memory (at application start address)
 *@param - dataBuff, ptr to the buffer data 
			NOTE:	to avoid errorneous write, max buff size is restricted to  MAX_CHUNK_SIZE
 *@param - maximumDataSize, firmware file size to write
 *@param - operStart, flag to indicate to begin writing from start address, if it is set
 *@param - isBackupArea, flag to indicate whether to perform operation on backup area, if it is set
 *@param - resumableChunkNo, chunk number from where to resume the operation
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int externalFlashAppWrite(void *dataBuff, unsigned int maximumDataSize, \
										char operStart, char isBackupArea, int resumableChunkNo);

/*brief read data buffer from external flash memory (at application start address)
 *@param - dataBuff, ptr to container buffer data
 *@param - maximumDataSize, firmware file size to read
 *@param - operStart, flag to indicate to begin reading from start address, if it is set
 *@param - isBackupArea, flag to indicate whether to perform operation on backup area, if it is set
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int externalFlashAppRead(void *dataBuff, unsigned int maximumDataSize, char operStart, char isBackupArea);

/*brief to get the boot configurations stored in external flash
 *@param - config, ptr to the stored config
 *@size - size of configuration
 *@ctype - configuration type
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int getExternalBootConfig(void* config, int size, configType_e ctype);

/*brief to set the boot configurations to the external flash
 *@param - config, config buffer pointer
	NOTE: to prevent erroneous write, read present config and then set final config
 *@param - size, size of configuration
 *@param - ctype, config type
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int setExternalBootConfig(void *config, int size, configType_e ctype);

/* brief update the flag for rollback
 *@param data, 1 to set, 0 to clear
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int updateRollbackFlag(unsigned char data);

/* brief update the flag for new firmware availability 
 *@param, data, 1 to set, 0 to clear
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int updateFirmwareAvailableFlag(unsigned char data);

/* brief get the available value of rollback flag
 *@param, data, ptr to the flag value container
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int getRollbackFlag(unsigned char *data);

/* brief get the available value of firmware availability flag
 *@param, data, ptr to the flag value container
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int getFirmwareAvailableFlag(unsigned char *data);

/* brief get the firmware file information
 *@param, *fhInfo, ptr to file header info 
 *@param, isCurrentFw, flag to tell whether its for currently running fw(present in internal flash)
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int getFirmwareHeader(firmwareHeader_s *fhInfo, char isCurrentFw);

/* brief set the firmware file information
 *@param, *fhInfo, ptr to file header info 
 *@param, isCurrentFw, flag to tell whether its  for currently running fw(present in internal flash)
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int setFirmwareHeader(firmwareHeader_s *fhInfo, char isCurrentFw);

/* brief get the last update firmware info, fw header and chunk no written
 *@param, lastFwInfo, ptr to the firmware info 
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int getLastFwUpdateInfo(lastFwUpdateInfo_s *lastFwInfo);

/* brief set the last updated firmware info used for resuming firmware download
 *@param, lastFwInfo, ptr to fw info
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int setLastFwUpdateInfo(lastFwUpdateInfo_s *lastFwInfo);

/* brief update the flag for last backup taken
 *@param, data, 1 to set, 0 to clear
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int updatePreviousBackupStatus(unsigned char data);

/* brief get the available value of backup status
 *@param, data, ptr to the flag value container
 *@return operation state OPER_STATE_SUCCESS for success, else state code
 */
int getPreviousBackupStatus(unsigned char *data);

#endif /* EXTERNALFLASH_H_ */