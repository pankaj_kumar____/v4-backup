#include "./storage_utils.h"
#include "./spi_utils.h"

#include <string.h>

#include <asf.h>
#include "conf_board.h"

#include "../../../common/instamsg/driver/include/log.h"
#include "../../../common/instamsg/driver/include/globals.h"
#include "../../../common/instamsg/driver/include/misc.h"

#define INVALID_SIZE														0
#define OPERATING_SECTOR_SIZE										       1024

#define MEMORY_FORMATTED_IDENTIFIER                                         'n'

volatile unsigned char persistent_storage_initialized;
static unsigned char tempBuffer[OPERATING_SECTOR_SIZE];


/*
 *
 * ALL DEVICE-SPECIFIC CODE GOES HERE NOW ...
 *
 */


/**********************************************************************************************************
 **************************   BOUNDARY WALL BETWEEN LOWER AND MIDDLE LAYER ********************************
 *********************************************************************************************************/

static void write_record_at_address(unsigned int address, unsigned char *buffer, int len)
{
	at25_status_t status = AT25_ERROR;
	external_wathdog_init();
	switch_spi(FLASH_PERIPHERAL, 0);
	
	/* Erase the block before write */
	if(0x00==address%4096)
	{
		status = at25dfx_erase_block(address);
		if (status != AT25_SUCCESS)
		{
			sg_sprintf(LOG_GLOBAL_BUFFER, "Could not erase flash-memory-block, resetting device");
			error_log(LOG_GLOBAL_BUFFER);
		
			resetDevice();	
		}
	}
	delay_us(200);
	/* Write the data to the SerialFlash */
	status = at25dfx_write(buffer, len, address);
	external_watchdog_feed();
	if (status != AT25_SUCCESS)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not write flash-memory-block, resetting device");
		error_log(LOG_GLOBAL_BUFFER);
		
		resetDevice();	
	}
}


static void read_record_at_address(unsigned int address, unsigned char *buffer, int len)
{		
	at25_status_t status = AT25_ERROR;
	external_wathdog_init();
	switch_spi(FLASH_PERIPHERAL, 0);
	
	status = at25dfx_read(buffer, len, address);
	external_watchdog_feed();
	if (status != AT25_SUCCESS)
	{
		sg_sprintf(LOG_GLOBAL_BUFFER, "Could not read flash-memory-block, resetting device");
		error_log(LOG_GLOBAL_BUFFER);
					
		resetDevice();
	}
}


static void write_record(int recordNumber, unsigned char *buffer, unsigned short len)
{
    unsigned char *tmp = tempBuffer;

    if(len >= (OPERATING_SECTOR_SIZE - 10))
    {
        sg_sprintf(LOG_GLOBAL_BUFFER, "Maximum allowed size exceeded");
        error_log(LOG_GLOBAL_BUFFER);

        resetDevice();
    }

    /*
     * Write the length of the record in two bytes itself at the start of record.
     */
    {
        int i = 2, j = 0;
        unsigned char *ptr = (unsigned char*) &len;

        tmp[0] = *ptr;
        tmp[1] = *(ptr + 1);

        for(i = 2, j = 0; j < len; i++, j++)
        {
            tmp[i] = buffer[j];
        }

        write_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, tmp, len + 2);
    }
}


static unsigned short read_record(int recordNumber, unsigned char *buffer)
{
    unsigned char *tmp = tempBuffer;
    unsigned short len = 0;
	
    /*
     * Read the length of the record, stored in the first two bytes of the record itself.
     */
    {
        unsigned char *ptr = (unsigned char*) &len;
		
        read_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, tmp, 2);
        *ptr        = tmp[0];
        *(ptr + 1)  = tmp[1];
		
		if(len > 1024) {
			len = 1024;	
		}
		
		read_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, tmp, len + 2);
		memcpy(buffer, tmp + 2, len);
    }
	
    return len;
}


static void read_mbr(int recordNumber, unsigned char *buffer)
{
    unsigned char *tmp = tempBuffer;

    read_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, tmp, 7);
	memcpy(buffer, tmp + 2, 5);
}


static unsigned short getRecordSize(unsigned short recordNumber)
{
    unsigned short c;
    unsigned char *ptr = (unsigned char*) &c;
	
    read_record_at_address(recordNumber * MAX_RECORD_SIZE_BYTES, (unsigned char*) LOG_GLOBAL_BUFFER, 2);
	
    *ptr        = ((unsigned char*) LOG_GLOBAL_BUFFER)[0];
    *(ptr + 1) 	= ((unsigned char*) LOG_GLOBAL_BUFFER)[1];
	
	memset(LOG_GLOBAL_BUFFER, 0, sizeof(LOG_GLOBAL_BUFFER));
    return c;
}

void clear_flash_memory (void)
{
	int i = 0x00;
	
	#if 1	/* chip erased complete for testing*/
	delay_s(1);
	sg_sprintf(LOG_GLOBAL_BUFFER, "Erasing Flash Memory...");
	info_log(LOG_GLOBAL_BUFFER);
	at25dfx_erase_chip();
	sg_sprintf(LOG_GLOBAL_BUFFER, "Erasing Flash Memory Completed!!!");
	info_log(LOG_GLOBAL_BUFFER);
	delay_s(1);
	#endif
}

void write_mbr(unsigned short readNumber, unsigned short writeNumber)
{
    sg_sprintf(LOG_GLOBAL_BUFFER, "Writing readNumber [%u], writeNumber [%u]", readNumber, writeNumber);
    debug_log(LOG_GLOBAL_BUFFER);
	
	unsigned char small[20] = {0};
		 
    {
        unsigned char *ptr = (unsigned char*) &readNumber;

        small[0] = MEMORY_FORMATTED_IDENTIFIER;

        small[1] = *ptr;
        small[2] = *(ptr + 1);

        ptr = (unsigned char*) &writeNumber;
        small[3] = *ptr;
        small[4] = *(ptr + 1);
		
        write_record(MBR_INDEX, small, 5);
    }
	
	#if 0
	sg_sprintf(LOG_GLOBAL_BUFFER, "small[0]: %x", small[0]);
	info_log(LOG_GLOBAL_BUFFER);
	
	char buffer[10] = {0};
	read_record(MBR_INDEX, buffer);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "buff[0]: %x", buffer[0]);
	info_log(LOG_GLOBAL_BUFFER);
	#endif
}


void read_next_read_and_write_record_number_values(unsigned short *readNumber, unsigned short *writeNumber)
{
    unsigned char *ptr;
    unsigned char small[10] = {0};

    read_record(MBR_INDEX, small);

    ptr = (unsigned char*) readNumber;
    *ptr        = small[1];
    *(ptr + 1)  = small[2];

    ptr = (unsigned char*) writeNumber;
    *ptr        = small[3];
    *(ptr + 1)  = small[4];

    sg_sprintf(LOG_GLOBAL_BUFFER, "Reading readNumber [%u], writeNumber [%u]", *readNumber, *writeNumber);
    debug_log(LOG_GLOBAL_BUFFER);
}


int is_record_valid(unsigned short recordNumber)
{
    if(getRecordSize(recordNumber) == INVALID_SIZE)
    {
		sg_sprintf(LOG_GLOBAL_BUFFER, "Record Number [%u] Failed !!!", recordNumber);
		debug_log(LOG_GLOBAL_BUFFER);
		
        return FAILURE;
    }
	sg_sprintf(LOG_GLOBAL_BUFFER, "Record Number [%u] Passed !!!", recordNumber);
	debug_log(LOG_GLOBAL_BUFFER);
	
    return SUCCESS;
}


void mark_record_invalid(unsigned short recordNumber)
{
    unsigned char tmp[1] = {0};
    write_record(recordNumber, tmp, INVALID_SIZE);
}


int read_record_from_persistent_storage(unsigned short recordNumber, char *buffer, int maxBufferLength, const char *recordType)
{
    read_record(recordNumber, (unsigned char*)buffer);
    return SUCCESS;
}

void display_config_val( void )
{
	int i=0;
	unsigned short len=0;
	 
	
	for( i=0;i<64;i++)
	{
	   len = getRecordSize(i);
	   if(len>1024)
	   {
		   len =1024;
	   }
	   sg_sprintf(LOG_GLOBAL_BUFFER, "Record Length is <%d>, Displaying Record at Index <%d>:",len, i+1);
	   info_log(LOG_GLOBAL_BUFFER); 

       read_record_at_address((i * MAX_RECORD_SIZE_BYTES)+2, GLOBAL_BUFFER, len);
	   error_log(GLOBAL_BUFFER);
	}
}

int write_record_on_persistent_storage(unsigned short recordNumber, const char *record, const char *recordType)
{
    write_record(recordNumber, (unsigned char *)record, strlen(record));	
    return SUCCESS;
}


void init_persistent_storage()
{
	 clear_flash_memory();
}
