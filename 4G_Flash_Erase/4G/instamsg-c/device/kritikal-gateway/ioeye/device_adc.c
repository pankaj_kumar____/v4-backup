#include "device_defines.h"

#include "spi_master.h"
#include "ioport.h"

#include "./device_adc.h"

#include "../common/spi_utils.h"

#include "../../../common/instamsg/driver/include/log.h"


static maxAdc_status_t maxAdc_write(Spi *p_spi, uint8_t *data, uint16_t size);
static void Max11253_initialize(uint8_t channelNo);
static maxAdc_status_t maxAdc_read(Spi *p_spi, uint16_t *spi_adc_data, uint8_t channelNo);

uint8_t cmd_buffer[4] = {0};
status_code_t spi_stat;

struct spi_device MAXADC_DEVICE1 = {
	.id = MAXAdc_CS
};


#define RDYB_GPIO           (PIO_PA2_IDX)
#define RDYB_FLAGS          IOPORT_MODE_OPEN_DRAIN  
#define RDYB_GPIO_SENSE     (IOPORT_SENSE_FALLING)

void read_adc(uint8_t channel, short *value)
{
	unsigned short tempCurrent = 0;
	unsigned int runningTotal = 0;
	
	float finalValue;
	
	int i = 0;
	
	switch_spi(ADC_PERIPHERAL, 0);
	
	/* Read and ignore some samples */
	for(i = 0; i < 10; i++)
	{
		Max11253_initialize(channel);
		while(ioport_get_pin_level(RDYB_GPIO) != 0);				// wait for RDYB for complete conversion
		
		maxAdc_read(MAXAdc_SPI_MODULE, &tempCurrent, channel);
	}
	
	for(i = 0; i < MAX_SAMPLE; i++)
	{
		Max11253_initialize(channel);
		while(ioport_get_pin_level(RDYB_GPIO) != 0);				// wait for RDYB for complete conversion
		
		tempCurrent = 0;
		maxAdc_read(MAXAdc_SPI_MODULE, &tempCurrent, channel);
		runningTotal = runningTotal + tempCurrent;
	}
	
	if(channel < channel4)
	{
		finalValue = (((runningTotal / MAX_SAMPLE) * Current_SLOPE) + Current_INTERCEPT) / MicroAMP_TO_MilliAMP;
		if(finalValue < THRESHOLD_CURRENT)
		{
			finalValue = 0;
		}
	}
	else
	{
		finalValue = (((runningTotal / MAX_SAMPLE) * Vol_SLOPE) + Vol_INTERCEPT) / MilliVolt_TO_VOLT;
		if(finalValue >= THRESHOLD_VOLT)									// check for threshold voltage and on/off the led
		{
			switch(channel)
			{
				case channel4:
					ioport_set_pin_level(LED_VDC1_GPIO, LED_VDC1_ACTIVE_LEVEL);		//(LED_VDC2_GPIO, LED_VDC2_ACTIVE_LEVEL)// for showing reasonable voltage presence
					break;
				case channel5:
					ioport_set_pin_level(LED_VDC2_GPIO, LED_VDC2_ACTIVE_LEVEL);		//(LED_VDC1_GPIO, LED_VDC1_ACTIVE_LEVEL)// for showing reasonable voltage presence
					break;
				default:
					break;
			}
		}
		else
		{
			switch(channel)
			{
				case channel4:
					ioport_set_pin_level(LED_VDC2_GPIO, LED_VDC2_INACTIVE_LEVEL);		// for showing reasonable voltage presence
					break;
				case channel5:
					ioport_set_pin_level(LED_VDC1_GPIO, LED_VDC1_INACTIVE_LEVEL);		// for showing reasonable voltage presence
					break;
				default:
					break;
			}
		}
	}
	
	
#if 0
	sg_sprintf(LOG_GLOBAL_BUFFER, "Before ADC-Value for channel [%u] = [%u]", channel, *value);
	info_log(LOG_GLOBAL_BUFFER);
#endif
		
	*value = (finalValue * 1000);
	
	sg_sprintf(LOG_GLOBAL_BUFFER, "ADC-Value for channel [%u] = [%u]", channel, *value);
	info_log(LOG_GLOBAL_BUFFER);
}


static void write_sequence_register(uint8_t channelNo)
{
	cmd_buffer[0] = SEQ_REG_CMD;

	if(channelNo == channel0)
	{
		cmd_buffer[1] = SEQ_REG_CH0_DATA;
	}
	else if(channelNo == channel1)
	{
		cmd_buffer[1] = SEQ_REG_CH1_DATA;
	}
	else if(channelNo == channel2)
	{
		cmd_buffer[1] = SEQ_REG_CH2_DATA;
	}
	else if(channelNo == channel3)
	{
		cmd_buffer[1] = SEQ_REG_CH3_DATA;
	}
	else if(channelNo == channel4)
	{
		cmd_buffer[1] = SEQ_REG_CH4_DATA;
	}	
	else if(channelNo == channel5)
	{
		cmd_buffer[1] = SEQ_REG_CH5_DATA;
	}

	spi_stat = maxAdc_write(MAXAdc_SPI_MODULE, cmd_buffer, 2);
	if (spi_stat != STATUS_OK)
	{
		while(1);
	}
}


static void write_delay_register(void)
{
	cmd_buffer[0] = DELAY_REG_CMD;
	cmd_buffer[1] = GPO_DELAY;
	cmd_buffer[2] = MUX_DELAY;
	
	spi_stat = maxAdc_write(MAXAdc_SPI_MODULE, cmd_buffer, 3);
	if (spi_stat != STATUS_OK)
	{
		while(1);
	}
}


static void write_control_register(uint8_t RegNo)
{
	if(RegNo == CRTL_REG1)
	{
		cmd_buffer[0] = CTRL1_REG_CMD;
		cmd_buffer[1] = CTRL1_REG_DATA;					//unipolar, offset binary format, continuous conversion mode
	}

	else if(RegNo == CRTL_REG2)
	{
		cmd_buffer[0] = CTRL2_REG_CMD;
		cmd_buffer[1] = CTRL2_REG_DATA;				// INT_LDO_EN,PGA_EN=0,gain=0---Not used
	}
	
	else if(RegNo == CRTL_REG3)						// not used
	{
		cmd_buffer[0] = CTRL3_REG_CMD;
		cmd_buffer[1] = CTRL3_REG_DATA;
	}
	
	spi_stat = maxAdc_write(MAXAdc_SPI_MODULE,cmd_buffer,2);
	if (spi_stat != STATUS_OK)
	{
		while(1);
	}
}


static void start_conversion(void)
{
	cmd_buffer[0]= START_CONVERSION;        //sequencer mode, data rate 6,400 sps;
	spi_stat = maxAdc_write(MAXAdc_SPI_MODULE,cmd_buffer,1);
	if (spi_stat != STATUS_OK)
	{
		while(1);
	}
}


static void Max11253_initialize(uint8_t channelNo)
{
	if(channelNo == channel0)
		write_sequence_register(channel0);
	else if(channelNo == channel1)
		write_sequence_register(channel1);
	else if(channelNo == channel2)
		write_sequence_register(channel2);
	else if(channelNo == channel3)
		write_sequence_register(channel3);
	else if(channelNo == channel4)
		write_sequence_register(channel4);
	else if(channelNo == channel5)
		write_sequence_register(channel5);

	write_delay_register();
	write_control_register(CRTL_REG1);	
	start_conversion();	
}


static maxAdc_status_t maxAdc_write(Spi *p_spi, uint8_t *data, uint16_t size)
{
	/* Enable Chip select corresponding to the SerialADC */
	spi_select_device(p_spi, &MAXADC_DEVICE1);

	spi_stat = spi_write_packet(p_spi,data,size);

	if (spi_stat != STATUS_OK)
	{
		return spi_stat;
	}

	/* Disable Chip select corresponding to the SerialADC */
	spi_deselect_device(p_spi, &MAXADC_DEVICE1);
	return MAX_SUCCESS;
}


#define DATA_REG_CMD(channel)

static maxAdc_status_t maxAdc_read(Spi *p_spi, uint16_t *spi_adc_data, uint8_t channelNo)
{
	uint8_t data[4];
	if(channelNo == channel0)
	{
		cmd_buffer[0] = DATA_REG0_CMD;
	}
	else if (channelNo == channel1)
	{
		cmd_buffer[0] = DATA_REG1_CMD;
	}
	else if (channelNo == channel2)
	{
		cmd_buffer[0] = DATA_REG2_CMD;
	}
	else if (channelNo == channel3)
	{
		cmd_buffer[0] = DATA_REG3_CMD;
	}
	else if (channelNo == channel4)
	{
		cmd_buffer[0] = DATA_REG4_CMD;
	}
	else if (channelNo == channel5)
	{
		cmd_buffer[0] = DATA_REG5_CMD;
	}

	cmd_buffer[1] = DUMMY_BYTE;
	cmd_buffer[2] = DUMMY_BYTE;
		
		
	/* Enable Chip select corresponding to the SerialADC */
	spi_select_device(p_spi, &MAXADC_DEVICE1);

	spi_stat = spi_transceive_packet(p_spi,cmd_buffer,data,3);
	if (spi_stat != STATUS_OK)
	{
		return spi_stat;
	}

	*spi_adc_data= ((data[1] << 8) + data[2]);

	/* Disable Chip select corresponding to the SerialADC */
	spi_deselect_device(p_spi, &MAXADC_DEVICE1);
	return MAX_SUCCESS;
}


static void maxim_delay(uint32_t un_delay)
{
	uint32_t un_count;
	for(un_count = 0; un_count < un_delay; un_count++);
}




static void maxim_adc_reset(void)
{
	ioport_set_pin_level(RSTB_GPIO, RSTB_ACTIVE_LEVEL);	    //Set RSTB pin low
	maxim_delay(100);
	ioport_set_pin_level(RSTB_GPIO, RSTB_INACTIVE_LEVEL);	//Set RSTB pin high
	maxim_delay(500);
}


static void maxim_adc_self_cal(uint8_t channel)
{
	uint8_t uch_command;
	
	cmd_buffer[0] = CTRL1_REG_CMD;
	cmd_buffer[1] = CTRL1_REG_SELF_CALIB_DATA;         
	maxAdc_write(MAXAdc_SPI_MODULE,cmd_buffer,2);

	cmd_buffer[0] = CTRL2_REG_CMD;
	cmd_buffer[1] = CTRL2_REG_SELF_CALIB_DATA;        // INT_LDO_EN,PGA_EN=0,gain=0
	maxAdc_write(MAXAdc_SPI_MODULE,cmd_buffer,2);

	cmd_buffer[0] = CTRL3_REG_CMD;
	cmd_buffer[1] = CTRL3_REG_SELF_CALIB_DATA;          
	maxAdc_write(MAXAdc_SPI_MODULE,cmd_buffer,2);

	cmd_buffer[0] = GPIO_CTRL_CMD;
	cmd_buffer[1] = GPIO_CTRL_DATA;          
	maxAdc_write(MAXAdc_SPI_MODULE,cmd_buffer,2);

	if(channel == channel0)
	{
		cmd_buffer[0] = SEQ_REG_CMD;
		cmd_buffer[1] = SEQ_REG_SELF_CALIB_CH0_DATA;   
	}
	else if(channel == channel1)
	{
		cmd_buffer[0] = SEQ_REG_CMD;
		cmd_buffer[1] = SEQ_REG_SELF_CALIB_CH1_DATA;
	}
	else if(channel == channel2)
	{
		cmd_buffer[0] = SEQ_REG_CMD;
		cmd_buffer[1] = SEQ_REG_SELF_CALIB_CH2_DATA;
	}
	else if(channel == channel3)
	{
		cmd_buffer[0] = SEQ_REG_CMD;
		cmd_buffer[1] = SEQ_REG_SELF_CALIB_CH3_DATA;
	}
	else if(channel == channel4)
	{
		cmd_buffer[0] = SEQ_REG_CMD;
		cmd_buffer[1] = SEQ_REG_SELF_CALIB_CH4_DATA;
	}
	else if(channel == channel5)
	{
		cmd_buffer[0] = SEQ_REG_CMD;
		cmd_buffer[1] = SEQ_REG_SELF_CALIB_CH5_DATA;
	}
	
	maxAdc_write(MAXAdc_SPI_MODULE, cmd_buffer, 2);

	uch_command = SELF_CALIB_CMD;									// Start self calibration	
	spi_select_device(MAXAdc_SPI_MODULE, &MAXADC_DEVICE1);			// Set CS pin low
	spi_write_packet(MAXAdc_SPI_MODULE,&uch_command,1);

	/* Disable Chip select corresponding to the SerialADC */
	spi_deselect_device(MAXAdc_SPI_MODULE, &MAXADC_DEVICE1);		// Set CS pin high
}


static void maxim_adc_power_down(void)
{
	uint8_t uch_command = POWER_DOWN_CMD;

	/* Enable Chip select corresponding to the SerialADC */
	spi_select_device(MAXAdc_SPI_MODULE, &MAXADC_DEVICE1);       //Set CS pin low

	spi_write_packet(MAXAdc_SPI_MODULE,&uch_command,1);

	/* Disable Chip select corresponding to the SerialADC */
	spi_deselect_device(MAXAdc_SPI_MODULE, &MAXADC_DEVICE1);		//Set CS pin high
}


void adc_spi_init(void)
{
	if(1)
	{
		int i = 0;
		
		spi_master_init(MAXAdc_SPI_MODULE);
		spi_master_setup_device(MAXAdc_SPI_MODULE, &MAXADC_DEVICE1, SPI_MODE_3, MaxAdc_SPI_MASTER_SPEED, 0);
		spi_enable(MAXAdc_SPI_MODULE);
		
		maxim_delay(50);
		maxim_adc_reset();
		
		for(i = 0; i < 6; i++)
		{
			maxim_adc_self_cal(i);								// perform self calibration one by one for all 6 channels
		}
		
		maxim_adc_power_down();
	}
}