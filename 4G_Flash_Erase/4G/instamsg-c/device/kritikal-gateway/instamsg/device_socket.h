#ifndef INSTAMSG_SOCKET
#define INSTAMSG_SOCKET

#include "../driver/include/globals.h"

#include <string.h>

#include "lwip/err.h"
#include "lwip/ip_addr.h"

#if SSL_ENABLED == 1
#include "../driver/include/wolfssl/internal.h"
#endif

typedef struct SG_Socket SG_Socket;

struct SG_Socket
{
    /* ============================= THIS SECTION MUST NOT BE TEMPERED ==================================== */
    char host[100];
    int port;
    char *type;

    unsigned char socketCorrupted;

#if SSL_ENABLED == 1
	WOLFSSL* ssl;
#endif

#if GSM_INTERFACE_ENABLED == 1
    char gsmApn[MAX_GSM_PROVISION_PARAM_SIZE];
    char gsmUser[MAX_GSM_PROVISION_PARAM_SIZE];
    char gsmPass[MAX_GSM_PROVISION_PARAM_SIZE];
    char gsmPin[MAX_GSM_PROVISION_PARAM_SIZE];
    char provPin[MAX_GSM_PROVISION_PARAM_SIZE];
	char gsmMode[MAX_GSM_PROVISION_PARAM_SIZE];
#endif

    int (*read) (SG_Socket *socket, unsigned char* buffer, int len, unsigned char guaranteed);
    int (*write)(SG_Socket *socket, unsigned char* buffer, int len);
    /* ============================= THIS SECTION MUST NOT BE TEMPERED ==================================== */



    /* ============================= ANY EXTRA FIELDS GO HERE ============================================= */
	int socket;
	
	struct netconn *netConnSocket;
	err_t internalStatus;
	struct ip_addr ipAddress;

	unsigned char *transitionBuffer;
	int transitionBufferSize;
	
	int readerIndex;
	int writerIndex;
	int transitionBufferTotalDataAvailable;
	
	int bytes_received;
    /* ============================= ANY EXTRA FIELDS GO HERE ============================================= */
};

#endif
