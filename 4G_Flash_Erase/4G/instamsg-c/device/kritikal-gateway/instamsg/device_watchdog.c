/*******************************************************************************
 * Contributors:
 *
 *      Ajay Garg <ajay.garg@sensegrow.com>
 *
 *******************************************************************************/

#include "../driver/include/watchdog.h"

volatile unsigned char watchdog;
volatile int watchdog_time;
volatile unsigned char immediate_reboot;


/*
 * This method initializes the watchdog-timer.
 */
void init_watchdog()
{
}


/*
 * This method resets the watchdog-timer.
 *
 * n         : Number of seconds the watchdog should wait for.
 *
 * immediate : 0/1
 *             Denotes whether the device should be reset/rebooted immediately.
 *
 */
void do_watchdog_reset_and_enable(int n, unsigned char immediate)
{
	watchdog = 1;
	watchdog_time = n;
	immediate_reboot = immediate;
}


/*
 * This method disables the watchdog-timer.
 */
void do_watchdog_disable()
{
	watchdog = 0;
}
