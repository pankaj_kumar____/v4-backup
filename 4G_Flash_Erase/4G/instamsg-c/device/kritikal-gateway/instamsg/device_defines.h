#ifndef DEVICE_DEFINES
#define DEVICE_DEFINES

/*
 * By default.. we use the in-built functionality, which uses variable-argument lists.
 *
 * However, this implementation has shown to give problems for Harvard/RISC architectures, so such devices will need
 * to provide the reference to an implementation shipped with their compiler-environment.
 */

#define DEVICE_VERSION											"17.7.0"

#include "portable.h"

#include <stdio.h>
#define sg_sprintf                                              sprintf

#include "../driver/include/sg_mem.h"

#define USE_DEFAULT_MALLOC                                      0
#if USE_DEFAULT_MALLOC == 1
#define sg_malloc                                               DEFAULT_MALLOC
#define sg_free                                                 DEFAULT_FREE
#else
#define sg_malloc												pvPortMalloc
#define sg_free													vPortFree
#endif

#define PROSTR

#define MAX_BUFFER_SIZE                                         1100
#define READ_BUFFER_SIZE                                        1100

#define DATA_LOGGING_ENABLED                                    1
#define MAX_DATA_LOGGER_SIZE_BYTES								10485760               // 10 MB Size */ // Prev Size 4000000	// 4 MB data-logging allowed by default changed from 2

#define SERIAL_COMMANDS_BUFFER_SIZE                             600

#define MAX_PORTS_ALLOWED                                       3

#define MODBUS_INTERFACE_ENABLED								1

#define FILE_LOGGING_ENABLED                                    0
#define FILE_SYSTEM_ENABLED                                     0
#define GSM_INTERFACE_ENABLED                                   1
#define SSL_ENABLED                                             1
#define MAX_DEBUG_LOGGER_FILE_SIZE_BYTES                        100000
#define SOCKET_SSL_ENABLED                                      0

#if SSL_ENABLED == 1
#define SSL_WIRE_BUFFER_SIZE									MAX_BUFFER_SIZE
#define DEFAULT_SSL_ENABLED										"0"
#define USE_DEFAULT_SEEDER                                      0
#endif

#define MAX_TCP_PACKET_SIZE										2500


#define GPS_TIME_SYNC_PRESENT                                   0
#if GPS_TIME_SYNC_PRESENT == 1
#define GPS_TIME_SYNC_SENTENCE_TYPE								"+CGPSINFO:"
#endif
#define GSM_TIME_SYNC_PRESENT                                   0

#define ACQUIRE_THREAD_MUTEX
#define RELEASE_THREAD_MUTEX

#define ACQUIRE_LOG_MUTEX
#define RELEASE_LOG_MUTEX

#define MAX_MESSAGE_HANDLERS                                    5
#define AT_INTERFACE_ENABLED                                    1

#define DEFAULT_COMPULSORY_SOCKET_READ_AFTER_WRITE_TIMEOUT      "3"

#define SERIAL_RESPONSE_TIMEOUT_SECS                            15
#define ENSURE_EXPLICIT_TIME_SYNC                               0
#define MAX_CYCLES_TO_WAIT_FOR_PUBACK                           20

#define SEND_GPS_LOCATION                                       1
#if SEND_GPS_LOCATION == 1
#define GPS_LOCATION_SENTENCE_TYPE                              "+CGPSINFO:"
#endif

#define CRON_ENABLED                                            0
#if CRON_ENABLED == 1
#define DEFAULT_CRON_CONFIG                                     ""
#endif

#define SEND_GPIO_INFORMATION                                   0
#if SEND_GPIO_INFORMATION == 1
#define DEFAULT_GPIO_PINS_ORIENTATION                           "in,in,out,out"
#define GPIO_PIN_ORIENTATION_CONFIG_LENGTH                      200
#endif

#define HTTP_PROXY_ENABLED										0

#define COMPULSORY_NTP_SYNC                                     0
#define NTP_TIME_SYNC_PRESENT                                   1
#define DEFAULT_NTP_SERVER                                      "pool.ntp.org" //changed from "184.106.106.79"

#define INSTAMSG_HOST                                           "device.instamsg.io" // changed from "104.130.68.125"
#define DEFAULT_SIMCOM_5360_SOCKET_ENABLED                      1
#define SIM_SLOT_SWITCHING_ENABLED                              1

#define OK_DELIMITER											"\r\nOK\r\n"

#define ETHERNET_AS_FIRST_PRIORITY
#define ETHERNET_STATIC_PARAMS_ENABLED							1


#include "board.h"
#include "conf_board.h"
#include "ioport.h"

#define GPIO_uC_ISO_DI1											(PIO_PD26_IDX)
#define GPIO_uC_ISO_DI2											(PIO_PD25_IDX)
#define uC_DO1_GPIO												(PIO_PA16_IDX)
#define uC_DO2_GPIO												(PIO_PA23_IDX)

#define uC_DO_ACTIVE_LEVEL										(IOPORT_PIN_LEVEL_HIGH)
#define uC_DO_INACTIVE_LEVEL									(IOPORT_PIN_LEVEL_LOW)

#define WDT_IP_GPIO            (PIO_PA0_IDX)
#define WDT_IP_ACTIVE_LEVEL    (IOPORT_PIN_LEVEL_LOW)
#define WDT_IP_INACTIVE_LEVEL  (IOPORT_PIN_LEVEL_HIGH)

#define RSTB_GPIO				(PIO_PA17_IDX)
#define RSTB_ACTIVE_LEVEL		IOPORT_PIN_LEVEL_LOW
#define RSTB_INACTIVE_LEVEL		IOPORT_PIN_LEVEL_HIGH

#define uC_RE_DE_GPIO             PIN_USART1_RTS_IDX
#define uC_RE_DE_ACTIVE_LEVEL    (IOPORT_PIN_LEVEL_HIGH)
#define uC_RE_DE_INACTIVE_LEVEL  (IOPORT_PIN_LEVEL_LOW)

#define LED_VDC1_GPIO           (PIO_PD17_IDX)
#define LED_VDC1_INACTIVE_LEVEL IOPORT_PIN_LEVEL_LOW
#define LED_VDC1_ACTIVE_LEVEL	IOPORT_PIN_LEVEL_HIGH

#define LED_VDC2_GPIO           (PIO_PD18_IDX)
#define LED_VDC2_INACTIVE_LEVEL IOPORT_PIN_LEVEL_LOW
#define LED_VDC2_ACTIVE_LEVEL	IOPORT_PIN_LEVEL_HIGH

#define RDYB_GPIO				(PIO_PA2_IDX)
#define RDYB_FLAGS				IOPORT_MODE_OPEN_DRAIN
#define RDYB_GPIO_SENSE			(IOPORT_SENSE_FALLING)

//#define TC1        ((Tc     *)0x40094000U) /**< \brief (TC1       ) Base Address */
//#define TC2        ((Tc     *)0x40098000U) /**< \brief (TC2       ) Base Address */
#endif
