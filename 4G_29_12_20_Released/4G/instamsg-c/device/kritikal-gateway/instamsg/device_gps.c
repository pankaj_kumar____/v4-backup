/*******************************************************************************
 * Contributors:
 *
 *      Ajay Garg <ajay.garg@sensegrow.com>
 *
 *******************************************************************************/

#include "device_defines.h"

#include "../driver/include/gps.h"
#include "../driver/include/payload.h"
#include "../driver/include/at.h"

#include <string.h>

/*
 * This method fills the "buffer" with NMEA-blob.
 *
 * This method must return whenever any of the following holds true ::
 *
 *          * Either the whole of "buffer" fills upto "bufferLength", OR
 *          * "maxTime" has elapsed while waiting/filling-in NMEA-blob.
 */
#define GPS_COMMAND				"AT+CGPSINFO\r"

void fill_in_gps_nmea_blob_until_buffer_fills_or_time_expires(unsigned char *buffer, int bufferLength, int maxTime)
{
	while(1)
	{
		memset(buffer, 0, bufferLength);
		run_simple_at_command_and_get_output(GPS_COMMAND, strlen(GPS_COMMAND), (char*) buffer, bufferLength, OK_DELIMITER, 0, 1);
		
		if(strstr((char*) buffer, GPS_LOCATION_SENTENCE_TYPE) == ((char*) buffer))
		{
			strcat((char*) buffer, "*");
			break;
		}
	}
}


#if SEND_GPS_LOCATION == 1
/*
 * The device may decide how to send the gps-location-string to the server.
 * Or how to consume this string in general.
 */
void send_gps_location(const char *gps_location_string)
{
	ioeye_send_gps_data_to_server((char*) gps_location_string);
}
#endif

