#ifndef SPI_UTILS
#define SPI_UTILS

#define NO_PERIPHERAL											"NONE"
#define FLASH_PERIPHERAL										"FLASH"
#define ADC_PERIPHERAL											"ADC"


void switch_spi(const char* peripeheral, unsigned char force);
extern const char *chosenPeripheral;

#endif