/*
 * externalFlash.c
 *
 * Created: 3/28/2020 11:40:59 PM
 *  Author: Kartik.Gupta
 */ 

#include "externalFlash.h"
#include "internalFlash.h"
#include "common.h"

int externalFlashInit(void)
{
	unsigned int state;
	
	at25dfx_initialize();		/* initialize corresponding SPI*/
	
	/*--------Flash Check--------*/
	
	/* Set the SerialFlash active */
	at25dfx_set_mem_active(AT25DFX_MEM_ID);
	
	/* Check if the SerialFlash is valid */
	state = at25dfx_mem_check();
	if(state != AT25_SUCCESS) {
		info_log("failed to read device id\r\n");
		return OPER_STATE_READ_DEV_ID;
	}
	
	state = Global_unprotect();
	if(state != AT25_SUCCESS) {
		info_log("failed to unprotect device\r\n");
		return OPER_STATE_DEV_UNPROTECT;
	}
	
	return OPER_STATE_SUCCESS;
}

int externalFlashAppWrite(void *dataBuff, unsigned int maxDataSize, \
									char operStart, char isBackupArea, int resumableChunkNo)
{
	static unsigned int totalBytesWritten = 0;
	static unsigned int remainingBytesToWrite = 0;
	unsigned int state, flashWriteAddress, flashEraseAddress;
	int chunkSize;
	unsigned int noOfBytesToBeLeftUntouched;
	
	if(!dataBuff) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	if(maxDataSize > EXTERNAL_FLASH_APP_SIZE_MAX || maxDataSize == 0) {
		info_log("error: invalid file size\r\n");
		return OPER_STATE_INVALID_FILE_SIZE;
	}
	
	if(resumableChunkNo % EXTERNAL_FLASH_CHUNK_OFFSET != 0) {
		info_log("error: invalid chunk provided\r\n");
		return OPER_STATE_INVALID_CHUNK;	
	}
	
	noOfBytesToBeLeftUntouched = resumableChunkNo * MAX_CHUNK_SIZE;
	
	/* reset the write parameters if operation start set*/
	if(1 == operStart) {
		if(1 == isBackupArea) {
			flashEraseAddress = EXTERNAL_FLASH_BACKUP_APP_START_ADDR + noOfBytesToBeLeftUntouched;
		} else {
			flashEraseAddress = EXTERNAL_FLASH_NEW_APP_START_ADDR + noOfBytesToBeLeftUntouched;
		}
		unsigned int noOfBlocksToErase = (EXTERNAL_FLASH_MAX_APP_SPACE * 1024 - noOfBytesToBeLeftUntouched) / (EXTERNAL_FLASH_ERASE_BLOCK_SIZE * 1024);
		/* erase block 4K at a time, erase requested space prior to writing*/
		for(int i = 0; i < noOfBlocksToErase; i++) {
			state = at25dfx_erase_block(flashEraseAddress + EXTERNAL_FLASH_ERASE_BLOCK_SIZE * 1024 * i);
			if(state != AT25_SUCCESS) {
				info_log("error: external flash erase\r\n");
				return OPER_STATE_FLASH_ERASE;
			}
		}
		
		remainingBytesToWrite = maxDataSize - noOfBytesToBeLeftUntouched;
		totalBytesWritten = noOfBytesToBeLeftUntouched;
	}
	
	if(0 == operStart) {
		if(0 == totalBytesWritten) {
			info_log("error: flash operation not started\r\n");
			return OPER_STATE_INVALID_OPERATION;
		}
	}
	
	remainingBytesToWrite = maxDataSize - totalBytesWritten;
	if(remainingBytesToWrite >= MAX_CHUNK_SIZE) {
		chunkSize = MAX_CHUNK_SIZE;
	} else {
		chunkSize = remainingBytesToWrite;
	}
	
	if(0 == remainingBytesToWrite) {
		info_log("warning: flash already written\r\n");
		return OPER_STATE_INVALID_OPERATION;
	}
	
	if(1 == isBackupArea) {
		flashWriteAddress = EXTERNAL_FLASH_BACKUP_APP_START_ADDR + totalBytesWritten;
	} else {
		flashWriteAddress = EXTERNAL_FLASH_NEW_APP_START_ADDR + totalBytesWritten;
	}
	
	state = at25dfx_write(dataBuff, chunkSize, flashWriteAddress);
	if (state != AT25_SUCCESS) {
		info_log("error: external flash write\r\n");
		return OPER_STATE_FLASH_WRITE;
	} else {
		totalBytesWritten = totalBytesWritten + chunkSize;
		remainingBytesToWrite = maxDataSize - totalBytesWritten;
		return OPER_STATE_SUCCESS;
	}
}

int externalFlashAppRead(void *dataBuff, unsigned int maxDataSize, char operStart, char isBackupArea)
{
	static unsigned int totalBytesRead = 0;
	static unsigned int remainingBytesToRead = 0;
	unsigned int flashReadAddress;
	unsigned char *readPtr;
	int chunkSize;
	
	if(!dataBuff) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	if(maxDataSize > EXTERNAL_FLASH_APP_SIZE_MAX || maxDataSize == 0) {
		info_log("error: invalid file size\r\n");
		return OPER_STATE_INVALID_FILE_SIZE;
	}
	
	/* reset the read parameters if operation start set*/
	if(1 == operStart) {
		remainingBytesToRead = 0;
		totalBytesRead = 0;
	}
	
	if(0 == operStart) {
		if(0 == totalBytesRead) {
			info_log("error: flash operation not started\r\n");
			return OPER_STATE_INVALID_OPERATION;
		}
	}
	
	remainingBytesToRead = maxDataSize - totalBytesRead;
	if(remainingBytesToRead >= MAX_CHUNK_SIZE) {
		chunkSize = MAX_CHUNK_SIZE;
	} else {
		chunkSize = remainingBytesToRead;
	}
	
	if(0 == remainingBytesToRead) {
		info_log("warning: flash already read\r\n");
		return OPER_STATE_INVALID_OPERATION;
	}
	
	if(1 == operStart) {
		if(1 == isBackupArea) {
			flashReadAddress = EXTERNAL_FLASH_BACKUP_APP_START_ADDR;
		} else {
			flashReadAddress = EXTERNAL_FLASH_NEW_APP_START_ADDR;
		}
	} else {
		if(1 == isBackupArea) {
			flashReadAddress = EXTERNAL_FLASH_BACKUP_APP_START_ADDR + totalBytesRead;
		} else {
			flashReadAddress = EXTERNAL_FLASH_NEW_APP_START_ADDR + totalBytesRead;
		}
	}
	
	readPtr = (unsigned char*)flashReadAddress;
	
	if(AT25_SUCCESS != at25dfx_read(dataBuff, chunkSize, readPtr)) {
		info_log("error: external flash read error\r\n");
		return OPER_STATE_FLASH_READ;
	}
	
	totalBytesRead = totalBytesRead + chunkSize;
	remainingBytesToRead = maxDataSize - totalBytesRead;
			
	return OPER_STATE_SUCCESS;
}

int getExternalBootConfig(void *config, int size, configType_e ctype)
{
	int state;
	unsigned int address;
	
	if(!config) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	if(ctype == CONFIG_TYPE_SYSTEM) {
		address = EXTERNAL_FLASH_SYS_BOOT_CONFIG_START_ADDR;
	} else if(ctype == CONFIG_TYPE_DOWNLOAD) {
		address = EXTERNAL_FLASH_DNLD_BOOT_CONFIG_START_ADDR;	
	} else {
		info_log("error: invalid config type\r\n");
		return OPER_STATE_INVALID_CONFIG;
	}
	
	state = at25dfx_read(config, size, address);
	if (state != AT25_SUCCESS) {
		info_log("error: boot config read\r\n");
		return OPER_STATE_FLASH_READ;
	}
	
	return OPER_STATE_SUCCESS;
}

int setExternalBootConfig(void *config, int size, configType_e ctype)
{
	int state;
	unsigned int address;
	
	if(!config) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	if(ctype == CONFIG_TYPE_SYSTEM) {
		address = EXTERNAL_FLASH_SYS_BOOT_CONFIG_START_ADDR;
	} else if(ctype == CONFIG_TYPE_DOWNLOAD) {
		address = EXTERNAL_FLASH_DNLD_BOOT_CONFIG_START_ADDR;
	} else {
		info_log("error: invalid config type\r\n");
		return OPER_STATE_INVALID_CONFIG;
	}
	
	state = at25dfx_erase_block(address);
	if(state != AT25_SUCCESS) {
		info_log("error: external flash erase\r\n");
		return OPER_STATE_FLASH_ERASE;
	}
	
	state = at25dfx_write(config, size, address);
	if (state != AT25_SUCCESS) {
		info_log("error: boot config write\r\n");
		return OPER_STATE_FLASH_WRITE;
	}
	
	return OPER_STATE_SUCCESS;
}

int updateRollbackFlag(unsigned char data)
{
	int size = sizeof(exSysBootConfig_s);
	unsigned char config[size];
	int state;
	exSysBootConfig_s *cfg;
	
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exSysBootConfig_s*)config;
		cfg->isRollBackRequested = data;
		state = setExternalBootConfig((void*)cfg, size, CONFIG_TYPE_SYSTEM);
		if(state != OPER_STATE_SUCCESS) {
			return state;
		} else {
			return OPER_STATE_SUCCESS;
		}
	}
}

int updateFirmwareAvailableFlag(unsigned char data)
{
	int size = sizeof(exSysBootConfig_s);
	unsigned char config[size];
	int state;
	exSysBootConfig_s *cfg;
	
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exSysBootConfig_s*)config;
		cfg->isNewFirmwareAvaliable = data;
		state = setExternalBootConfig((void*)cfg, size, CONFIG_TYPE_SYSTEM);
		if(state != OPER_STATE_SUCCESS) {
			return state;
		} else {
			return OPER_STATE_SUCCESS;
		}
	}
}

int getRollbackFlag(unsigned char *data)
{
	int size = sizeof(exSysBootConfig_s);
	unsigned char config[size];
	int state;
	exSysBootConfig_s *cfg;
	
	if(!data) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exSysBootConfig_s *)config;
		*data = cfg->isRollBackRequested;
		return OPER_STATE_SUCCESS;
	}
}

int getFirmwareAvailableFlag(unsigned char *data)
{
	int size = sizeof(exSysBootConfig_s);
	unsigned char config[size];
	int state;
	exSysBootConfig_s *cfg;
	
	if(!data) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exSysBootConfig_s *)config;
		*data = cfg->isNewFirmwareAvaliable;
		return OPER_STATE_SUCCESS;
	}
}

int getFirmwareHeader(firmwareHeader_s *fhInfo, char isCurrentFw)
{
	int size = sizeof(exSysBootConfig_s);
	unsigned char config[size];
	int state;
	exSysBootConfig_s *cfg;
	
	if(!fhInfo) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exSysBootConfig_s*) config;
		if(isCurrentFw) {
			memcpy(fhInfo, &cfg->currentFirmwareHeader, sizeof(firmwareHeader_s));
		} else {
			memcpy(fhInfo, &cfg->newFirmwareHeader, sizeof(firmwareHeader_s));
		}
		return OPER_STATE_SUCCESS;
	}
}

int setFirmwareHeader(firmwareHeader_s *fhInfo, char isCurrentFw)
{
	int size = sizeof(exSysBootConfig_s);
	unsigned char config[size];
	int state;
	exSysBootConfig_s *cfg;
	
	if(!fhInfo) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}

	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exSysBootConfig_s*) config;
		if(isCurrentFw) {
			memcpy(&cfg->currentFirmwareHeader, fhInfo, sizeof(firmwareHeader_s));
		} else {
			memcpy(&cfg->newFirmwareHeader, fhInfo, sizeof(firmwareHeader_s));			
		}
		
		state = setExternalBootConfig((void*)cfg, size, CONFIG_TYPE_SYSTEM);
		if(state != OPER_STATE_SUCCESS) {
			return state;
		} else {
			return OPER_STATE_SUCCESS;
		}
	}
}

int getLastFwUpdateInfo(lastFwUpdateInfo_s *lastFwInfo)
{
	int size = sizeof(exDnldBootConfig_s);
	unsigned char config[size];
	int state;
	exDnldBootConfig_s *cfg;
	
	if(!lastFwInfo) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_DOWNLOAD);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exDnldBootConfig_s*) config;
		memcpy(lastFwInfo, &cfg->lastFwUpdateInfo, sizeof(lastFwUpdateInfo_s));
		return OPER_STATE_SUCCESS;
	}
}

int setLastFwUpdateInfo(lastFwUpdateInfo_s *lastFwInfo)
{
	int size = sizeof(exDnldBootConfig_s);
	unsigned char config[size];
	int state;
	exDnldBootConfig_s *cfg;
	
	if(!lastFwInfo) {
		info_log("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_DOWNLOAD);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exDnldBootConfig_s*)config;
		memcpy(&cfg->lastFwUpdateInfo, lastFwInfo, sizeof(lastFwUpdateInfo_s));
		state = setExternalBootConfig((void*)cfg, size, CONFIG_TYPE_DOWNLOAD);
		if(state != OPER_STATE_SUCCESS) {
			return state;
		} else {
			return OPER_STATE_SUCCESS;
		}
	}
}

int updatePreviousBackupStatus(unsigned char data)
{
	int size = sizeof(exSysBootConfig_s);
	unsigned char config[size];
	int state;
	exSysBootConfig_s *cfg;
	
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exSysBootConfig_s*)config;
		cfg->previousBackupStatus = data;
		state = setExternalBootConfig((void*)cfg, size, CONFIG_TYPE_SYSTEM);
		if(state != OPER_STATE_SUCCESS) {
			return state;
		} else {
			return OPER_STATE_SUCCESS;
		}
	}
}

int getPreviousBackupStatus(unsigned char *data)
{
	int size = sizeof(exSysBootConfig_s);
	unsigned char config[size];
	int state;
	exSysBootConfig_s *cfg;
	
	if(!data) {
		printf("error: invalid buffer\r\n");
		return OPER_STATE_INVALID_BUFFER;
	}
	
	state = getExternalBootConfig((void*)config, size, CONFIG_TYPE_SYSTEM);
	if(state != OPER_STATE_SUCCESS) {
		return state;
	} else {
		cfg = (exSysBootConfig_s *)config;
		*data = cfg->previousBackupStatus;
		return OPER_STATE_SUCCESS;
	}
}