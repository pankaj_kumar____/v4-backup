/*
 * userTimer.h
 *
 * Created: 4/1/2020 5:11:41 AM
 *  Author: Kartik.Gupta
 */ 


#ifndef USERTIMER_H_
#define USERTIMER_H_

/* timer handler callback*/
typedef void (*Cb)(void);

/* @brief initialize the timer module
 * @param timer handler callback to be called when timer expired
 */
void timerInit(Cb timerHandler);

/* @brief start the single shot timer
 * @param timeInSecond	value in second for timer to get run
 * @return 0 for success, 1 for failure
 */
int timerStart(int timeInSecond);

/* @brief stop any timer already running
 */
void timerStop(void);

/* @brief check if timer is running
 * @return 1 if running, 0 if not
 */
int isTimerRunning(void);

#endif /* USERTIMER_H_ */